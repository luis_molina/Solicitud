$(document).ready(function()
{

    var base_url = $("body").attr("data-base-url");


	$("body").on("click","#btnEvaluarEstudiantesExcel",function()
	{
		
		//let id_usuario = tablaClientes.rows($(this).closest("tr").index()).data().pluck(0)[0];

        let id_poliza = $(this).closest("tr").attr("data-id_poliza");


        $("#modalCargarFilesPolizasUsuario").prop("id_poliza",id_poliza);

		$("#modalCargarFilesPolizasUsuario").modal("show");

	});


    $("body").on("click",".fileinput-remove-button",function()
    {

           $("#formValidaUploadExcel").bootstrapValidator('resetForm', true);


    });

	validaUploadExcel();

	$("#formValidaUploadExcel").on("submit",function(event)
	{
		event.preventDefault();

		$("#formValidaUploadExcel").bootstrapValidator();

	});


	function validaUploadExcel()
	{
		

		$('#formValidaUploadExcel').bootstrapValidator(
		{

    	        message: 'This value is not valid',
    	        container: 'tooltip',
    	        feedbackIcons: {
    	            valid: 'glyphicon glyphicon-ok',
    	            invalid: 'glyphicon glyphicon-remove',
    	            validating: 'glyphicon glyphicon-refresh'
    	        },
    	        fields: {

    	            fileExcel: {
    	                group: '.form-group',
    	                validators: 
    	                {
    	                    notEmpty: {
    	                        message: 'Este campo es requerido'
    	                    }
    	                     

    	                }
    	            },
    	}
        }).on('success.form.bv', function (e) 
        {

        	    e.preventDefault();

        				var archivos = document.getElementById("fileExcel");  

                        var archivo = archivos.files;
                        var archivos = new FormData();
                        for(i=0; i<archivo.length;i++)
                        {
                          archivos.append('archivo',archivo[i])
                        }


                          $.ajax(
                          {
                                    type: "POST",
                                    url: base_url+"EstudiantesAdmin/uploadExcelEvaluarEstudiantes",
                                    dataType:"json",
                                    contentType:false,
                                    processData:false,
                                    data: archivos,
                                    async: true,
                                    success: function(result)
                                        {

                                            //debugger;

                                            $("#btnEvaluarEstudiantesExcel").prop("disabled",false);
                                          
                                          if(typeof(result.baja) == "undefined") 
                                          {

                                             if(result.status='OK')
                                             {
                                                if(result.errorEvaluacion=="SI")
                                                {
                                                    $("#modalAlerta .modal-body").html("No se pudieron evaluar las materias de los estudiantes debido a que hay datos en su archivo que no coinciden con la base de datos. <br><br> por favor verifique su archivo, corríjalo e intente de nuevo");
                                                    $("#modalAlerta").modal("show");
                                                }
                                                else
                                                {
                                                    $("#modalAlerta .modal-body").html("Los estudiantes fueron evaluados correctamente, ahora puede verficarlo en su kardex");
                                                    $("#modalAlerta").modal("show");
                                                }
                                             }
                                             else
                                             {
                                                 $("#modalAlerta .modal-body").text(resul.msjConsulta);
                                                 ("#modalAlerta").modal("show");
                                             }
                                             
                                          }
                                          else
                                          {
                                            window.location = result.url;
                                          }

                                        },
                                   error:function(result)
                                      {
                                        alert("Error");
                                       console.log(result.responseText);
                                        
                                      }
                                      
                          });    
        });


	}




  $('#modalAlerta').on('hide.bs.modal', function (e) 
    {
      
     location.reload();
      
        
    });



    



});

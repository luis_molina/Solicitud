$(document).on('ready',function()
{

	var base_url = $("body").attr("data-base-url");

     //validaDatosCarreras();

       var tblEstudiantesKardex = $('#tblEstudiantesKardex').DataTable(
       {
          "processing": true,
          "serverSide": true,
          "ordering": true,
           "language": {
                          "url": base_url+"public/libreriasJS/Spanish.json"
                        },
                    "scrollY":        "500px",
                    "scrollCollapse": true,
          "ajax":{
            url :base_url+"EstudiantesAdmin/cargarTablaEstudiantesKardex", 
            type: "post",  
            error: function(d){ 
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
              
            }
            ,
            // success:function(d)
            // {
            //   debugger;
            //  console.log(d);
            // }
          },
          "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
           
       
       });


     $("body").on("click",'.btnKardexEstudiante',function()
     {
           	let id_alumno = tblEstudiantesKardex.rows($(this).closest("tr").index()).data().pluck(0)[0];

           	// <a href="http://localhost:8080/Solicitud//Home/solicitudPDFEstudiante?num_solicitud=2" target="_blank" class="demo">
      						// 											<input type="button" class="btn btn-primary btnVerFormatoSolicitud" value="Ver solicitud">
      						// 										</a>


           	let url = `${base_url}EstudiantesAdmin/pdfKardexEstudiante?id_alumno=${id_alumno}`;

           	//let url = 'http://localhost:8080/Solicitud//Home/solicitudPDFEstudiante?num_solicitud=2';

              var a = document.createElement("a");
          		a.target = "_blank";
          		a.href = url;
          		a.click();
      																	

     });


});
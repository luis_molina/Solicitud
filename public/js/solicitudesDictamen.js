$(document).ready(function()
{
   
   var base_url = $("body").attr("data-base-url");

   var tblSolicitudes = $('#tblSolicitudes').DataTable(
       {
          "processing": true,
          "serverSide": true,
          "ordering": true,
           "language": {
                          "url": base_url+"public/libreriasJS/Spanish.json"
                        },
                    "scrollY":        "500px",
                    "scrollCollapse": true,
          "ajax":{
            url :base_url+"Home/cargarTablaSolicitudes", 
            type: "post",  
            error: function(d){ 
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
              
            }
            ,
            // success:function(d)
            // {
            //   debugger;
            //  console.log(d);
            // }
          },
          "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
           
       
       });


    $("body").on("click",".btnVerSolicitudes",function()
   {


        let id_alumno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(0)[0];

        let nombre = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(2)[0];
        let apellido_paterno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(3)[0];
        let apellido_materno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(4)[0];

        let nombre_completo = nombre+" "+apellido_paterno +" "+apellido_materno;
    

        $.ajax(
        {
                  type: "POST",
                  dataType:"json",
                  url: base_url+"Home/getCountSolicitudesEstudianteDictamen",
                  data: {id_alumno:id_alumno},
                  async: true,
                  success: function(result)
                    {
                       if( typeof(result.redirect) == 'undefined')
                       {
                           if(result.status == "OK")
                           {

                            let tbodySolicitudes = '';
                            let x=1;

                            $("body #tblSolicitudesEstudiante tbody").empty();

                            result.solicitudes.forEach(function(solicitud) {

                               


                            //<td class='text-center'>${solicitud.nombre_reducido}</td>

                                tbodySolicitudes = `    <tr data-num_solicitud='${solicitud.num_solicitud}' >
                                                    <td class='text-center no'>${x}</td>
                                                <td class='text-center'>${solicitud.asunto}</td>
                                                <td class='text-center'>${solicitud.lugar}</td>
                                                <td class='text-center'>${solicitud.fecha}</td>
                                                <td class='text-center'>${solicitud.nombre_semestre}</td>
                                                <td class='text-center'>${solicitud.identificacion_larga}</td>
                                                <td><p class='demo'><a href='${base_url}/Home/solicitudPDFEstudiante?num_solicitud=${solicitud.num_solicitud}' target='_blank' class='demo'>
                                                                    <input type='button' class="btn btn-primary btnVerFormatoSolicitud"  value='Ver solicitud' />
                                                                </a>
                                                    </p> 
                                                </td>
                                                <td><p class='demo'><a href='${base_url}/Home/dictamenPDFEstudiante?num_solicitud=${solicitud.num_solicitud}' target='_blank' class='demo'>
                                                                    <input type='button' class="btn btn-primary btnVerFormatoDictamen"  value='Ver Dictamen' />
                                                                </a>
                                                    </p> 
                                                </td>
                                                <td class='text-center'>${solicitud.Respuesta_Dictamen}</td>
                                                <td><p class='demo'><a href='${base_url}/Home/dictamenDirectorPDFEstudiante?num_solicitud=${solicitud.num_solicitud}' target='_blank' class='demo'>
                                                                    <input type='button' class="btn btn-primary btnVerFormatoDictamen"  value='Ver Dictamen director' />
                                                                </a>
                                                    </p> 
                                                </td>
                                                <td class='text-center'>${solicitud.Respuesta_dictamen_director}</td>
                       
                                            </tr>`;

                            $("body #tblSolicitudesEstudiante tbody").append(tbodySolicitudes);
                            x++;

                        });

                                $("#modalSolicitudesEstudiante #nameEstudiante").text(nombre_completo);
                                $("#modalSolicitudesEstudiante").modal("show");
                                
                           }
                           else
                           {
                                // $('#modalAlerta .modal-body').text(result.mensaje);
                                // $('#modalAlerta').modal('show');
                           }
                       }
                       else
                       {
                         location.href = result.url;
                       }
                       
                        
                    },
               error:function(result)
                {
                  console.log(result.responseText);
                  //$("#error").html(data.responseText); 
                }
                  
        });


   });


    $("body").on("click",".btnDictamenSolicitud",function()
    {


            let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");

            $.ajax(
                {
                      type: "POST",
                      dataType:"json",
                      url: base_url+"Home/getDatosActaSolicitud",
                      data: {num_solicitud:num_solicitud},
                      async: true,
                      success: function(result)
                        {

                           if( typeof(result.redirect) == 'undefined')
                           {


                               if(result.status == "OK")
                               {

                                    $("#txtObservacionesSolicitud").val(result.datosSolicitud[0].observacion);
                                    $("#txtRespuestaSolicitudDictamen").val(result.datosSolicitud[0].respuesta_solicitud_dictamen);
                                    $("#txtSolicitudNoDictamen").val(result.datosSolicitud[0].no_dictamen);

                                    $("#modalDatosDictamenSolicitud").prop("num_solicitud",num_solicitud);
                                    $("#modalDatosDictamenSolicitud").modal("show");

                                     

                               }
                               else
                               {
                                   $('#modalAlerta .modal-body').text(result.mensaje);
                                    $('#modalAlerta').modal('show');
                               }
                           }
                           else
                           {
                             location.href = result.url;
                           }
                           
                            
                        },
                   error:function(result)
                    {
                      console.log(result.responseText);
                      //$("#error").html(data.responseText); 
                    }
                      
             });

                    

    });


     



    $("body").on("click","#btnCancelarDatosDictamenSolicitud",function()
   {


      $('#modalDatosDictamenSolicitud').modal('hide'); 
    


   });

    $("body").on("click","#btnCancelarDatosDictamenDirectorSolicitud",function()
   {


      $('#modalDatosDictamenDirectorSolicitud').modal('hide'); 
    


   });



     $('#modalDatosDictamenSolicitud').on('hide.bs.modal', function (e) 
    {
        
        $("#formDatosDictamenSolicitud").bootstrapValidator('resetForm', true);

    });


     $('#modalDatosDictamenDirectorSolicitud').on('hide.bs.modal', function (e) 
    {
        
        $("#formDatosDictamenDirectorSolicitud").bootstrapValidator('resetForm', true);

    });




    validaDatosDictamenSolicitud();

    $("#formDatosDictamenSolicitud").on("submit",function(event)
    {
        event.preventDefault();

        $("#formDatosDictamenSolicitud").bootstrapValidator();

    });


    function validaDatosDictamenSolicitud()
    {
        

        $('#formDatosDictamenSolicitud').bootstrapValidator(
        {

            message: 'This value is not valid',
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

               txtRespuestaSolicitudDictamen: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                },
                 txtSolicitudNoDictamen: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                }

            }
        }).on('success.form.bv', function (e) 
        {

            e.preventDefault();

            let num_solicitud = $("#modalDatosDictamenSolicitud").prop("num_solicitud");
            let respuesta_solicitud_dictamen= $("#txtRespuestaSolicitudDictamen").val().trim();
            let no_dictamen = $("#txtSolicitudNoDictamen").val().trim();
            

            $.ajax(
            {
              type: "POST",
              dataType:"json",
              url: base_url+"Home/guardarDatosRespuestaDictamenSolicitud",
              data: {
                    num_solicitud: num_solicitud,
                    respuesta_solicitud_dictamen:respuesta_solicitud_dictamen,
                    no_dictamen : no_dictamen
              },
              async: true,
              success: function(result)
                  {
                    //console.log(result);

                        if( typeof(result.redirect) == 'undefined')
                        {

                            if(result.status == 'OK')
                            {
                               
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalDatosDictamenSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }
                            else
                            {
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalDatosDictamenSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }

                        }
                       else
                       {
                         location.href = result.url;
                       }

                  },
               error:function(result)
                  {
                    console.log(result.responseText);
                    //$("#error").html(data.responseText); 
                  }
              
            });





        });


    }


    $("body").on("click","#btnModalAlerta",function()
   {

      $('#modalAlerta').modal('hide'); 

   });



     validaDatosDictamenDirectorSolicitud();

    $("#formDatosDictamenDirectorSolicitud").on("submit",function(event)
    {
        event.preventDefault();

        $("#formDatosDictamenDirectorSolicitud").bootstrapValidator();

    });


    function validaDatosDictamenDirectorSolicitud()
    {
        

        $('#formDatosDictamenDirectorSolicitud').bootstrapValidator(
        {

            message: 'This value is not valid',
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

               rdSolicitud: {
                    group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                },
                 txtRespuestaSolicitudDictamenDirector: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                }
                ,
                 txtRespuestaSolicitudDictamenDirector: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                }

                ,
                 txtMotivosDictamenDirector: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                }

                ,
                 txtSolicitudNoRecomendacion: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                }

            }
        }).on('success.form.bv', function (e) 
        {

            e.preventDefault();

            let num_solicitud = $("#modalDatosDictamenDirectorSolicitud").prop("num_solicitud");
            let recomienda = ( $("#rdSiRecomienda").is(":checked") ) ? '1' : '0'; 
            let respuesta_solicitud_acta= $("#txtRespuestaSolicitudDictamenDirector").val().trim();
            let motivos_solicitud_dictamen = $("#txtMotivosDictamenDirector").val().trim();
            let no_recomendacion_dictamen = $("#txtSolicitudNoRecomendacion").val().trim();

            
            $.ajax(
            {
                  type: "POST",
                  dataType:"json",
                  url: base_url+"Home/guardarDatosRespuestaDictamenDirectSolicitud",
                  data: {
                        num_solicitud: num_solicitud,
                        recomienda:recomienda,
                        respuesta_solicitud_acta : respuesta_solicitud_acta,
                        motivos_solicitud_dictamen:motivos_solicitud_dictamen,
                        no_recomendacion_dictamen:no_recomendacion_dictamen
                  },
                  async: true,
                  success: function(result)
                      {
                        //console.log(result);

                            if( typeof(result.redirect) == 'undefined')
                            {

                                if(result.status == 'OK')
                                {
                                   
                                    $("#modalAlerta .modal-body").text(result.mensaje);

                                    $("#modalDatosDictamenDirectorSolicitud").modal("hide");
                                    $("#modalAlerta").modal("show");
                                    

                                }
                                else
                                {
                                    $("#modalAlerta .modal-body").text(result.mensaje);

                                    $("#modalDatosDictamenDirectorSolicitud").modal("hide");
                                    $("#modalAlerta").modal("show");
                                    

                                }

                            }
                           else
                           {
                             location.href = result.url;
                           }

                      },
                   error:function(result)
                      {
                        console.log(result.responseText);
                        //$("#error").html(data.responseText); 
                      }
              
            });





        });


    }


    $("body").on("click",".btnDictamenDirectorSolicitud",function()
    {


            let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");

            $.ajax(
                {
                      type: "POST",
                      dataType:"json",
                      url: base_url+"Home/getDatosActaSolicitud",
                      data: {num_solicitud:num_solicitud},
                      async: true,
                      success: function(result)
                        {

                           if( typeof(result.redirect) == 'undefined')
                           {


                               if(result.status == "OK")
                               {

                                    $("#txtObservacionesSolicitudDictamen").val(result.datosSolicitud[0].observacion);
                                    $("#txtRespuestaSolicitudDictamenDirector").val(result.datosSolicitud[0].respuesta_solicitud_acta);

                                    if(result.datosSolicitud[0].recomienda == '1')
                                    {
                                      $("#rdSiRecomienda").prop("checked",true);
                                    }
                                    else if(result.datosSolicitud[0].recomienda == '0')
                                    {
                                      $("#rdNoRecomienda").prop("checked",true);
                                    }

                                    $("#txtMotivosDictamenDirector").val(result.datosSolicitud[0].motivos_solicitud_dictamen);
                                    $("#txtSolicitudNoRecomendacion").val(result.datosSolicitud[0].no_recomendacion_dictamen);

                                    $("#modalDatosDictamenDirectorSolicitud").prop("num_solicitud",num_solicitud);
                                    $("#modalDatosDictamenDirectorSolicitud").modal("show");

                                     

                               }
                               else
                               {
                                   $('#modalAlerta .modal-body').text(result.mensaje);
                                    $('#modalAlerta').modal('show');
                               }
                           }
                           else
                           {
                             location.href = result.url;
                           }
                           
                            
                        },
                   error:function(result)
                    {
                      console.log(result.responseText);
                      //$("#error").html(data.responseText); 
                    }
                      
             });

                    

    });



});
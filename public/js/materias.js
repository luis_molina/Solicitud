$(document).ready(function()
{
   
   var base_url = $("body").attr("data-base-url");

   validaDatosMaterias();


   $("#formDatosMaterias").on("submit",function(event)
	{
		event.preventDefault();

		$("#formDatosMaterias").bootstrapValidator();

	});


   function validaDatosMaterias()
	{
		

		$('#formDatosMaterias').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	        	txtClaveMateria: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },
                                  callback: {
                                  message: 'La clave no esta disponible',
                                  callback: function(value, validator) {
                                      // Get the selected options

                                      var valida = true;

                                      var dato = {
                                                            materia:$("#txtClaveMateria").val().trim()
                                                          }


                                                  $.ajax(
                                                  {
                                                      type: "POST",
                                                      url: base_url+"Materias/checkClaveMateria",
                                                      dataType:"json",
                                                      data: dato,
                                                      async: false,
                                                      success: function(result)
                                                          {

                                                          	  if( typeof(result.redirect) == 'undefined')
                                                          	  {

	                                                              if(result.resultado == 'NO_DISPONIBLE')
	                                                              {
	                                                                 valida = false;
	                                                              }
	                                                              else
	                                                              {
	                                                                valida = true;
	                                                              }

	                                                          }
	                                                          else
	                                                          {
	                                                          	location.href = result.url;
	                                                          }
                                                           
                                                          },
                                                     error:function(result)
                                                        {
                                                          alert("Error");
                                                         console.log(result.responseText);
                                                          
                                                        }
                                                        
                                                  });

                                                  return valida;

                                  }
                              },


	                },
	            }
	            ,
	            txtNombreMateria: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            txtNombreMateriaAbreviado: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	           
	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			var datosMateria = {
				materia:$("#txtClaveMateria").val().trim(),
				nombre_completo_materia:$("#txtNombreMateria").val().trim(),
				nombre_abreviado_materia:$("#txtNombreMateriaAbreviado").val().trim(),
				
			}

			//console.log(datosCarrera);

			$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Materias/guardarMaterias",
	          data: datosMateria,
	          async: true,
	          success: function(result)
		          {
					
					if( typeof(result.redirect) == 'undefined')
                    {
                    	$('#modalAlerta .modal-body').text(result.mensaje);
                    	$('#modalAlerta').modal('show');
                    	if(result.resultado == 'OK')
                    	{
                    		$("#btnGuardarMateria").prop("resultado",result.resultado);
                    		
                    	}
                    }
                    else
                    {
                      location.href = result.url;
                    }
		          	

		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });

	    });


	}


	$('#modalAlerta').on('hide.bs.modal', function (e) 
    {
    	if($("#btnGuardarMateria").prop("resultado") =='OK')
    	{
    		location.reload();
    	}

    	$("#btnGuardarMateria").removeProp("resultado");
        
    });




    


});
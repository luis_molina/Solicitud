$(document).on('ready',function()
{

	var base_url = $("body").attr("data-base-url");

     //validaDatosCarreras();

               var tblEstudiantesEvaluar = $('#tblEstudiantesEvaluar').DataTable(
               {
                  "processing": true,
                  "serverSide": true,
                  "ordering": true,
                   "language": {
                                  "url": base_url+"public/libreriasJS/Spanish.json"
                                },
                            "scrollY":        "500px",
                            "scrollCollapse": true,
                  "ajax":{
                    url :base_url+"EstudiantesAdmin/cargarTablaEstudiantesEvaluar", 
                    type: "post",  
                    error: function(d){ 
                      $(".employee-grid-error").html("");
                      $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
                      $("#employee-grid_processing").css("display","none");
                      
                    }
                    ,
                    // success:function(d)
                    // {
                    //   debugger;
                    //  console.log(d);
                    // }
                  },
                  "columnDefs": [
                                {
                                    "targets": [ 0 ],
                                    "visible": false,
                                    "searchable": false
                                }
                            ],
                   
               
               });


    $("body").on("click",".btnEvaluarEstudiante",function()
    {

          let id_alumno = tblEstudiantesEvaluar.rows($(this).closest("tr").index()).data().pluck(0)[0];

          $.ajax(
               {
               type: "POST",
               dataType:"json",
               url: base_url+"EstudiantesAdmin/getInfoEvaluarAlumno",
               data: {id_alumno:id_alumno},
               async: true,
               success: function(result)
                    {

                         if( typeof(result.redirect) == 'undefined')
                         {

                                  let alumno = result.evaluar[0].alumno;

                                  let id_alumno = result.evaluar[0].id_alumno;

                                  let tempAlumno = `<h4>Evaluar al estudiante <b>${alumno}</b><h4><br><br>`;

                                  $("#modalEvaluarEstudiante #contEstudiante").html(tempAlumno);
                                  $("#modalEvaluarEstudiante").prop("id_alumno",id_alumno);


                                   $('#modalEvaluarEstudiante .modal-body #contInfoEvaluarEstudiante').html("");

                                  let $tabla =     
                                           `<table cellspacing='5' cellpadding='5' border='1' style='width:800px;'>
                                               <tr>
                                                   <td><b>Clave</b></td>
                                                   <td><b>Materia</b></td>
                                                   <td><b>Créditos Cursados</b></td>
                                                   <td><b>Créditos Aprobados</b></td>
                                                   <td><b>Evaluar Materia</b></td>
                                               </tr>`;

                                  let $cierreTabla = "</table>";

                                  let $tbody = "";

                                  let $suma_creditos_materia = 0;
                                  let $suma_creditos_aprobados = 0;
                                  let $suma_creditos_cursados  = 0;


                                   for(let $i = 0; $i < result.evaluar.length; ++$i)
                                   {

                                             $suma_creditos_materia = $suma_creditos_materia +  result.evaluar[$i].creditos_cursados;

                                             let textEvaluarBoton = '';

                                             let claseEvaluarBoton = '';

                                             let color = "";

                                             if(result.evaluar[$i].materia_aprobada == '1' )
                                              {
                                                   $suma_creditos_aprobados =  $suma_creditos_aprobados + parseInt(result.evaluar[$i].creditos_cursados);

                                                   $creditos_aprobados = result.evaluar[$i].creditos_cursados;

                                                   textEvaluarBoton = 'Evaluada';
                                                    claseEvaluarBoton = 'btn-success';
                                              }
                                              else
                                              {
                                                
                                                if(result.evaluar[$i].materia_aprobada == '0' )
                                                {
                                                    color = 'red';
                                                }

                                                  $creditos_aprobados = 0;
                                                  textEvaluarBoton = 'Evaluar';
                                                   claseEvaluarBoton = 'btn-primary';
                                              }
                                             
                                              $suma_creditos_cursados =  $suma_creditos_cursados +  parseInt(result.evaluar[$i].creditos_cursados);

                                              $tbody += `<tr data-no_de_control='${result.evaluar[$i].no_de_control}' data-clave_oficial = '${result.evaluar[$i].clave_oficial}' data-id_periodo_escolar='${result.evaluar[$i].id_periodo_escolar}' data-id_semestre='${result.evaluar[$i].id_semestre}' data-clave_materia='${result.evaluar[$i].materia}'>
                                                             <td style='text-align: center;'>${result.evaluar[$i].materia}</td>
                                                             <td style='text-align: center;color:${color}'>${result.evaluar[$i].nombre_completo_materia} </td>
                                                             <td style='text-align: center;'>${result.evaluar[$i].creditos_cursados}</td>
                                                             <td style='text-align: center;'>${$creditos_aprobados}</td>
                                                             <td style='text-align: center;height: 50px;'><input type='button' class="btn ${claseEvaluarBoton} btnEvaluarEstudianteMateria"  value='${textEvaluarBoton}' /></td>
                                                       </tr>`;

                               
                                             //<td style='width:100px'><b>Estudiante:</b></td>
                                             // <td><b>${result.evaluar[$i].alumno}</b></td>


                                              $encabezado =   `<table style='border-top:1px solid black;border-left:1px solid black;border-right:1px solid black;width:800px'>
                                                               <tr>
                                                                   <td><b>Número de control:</b></td>
                                                                   <td><b>${result.evaluar[$i].no_de_control}</b></td>
                                                                   <td><b>Carrera:</b></td>
                                                                   <td style='width:385px'><b>${result.evaluar[$i].nombre_carrera}</b></td>
                                                               </tr>
                                                               <tr>
                                                                   <td><b>Semestre:</b></td>
                                                                   <td><b>${result.evaluar[$i].nombre_semestre}</b></td>
                                                                   <td></td>
                                                                   <td></td>
                                                               </tr>
                                                           </table>`;


                                              if( $i < result.evaluar.length -1)
                                              {
                                                  
                                                      if( result.evaluar[$i].nombre_semestre != result.evaluar[$i+1].nombre_semestre )
                                                      {

                                                          $filaTotales = `<tr>
                                                                              <td></td>
                                                                              <td><b>Totales</b></td>
                                                                              <td style='text-align: center;'><b>${$suma_creditos_cursados}</b></td>
                                                                              <td style='text-align: center;' class='tdSumaCreditosAprobados' ><b>${$suma_creditos_aprobados}</b></td>
                                                                              <td></td>
                                                                          </tr>`;

                                                          let temp = $encabezado+$tabla+$tbody+$filaTotales+"</table><br><br>";

                                                          $('#modalEvaluarEstudiante .modal-body #contInfoEvaluarEstudiante').append(temp);


                                                          $suma_creditos_materia = 0;
                                                          $suma_creditos_aprobados = 0;
                                                          $suma_creditos_cursados = 0;

                                                           $tbody='';
                                                      }
                                              }

                                                  

                                              if(result.evaluar.length-1 == $i)
                                              {
                                                       $filaTotales =          
                                                                  `<tr>
                                                                      <td></td>
                                                                      <td><b>Totales</b></td>
                                                                      <td style='text-align: center;'><b>${$suma_creditos_cursados}</b></td>
                                                                      <td style='text-align: center;' class='tdSumaCreditosAprobados' ><b>${$suma_creditos_aprobados}</b></td>
                                                                      <td></td>
                                                                  </tr>`;

                                                       let  temp = $encabezado+$tabla+$tbody+$filaTotales+"</table><br><br>";

                                                       $('#modalEvaluarEstudiante .modal-body #contInfoEvaluarEstudiante').append(temp);
                                              }

                                   }


                                  



                                  $('#modalEvaluarEstudiante').modal('show');
                              
                         }
                         else
                         {
                              location.href = result.url;
                         }
                         
                         

                    },
                  error:function(result)
                      {
                         console.log(result.responseText);
                         //$("#error").html(data.responseText); 
                      }
               
             });


    });


     $("body").on("click",".btnEvaluarEstudianteMateria",function()
     {

          let id_alumno = $("#modalEvaluarEstudiante").prop("id_alumno");
          let no_de_control = $(this).closest("tr").attr("data-no_de_control");
          let clave_oficial = $(this).closest("tr").attr("data-clave_oficial");
          let clave_materia = $(this).closest("tr").attr("data-clave_materia");
          let id_periodo_escolar = $(this).closest("tr").attr("data-id_periodo_escolar");
          let id_semestre = $(this).closest("tr").attr("data-id_semestre");

          let botonEvaluado = $(this);

          let datos = {  
                         id_alumno:id_alumno,
                         no_de_control:no_de_control,
                         clave_oficial:clave_oficial,
                         clave_materia:clave_materia,
                         id_periodo_escolar:id_periodo_escolar,
                         id_semestre:id_semestre
                      }

     

          $.ajax(
               {
               type: "POST",
               dataType:"json",
               url: base_url+"EstudiantesAdmin/evaluarAlumnoMateria",
               data:datos ,
               async: true,
               success: function(result)
               {              

                    if( typeof(result.redirect) == 'undefined')
                    {
                         if(result.status=='OK')
                         {

                              if(result.aprobada=='1')
                              {
                                   $(botonEvaluado).addClass("btn-success");
                                   $(botonEvaluado).val("Evaluada");
                                   $(botonEvaluado).parent().siblings().eq(3).text(result.creditos_materia);
                                   $(botonEvaluado).parent().siblings().eq(1).css("color","");

                                    
                              }
                              else
                              {
                                   $(botonEvaluado).removeClass("btn-success");
                                   $(botonEvaluado).addClass("btn-primary");
                                   $(botonEvaluado).val("Evaluar");
                                   $(botonEvaluado).parent().siblings().eq(3).text('0');
                                   $(botonEvaluado).parent().siblings().eq(1).css("color","red");


                              }

                                   let creditos_cursados = 0;
                                   let suma_creditos_cursados = 0;


                                   $(botonEvaluado).closest("table").find("tr").each(function(){


                                        if( !isNaN($(this).find("td").eq(3).text()) && $(this).find("td").eq(3).attr("class") == undefined )
                                        {
                                             creditos_cursados = parseInt($(this).find("td").eq(3).text());


                                             suma_creditos_cursados = suma_creditos_cursados +  creditos_cursados;
                                        }


                                   });


                                   $(botonEvaluado).closest("table").find('td.tdSumaCreditosAprobados').html("<b>"+suma_creditos_cursados+"</b>");


                              alert(result.mensaje);
                         }
                         else
                         {
                              alert(result.mensaje);
                         }    
                    }
                    else
                    {
                         location.href = result.url;
                    }

               },
               error:function(result)
               {
                    console.log(result.responseText);
                    //$("#error").html(data.responseText); 
               }
          
           });



     });






});
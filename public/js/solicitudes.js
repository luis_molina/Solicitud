$(document).ready(function()
{
   
   var base_url = $("body").attr("data-base-url");

   var tblSolicitudes = $('#tblSolicitudes').DataTable(
       {
          "processing": true,
          "serverSide": true,
          "ordering": true,
           "language": {
                          "url": base_url+"public/libreriasJS/Spanish.json"
                        },
                    "scrollY":        "500px",
                    "scrollCollapse": true,
          "ajax":{
            url :base_url+"Home/cargarTablaSolicitudes", 
            type: "post",  
            error: function(d){ 
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
              
            }
            ,
            // success:function(d)
            // {
            //   debugger;
            //  console.log(d);
            // }
          },
          "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
           
       
       });

   $("body").on("click",".btnVerSolicitudes",function()
   {

  


   		let id_alumno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(0)[0];

   		let nombre = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(2)[0];
   		let apellido_paterno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(3)[0];
   		let apellido_materno = tblSolicitudes.rows($(this).closest("tr").index()).data().pluck(4)[0];

   		let nombre_completo = nombre+" "+apellido_paterno +" "+apellido_materno;
   	

   		$.ajax(
   		{
   		          type: "POST",
   		          dataType:"json",
   		          url: base_url+"Home/getCountSolicitudesEstudiante",
   		          data: {id_alumno:id_alumno},
   		          async: true,
   		          success: function(result)
   		            {
   		               if( typeof(result.redirect) == 'undefined')
   		               {
   		                   if(result.status == "OK")
   		                   {

   		                   	let tbodySolicitudes = '';
   		                   	let x=1;

   		                   	$("body #tblSolicitudesEstudiante tbody").empty();


   		                   	result.solicitudes.forEach(function(solicitud) 
                          {

                   		                   		let selectEstatus = ""

                   		                   		if(solicitud.status == "Pendiente")
                   		                   		{
                   		                   			selectEstatus = `<select class="slStatusSolicitud form-control" >
                	                                           						<option value="Pendiente" selected >Pendiente</option>
                	                                           						<option value="Aceptado" >Aceptado</option>
                	                                           						<option value="Rechazado" >Rechazado</option>
                	                                       					</select> `;
                   		                   		}
                   		                   		else
                   		                   		{
                   		                   			if(solicitud.status == "Aceptado")
                	   		                   		{
                	   		                   			selectEstatus = `<select class="slStatusSolicitud form-control"  >
                	                                           						<option value="Pendiente"  >Pendiente</option>
                	                                           						<option value="Aceptado" selected >Aceptado</option>
                	                                           						<option value="Rechazado" >Rechazado</option>
                	                                       					</select> `;
                	   		                   		}
                	   		                   		else
                	   		                   		{
                	   		                   			selectEstatus = `<select class="slStatusSolicitud form-control"  >
                	                                           						<option value="Pendiente"  >Pendiente</option>
                	                                           						<option value="Aceptado"  >Aceptado</option>
                	                                           						<option value="Rechazado" selected >Rechazado</option>
                	                                       					</select> `;
                	   		                   		}
                   		                   		}


                                            //<td class='text-center'>${solicitud.nombre_reducido}</td>

                   		                   		tbodySolicitudes = `	<tr data-num_solicitud='${solicitud.num_solicitud}' >
                   		                   							<td class='text-center no'>${x}</td>
                												<td class='text-center grand'>${solicitud.asunto}</td>
                												<td class='text-center grand'>${solicitud.lugar}</td>
                												<td class='text-center '>${solicitud.fecha}</td>
                                        <td class='text-center'>${solicitud.nombre_semestre}</td>
                												<td class='text-center'>${solicitud.identificacion_larga}</td>
                                        
                												<td><p class='demo'><a href='${base_url}/Home/solicitudPDFEstudiante?num_solicitud=${solicitud.num_solicitud}' target='_blank' class='demo'>
                																	<input type='button' class="btn btn-primary btnVerFormatoSolicitud"  value='Ver solicitud' />
                																</a>
                													</p> 
                												</td>
                												<td>
                													${selectEstatus}
                												</td>
                                        <td class='text-center'>${solicitud.Modificar}</td>
                                        <td class='text-center'>${solicitud.Acta}</td>
                                        <td class='text-center'>${solicitud.Eliminar}</td>
                											</tr>`;

                      							$("body #tblSolicitudesEstudiante tbody").append(tbodySolicitudes);

                      							x++;

						              });

   		                        $("#modalSolicitudesEstudiante #nameEstudiante").text(nombre_completo);
   		                        $("#modalSolicitudesEstudiante").modal("show");
   		                        
   		                   }
   		                   else
   		                   {
   		                        // $('#modalAlerta .modal-body').text(result.mensaje);
   		                        // $('#modalAlerta').modal('show');
   		                   }
   		               }
   		               else
   		               {
   		                 location.href = result.url;
   		               }
   		               
   		                
   		            },
   		       error:function(result)
   		        {
   		          console.log(result.responseText);
   		          //$("#error").html(data.responseText); 
   		        }
   		          
   		});


   });




  $("body").on("click","#btnCancelarModificarSolicitud",function()
   {


      $('#modalModificarMotivosSolicitud').modal('hide'); 

   });



  $("body").on("click","#btnModalAlerta",function()
   {


      $('#modalAlerta').modal('hide'); 

   });


  $("body").on("click","#btnCancelarEliminarSolicitud",function()
   {


      $('#modalEliminarSolicitud').modal('hide'); 

   });



  $("body").on("click","#btnMensajeEliminarSolciitud",function()
   {


      $('#modalMensajeEliminarSolciitud').modal('hide'); 

   });


  $("body").on("click","#btnCancelarDatosActaSolicitud",function()
   {


      $('#modalDatosActaSolicitud').modal('hide'); 
      



   });


   $('#modalDatosActaSolicitud').on('hide.bs.modal', function (e) 
    {
        
        $("#formDatosActaSolicitud").bootstrapValidator('resetForm', true);

    });




   $("body").on("click",".btnModificarSolicitud",function()
   {

   	  let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");

      $.ajax(
            {
                  type: "POST",
                  dataType:"json",
                  url: base_url+"Home/getInfoMotivosSolicitud",
                  data: {num_solicitud:num_solicitud},
                  async: true,
                  success: function(result)
                    {

                       if( typeof(result.redirect) == 'undefined')
                       {
                           if(result.status == "OK")
                           {

                                $("#txtMotivosAcademicos").val(result.motivos[0].motivos_academicos);
                                $("#txtMotivosPersonales").val(result.motivos[0].motivos_personales);
                                $("#txtOtros").val(result.motivos[0].otros);


                                $("#modalModificarMotivosSolicitud").prop("num_solicitud",num_solicitud);
                                $("#modalModificarMotivosSolicitud").modal("show");

                                
                                
                           }
                           else
                           {
                                $('#modalAlerta .modal-body').text(result.mensaje);
                                $('#modalAlerta').modal('show');
                           }
                       }
                       else
                       {
                         location.href = result.url;
                       }
                       
                        
                    },
               error:function(result)
                {
                  console.log(result.responseText);
                  //$("#error").html(data.responseText); 
                }
                  
         });


   })


     $("body").on("change",".slStatusSolicitud",function()
     {
      
     	    let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");


         	$.ajax(
           		    {
           		          type: "POST",
           		          dataType:"json",
           		          url: base_url+"Home/evaluarSolicitudEstudiante",
           		          data: {num_solicitud:num_solicitud,status:$(this).val()},
           		          async: true,
           		          success: function(result)
           		            {
           		               if( typeof(result.redirect) == 'undefined')
           		               {
           		                   if(result.resultado == "OK")
           		                   {

           		                   	    alert(result.mensaje);
           		                        
           		                   }
           		                   else
           		                   {
           		                        alert(result.mensaje);
           		                   }
           		               }
           		               else
           		               {
           		                 location.href = result.url;
           		               }
           		               
           		                
           		            },
           		       error:function(result)
           		        {
           		          console.log(result.responseText);
           		          //$("#error").html(data.responseText); 
           		        }
           		          
           		 });


     });


     $('#modalModificarMotivosSolicitud').on('hide.bs.modal', function (e) 
    {
        
        $("#formModificarMotivosSolicitud").bootstrapValidator('resetForm', true);


    });


     validaMotivosSolicitud();

    $("#formModificarMotivosSolicitud").on("submit",function(event)
    {
        event.preventDefault();

        $("#formModificarMotivosSolicitud").bootstrapValidator();

    });



     function validaMotivosSolicitud()
    {
        

        $('#formModificarMotivosSolicitud').bootstrapValidator(
        {

            message: 'This value is not valid',
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

               txtMotivosAcademicos: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                },

                // txtMotivosPersonales: {
                //     //group: '.form-group',
                //     validators: {
                //         notEmpty: {
                //             message: 'Debe escribir por lo menos un motivo'
                //         }
                     
 
                //     }
                // },

                //  txtOtros: {
                //    // group: '.form-group',
                //     validators: {
                //         notEmpty: {
                //             message: 'Debe escribir por lo menos un motivo'
                //         }

                //     }
                // },

            }
        }).on('success.form.bv', function (e) 
        {

            e.preventDefault();

            let num_solicitud = $("#modalModificarMotivosSolicitud").prop("num_solicitud");
            let motivos_academicos = $("#txtMotivosAcademicos").val().trim();
            let motivos_personales = $("#txtMotivosPersonales").val().trim();
            let otros = $("#txtOtros").val().trim();



            $.ajax(
            {
              type: "POST",
              dataType:"json",
              url: base_url+"Home/modificarSolcitudEstudiante",
              data: {
                    num_solicitud: num_solicitud,
                    motivos_academicos:motivos_academicos,
                    motivos_personales:motivos_personales,
                    otros:otros

              },
              async: true,
              success: function(result)
                  {
                    //console.log(result);

                        if( typeof(result.redirect) == 'undefined')
                        {

                            if(result.status == 'OK')
                            {
                               
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalModificarMotivosSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }
                            else
                            {
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalModificarMotivosSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }

                        }
                       else
                       {
                         location.href = result.url;
                       }

                  },
               error:function(result)
                  {
                    console.log(result.responseText);
                    //$("#error").html(data.responseText); 
                  }
              
            });





        });


    }



    validaDatosActaSolicitud();

    $("#formDatosActaSolicitud").on("submit",function(event)
    {
        event.preventDefault();

        $("#formDatosActaSolicitud").bootstrapValidator();

    });


    function validaDatosActaSolicitud()
    {
        

        $('#formDatosActaSolicitud').bootstrapValidator(
        {

            message: 'This value is not valid',
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

               txtManualLineamientoActa: {
                    //group: '.form-group',
                    validators: {

                         notEmpty: {
                            message: 'Este campo es requerido'
                        }
                       

                    }
                },

                txtRespuestaSolicitudActa: {
                    //group: '.form-group',
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                     
 
                    }
                }

            }
        }).on('success.form.bv', function (e) 
        {

            e.preventDefault();

            let num_solicitud = $("#modalDatosActaSolicitud").prop("num_solicitud");
            let verificacion_manual_lineamiento = $("#txtManualLineamientoActa").val().trim();
            let respuesta_solicitud_acta = $("#txtRespuestaSolicitudActa").val().trim();


            $.ajax(
            {
              type: "POST",
              dataType:"json",
              url: base_url+"Home/guardarDatosRespuestaActaSolicitud",
              data: {
                    num_solicitud: num_solicitud,
                    verificacion_manual_lineamiento:verificacion_manual_lineamiento,
                    respuesta_solicitud_acta:respuesta_solicitud_acta
              },
              async: true,
              success: function(result)
                  {
                    //console.log(result);

                        if( typeof(result.redirect) == 'undefined')
                        {

                            if(result.status == 'OK')
                            {
                               
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalDatosActaSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }
                            else
                            {
                                $("#modalAlerta .modal-body").text(result.mensaje);

                                $("#modalDatosActaSolicitud").modal("hide");
                                $("#modalAlerta").modal("show");
                                

                            }

                        }
                       else
                       {
                         location.href = result.url;
                       }

                  },
               error:function(result)
                  {
                    console.log(result.responseText);
                    //$("#error").html(data.responseText); 
                  }
              
            });





        });


    }



     $("body").on("click",".btnEliminarSolicitud",function()
     {


          let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");

            let asunto = $(this).closest("tr").find("td").eq(1).text();
            let semestre = $(this).closest("tr").find("td").eq(4).text();
            let periodo = $(this).closest("tr").find("td").eq(5).text();

        $("#btnAceptarEliminarSolicitud").prop("num_solicitud",num_solicitud);
        $("#modalEliminarSolicitud .modal-body").html("¿Desea eliminar la solicitud de <strong>" +asunto+ "</strong> de <strong>" +semestre+ "</strong> en el periodo <strong>" +periodo+ "</strong>?");
        //$("#modalEliminarSolicitud .modal-body").html("¿Desea eliminar la solicitud?");
        $("#modalEliminarSolicitud").modal("show");

     });


       $("body").on("click",".btnActaSolcitud",function()
       {


            let num_solicitud = $(this).closest("tr").attr("data-num_solicitud");

            $.ajax(
                {
                      type: "POST",
                      dataType:"json",
                      url: base_url+"Home/getDatosActaSolicitud",
                      data: {num_solicitud:num_solicitud},
                      async: true,
                      success: function(result)
                        {

                           if( typeof(result.redirect) == 'undefined')
                           {


                               if(result.status == "OK")
                               {

                                    $("#txtObservacionesSolicitud").val(result.datosSolicitud[0].observacion);
                                    $("#txtManualLineamientoActa").val(result.datosSolicitud[0].verificacion_manual_lineamiento);
                                    $("#txtRespuestaSolicitudActa").val(result.datosSolicitud[0].respuesta_solicitud_acta);


                                    $("#modalDatosActaSolicitud").prop("num_solicitud",num_solicitud);
                                    $("#modalDatosActaSolicitud").modal("show");

                                     

                               }
                               else
                               {
                                   $('#modalAlerta .modal-body').text(result.mensaje);
                                    $('#modalAlerta').modal('show');
                               }
                           }
                           else
                           {
                             location.href = result.url;
                           }
                           
                            
                        },
                   error:function(result)
                    {
                      console.log(result.responseText);
                      //$("#error").html(data.responseText); 
                    }
                      
             });

                    

       });





   $("body").on("click","#btnAceptarEliminarSolicitud",function()
   {

        let num_solicitud = $("#btnAceptarEliminarSolicitud").prop("num_solicitud");


            $.ajax(
                {
                      type: "POST",
                      dataType:"json",
                      url: base_url+"Home/eliminarSolicitudEstudiante",
                      data: {num_solicitud:num_solicitud},
                      async: true,
                      success: function(result)
                        {

                           if( typeof(result.redirect) == 'undefined')
                           {

                                $("#modalEliminarSolicitud").modal("hide");


                               if(result.status == "OK")
                               {

                                    $("#tblSolicitudesEstudiante tbody tr[data-num_solicitud='"+num_solicitud+"']").remove();

                                    if($("#tblSolicitudesEstudiante tbody tr").length == 0)
                                    {

                                        $("#modalEliminarSolicitud").modal("hide");
                                        $('#modalMensajeEliminarSolciitud .modal-body').text(result.mensaje);
                                        $('#modalMensajeEliminarSolciitud').modal('show');
                                    }
                                    else{
                                        $("#modalEliminarSolicitud").modal("hide");
                                        $('#modalMensajeEliminarSolciitud .modal-body').text(result.mensaje);
                                        $('#modalMensajeEliminarSolciitud').modal('show');
                                    }

                                     

                               }
                               else
                               {
                                    $("#modalEliminarSolicitud").modal("hide");
                                    $('#modalMensajeEliminarSolciitud .modal-body').text(result.mensaje);
                                    $('#modalMensajeEliminarSolciitud').modal('show');
                               }
                           }
                           else
                           {
                             location.href = result.url;
                           }
                           
                            
                        },
                   error:function(result)
                    {
                      console.log(result.responseText);
                      //$("#error").html(data.responseText); 
                    }
                      
             });



    

   })

     $('#modalMensajeEliminarSolciitud').on('hide.bs.modal', function (e) 
    {
        
        location.reload();

    });

     

     $(".resp-acta").blur(function(event) {
    

     $(this).val($(this).val().toUpperCase())


  });





















    // $("#txtMotivosAcademicos").on("keyup",function()
    // {
    
    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtMotivosAcademicos != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);
    //     }


    // });

    // $("#txtMotivosPersonales").on("keyup",function()
    // {

    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtMotivosPersonales != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);
    //     }


    // });

    // $("#txtOtros").on("keyup",function()
    // {
    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtOtros != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }


    // });


    // $("#txtMotivosAcademicos").on("change",function()
    // {
    
    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtMotivosAcademicos != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);
    //     }


    // });

    // $("#txtMotivosPersonales").on("change",function()
    // {

    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtMotivosPersonales != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);
    //     }


    // });

    // $("#txtOtros").on("change",function()
    // {
    //     let txtMotivosAcademicos = $("#txtMotivosAcademicos").val().trim();
    //     let txtMotivosPersonales = $("#txtMotivosPersonales").val().trim();
    //     let txtOtros = $("#txtOtros").val().trim();


    //     if(txtMotivosAcademicos == "" &&  txtMotivosPersonales == "" &&  txtOtros == "")
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosAcademicos",true);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtMotivosPersonales",true);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }
    //     else
    //     {
    //         let bootstrapValidator1 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator1.enableFieldValidators('txtMotivosAcademicos', false);

    //         let bootstrapValidator2 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator2.enableFieldValidators('txtMotivosPersonales', false);

    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', false);
    //     }

    //     if(txtOtros != "")
    //     {
    //         let bootstrapValidator3 = $('#formModificarMotivosSolicitud').data('bootstrapValidator');
    //         bootstrapValidator3.enableFieldValidators('txtOtros', true);

    //         $("#formModificarMotivosSolicitud").data("bootstrapValidator").revalidateField("txtOtros",true);
    //     }


    // });









});
$(document).on("ready",function()
{

	var base_url = $("body").attr("data-base-url");


	$("body").on("click","#rdNONuevoIngreso",function()
	{
		$("#modalMsjInicialEstudiante").modal("show");
		$("#txtNumControl").val("");
		$("#txtNumControl").attr('readonly', true);
		$("#txtNombreEstudiante").attr('readonly', true);
		$("#txtApellidoPaterno").attr('readonly', true);
		$("#txtApellidoMaterno").attr('readonly', true);
		//$("#txtCurp").attr('readonly', true);

		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNumControl",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNombreEstudiante",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoPaterno",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoMaterno",true);
		//$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtCurp",true);
		$("#btnRegistroEstudiante").attr("disabled",false);



	});

	$("body").on("click","#rdSINuevoIngreso",function()
	{
		// let num_control = $("#txtNumControl").val().trim();
		$("#txtNumControl").attr('readonly', false);
		$("#txtNombreEstudiante").attr('readonly', false);
		$("#txtApellidoPaterno").attr('readonly', false);
		$("#txtApellidoMaterno").attr('readonly', false);
		//$("#txtCurp").attr('readonly', false);
		
		$("#btnRegistroEstudiante").attr("disabled",false);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNumControl",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNombreEstudiante",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoPaterno",true);
		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoMaterno",true);
		//$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtCurp",true);

		// $("#txtNumControl").val(num_control);
		

		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNumControl",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNombreEstudiante",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoPaterno",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoMaterno",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slCarreras",true);

    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slPeriodoEscolar",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slCarreras",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slSemestres",true);
    	//$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtCurp",true);
    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtGrupo",true);
		

	});


	 function cargarSelectPeriodosEscolares(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectPeriodosEscolares",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija un periodo escolar</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_periodo_escolar+'">'+elemento.identificacion_larga+'</option>';
    	                      
    	                  });

    	                   if(solicitud=='sn')
    	                   {
    	                   		$("#slPeriodoEscolar").html(options);
    	                   }
    	                   else
    	                   {
    	                   	$("#slPeriodoEscolarCambio").html(options);
    	                   }

    	                   

    	                }
    	               

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectPeriodosEscolares('sn');



	 function cargarSelectCarreras(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectCarreras",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una carrera</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.clave_oficial+'">'+elemento.nombre_carrera+'</option>';
    	                      
    	                  });

    	                   if(solicitud=='sn')
    	                   {
    	                   		$("#slCarreras").html(options);
    	                   }
    	                   else
    	                   {
    	                   		$("#slCarrerasCambio").html(options);
    	                   }


    	                   

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron carreras disponibles porfavor agregue carreras para poder continuar");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectCarreras('sn');


	function cargarSelectSemestres(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectSemestres",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
                    	if(result.length > 0)
                    	{
                    	   let options ="<option selected disabled >Elija un semestre</option>";
                    	   result.forEach(function(elemento,index) 
                    	   {
                    	
                    	       options += '<option value="'+elemento.id_semestre+'">'+elemento.nombre_semestre+'</option>';
                    	      
                    	  });

                    	   if(solicitud=='sn')
    	                   {
    	                   		$("#slSemestres").html(options);
    	                   }
    	                   else
    	                   {
    	                   		$("#slSemestresCambio").html(options);
    	                   }

                    	}
                    	

	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectSemestres('sn');

	

	$("#formRegistrarEstudiantes").on("submit",function(event)
	{
		event.preventDefault();

		$("#formRegistrarEstudiantes").bootstrapValidator();

	});

	

	$("#txtNombreEstudiante").on("blur",function(event)
	{
		
		let dato = $(this).val().trim();

		let partes = [];

		let datoCorrecto = '';

		if( dato != "" )
		{
			
			partes = dato.split(" ");

			for(let x = 0 ; x < partes.length;x++)
			{
				if(x==0)
				{
					datoCorrecto = datoCorrecto + MaysPrimera(partes[x]);
				}
				else
				{
					datoCorrecto = datoCorrecto + ' ' + MaysPrimera(partes[x]);
				}
				
			}


			$(this).val(datoCorrecto);

		}
		

	});

	$("#txtApellidoPaterno").on("blur",function(event)
	{
		
		let dato = $(this).val().trim();

		let partes = [];

		let datoCorrecto = '';

		if( dato != "" )
		{
			
			partes = dato.split(" ");

			for(let x = 0 ; x < partes.length;x++)
			{
				if(x==0)
				{
					datoCorrecto = datoCorrecto + MaysPrimera(partes[x]);
				}
				else
				{
					datoCorrecto = datoCorrecto + ' ' + MaysPrimera(partes[x]);
				}
			}


			$(this).val(datoCorrecto);

		}
		

	});


	$("#txtApellidoMaterno").on("blur",function(event)
	{
		
		let dato = $(this).val().trim();

		let partes = [];

		let datoCorrecto = '';

		if( dato != "" )
		{
			
			partes = dato.split(" ");

			for(let x = 0 ; x < partes.length;x++)
			{
				if(x==0)
				{
					datoCorrecto = datoCorrecto + MaysPrimera(partes[x]);
				}
				else
				{
					datoCorrecto = datoCorrecto + ' ' + MaysPrimera(partes[x]);
				}
			}


			$(this).val(datoCorrecto);

		}
		

	});



	function validaRegistroEstudiantes()
	{
		

		$('#formRegistrarEstudiantes').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            rdRegistro: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtNumControl: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtNombreEstudiante: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtApellidoPaterno: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtApellidoMaterno: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            slPeriodoEscolar: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            slCarreras: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            slSemestres: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            // txtCurp: {
	            //     group: '.form-group',
	            //     validators: 
	            //     {
	            //         notEmpty: {
	            //             message: 'Este campo es requerido'
	            //         }
	                     

	            //     }
	            // },

	            txtGrupo: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	        }
	    }).on('success.form.bv', function (e) 
	    {

	    	e.preventDefault();

	    	let datos= 
	    	{
	    		nuevoIngreso: $("input[name='rdRegistro']").eq(0).is(':checked') ? '1' : '0',
	    		no_de_control: $("#txtNumControl").val().trim(), 
	    		id_periodo_escolar: $("#slPeriodoEscolar").val(),
	    		clave_oficial: $("#slCarreras").val(),
	    		id_semestre: $("#slSemestres").val()
	    	}

	    	$.ajax(
				{
		          type: "POST",
		          dataType:"json",
		          url: base_url+"RegistroEstudiantes/validateDatosRegistroEstudiantes",
		          data: datos,
		          async: true,
		          success: function(result)
			          {
			          	///console.log(result);

			          		let no_de_control = $("#txtNumControl").val().trim();

							let apellido_paterno = $("#txtApellidoPaterno").val().trim();
							let	apellido_materno = $("#txtApellidoMaterno").val().trim();
							let	nombre_alumno = $("#txtNombreEstudiante").val().trim();

							let	periodoEscolar = $("#slPeriodoEscolar option:selected").text();
							let	carrera = $("#slCarreras option:selected").text();
							let	semestre = $("#slSemestres option:selected").text();

			          	if(result.status == 'OK')
			          	{
			          		

					    	let mensaje = `El(la) estudiante <strong>${nombre_alumno} ${apellido_paterno} ${apellido_materno}</strong> con número de control <strong>${no_de_control}</strong> 
											esta apunto de realizar su registro para el periodo escolar <strong>${periodoEscolar}</strong> de la carrera de <strong>${carrera}</strong> para el <strong> ${semestre} </strong><br><br><br>
											Por favor antes de continuar verifique que la información de su registro esté escrita correctamente<br><br> 
											Si su información esta escrita correctamente puede pulsar el botón <strong>Realizar registro </strong> de lo contrario pulse el botón <strong>Regresar</strong>`;


							$("#modalConfirDatosCorrectosRegistroEstudiantes .modal-body").html(mensaje);
					    	$("#modalConfirDatosCorrectosRegistroEstudiantes").modal("show");

					    	$("#btnRegistroEstudiante").attr("disabled",false);

			          	}
			          	else
			          	{

			          		if(result.select == 'materia')
			          		{
			          			let mensaje = `<strong> No puedes pasar al ${$("#slSemestres option:selected").text()} debido a que no tienes evaluadas las materias : <br><br>`;

			          				result.materias.forEach(function(elemento,index) 
		    	                   {
		    	  						
		    	                       mensaje += `${elemento.nombre_materia} <br>`;
		    	                      
		    	                   });

			          			$("#modalAlertaValidateDatosRegistro .modal-body").html(mensaje+"<strong>");
			          		}
			          		else
			          		{

				          		if(result.select == 'control')
				          		{
				          			$("#modalAlertaValidateDatosRegistro .modal-body").html(result.mensaje);
				          		}
				          		else 
				          		{
				          			if(result.select == 'periodo')
				          			{
				          				$("#modalAlertaValidateDatosRegistro .modal-body").html(result.mensaje + " <strong>" +periodoEscolar +"</strong>" );
				          			}
				          			else
				          			{
				          				if(result.select == 'carrera')
						          		{
						          			$("#modalAlertaValidateDatosRegistro .modal-body").html(result.mensaje + " <strong>"+ carrera +"</strong>");
						          		}
						          		else
						          		{
						          			if(result.select == 'semestre')
							          		{
							          			$("#modalAlertaValidateDatosRegistro .modal-body").html(result.mensaje + " <strong>"+ semestre +"</strong>" );
							          		}
						          		}
				          			}
				          			
				          		}
			          		
			          		}
			          		
			          		
			          		$("#modalAlertaValidateDatosRegistro").modal("show");
			          		$("#btnRegistroEstudiante").attr("disabled",false);
			          	}

			          },
				   error:function(result)
					  {
					  	console.log(result.responseText);
					  	//$("#error").html(data.responseText); 
					  }
		          
		        });

	    	


	   

	    });


	}

	validaRegistroEstudiantes();


	$("body").on("click","#btnEnviarRegistroEstudiante",function()
	{
		    	let datos = {
		    					nuevoIngreso: $("input[name='rdRegistro']").eq(0).is(':checked') ? '1' : '0',
		    					no_de_control:$("#txtNumControl").val().trim(),
		    					id_semestre:$("#slSemestres").val(),
		    					apellido_paterno:$("#txtApellidoPaterno").val().trim(),
		    					apellido_materno:$("#txtApellidoMaterno").val().trim(),
		    					nombre_alumno:$("#txtNombreEstudiante").val().trim(),
		    					// curp_alumno:$("#txtCurp").val().trim(),
		    					curp_alumno: '',
		    					creditos_aprobados:'',
		    					creditos_cursados:'',
		    					clave_oficial:$("#slCarreras").val(),
		    					id_periodo_escolar:$("#slPeriodoEscolar").val(),
		    					grupo:$("#txtGrupo").val().trim()

		    				}

					$.ajax(
					{
			          type: "POST",
			          dataType:"json",
			          url: base_url+"RegistroEstudiantes/insertRegistroEstudiantes",
			          data: datos,
			          async: true,
			          success: function(result)
				          {
				          	console.log(result);

				          	$("#modalConfirDatosCorrectosRegistroEstudiantes").modal("hide");

				          	if(result.status == 'OK')
				          	{
				          		$("#modalAlerta .modal-body").text(result.mensaje);
				          		$("#modalAlerta").modal("show");

				          	}
				          	else
				          	{
				          		$("#modalAlerta .modal-body").text(result.mensaje);
				          		$("#modalAlerta").modal("show");
				          	}

				          },
					   error:function(result)
						  {
						  	console.log(result.responseText);
						  	//$("#error").html(data.responseText); 
						  }
			          
			        });


	})


	$("#formNumeroControlEstudiantes").on("submit",function(event)
	{
		event.preventDefault();

		$("#formNumeroControlEstudiantes").bootstrapValidator();

	});



	function validaNumeroControlEstudiante()
	{
		

		$('#formNumeroControlEstudiantes').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            mdlTxtNumControl: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },
	        }
	    }).on('success.form.bv', function (e) 
	    {

	    	e.preventDefault();

	    	

			$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Estudiantes/verifyNoControlGetInfo",
	          data: {
	          		no_de_control: $("#mdlTxtNumControl").val().trim()
	          },
	          async: true,
	          success: function(result)
		          {
		          	//console.log(result);

		          	if(result.msjCantidadRegistros > 0)
		          	{
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNumControl",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtNombreEstudiante",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoPaterno",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtApellidoMaterno",true);
		          		//$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("txtCurp",true);
		          		

		          		$("#txtNumControl").val(result.alumno[0].no_de_control);
		          		$("#txtNombreEstudiante").val(result.alumno[0].nombre_alumno);
		          		$("#txtApellidoPaterno").val(result.alumno[0].apellido_paterno);
		          		$("#txtApellidoMaterno").val(result.alumno[0].apellido_materno);
		          		//$("#txtCurp").val(result.alumno[0].curp_alumno);

		          		$("#slCarreras option[value="+ result.alumno[0].clave_oficial +"]").attr("selected",true);

		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNumControl",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNombreEstudiante",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoPaterno",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoMaterno",true);
		          		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slCarreras",true);
		          		//$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtCurp",true);
		          		
		          		

		          		$("#modalMsjInicialEstudiante").modal("hide");
		          		$("#txtNumControl").focus();

		          	}
		          	else
		          	{

		          		alert(result.mensaje);
		          		$("#btnRegistroEstudiante").attr("disabled",false);
		          		let no_control = $("#mdlTxtNumControl").val().trim();
		          		//$("#formNumeroControlAlumno").bootstrapValidator('resetForm', true);
		          		$("#mdlTxtNumControl").val(no_control);
		          		

		          	}

		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });

	    });


	}

	validaNumeroControlEstudiante();


	$('#modalMsjInicialEstudiante').on('hide.bs.modal', function (e) 
    {
    	
    	$("#formNumeroControlEstudiantes").data("bootstrapValidator").resetField("mdlTxtNumControl",true);

    	if($("#txtNumControl").val().trim() == "")
    	{

    		$("#formRegistrarEstudiantes").data("bootstrapValidator").resetField("rdRegistro",true);


    		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("rdRegistro",true);
    		$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNumControl",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtNombreEstudiante",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoPaterno",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtApellidoMaterno",true);

	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slPeriodoEscolar",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slCarreras",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("slSemestres",true);
	    	//$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtCurp",true);
	    	$("#formRegistrarEstudiantes").data("bootstrapValidator").revalidateField("txtGrupo",true);


    	}
    	
        

    });

	$('#modalAlerta').on('hide.bs.modal', function (e) 
    {
    	
    	location.reload();

    });


    $("#modalImageDiagrama").modal("show");


    function MaysPrimera(dato){
	  return dato.charAt(0).toUpperCase() + dato.slice(1);
	}



});

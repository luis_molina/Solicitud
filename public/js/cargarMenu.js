$(document).ready(function()
{
   var base_url = $("body").attr("data-base-url");

   $.ajax(
   {
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Home/getComitePersonal",
	          data: {a:''},
	          async: true,
	          success: function(result)
		          {
		          	
		          		let personal = ``;

		          		if(result != "2")
		          		{
		          			personal = `<li>
												<a href="#" data-toggle="collapse" data-target="#Personal" class="collapse active" aria-expanded="false">
															    	 <i class="fa fa-pencil-square-o"></i>
															    	 <span class="nav-label">Personal</span>
															    	 <i class="fa fa-chevron-left pull-right"></i>
																 </a>

												 <ul class="sub-menu collapse" id="Personal" aria-expanded="false" style="">
											    	 <li><a href="${base_url}Personal/registrar"><i class=""></i>Registrar</a></li>
											    	 <li><a href="${base_url}Personal/modificar"><i class=""></i>Modificar</a></li>
												 </ul>
											</li>`
		          		}



		          		  let menu =	` <ul class="list-sidebar bg-defoult" >
											  <ul class="list-sidebar bg-defoult">
													<li>
														<a href="#" data-toggle="collapse" data-target="#Solicitudes" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-user"></i>
																	    	 <span class="nav-label">Solicitudes</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="Solicitudes" aria-expanded="false" style="">
													    	 <li><a href="${base_url}Home"><i class=""></i>Estudiantes</a></li>
													    	 <li><a href="${base_url}Home/solicitudHistorial"><i class=""></i>Historial</a></li>
													    	 <li><a href="${base_url}Home/solicitudDictamen"><i class=""></i>Dictamen</a></li>
														 </ul>

													</li>
													<li>
														<a href="#" data-toggle="collapse" data-target="#Carreras" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-user"></i>
																	    	 <span class="nav-label">Carreras</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="Carreras" aria-expanded="false" style="">
													    	 <li><a href="${base_url}Carreras/registrar"><i class=""></i>Registrar</a></li>
													    	 <li><a href="${base_url}Carreras/modificar"><i class=""></i>Modificar</a></li>
														 </ul>
													</li>
													<li>
														<a href="#" data-toggle="collapse" data-target="#Materias" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-pencil-square-o"></i>
																	    	 <span class="nav-label">Materias</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="Materias" aria-expanded="false" style="">
													    	 <li><a href="${base_url}Materias/registrar"><i class=""></i>Registrar</a></li>
													    	 <li><a href="${base_url}Materias/modificar"><i class=""></i>Modificar</a></li>
														 </ul>
													</li>
													<li>
														<a href="${base_url}CarrerasMaterias" >
															<i class="fa fa-list-alt"></i>Carreras y Materias
														</a>
													</li>
													 <li>
														<a href="#" data-toggle="collapse" data-target="#PeriodosEscolares" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-pencil-square-o"></i>
																	    	 <span class="nav-label">Periodos Escolares</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="PeriodosEscolares" aria-expanded="false" style="">
													    	 <li><a href="${base_url}PeriodosEscolares/registrar"><i class=""></i>Registrar</a></li>
													    	 <li><a href="${base_url}PeriodosEscolares/modificar"><i class=""></i>Modificar</a></li>
														 </ul>
													</li>
													<li>
														<a href="#" data-toggle="collapse" data-target="#Estudiantes" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-pencil-square-o"></i>
																	    	 <span class="nav-label">Estudiantes</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="Estudiantes" aria-expanded="false" style="">
													    	 <li><a href="${base_url}EstudiantesAdmin/evaluar"><i class=""></i>Evaluar</a></li>
													    	 <li><a href="${base_url}EstudiantesAdmin/evaluarExcel"><i class=""></i>Evaluar - Excel</a></li>
													    	 <li><a href="${base_url}EstudiantesAdmin/kardex"><i class=""></i>Kardex</a></li>
													    	 <li><a href="${base_url}EstudiantesAdmin/modificar"><i class=""></i>Modificar</a></li>
														 </ul>
													</li>
													
													${personal}

													<li>
														<a href="#" data-toggle="collapse" data-target="#Reuniones" class="collapse active" aria-expanded="false">
																	    	 <i class="fa fa-pencil-square-o"></i>
																	    	 <span class="nav-label">Reuniones</span>
																	    	 <i class="fa fa-chevron-left pull-right"></i>
																		 </a>

														 <ul class="sub-menu collapse" id="Reuniones" aria-expanded="false" style="">
													    	 <li><a href="${base_url}Reunion/registrar"><i class=""></i>Registrar</a></li>
													    	 <li><a href="${base_url}Reunion/modificar"><i class=""></i>Modificar</a></li>
														 </ul>
													</li>

													<li>
														<a href="${base_url}ActaEstudiantes/acta" >
															<i class="fa fa-list-alt"></i>Acta
														</a>
													</li>
													
												</ul> 
		  								  </ul>`;

		  				 $(".sidebar.left").html(menu);


		  				 $("body .sidebar a[href='"+window.location.href.replace("#","")+"']").closest("ul").siblings().closest("ul").siblings().click();
					      $("body .sidebar a[href='"+window.location.href.replace("#","")+"']").closest("ul").siblings().click();
					      $("body .sidebar a[href='"+window.location.href.replace("#","")+"']").addClass('selecionado');

		          	
		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
    });



  });
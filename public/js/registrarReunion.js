$(document).ready(function()
{

	var base_url = $("body").attr("data-base-url");

	var datosReunion = '';

	$.ajax(
	{
	      
	      type: "POST",
	      url: base_url+"Reunion/getInfoPersonalReunion",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	if( typeof(result.redirect) == 'undefined')
                    {
	            	
	    	                if(result.status == "OK")
	    	                {

    	                			//let mensaje1 = 'En la cuidad y puerto de salina cruz siendo las trece horas con treinta minutos del dia catorce de agostro del año dos mil diecisiete, se reunieron en la sala de juntas ubicado dentro de las instalaciones que ocupa el instituto tecnologico de salina cruz, con direccion en carretera a san Antonio Motenrrey KM 1,7 en Salina Cruz Oaxaca, Los CC '

//     	                			let mensaje2 = `PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA : 
// 1.- PASE DE LA LISTA DE LOS INTEGRANTES DEL COMITÉ ACADÉMICO,
// 2.- LECTURA DE LAS SOLICITUDES DE ESTUDIANTES DE LA INSTITUCIÓN ENVIADAS AL COMITÉ ACADÉMICO, PARA SU ANÁLISIS ACADÉMICO Y RECOMENDACIÓN DE LAS MISMAS, PARA EL PERIODO ESCOLAR AGOSTO-DICIEMBRE DE 2017, APEGÁNDOSE AL MANUAL DE LINEAMIENTOS ACADÉMICO.ADMINISTRATIVO DEL TECNOLÓGICO NACIONAL DE MÉXICO.........
// COMO PRIMER PUNTO SE PROCEDIO A DAR PASE A SU LISTA, CONTANDO CON EL CIEN POR CIENTO DE ASISTENCIA, COMO SEGUNDO PUNTO SE DIO LECTURA , PARA ANALIZAR, LAS SOLICITUDES TURNADAS Y RECIBIDAS POR EL COMITÉ ACADÉMICO, PARA SU EFECTO EN EL PERIODO ESCOLAR AGOSTO-DICIEMBRE DE 2017 ........................................................`;

	    	                		let personales = '';
	    	                  
		    	                  	result.personal.forEach(function(elemento,index) 
		    	                   {
		    	                       personales +=  elemento.profesion+ " " + elemento.nombre + " " + elemento.apellidos + ", ";
		    	                      
		    	                  });

		    	                  //$("#txtPrimeraParteReunion").val(mensaje1.toUpperCase());
	    	                  	  $("#txtPersonal").val("LOS CC. "+personales.toUpperCase()+" PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA :");
	    	                  	  //$("#txtSegundaParteReunion").val(mensaje2.toUpperCase());

	    	                }
	    	                else if(result.status  == "NO_DATA")
	    	                {
	    	                	$("#modalAlerta .modal-body").html("No se encontraron personales disponibles");
	    						$("#modalAlerta").modal("show");
	    	                }
	    	                else
	    	                {
	    	                	$("#modalAlerta .modal-body").html(result.mensaje);
	    						$("#modalAlerta").modal("show");
	    	                }


	                 }
                    else
                    {
                      location.href = result.url;
                    }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	});




	validaRegistrarReunionPersonal();

	$("#formRegistrarReunionPersonal").on("submit",function(event)
	{
		event.preventDefault();

		$("#formRegistrarReunionPersonal").bootstrapValidator();

	});


	function validaRegistrarReunionPersonal()
	{
		

		$('#formRegistrarReunionPersonal').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	        	txtNombreReunion: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            slPeriodoEscolar: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtNoLibro: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },
	                    regexp: {
	                        regexp: /^[0-9.]+$/,

	                        message: 'Solo debe ingresar números',

	                    }

	                }
	            },

	            txtNoActa: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },
	                    regexp: {
	                        regexp: /^[0-9.]+$/,

	                        message: 'Solo debe ingresar números',

	                    }
	                     

	                }
	            },

	            txtPrimeraParteReunion: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtPersonal: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtSegundaParteReunion: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },


	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			$("#btnGuardarReunionPersonal").attr("disabled",false);

			 datosReunion = {
					nombre_reunion: $("#txtNombreReunion").val().trim(),
					id_periodo_escolar: $("#slPeriodoEscolar").val(),
					no_libro: $("#txtNoLibro").val().trim(),
					no_acta: $("#txtNoActa").val().trim(),
					mensaje_1: $("#txtPrimeraParteReunion").val().trim(),
					personal: $("#txtPersonal").val().trim(),
					mensaje_2: $("#txtSegundaParteReunion").val().trim(),
			}


			$.ajax(
			{
	  
			      type: "POST",
			      url: base_url+"Reunion/verifySolicitudesDisponibles",
			      dataType:"json",
			      data: {id_periodo_escolar:$("#slPeriodoEscolar").val()},
			      async: true,
			        success: function(result)
		            {

		            	if( typeof(result.redirect) == 'undefined')
	            		{

		    	                if(result.status == "OK")
		    	                {
			    	                let mensajeReunion = 'Hasta este momento se han encontrado <strong>'+result.rows+'</strong> solicitude(s) disponible(s) para la reunión <strong>'+$("#txtNombreReunion").val().trim()+ "</strong> en el periodo escolar <strong>"+$("#slPeriodoEscolar option:selected").text()+ "</strong><br><br><br> ¿Desea continuar?";

									$("#modalMensajeReunionEnvio .modal-body").html(mensajeReunion);
									$("#modalMensajeReunionEnvio").modal("show");

		    	                }
		    	                else if(result.status == "NO_DATA")
		    	                {
		    	                	$("#modalVerifySolicitudesDisponibles .modal-body").html(result.mensaje+" <strong>"+$("#slPeriodoEscolar option:selected").text()+"</strong>");
		    						$("#modalVerifySolicitudesDisponibles").modal("show");
		    	                }
		    	                else
		    	                {
		    	                	$("#modalAlerta .modal-body").text(result.mensaje);
		    						$("#modalAlerta").modal("show");
		    	                }

		    	        }
	                    else
	                    {
	                      location.href = result.url;
	                    }
		              
		            },
		       error:function(result)
		          {
		            alert("Error");
		           console.log(result.responseText);
		            
		          }
			});



			


	    });


	}


	$("#btnMensajeReunionEnvio").click(function(event) 
	{
		
		$.ajax(
		{
	  
		      type: "POST",
		      url: base_url+"Reunion/guardarReunion",
		      dataType:"json",
		      data: datosReunion,
		      async: true,
		        success: function(result)
		            {

		            	if( typeof(result.redirect) == 'undefined')
	            		{

		    	                if(result.status == "OK")
		    	                {
			    	                $("#modalAlerta .modal-body").text(result.mensaje);
		    						$("#modalAlerta").modal("show");

		    	                }
		    	                else
		    	                {
		    	                	$("#modalAlerta .modal-body").text(result.mensaje);
		    						$("#modalAlerta").modal("show");
		    	                }

		    	        }
	                    else
	                    {
	                      location.href = result.url;
	                    }
		              
		            },
		       error:function(result)
		          {
		            alert("Error");
		           console.log(result.responseText);
		            
		          }
		});

	});


	$('#modalAlerta').on('hide.bs.modal', function (e) 
    {
    	
    	location.reload();
        
    });



	$(".form-control").blur(function(event) {
		

		$(this).val($(this).val().toUpperCase())


	});


	function cargarSelectPeriodosEscolares()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectPeriodosEscolares",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija un periodo escolar</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_periodo_escolar+'">'+elemento.identificacion_larga+'</option>';
    	                      
    	                  });

    	                   
    	                   	$("#slPeriodoEscolar").html(options);

    	                }
    	               

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectPeriodosEscolares();



});
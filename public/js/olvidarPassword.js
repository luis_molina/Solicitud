$(document).on("ready",function()
{

	var base_url = $("body").attr("data-base-url");

	validaFormOlvidarPassword();

	$("#FormOlvidarPassword").on("submit",function(event)
	{
		event.preventDefault();

		$("#FormOlvidarPassword").bootstrapValidator();

	});


	function validaFormOlvidarPassword()
	{
		

		$('#FormOlvidarPassword').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {
	            email: {
	                group: '.input-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },
	                     regexp: {
                            regexp: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,

                            message: 'La dirección de email no es válida',

                        },

	                }
	            }
	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"OlvidarPassword/verifyEmail",
	          data: {
	          		email: $("#email").val().trim()
	          },
	          async: true,
	          success: function(result)
		          {
		          	console.log(result);

		          		if(result.msjConsulta == 'NO_DISPONIBLE')
		          		{
		          			$('#modalAlerta .modal-body').text(result.mensaje);
		          			$('#modalAlerta').modal('show');
		          		}
		          		else if(result.msjConsulta == 'Error')
		          		{
		          			$("#modalAlerta .modal-body").text("Ocurrió un error a la hora de enviar las instrucciones para el cambio de contraseña.");
                            $("#modalAlerta").modal("show");
		          		}
		          		else if(result.msjConsulta == 'OK'){

		          			let mensaje = "<h3>¡Éxito!</h3><hr>"+
                                                  "<strong><br><p>Te enviamos las instrucciones para restablecer tu contraseña al correo "+result.correo+", por favor revisa tu email.</p><br>"+
                                                  "<br><p>Si no recibiste un email, por favor revisa la carpeta de correo no deseado o ponte en contacto con el administrador.</p></strong><br><br>";

                                  $("#modalAlerta .modal-body").html(mensaje);
                                  $("#modalAlerta").modal("show");

                                  $("#FormOlvidarPassword").data("bootstrapValidator").resetField("email",true);
		          		}

		          	
		          	
		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });

	    });


	}

	function enviarEmailCambioPassword(email){

		$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"OlvidarPassword/enviarEmailCambioPassword",
	          data: {
	          		email: email
	          },
	          async: true,
	          success: function(result)
		          {
		          		console.log(result);

		          	
		          		if(result.resultado == 'OK')
		          		{
		          			
		          		}
		          		else
		          		{
		          			$('#modalAlerta .modal-body').text(result.mensaje);
		          			$('#modalAlerta').modal('show');
		          		}
		          	
		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });

	}
		

});



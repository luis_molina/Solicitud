$(document).ready(function()
{

	var base_url = $("body").attr("data-base-url");

	var tblReunion = $('#tblReunion').DataTable(
	    {
		       "processing": true,
		       "serverSide": true,
		       "ordering": true,
		        "language": {
		                       "url": base_url+"public/libreriasJS/Spanish.json"
		                     },
		                 "scrollY":        "500px",
		                 "scrollCollapse": true,
		       "ajax":{
		         url :base_url+"Reunion/cargarTablaReunion", 
		         type: "post",  
		         error: function(d){ 
		           $(".employee-grid-error").html("");
		           $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
		           $("#employee-grid_processing").css("display","none");
		           
		         }
		         ,
		         // success:function(d)
		         // {
		         //   debugger;
		         //  console.log(d);
		         // }
		       },
		       "columnDefs": [
		                     {
		                         "targets": [ 0 ],
		                         "visible": false,
		                         "searchable": false
		                     }
		                 ],
	        
	    
	    });



	$("body").on("click",".btnModificarReunion",function()
   {

         let id_reunion = tblReunion.rows($(this).closest("tr").index()).data().pluck(0)[0];


         $.ajax(
                 {
                       type: "POST",
                       dataType:"json",
                       url: base_url+"Reunion/getInfoReunion",
                       data: {id_reunion:id_reunion},
                       async: true,
                       success: function(result)
                         {

	                            if( typeof(result.redirect) == 'undefined')
	                            {

	                                if(result.status == "OK")
	                                {
	                                     $("#txtNombreReunion").val(result.reunion[0].nombre_reunion);
	                                     $("#txtNombreReunion").prop("id_reunion",result.reunion[0].id_reunion);
	                                     $("#slPeriodoEscolar").val(result.reunion[0].id_periodo_escolar).attr("selected", true);
	                                     $("#txtNoLibro").val(result.reunion[0].no_libro);
	                                     $("#txtNoActa").val(result.reunion[0].no_acta);
	                                     $("#txtPrimeraParteReunion").val(result.reunion[0].mensaje_1);
	                                     $("#txtPersonal").val(result.reunion[0].personal);
	                                     $("#txtSegundaParteReunion").val(result.reunion[0].mensaje_2);
	                                   

	                                     $("#modalModificarReunion").modal("show");
	                                     
	                                     
	                                }
	                                 else if(result.status  == "NO_DATA")
			    	                {
			    	                	$("#modalAlerta .modal-body").html("No se encontraron reuniones disponibles");
			    						$("#modalAlerta").modal("show");
			    	                }
			    	                else
			    	                {
			    	                	$("#modalAlerta .modal-body").html(result.mensaje);
			    						$("#modalAlerta").modal("show");
			    	                }
	                              
	                            }
	                            else
	                            {
	                              location.href = result.url;
	                            }
                              

                         },
                    error:function(result)
                     {
                       console.log(result.responseText);
                       //$("#error").html(data.responseText); 
                     }
                       
              });



   });


	 $("body").on("click",'.btnEliminarReunion',function() 
    {

         let id_reunion = tblReunion.rows($(this).closest("tr").index()).data().pluck(0)[0];

         let reunion = tblReunion.rows($(this).closest("tr").index()).data().pluck(1)[0];

         $("#modalAlertaEliminarReunion").prop("id_reunion",id_reunion);

         $("#modalAlertaEliminarReunion .modal-body").html("¿Desea eliminar la reunión <strong>"+reunion+" </strong> ? <br><br><br><br> <strong> Nota: <br> Al eliminar la reunión las solicitudes relacionadas con ésta quedaran disponibles </strong> <br><br><br>");

          $("#modalAlertaEliminarReunion").modal("show");


    });



	$("body").on("click",'#btnMdlEliminarReunion',function() 
    {

         let id_reunion =  $("#modalAlertaEliminarReunion").prop("id_reunion");

             $.ajax(
             {
                     type: "POST",
                     url: base_url+"Reunion/eliminarReunion",
                     dataType:"json",
                     data: {id_reunion:id_reunion},
                     async: true,
                     success: function(result)
                     {
                           
                            if( typeof(result.redirect) == 'undefined')
                            {
                                if(result.status == 'OK')
                                {
                                	$("#modalAlertaEliminarReunion").modal('hide');
                                   $("#modalAlerta .modal-body").text(result.mensaje);
                                   $("#modalAlerta").modal('show');
                                }
                                else
                                {
                                	$("#modalAlertaEliminarReunion").modal('hide');
                                  $("#modalAlerta .modal-body").text(result.mensaje);
                                   $("#modalAlerta").modal('show');
                                }
                            }
                            else
                            {
                              location.href = result.url;
                            }

                     },
                     error:function(result)
                       {
                         alert("Error");
                        console.log(result.responseText);
                         
                       }
                       
            });


    });


	validaModificarReunion();

	$("#formModificarReunion").on("submit",function(event)
	{
		event.preventDefault();

		$("#formModificarReunion").bootstrapValidator();

	});


	function validaModificarReunion()
	{
		

			$('#formModificarReunion').bootstrapValidator(
			{

		        message: 'This value is not valid',
		        container: 'tooltip',
		        feedbackIcons: {
		            valid: 'glyphicon glyphicon-ok',
		            invalid: 'glyphicon glyphicon-remove',
		            validating: 'glyphicon glyphicon-refresh'
		        },
		        fields: {

		        	txtNombreReunion: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    }
		                     

		                }
		            },

		            // slPeriodoEscolar: {
		            //     group: '.form-group',
		            //     validators: 
		            //     {
		            //         notEmpty: {
		            //             message: 'Este campo es requerido'
		            //         }
		                     

		            //     }
		            // },

		            txtNoLibro: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    },
		                    regexp: {
		                        regexp: /^[0-9.]+$/,

		                        message: 'Solo debe ingresar números',

		                    }
		                     

		                }
		            },

		            txtNoActa: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    },
		                    regexp: {
		                        regexp: /^[0-9.]+$/,

		                        message: 'Solo debe ingresar números',

		                    }
		                     

		                }
		            },

		            txtPrimeraParteReunion: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    }
		                     

		                }
		            },

		            txtPersonal: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    }
		                     

		                }
		            },

		            txtSegundaParteReunion: {
		                group: '.form-group',
		                validators: 
		                {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    }
		                     

		                }
		            },


		        }
		    }).on('success.form.bv', function (e) 
		    {

				e.preventDefault();

				$("#btnModificarReunion").attr("disabled",false);

				 datosReunion = {
				 		id_reunion:$("#txtNombreReunion").prop("id_reunion"),
						nombre_reunion: $("#txtNombreReunion").val().trim(),
						id_periodo_escolar: $("#slPeriodoEscolar").val(),
						no_libro: $("#txtNoLibro").val().trim(),
						no_acta: $("#txtNoActa").val().trim(),
						mensaje_1: $("#txtPrimeraParteReunion").val().trim(),
						personal: $("#txtPersonal").val().trim(),
						mensaje_2: $("#txtSegundaParteReunion").val().trim(),
				}

				
				$.ajax(
				{
		      
				      type: "POST",
				      url: base_url+"Reunion/modificarReunion",
				      dataType:"json",
				      data: datosReunion,
				      async: true,
				        success: function(result)
				            {

				            	if( typeof(result.redirect) == 'undefined')
			                 	{

			    	                if(result.status == "OK")
			    	                {
			    	                	$("#modalModificarReunion").modal("hide");
				    	                $("#modalAlerta .modal-body").text(result.mensaje);
			    						$("#modalAlerta").modal("show");

			    	                }
			    	                else
			    	                {
			    	                	$("#modalModificarReunion").modal("hide");
			    	                	$("#modalAlerta .modal-body").text(result.mensaje);
			    						$("#modalAlerta").modal("show");
			    	                }

				    	        }
			                    else
			                    {
			                      location.href = result.url;
			                    }
				              
				            },
				       error:function(result)
				          {
				            alert("Error");
				           console.log(result.responseText);
				            
				          }
				});


		    });


	}


	$('#modalModificarReunion').on('hide.bs.modal', function (e) 
    {
    	
    	$("#formModificarReunion").bootstrapValidator('resetForm', true);
        
    });


	 function cargarSelectPeriodosEscolares()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectPeriodosEscolares",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija un periodo escolar</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_periodo_escolar+'">'+elemento.identificacion_larga+'</option>';
    	                      
    	                  });

    	                   
    	                   	$("#slPeriodoEscolar").html(options);

    	                }
    	               

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectPeriodosEscolares();




	$('#modalAlerta').on('hide.bs.modal', function (e) 
    {
    	
    		location.reload();
        
    });


    $(".form-control").blur(function(event) {
		

		$(this).val($(this).val().toUpperCase())


	});



});
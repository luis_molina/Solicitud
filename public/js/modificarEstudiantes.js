$(document).ready(function()
{
   
	var base_url = $("body").attr("data-base-url");

   var tblModificarEstudiantes = $('#tblModificarEstudiantes').DataTable(
       {
          "processing": true,
          "serverSide": true,
          "ordering": true,
           "language": {
                          "url": base_url+"public/libreriasJS/Spanish.json"
                        },
                    "scrollY":        "500px",
                    "scrollCollapse": true,
          "ajax":{
            url :base_url+"EstudiantesAdmin/cargarTablaModificarEstudiantes", 
            type: "post",  
            error: function(d){ 
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
              
            }
            ,
            // success:function(d)
            // {
            //   debugger;
            //  console.log(d);
            // }
          },
          "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
           
       
       });



   $("body").on("click",".btnModificarEstudiante",function()
   {

         let id_alumno = tblModificarEstudiantes.rows($(this).closest("tr").index()).data().pluck(0)[0];


         $.ajax(
                 {
                       type: "POST",
                       dataType:"json",
                       url: base_url+"EstudiantesAdmin/getInfoEstudiante",
                       data: {id_alumno:id_alumno},
                       async: true,
                       success: function(result)
                         {
                            if( typeof(result.redirect) == 'undefined')
                            {

                                if(result.status == "OK")
                                {
                                     $("#txtNumControl").val(result.alumno[0].no_de_control);
                                     $("#txtNumControl").prop("no_de_control",result.alumno[0].no_de_control);
                                     $("#txtNumControl").prop("id_alumno",id_alumno);
                                     $("#txtNombreEstudiante").val(result.alumno[0].nombre_alumno);
                                     $("#txtApellidoPaterno").val(result.alumno[0].apellido_paterno);
                                     $("#txtApellidoMaterno").val(result.alumno[0].apellido_materno);
                                     $("#txtCurp").val(result.alumno[0].curp_alumno);
                                   

                                     $("#modalAlertaModificarEstudiante").modal("show");
                                     

                                     
                                }
                                else
                                {
                                     $('#modalAlerta .modal-body').text(result.mensaje);
                                     $('#modalAlerta').modal('show');
                                }
                              
                            }
                            else
                            {
                              location.href = result.url;
                            }
                              
                          

                         },
                    error:function(result)
                     {
                       console.log(result.responseText);
                       //$("#error").html(data.responseText); 
                     }
                       
              });



   });


   $("body").on("click",".btnEliminarEstudiante",function()
   {

          let id_alumno = tblModificarEstudiantes.rows($(this).closest("tr").index()).data().pluck(0)[0];

          


   });



   $('#modalAlertaModificarEstudiante').on('hide.bs.modal', function (e) 
    {
          
          $("#formDatosModificarEstudiantes").bootstrapValidator('resetForm', true);

    });


   $('#modalMsjModificarEstudiante').on('hide.bs.modal', function (e) 
    {
     
          location.reload();
        

    });


     $("body").on("click",".btnEliminarEstudiante",function()
     {

         let id_alumno = tblModificarEstudiantes.rows($(this).closest("tr").index()).data().pluck(0)[0];

         let nombre =  $(this).closest("tr").find("td").eq(1).text();
         let paterno =  $(this).closest("tr").find("td").eq(2).text();
         let materno =  $(this).closest("tr").find("td").eq(3).text();


         $("#btnMdlAlertEliminarEstudiante").prop("id_alumno",id_alumno);
         $("#modalAlertaEliminarEstudiante .modal-body").html(`<h5>¿Desea dar de baja al estudiante <strong> ${nombre} ${paterno} ${materno} </strong> ?<h5>`);
         $("#modalAlertaEliminarEstudiante").modal("show");
       
       
       
     });

    $("body").on("click","#btnMdlAlertEliminarEstudiante",function()
     {

            let id_alumno = $("#btnMdlAlertEliminarEstudiante").prop("id_alumno");

                $.ajax(
                 {
                       type: "POST",
                       dataType:"json",
                       url: base_url+"EstudiantesAdmin/deleteEstudiantes",
                       data: {id_alumno:id_alumno},
                       async: true,
                       success: function(result)
                         {

                            if( typeof(result.redirect) == 'undefined')
                            {

                                   if(result.status == "OK")
                                   {
                                       $("#btnMdlAlertEliminarEstudiante").removeProp("id_alumno");
                                       $('#modalAlertaMsjEliminarEstudiante .modal-body').html("<h5>"+result.mensaje+"</h5>");
                                       $('#modalAlertaMsjEliminarEstudiante').modal('show');

                                        
                                   }
                                   else
                                   {

                                      if(result.status == "NO_DISPONIBLE")
                                     {
                                         $('#modalAlertaMsjEliminarEstudiante .modal-body').html("<h5>"+result.mensaje+"</h5>");
                                         $('#modalAlertaMsjEliminarEstudiante').modal('show');
                                     }
                                     else{
                                         $('#modalAlertaMsjEliminarEstudiante .modal-body').html("<h5>"+result.mensaje+"</h5>");
                                         $('#modalAlertaMsjEliminarEstudiante').modal('show');
                                     }

                                         
                                        
                                   }

                            }
                            else
                            {
                              location.href = result.url;
                            }

                              
                           
                          

                         },
                    error:function(result)
                     {
                       console.log(result.responseText);
                       //$("#error").html(data.responseText); 
                     }
                       
                     });

     });



     $('#modalAlertaMsjEliminarEstudiante').on('hide.bs.modal', function (e) 
    {
           
          if( $("#btnMdlAlertEliminarEstudiante").prop("id_alumno") == undefined )
            {
              location.reload();
            }
        
    });



     function validaModificarEstudiante()
     {
          

          $('#formDatosModificarEstudiantes').bootstrapValidator(
          {

             message: 'This value is not valid',
             container: 'tooltip',
             feedbackIcons: {
                 valid: 'glyphicon glyphicon-ok',
                 invalid: 'glyphicon glyphicon-remove',
                 validating: 'glyphicon glyphicon-refresh'
             },
             fields: {

                 txtNumControl: {
                     group: '.form-group',
                     validators: 
                     {
                         notEmpty: {
                             message: 'Este campo es requerido'
                         },
                         callback: {
                                   message: 'El número de control no esta disponible',
                                   callback: function(value, validator) {
                                       // Get the selected options

                                       var valida = true;

                                       var datos = {
                                                             no_de_control_new:$("#txtNumControl").val().trim(),
                                                             no_de_control_origen:$("#txtNumControl").prop("no_de_control")
                                                           }


                                                   $.ajax(
                                                   {
                                                       type: "POST",
                                                       url: base_url+"EstudiantesAdmin/checkModificarNumeroControlEstudiante",
                                                     dataType:"json",
                                                       data: datos,
                                                        async: false,
                                                       success: function(result)
                                                           {
                                                                 //console.log(result);
                                                              if( typeof(result.redirect) == 'undefined')
                                                              {
                                                                  if(result.resultado == 'NO_DISPONIBLE')
                                                                  {
                                                                     valida = false;
                                                                  }
                                                                  else
                                                                  {
                                                                    valida = true;
                                                                  }
                                                              }
                                                              else
                                                              {
                                                                location.href = result.url;
                                                              }

                                                           },
                                                      error:function(result)
                                                         {
                                                           alert("Error");
                                                          console.log(result.responseText);
                                                           
                                                         }
                                                         
                                                   });

                                                   return valida;

                                   }
                               },

                          

                     }
                 },
                 txtNombreEstudiante: {
                     group: '.form-group',
                     validators: 
                     {
                         notEmpty: {
                             message: 'Este campo es requerido'
                         }
                          

                     }
                 },
                 txtApellidoPaterno: {
                     group: '.form-group',
                     validators: 
                     {
                         notEmpty: {
                             message: 'Este campo es requerido'
                         }
                          

                     }
                 },
                 txtApellidoMaterno: {
                     group: '.form-group',
                     validators: 
                     {
                         notEmpty: {
                             message: 'Este campo es requerido'
                         }
                          

                     }
                 },
                 // txtCurp: {
                 //     group: '.form-group',
                 //     validators: 
                 //     {
                 //         notEmpty: {
                 //             message: 'Este campo es requerido'
                 //         }
                          

                 //     }
                 // },
             }
         }).on('success.form.bv', function (e) 
         {

          e.preventDefault();


               let datos = {

                    id_alumno:$("#txtNumControl").prop("id_alumno"),
                    no_de_control:$("#txtNumControl").val().trim(),
                    nombre_alumno:$("#txtNombreEstudiante").val().trim(),
                    apellido_paterno:$("#txtApellidoPaterno").val().trim(),
                    apellido_materno:$("#txtApellidoMaterno").val().trim(),
                    curp_alumno:$("#txtCurp").val().trim()
               }


               $.ajax(
               {
                    type: "POST",
                    dataType:"json",
                    url: base_url+"EstudiantesAdmin/modificarDatosEstudiante",
                    data: datos,
                    async: true,
                    success: function(result)
                    {
                         //console.log(result);

                         if(result.status > 0)
                         {
                              
                              $("#modalAlertaModificarEstudiante").modal("hide");

                              $("#modalMsjModificarEstudiante .modal-body").text(result.mensaje);
                              $("#modalMsjModificarEstudiante").modal("show");
                              

                         }
                         else
                         {
                              $("#modalAlertaModificarEstudiante").modal("hide");
                              $("#modalMsjModificarEstudiante .modal-body").text(result.mensaje);
                              $("#modalMsjModificarEstudiante").modal("show");

                         }

                    },
                      error:function(result)
                      {
                         console.log(result.responseText);
                         //$("#error").html(data.responseText); 
                      }
               
             });





         });


     }

     validaModificarEstudiante();


  
















 });
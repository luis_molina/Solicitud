$(document).on("ready",function()
{

	var base_url = $("body").attr("data-base-url");

	// $('#slTipoSolicitud').multiselect({
	// 	buttonWidth: '530px',
	// 	buttonText: function(options, select) {
 //                if (options.length === 0) {
 //                    return 'Elija las opciones que necesite';
 //                }
 //            }
	// });



	var hoy = new Date();
	var dd = hoy.getDate();
	var mm = hoy.getMonth()+1; 
	var yyyy = hoy.getFullYear();

	if(dd<10) {
	    dd='0'+dd
	} 

	if(mm<10) {
	    mm='0'+mm
	} 

	hoy = yyyy +'-' +mm+'-'+dd;


	$("#txtFecha").val(hoy);



	$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Estudiantes/getDatosSecretario",
	          data: {
	          		a:''
	          },
	          async: true,
	          success: function(result)
		          {
		          	//console.log(result);

		          	if(result.msjCantidadRegistros > 0)
		          	{
		          		
		          		$("#jefeEstudiosProfesionales").text(result.personal[0].secretario);
		          		$("#departamentoSecretario").text(result.personal[0].departamento);


		          	}
		          	else
		          	{

		          		alert(result.mensaje);
		          		$("#btnNumControlGetDatosEstudiante").attr("disabled",false);
		          		let no_control = $("#mdlTxtNumControl").val().trim();
		          		//$("#formNumeroControlAlumno").bootstrapValidator('resetForm', true);
		          		$("#mdlTxtNumControl").val(no_control);
		          		

		          	}

		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });



	validaTipoSolicitudEstudiante();

	$("#formTipoSolicitudEstudiante").on("submit",function(event)
	{
		event.preventDefault();

		$("#formTipoSolicitudEstudiante").bootstrapValidator();

	});


	function validaTipoSolicitudEstudiante()
	{
		

		$('#formTipoSolicitudEstudiante').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	        	slTipoSolicitud: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            }
	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			$("#modalSeleccionarTipoSolicitud").modal("hide");

			$("#btnValidaTipoSolicitud").attr("disabled",false);

	    });


	}



	validaSolicitudEstudiante();

	$("#formSolicitudEstudiante").on("submit",function(event)
	{
		event.preventDefault();

		$("#formSolicitudEstudiante").bootstrapValidator();

	});


	function validaSolicitudEstudiante()
	{
		

		$('#formSolicitudEstudiante').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            txtLugar: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },

	            txtFecha: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            slPeriodoEscolar: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            txtAsunto: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            slTipoSolicitud: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            

	            txtNombreEstudiante: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            txtApellidoPaterno: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            txtApellidoMaterno: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            slSemestres: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            slCarreras: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            txtNumControl: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            txtObservacion: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }

	                }
	            },

	            // txtCurpEstudiante: {
	            //     group: '.form-group',
	            //     validators: {
	            //         notEmpty: {
	            //             message: 'Este campo es requerido'
	            //         }

	            //     }
	            // },

	            txtMotivosAcademicos: {
	                //group: '.form-group',
	                validators: {

	                	 notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                   

	                }
	            },

	            // txtMotivosPersonales: {
	            //    // group: '.form-group',
	            //     validators: {
	            //     	notEmpty: {
	            //             message: 'Debe escribir por lo menos un motivo'
	            //         }
	                 
 
	            //     }
	            // },

	            //  txtOtros: {
	            //    // group: '.form-group',
	            //     validators: {
	            //     	notEmpty: {
	            //             message: 'Debe escribir por lo menos un motivo'
	            //         }

	            //     }
	            // },

	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			let sltipoSolicitud = $("#slTipoSolicitud").val();

			if(sltipoSolicitud == null || sltipoSolicitud == "")
			{
				$("#modalMensjValidaDatosTipoSolicitud .modal-body").html("<strong>Debe elegir el de tipo de solicitud a realizar</strong>");
				$("#modalMensjValidaDatosTipoSolicitud").modal("show");
			}
			else
			{

					let datoElement = '';
					let bandera = true;

					$("#divContainerTipoSolicitud .has-feedback > .form-control").each(function(index, el) 
					{
						datoElement = $(this).val();

						if(datoElement == null || datoElement == "")
						{
							bandera = false;
							return false;
						}

					});

					if(bandera)
					{
						let no_de_control = $("#txtNumControl").val().trim();
						let apellido_paterno = $("#txtApellidoPaterno").val().trim();
						let	apellido_materno = $("#txtApellidoMaterno").val().trim();
						let	nombre_alumno = $("#txtNombreEstudiante").val().trim();
						
						let mensaje = `El(la) estudiante <strong>${nombre_alumno} ${apellido_paterno} ${apellido_materno}</strong> con número de control <strong>${no_de_control}</strong> 
										esta apunto de enviar su solicitud <br><br><br>
										Por favor antes de continuar verifique que la información de su solicitud esté escrita correctamente, debido a que ya no se podrán hacer cambios a la hora de enviar la solictud 
										<br><br><br> 
										Si su información esta escrita correctamente puede pulsar el botón <strong>Enviar Solicitud </strong> de lo contrario pulse el botón <strong>Regresar</strong>`;

						$("#modalConfirDatosCorrectosSolicitud .modal-body").html(mensaje);
						$("#modalConfirDatosCorrectosSolicitud").modal("show");

					}
					else
					{
						let solicitud = $("#slTipoSolicitud option:selected").text();

						$("#modalMensjValidaDatosTipoSolicitud .modal-body").html("<strong>Debe elegir todos los datos para el tipo de solicitud</strong> <strong style='color:red'>"+solicitud+ " </strong>");
						$("#modalMensjValidaDatosTipoSolicitud").modal("show");


					}

			}

			$("#btnAceptarSolicitudEstudiante").attr("disabled",false);

			



	    });


	}



	$('#modalMensjValidaDatosTipoSolicitud').on('hide.bs.modal', function (e) 
    {
    	$("body").css("padding-right","19px");
    	$("#modalSeleccionarTipoSolicitud").modal("show");
    	$("body").css("padding-right","19px");


    });


	$("#btnEnviarSolicitudEstudiante").on("click",function(){

		let datosAlumno = 
			{
				no_de_control:$("#txtNumControl").val().trim(),
				id_semestre:$("#slSemestres").val(),
				apellido_paterno:$("#txtApellidoPaterno").val().trim(),
				apellido_materno:$("#txtApellidoMaterno").val().trim(),
				nombre_alumno:$("#txtNombreEstudiante").val().trim(),
				// curp_alumno:$("#txtCurpEstudiante").val().trim(),
				curp_alumno: '',
				creditos_aprobados:'',
				creditos_cursados:'',
				clave_oficial:$("#slCarreras").val()
			}

			let datosSolicitud = 
			{
				status:'Pendiente',
				observacion:$("#txtObservacion").val().trim(),
				asunto:$("#txtAsunto").val().trim(),
				lugar:$("#txtLugar").val().trim(),
				fecha:$("#txtFecha").val().trim(),
				motivos_academicos:$("#txtMotivosAcademicos").val().trim(),
				motivos_personales:$("#txtMotivosPersonales").val().trim(),
				otros:$("#txtOtros").val().trim(),
				id_semestre:$("#slSemestres").val(),
				clave_oficial:$("#slCarreras").val(),
				id_periodo_escolar:$("#slPeriodoEscolar").val()
				
			}


			let datosMateriaCursoRepeticion = "_";

			if($("#divContCursoRepeticion").length > 0)
			{

				 datosMateriaCursoRepeticion = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $("#slMateriasCursoRepeticion").val(),
					tipo_solicitud : ''

				}

			}


			let datosMateriaCursoEspecial = "_";

			if($("#divContCursoEspecial").length > 0)
			{

				 datosMateriaCursoEspecial = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $("#slMateriasCursoEspecial").val(),
					tipo_solicitud : ''

				}

			}


			


			let materia2CursosEspecAsig = "_";

			let  datosMateria2CursosEspecAsig = [""];

			if($("#divCont2CursosEspecAsig").length > 0)
			{

				datosMateria2CursosEspecAsig = [];

				$("#divCont2CursosEspecAsig .slMaterias").each(function(index,value)
				{

					materia2CursosEspecAsig = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $(this).val(),
					tipo_solicitud : ''

					}

					datosMateria2CursosEspecAsig.push(materia2CursosEspecAsig)
				


				});
				
			}


			let materia2CursosEspecVerano = "_";

			let  datosMateria2CursosEspecVerano = [""];

			if($("#divCont2CursosEspecVerano").length > 0)
			{


				datosMateria2CursosEspecVerano = [];


				$("#divCont2CursosEspecVerano .slMaterias").each(function(index,value)
				{

					

					materia2CursosEspecVerano = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $(this).val(),
					tipo_solicitud : ''

					}

					datosMateria2CursosEspecVerano.push(materia2CursosEspecVerano)
				


				});
				
			}



			let datosMateriaCursoSobreCreditos = "_";

			if($("#divContCursoSobreCreditos").length > 0)
			{

				 datosMateriaCursoSobreCreditos = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $("#slMateriasCursoSobreCreditos").val(),
					tipo_solicitud : ''

				}

			}


			let datosMateriaCursoEspecialResidencia = "_";

			if($("#divContCursoEspecialResidencia").length > 0)
			{

				 datosMateriaCursoEspecialResidencia = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : $("#slMateriasCursoEspecialResidencia").val(),
					tipo_solicitud : ''

				}

			}


			let datosBajaDefinitiva = "_";

			if($("#divContBajaDefinitiva").length > 0)
			{

				 datosBajaDefinitiva = {
				 	id_kardex:'',
					num_solicitud:'',
					id_alumno:'',
					clave_oficial: $("#slCarreras").val(),
					id_semestre: $("#slSemestres").val(),
					id_periodo_escolar : $("#slPeriodoEscolar").val(),
					materia : '',
					tipo_solicitud : ''

				}

			}

			// let relSolicitudCambioCarrera = {
			// 		id_alumno:'',
			// 		no_de_control: '',
			// 		num_solicitud:'',
			// 		clave_oficial: '',
			// 		id_semestre: '',
			// 		id_periodo_escolar :'',
			// 		grupo :''

			// 	}


			// if($("#checkSICambioCarrrera").is(':checked'))
			// {
			// 	 relSolicitudCambioCarrera = 
			// 	{
			// 		id_alumno:'',
			// 		no_de_control: $("#txtNumControlCambio").val().trim(),
			// 		num_solicitud:'',
			// 		clave_oficial: $("#slCarrerasCambio").val(),
			// 		id_semestre: $("#slSemestresCambio").val(),
			// 		id_periodo_escolar : $("#slPeriodoEscolarCambio").val(),
			// 		grupo:$("#txtGrupo").val().trim()
			// 	}
			// }
			


			let no_de_control = $("#txtNumControl").val().trim();

			// console.log(datosAlumno);
			// console.log(datosSolicitud);
			// console.log(datosMateriaCursoRepeticion);
			// console.log(datosMateriaCursoEspecial);
			// console.log(datosMateria2CursosEspecAsig);
			//console.log(datosMateria2CursosEspecVerano);
			// console.log(datosMateriaCursoSobreCreditos);
			// console.log(datosMateriaCursoEspecialResidencia);
			// console.log(datosBajaDefinitiva);

			$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Estudiantes/enviarSolicitudEstudiante",
	          data: {
	          		no_de_control: $("#txtNumControl").val().trim(),
	          		datosAlumno:datosAlumno,
	          		datosSolicitud:datosSolicitud,
	          		datosMateriaCursoEspecial : datosMateriaCursoEspecial,
	          		datosMateriaCursoRepeticion: datosMateriaCursoRepeticion,
	          		datosMateria2CursosEspecAsig:datosMateria2CursosEspecAsig,
	          		datosMateria2CursosEspecVerano:datosMateria2CursosEspecVerano,
	          		datosMateriaCursoSobreCreditos:datosMateriaCursoSobreCreditos,
	          		datosMateriaCursoEspecialResidencia:datosMateriaCursoEspecialResidencia,
	          		datosBajaDefinitiva:datosBajaDefinitiva


	          },
	          async: true,
	          success: function(result)
		          {

		          	if(result.status == 'OK')
		          	{
		          		$("#modalConfirDatosCorrectosSolicitud").modal("hide");
		          		$("#modalAlertaMsJSolicitudEstudiante .modal-body").html(result.mensaje);
		          		$("#modalAlertaMsJSolicitudEstudiante").prop('statusSolicitud','OK');
		          		$("#modalAlertaMsJSolicitudEstudiante").modal("show");
		          	}
		          	else
		          	{

		          		$("#modalConfirDatosCorrectosSolicitud").modal("hide");
		          		$("#modalAlertaMsJSolicitudEstudiante .modal-body").html(result.mensaje);
		          		$("#modalAlertaMsJSolicitudEstudiante").modal("show");
		          		
		          	}

		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });





	})



	$('#modalAlertaMsJSolicitudEstudiante').on('hide.bs.modal', function (e) 
    {
    	
    	if($("#modalAlertaMsJSolicitudEstudiante").prop('statusSolicitud') != undefined)
    	{
    		location.reload();
    		
    	}
        

    });


	 function cargarSelectPeriodosEscolares(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectPeriodosEscolares",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija un periodo escolar</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_periodo_escolar+'">'+elemento.identificacion_larga+'</option>';
    	                      
    	                  });

    	                   if(solicitud=='sn')
    	                   {
    	                   		$("#slPeriodoEscolar").html(options);
    	                   }
    	                   else
    	                   {
    	                   	$("#slPeriodoEscolarCambio").html(options);
    	                   }

    	                   

    	                }
    	               

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectPeriodosEscolares('sn');



	 function cargarSelectCarreras(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectCarreras",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una carrera</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.clave_oficial+'">'+elemento.nombre_carrera+'</option>';
    	                      
    	                  });

    	                   if(solicitud=='sn')
    	                   {
    	                   		$("#slCarreras").html(options);
    	                   }
    	                   else
    	                   {
    	                   		$("#slCarrerasCambio").html(options);
    	                   }


    	                   

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron carreras disponibles porfavor agregue carreras para poder continuar");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectCarreras('sn');


	function cargarSelectSemestres(solicitud)
	{

		$.ajax(
	    {
	      
		      type: "POST",
		      url: base_url+"Estudiantes/cargarSelectSemestres",
		      dataType:"json",
		      data: '',
		      async: true,
		        success: function(result)
		            {

		            	
	                    	if(result.length > 0)
	                    	{
	                    		if(solicitud=='sn')
	    	                   {
	    	                   		 let options ="<option selected disabled >Elija un semestre</option>";
	    	                   		 result.forEach(function(elemento,index) 
	    	                   		 {
	    	                   		
	    	                   		     options += '<option value="'+elemento.id_semestre+'">'+elemento.nombre_semestre+'</option>';
	    	                   		    
	    	                   		});

	    	                   		 $("#slSemestres").html(options);
	    	                   }
	    	                   else
	    	                   {
	    	                   		 let options ="<option selected disabled >Elija una opción</option>";
	    	                   		 result.forEach(function(elemento,index) 
	    	                   		 {
	    	                   		
	    	                   		     options += '<option value="'+elemento.id_semestre+'">'+elemento.nombre_semestre+'</option>';
	    	                   		    
	    	                   		});

	    	                   		$(".slSemestres").html(options);
	    	                   }

	                    	   

	                    	}
	                    	

		              
		            },
		       error:function(result)
		          {
		            alert("Error");
		           console.log(result.responseText);
		            
		          }
	   });


	}
	cargarSelectSemestres('sn');


	function cargarSelectSemestresForSolicitud(solicitud)
	{

		$.ajax(
	    {
	      
		      type: "POST",
		      url: base_url+"Estudiantes/cargarSelectSemestresForSolicitud",
		      dataType:"json",
		      data: {
		      		 no_de_control: $("#txtNumControl").val().trim(),
		      		 tipo_solicitud: $("#slTipoSolicitud").val()
		      		},
		      async: true,
		        success: function(result)
		            {

		            	
	                    	if(result.length > 0)
	                    	{
	                    		if(solicitud=='sn')
	    	                   {
	    	                   		 let options ="<option selected disabled >Elija un semestre</option>";
	    	                   		 result.forEach(function(elemento,index) 
	    	                   		 {
	    	                   		
	    	                   		     options += '<option value="'+elemento.id_semestre+'">'+elemento.nombre_semestre+'</option>';
	    	                   		    
	    	                   		});

	    	                   		 $("#slSemestres").html(options);
	    	                   }
	    	                   else
	    	                   {
	    	                   		 let options ="<option selected disabled >Elija una opción</option>";
	    	                   		 result.forEach(function(elemento,index) 
	    	                   		 {
	    	                   		
	    	                   		     options += '<option value="'+elemento.id_semestre+'">'+elemento.nombre_semestre+'</option>';
	    	                   		    
	    	                   		});

	    	                   		$(".slSemestres").html(options);
	    	                   }

	                    	   

	                    	}
	                    	

		              
		            },
		       error:function(result)
		          {
		            alert("Error");
		           console.log(result.responseText);
		            
		          }
	   });


	}
	//cargarSelectSemestresForSolicitud('sn');



	// $("body").on("click","#rdSolicitudNO",function()
	// {
	// 	$("#modalAlertaIngresaNocontrol").modal("show");
	// 	$("#txtNumControl").val("");
	// 	$("#txtNumControl").attr('readonly', true);
	// 	$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtNumControl",true);
	// 	$("#btnAceptarSolicitudEstudiante").attr("disabled",false);

	// });

	// $("body").on("click","#rdSolicitudSI",function()
	// {
	// 	let num_control = $("#txtNumControl").val().trim();
	// 	$("#txtNumControl").attr('readonly', false);
	// 	$("#btnAceptarSolicitudEstudiante").attr("disabled",false);
	// 	$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtNumControl",true);
	// 	$("#txtNumControl").val(num_control);
	// 	$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtNumControl",true);
		

	// });

	validaNumeroControEstudiante();

	$("#formNumeroControlAlumno").on("submit",function(event)
	{
		event.preventDefault();

		$("#formNumeroControlAlumno").bootstrapValidator();

	});



	function validaNumeroControEstudiante()
	{
		

		$('#formNumeroControlAlumno').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            mdlTxtNumControl: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },
	        }
	    }).on('success.form.bv', function (e) 
	    {

	    	e.preventDefault();

			$.ajax(
			{
	          type: "POST",
	          dataType:"json",
	          url: base_url+"Estudiantes/verifyNoControlGetInfo",
	          data: {
	          		no_de_control: $("#mdlTxtNumControl").val().trim()
	          },
	          async: true,
	          success: function(result)
		          {
		          	//console.log(result);

		          	if(result.msjCantidadRegistros > 0)
		          	{
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtNumControl",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtNombreEstudiante",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtApellidoPaterno",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtApellidoMaterno",true);
		          		//$("#formSolicitudEstudiante").data("bootstrapValidator").resetField("txtCurpEstudiante",true);

		          		$("#txtNumControl").val(result.alumno[0].no_de_control);
		          		$("#txtNombreEstudiante").val(result.alumno[0].nombre_alumno);
		          		$("#txtApellidoPaterno").val(result.alumno[0].apellido_paterno);
		          		$("#txtApellidoMaterno").val(result.alumno[0].apellido_materno);
		          		//$("#txtCurpEstudiante").val(result.alumno[0].curp_alumno);

		          		$("#strCantSolid").text(result.alumno[0].cantidad_solicitud);
		          		$("#jefeEstudiosProfesionales").text(result.alumno[0].secretario);
		          		$("#departamentoSecretario").text(result.alumno[0].departamento);


		          		$("#slPeriodoEscolar").val(result.alumno[0].id_periodo_escolar).attr("selected", true);
		
		          		$("#slSemestres").val(result.alumno[0].id_semestre).attr("selected", true);
		          		$("#slCarreras").val(result.alumno[0].clave_oficial).attr("selected", true);

		          		$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtNumControl",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtNombreEstudiante",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtApellidoPaterno",true);
		          		$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtApellidoMaterno",true);
		          		//$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtCurpEstudiante",true);

		          		$("#modalAlertaIngresaNocontrol").modal("hide");
		          		$("#txtNumControl").focus();

		          	}
		          	else
		          	{

		          		alert(result.mensaje);
		          		$("#btnNumControlGetDatosEstudiante").attr("disabled",false);
		          		let no_control = $("#mdlTxtNumControl").val().trim();
		          		//$("#formNumeroControlAlumno").bootstrapValidator('resetForm', true);
		          		$("#mdlTxtNumControl").val(no_control);
		          		

		          	}

		          },
			   error:function(result)
				  {
				  	console.log(result.responseText);
				  	//$("#error").html(data.responseText); 
				  }
	          
	        });





	    });


	}



	// $("body").on("change",'#slCarrerasCambio',function() {
		
	// 	let new_no_de_control = $("#txtNumControl").val() + "C"; // $("#slCarrerasCambio").val().substring(1, 2);

	// 	$("#txtNumControlCambio").val(new_no_de_control);
	// 	$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtNumControlCambio",true);


	// })

	$('#modalAlertaIngresaNocontrol').on('hide.bs.modal', function (e) 
    {
    	$("#btnAceptarSolicitudEstudiante").attr("disabled",false);
    	$("#formNumeroControlAlumno").data("bootstrapValidator").resetField("mdlTxtNumControl",true);
    	$("#formSolicitudEstudiante").data("bootstrapValidator").revalidateField("txtNumControl",true);
        

    });


	$("#modalImageDiagrama").modal("show");

	$('#modalImageDiagrama').on('hide.bs.modal', function (e) 
    {
    	
    	$("#modalAlertaIngresaNocontrol").modal("show");

    });


	$("#btnOpenMdlTipoSolitud").on("click",function()
	{
		$("#modalSeleccionarTipoSolicitud").modal("show");
	});



	$("body").on("change",'#slTipoSolicitud',function() {
		

		var tiposSolicitud = $(this).val();


		$("#divContainerTipoSolicitud").html("");


		for(let x=0 ; x < tiposSolicitud.length;x++)
		{
			
			if( tiposSolicitud[x] == 1)
			{

				$("#divContainerTipoSolicitud").append(tempCursoRepeticion());


				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestresCursoRepeticion',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMateriasCursoRepeticion',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

			}
			else if( tiposSolicitud[x] == 2)
			{
				
				$("#divContainerTipoSolicitud").append(tempCursoEspecial());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestresCursoEspecial',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMateriasCursoEspecial',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
			

				
			}
			else if( tiposSolicitud[x] == 3)
			{
				
				$("#divContainerTipoSolicitud").append(temp2CursosEspecAsig());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestres2CursosEspecAsig1',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMaterias2CursosEspecAsig1',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
				
				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestres2CursosEspecAsig2',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMaterias2CursosEspecAsig2',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				
			}
			else if( tiposSolicitud[x] == 4)
			{
				
				$("#divContainerTipoSolicitud").append(temp2CursosEspecVerano());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestres2CursosEspecVerano1',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMaterias2CursosEspecVerano1',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
				
				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestres2CursosEspecVerano2',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMaterias2CursosEspecVerano2',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				
			}
			else if( tiposSolicitud[x] == 5)
			{


				$("#divContainerTipoSolicitud").append(tempCursoSobreCreditos());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestresCursoSobreCreditos',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMateriasCursoSobreCreditos',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
			

				
			}
			else if( tiposSolicitud[x] == 6)
			{


				$("#divContainerTipoSolicitud").append(tempCursoEspecialResidencia());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestresCursoEspecialResidencia',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slMateriasCursoEspecialResidencia',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
			

				
			}
			else
			{


				$("#divContainerTipoSolicitud").append(tempBajaDefinitiva());

				$('#formTipoSolicitudEstudiante').bootstrapValidator('addField','slSemestresBajaDefinitiva',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            }

                        }
                    });
			

				
			}



		}


		cargarSelectSemestresForSolicitud('s');


	});


	function tempCursoEspecial()
	{
		let tempCursoEspecial = `<br>
									<fieldset style='border: 1px solid silver;border-radius: 10px;padding: 5px;width: 100%;' id='divContCursoEspecial'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por curso especial</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            
                                            <div class="form-group">
                                                  <select id="slSemestresCursoEspecial" class="form-control slSemestres" name="slSemestresCursoEspecial" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la materia a llevar en curso especial</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slMateriasCursoEspecial" class="form-control slMaterias" name="slMateriasCursoEspecial" style='width:100%;'>
                                                </select> 
                                            </div>
										<br>
                                       </fieldset>`; 


			return  tempCursoEspecial;
	}

	function tempCursoRepeticion()
	{
		let tempCursoRepeticion = `     
										<fieldset style='border: 1px solid silver;border-radius:10px;padding: 5px;width:100%;' id='divContCursoRepeticion'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por curso de repetición</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slSemestresCursoRepeticion" class="form-control slSemestres" name="slSemestresCursoRepeticion" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la materia a llevar en curso de repetición</label> 
                                            </div>
                                            <div class="form-group" >
                                                  <select id="slMateriasCursoRepeticion" class="form-control slMaterias" name="slMateriasCursoRepeticion" style='width:100%;'>
                                                </select> 
                                            </div>
                                            <br>
										</fieldset>`; 

			return  tempCursoRepeticion;
	}


	function temp2CursosEspecAsig()
	{
		let temp2CursosEspecAsig = `     
										<fieldset style='border: 1px solid silver;border-radius:10px;padding: 5px;width:100%;height: 340px;overflow:auto;' id='divCont2CursosEspecAsig'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por 2 Cursos especiales con otras asignaturas</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slSemestres2CursosEspecAsig1" class="form-control slSemestres" name="slSemestres2CursosEspecAsig1" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la <label style="color:red"> primera materia </label> a llevar en curso especial</label> 
                                            </div>
                                            <div class="form-group" >
                                                  <select id="slMaterias2CursosEspecAsig1" class="form-control slMaterias" name="slMaterias2CursosEspecAsig1" style='width:100%;'>
                                                </select> 
                                            </div>

                                            <br>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slSemestres2CursosEspecAsig2" class="form-control slSemestres" name="slSemestres2CursosEspecAsig2" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la <label style="color:red"> segunda materia </label> a llevar en curso de especial</label> 
                                            </div>
                                            <div class="form-group" >
                                                  <select id="slMaterias2CursosEspecAsig2" class="form-control slMaterias" name="slMaterias2CursosEspecAsig2" style='width:100%;'>
                                                </select> 
                                            </div>
                                            <br>
										</fieldset>`; 

			return  temp2CursosEspecAsig;
	}


	function temp2CursosEspecVerano()
	{
		let temp2CursosEspecVerano = `     
										<fieldset style='border: 1px solid silver;border-radius:10px;padding: 5px;width:100%;height: 340px;overflow:auto;' id='divCont2CursosEspecVerano'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por 2 Cursos especiales en un curso de verano</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slSemestres2CursosEspecVerano1" class="form-control slSemestres" name="slSemestres2CursosEspecVerano1" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la <label style="color:red"> primera materia </label> a llevar en curso especial</label> 
                                            </div>
                                            <div class="form-group" >
                                                  <select id="slMaterias2CursosEspecVerano1" class="form-control slMaterias" name="slMaterias2CursosEspecVerano1" style='width:100%;'>
                                                </select> 
                                            </div>

                                            <br>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slSemestres2CursosEspecVerano2" class="form-control slSemestres" name="slSemestres2CursosEspecVerano2" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la <label style="color:red"> segunda materia </label> a llevar en curso de especial</label> 
                                            </div>
                                            <div class="form-group" >
                                                  <select id="slMaterias2CursosEspecVerano2" class="form-control slMaterias" name="slMaterias2CursosEspecVerano2" style='width:100%;'>
                                                </select> 
                                            </div>
                                            <br>
										</fieldset>`; 

			return  temp2CursosEspecVerano;
	}


	function tempCursoSobreCreditos()
	{


		let tempCursoSobreCreditos = `<br>
									<fieldset style='border: 1px solid silver;border-radius: 10px;padding: 5px;width: 100%;' id='divContCursoSobreCreditos'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por sobre créditos </label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            
                                            <div class="form-group">
                                                  <select id="slSemestresCursoSobreCreditos" class="form-control slSemestres" name="slSemestresCursoSobreCreditos" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la materia a llevar en curso ordinario</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slMateriasCursoSobreCreditos" class="form-control slMaterias" name="slMateriasCursoSobreCreditos" style='width:100%;'>
                                                </select> 
                                            </div>
										<br>
                                       </fieldset>`; 


			return  tempCursoSobreCreditos;
	}


	function tempCursoEspecialResidencia()
	{


		let tempCursoEspecialResidencia = `<br>
									<fieldset style='border: 1px solid silver;border-radius: 10px;padding: 5px;width: 100%;' id='divContCursoEspecialResidencia'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por Residencia Profesional con una asignatura en curso especial</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre</label> 
                                            </div>
                                            
                                            <div class="form-group">
                                                  <select id="slSemestresCursoEspecialResidencia" class="form-control slSemestres" name="slSemestresCursoEspecialResidencia" style='width:100%;'>
                                                </select> 
                                            </div>
                                             <div class="form-group" >
                                                  <label>Elije la materia a llevar en curso especial</label> 
                                            </div>
                                            <div class="form-group">
                                                  <select id="slMateriasCursoEspecialResidencia" class="form-control slMaterias" name="slMateriasCursoEspecialResidencia" style='width:100%;'>
                                                </select> 
                                            </div>
										<br>
                                       </fieldset>`; 


			return  tempCursoEspecialResidencia;
	}


	function tempBajaDefinitiva()
	{


		let tempBajaDefinitiva = `<br>
									<fieldset style='border: 1px solid silver;border-radius: 10px;padding: 5px;width: 100%;' id='divContBajaDefinitiva'>
                                        <br>
                                            <div class="form-group" class="text-center">
                                                  <label style="color:red">Has elegido solicitud por Baja Definitiva con posibilidad de inscribirse de nuevo a la Institución</label> 
                                            </div>
                                            <div class="form-group">
                                                 <label>Elije el semestre al cual te darás de baja definitiva</label> 
                                            </div>
                                            
                                            <div class="form-group">
                                                  <select id="slSemestresBajaDefinitiva" class="form-control slSemestres" name="slSemestresBajaDefinitiva" style='width:100%;'>
                                                </select> 
                                            </div>
										<br>
                                       </fieldset>`; 


			return  tempBajaDefinitiva;
	}

	

	$("body").on("change",".slMaterias",function()
	{
		$("#btnValidaTipoSolicitud").attr("disabled",false);

	});





	$("body").on("change","#slSemestresCursoRepeticion",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim()
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMateriasCursoRepeticion",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMateriasCursoRepeticion");

	});



	$("body").on("change","#slSemestresCursoEspecial",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMateriasCursoEspecial",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMateriasCursoEspecial");
		

	});


	$("body").on("change","#slSemestres2CursosEspecAsig1",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMaterias2CursosEspecAsig1",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMaterias2CursosEspecAsig1");
		

	});

	$("body").on("change","#slSemestres2CursosEspecAsig2",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMaterias2CursosEspecAsig2",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMaterias2CursosEspecAsig2");
		

	});


		$("body").on("change","#slSemestres2CursosEspecVerano1",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMaterias2CursosEspecVerano1",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMaterias2CursosEspecVerano1");
		

	});

	$("body").on("change","#slSemestres2CursosEspecVerano2",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMaterias2CursosEspecVerano2",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMaterias2CursosEspecVerano2");
		

	});


	$("body").on("change","#slSemestresCursoSobreCreditos",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMateriasCursoSobreCreditos",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMateriasCursoSobreCreditos");
		

	});


	$("body").on("change","#slSemestresCursoEspecialResidencia",function()
	{
		
		let no_de_control = $("#txtNumControl").val().trim(); 
		let id_semestre = $(this).val();
		let clave_oficial = $("#slCarreras").val();

		$("#formTipoSolicitudEstudiante").data("bootstrapValidator").resetField("slMateriasCursoEspecialResidencia",true);
		cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,"slMateriasCursoEspecialResidencia");
		

	});


	function cargarSelectMateriaSemestre(no_de_control,clave_oficial,id_semestre,tipoSelectMaterias)
	{

			$.ajax(
				{
		          type: "POST",
		          dataType:"json",
		          url: base_url+"Estudiantes/cargarSelectMateriaSemestre",
		          data: {
		          		no_de_control:no_de_control,
		          		id_semestre: id_semestre,
		          		clave_oficial:clave_oficial
		          },
		          async: true,
		          success: function(result)
			          {
			          	

			          	if(result.length > 0)
			          	{

			          		let options ="<option selected disabled >Elija la materia</option>";
	                   		 result.forEach(function(elemento,index) 
	                   		 {
	                   		     options += '<option value="'+elemento.materia+'">'+elemento.nombre_completo_materia+'</option>';
	                   		    
	                   		});


			          		$("#"+tipoSelectMaterias).html(options)

			          	}
			          	

			          },
				   error:function(result)
					  {
					  	console.log(result.responseText);
					  	//$("#error").html(data.responseText); 
					  }
		          
		        });

	}





});

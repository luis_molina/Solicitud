$(document).ready(function()
{

	var base_url = $("body").attr("data-base-url");

   validaRegistrarPersonal();

   $("#formRegistrarPersonal").on("submit",function(event)
	{
		event.preventDefault();

		$("#formRegistrarPersonal").bootstrapValidator();

	});


    function validaRegistrarPersonal()
	{
		

		$('#formRegistrarPersonal').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            txtNombre: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            txtApellidos: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            txtProfesion: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            slCargoPersonal: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            slDepartamento: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            slComite: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            txtFechaInicio: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	            ,
	            txtFechaFin: {
	                group: '.form-group',
	                validators: {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    },

	                }
	            }
	        }
	    }).on('success.form.bv', function (e) 
	    {

			e.preventDefault();

			let fecha_inicio = "";
			let vector1 = [];

			vector1 = $("#txtFechaInicio").val().split("-");
			fecha_inicio = vector1[0] + "-" +vector1[1] + "-" +vector1[2];

			let fecha_fin = "";
			let vector2 = [];

			vector2 = $("#txtFechaFin").val().split("-");
			fecha_fin = vector2[0] + "-" +vector2[1] + "-" +vector2[2];


				var datos = {
					nombre:$("#txtNombre").val().trim(),
					apellidos:$("#txtApellidos").val().trim(),
					profesion:$("#txtProfesion").val().trim(),
					id_cargo_personal:$("#slCargoPersonal").val(),
					id_departamento:$("#slDepartamento").val(),
					id_comite:$("#slComite").val(),
					fecha_inicio:fecha_inicio,
					fecha_fin:fecha_fin,
					correo:'',
					password:''
					
				}

				if($("#contDatosAdministrador div").length > 0)
				{
					datos.correo = $("#txtCorreo").val().trim();
					datos.password = $("#txtPassword").val().trim();
				}

				

				$.ajax(
				{
		          type: "POST",
		          dataType:"json",
		          url: base_url+"Personal/guardarPersonal",
		          data: datos,
		          async: true,
		          success: function(result)
			          {
						
						if( typeof(result.redirect) == 'undefined')
	                    {
	                    	
	                    	if(result.status == 'OK')
	                    	{
	                    		$('#modalAlerta .modal-body').text(result.mensaje);
	                    		$('#modalAlerta').modal('show');
	                    		
	                    	}
	                    	else
	                    	{
	                    		$('#modalAlerta .modal-body').text(result.mensaje);
	                    		$('#modalAlerta').modal('show');
	                    	}


	                    }
	                    else
	                    {
	                      location.href = result.url;
	                    }
			          	

			          },
				   error:function(result)
					  {
					  	console.log(result.responseText);
					  	//$("#error").html(data.responseText); 
					  }
		          
		        });

	    });


	}


	

	$('#modalAlerta').on('hide.bs.modal', function (e) 
    {
    	
    	location.reload();
        
    });


	function cargarSelectCargoPersonal()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectCargoPersonal",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_cargo_personal+'">'+elemento.cargo+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slCargoPersonal").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectCargoPersonal();


	function cargarSelectDepartamentos()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectDepartamentos",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_departamento+'">'+elemento.departamento+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slDepartamento").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	    });


	}
	cargarSelectDepartamentos();



	function cargarSelectComite()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectComite",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_comite+'">'+elemento.comite+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slComite").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	    });


	}
	cargarSelectComite();


	$("body").on("change",'#slComite',function() {
		

		let comite = $(this).val();

		let tempAdmin = ``;

		$("#contDatosAdministrador").html("");

		if(comite == '2')
		{

			tempAdmin = `<div class="col-xs-6">
	                        <div class="form-group">
	                            <label for="txtCorreo">Correo:</label>
	                            <input type="text" id="txtCorreo" name="txtCorreo"  class="form-control" placeholder="Correo" minlength="1" maxlength='100'>
	                        </div>
	                     </div>
	                     <div class="col-xs-6">
	                        <div class="form-group">
	                            <label for="txtPassword">Contraseña:</label>
	                            <input type="text" id="txtPassword" name="txtPassword"  class="form-control" placeholder="Contraseña" minlength="5" maxlength='20'>
	                        </div>
	                      </div>`;


	          $("#contDatosAdministrador").html(tempAdmin);


			$('#formRegistrarPersonal').bootstrapValidator('addField','txtCorreo',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
		                     regexp: {
	                            regexp: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,

	                            message: 'La dirección de correo no es válida',

	                        },
	                        callback: {
                                   message: 'La dirección de correo no esta disponible',
                                   callback: function(value, validator) {
                                       // Get the selected options

                                       var valida = true;

                                       var datos = {
                                                             correo:$("#txtCorreo").val().trim(),
                                                             
                                                           }


                                                   $.ajax(
                                                   {
                                                       type: "POST",
                                                       url: base_url+"Personal/checkCorreoPersonal",
                                                     dataType:"json",
                                                       data: datos,
                                                        async: false,
                                                       success: function(result)
                                                           {
                                                                 
                                                              if( typeof(result.redirect) == 'undefined')
                                                              {
                                                                  if(result.status == 'NO_DISPONIBLE')
                                                                  {
                                                                     valida = false;
                                                                  }
                                                                  else
                                                                  {
                                                                    valida = true;
                                                                  }
                                                              }
                                                              else
                                                              {
                                                                location.href = result.url;
                                                              }

                                                           },
                                                      error:function(result)
                                                         {
                                                           alert("Error");
                                                          console.log(result.responseText);
                                                           
                                                         }
                                                         
                                                   });

                                                   return valida;

                                   }
                               },

                        }
                    });

				$('#formRegistrarPersonal').bootstrapValidator('addField','txtPassword',{
                        group: '.form-group',
                        validators: {
                            notEmpty: {
                                message: 'Este campo es requerido'
                            },
		                    stringLength: {
		                        enabled: true,
		                        min: 5,
		                        max: 20,
		                        message: 'La contraseña debe contener como mínimo 5 caracteres y 20 como máximo'
		                    },

                        }
                    });
		}
		



	});



});
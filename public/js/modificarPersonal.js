$(document).ready(function()
{

	   var base_url = $("body").attr("data-base-url");

	   var tblPersonal = $('#tblPersonal').DataTable(
       {
          "processing": true,
          "serverSide": true,
          "ordering": true,
           "language": {
                          "url": base_url+"public/libreriasJS/Spanish.json"
                        },
                    "scrollY":        "500px",
                    "scrollX":        "100%",
                    "scrollCollapse": true,
          "ajax":{
            url :base_url+"Personal/cargarTablaPersonal", 
            type: "post",  
            error: function(d){ 
              $(".employee-grid-error").html("");
              $("#employee-grid").append('<tbody class="employee-grid-error"><tr><th colspan="3">No se encontraron datos</th></tr></tbody>');
              $("#employee-grid_processing").css("display","none");
              
            }
            ,
            // success:function(d)
            // {
            //   debugger;
            //  console.log(d);
            // }
          },
          "columnDefs": [
                        {
                            "targets": [ 0 ],
                            "visible": false,
                            "searchable": false
                        }
                    ],
           
       
       });


          validaModificarPersonal();

          $("#formModificarPersonal").on("submit",function(event)
       	{
       		event.preventDefault();

       		$("#formModificarPersonal").bootstrapValidator();

       	});


         function validaModificarPersonal()
       	{
       		

       		$('#formModificarPersonal').bootstrapValidator(
       		{

       	        message: 'This value is not valid',
       	        container: 'tooltip',
       	        feedbackIcons: {
       	            valid: 'glyphicon glyphicon-ok',
       	            invalid: 'glyphicon glyphicon-remove',
       	            validating: 'glyphicon glyphicon-refresh'
       	        },
       	        fields: {

       	            txtNombre: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            txtApellidos: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            txtProfesion: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            slCargoPersonal: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            slDepartamento: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            slComite: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            txtFechaInicio: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	            ,
       	            txtFechaFin: {
       	                group: '.form-group',
       	                validators: {
       	                    notEmpty: {
       	                        message: 'Este campo es requerido'
       	                    },

       	                }
       	            }
       	        }
       	    }).on('success.form.bv', function (e) 
       	    {

       			e.preventDefault();

       			let fecha_inicio = "";
       			let vector1 = [];

       			vector1 = $("#txtFechaInicio").val().split("-");
       			fecha_inicio = vector1[0] + "-" +vector1[1] + "-" +vector1[2];

       			let fecha_fin = "";
       			let vector2 = [];

       			vector2 = $("#txtFechaFin").val().split("-");
       			fecha_fin = vector2[0] + "-" +vector2[1] + "-" +vector2[2];


       				var datos = {
       					id_personal :$("#txtNombre").prop("id_personal"),
       					nombre:$("#txtNombre").val().trim(),
       					apellidos:$("#txtApellidos").val().trim(),
       					profesion:$("#txtProfesion").val().trim(),
       					id_cargo_personal:$("#slCargoPersonal").val(),
       					id_departamento:$("#slDepartamento").val(),
       					id_comite:$("#slComite").val(),
       					fecha_inicio:fecha_inicio,
       					fecha_fin:fecha_fin,
       					correo:'',
       					password:''
       					
       				}

       				if($("#contDatosAdministrador div").length > 0)
       				{
       					datos.correo = $("#txtCorreo").val().trim();
       					datos.password = $("#txtPassword").val().trim();
       				}

       				

       				$.ajax(
       				{
       		          type: "POST",
       		          dataType:"json",
       		          url: base_url+"Personal/modificarPersonal",
       		          data: datos,
       		          async: true,
       		          success: function(result)
       			          {
       						
             						if( typeof(result.redirect) == 'undefined')
                          {
                          	
                          	if(result.status == 'OK')
                          	{

                          		$('#modalMensajeModificarPersonal .modal-body').text(result.mensaje);
                          		$('#modalMensajeModificarPersonal').modal('show');
                          		
                          	}
                          	else
                          	{
                          		$('#modalMensajeModificarPersonal .modal-body').text(result.mensaje);
                          		$('#modalMensajeModificarPersonal').modal('show');
                          	}


                          }
                          else
                          {
                            location.href = result.url;
                          }
       			          	

       			          },
       				   error:function(result)
       					  {
       					  	console.log(result.responseText);
       					  	//$("#error").html(data.responseText); 
       					  }
       		          
       		        });

       	    });


       	}


	   $("body").on("click",".btnModificarPersonal",function()
	   {

	         let id_personal = tblPersonal.rows($(this).closest("tr").index()).data().pluck(0)[0];



	         $.ajax(
	                 {
	                       type: "POST",
	                       dataType:"json",
	                       url: base_url+"Personal/getInfoPersonal",
	                       // url: base_url+"EstudiantesAdmin/getInfoEstudiante",
	                       data: {id_personal:id_personal},
	                       async: true,
	                       success: function(result)
	                         {


	                            if( typeof(result.redirect) == 'undefined')
	                            {

	                                if(result.status == "OK")
	                                {
	                                     $("#txtNombre").val(result.personal[0].nombre);
	                                     $("#txtNombre").prop("id_personal",result.personal[0].id_personal);
	                                     $("#txtNombre").prop("correo",result.personal[0].correo);
	                                     $("#txtApellidos").val(result.personal[0].apellidos);
	                                     $("#txtProfesion").val(result.personal[0].profesion);
	                                     $("#slCargoPersonal").val(result.personal[0].id_cargo_personal).attr("selected", true);
	                                     $("#slDepartamento").val(result.personal[0].id_departamento).attr("selected", true);
	                                     $("#slComite").val(result.personal[0].id_comite).attr("selected", true);
	                                     $("#txtFechaInicio").val(result.personal[0].fecha_inicio);
	                                     $("#txtFechaFin").val(result.personal[0].fecha_fin);


	                                     $("#contDatosAdministrador").html("");

	                                     if( result.personal[0].id_comite == "2" )
	                                     {


	                                     			let tempAdmin = ``;


	                                     				tempAdmin = `<div class="col-xs-6">
	                                     		                        <div class="form-group">
	                                     		                            <label for="txtCorreo">Correo:</label>
	                                     		                            <input type="text" id="txtCorreo" name="txtCorreo"  class="form-control" placeholder="Correo" minlength="1" maxlength='100'>
	                                     		                        </div>
	                                     		                     </div>
	                                     		                     <div class="col-xs-6">
	                                     		                        <div class="form-group" style='display:none' >
	                                     		                            <label for="txtPassword">Contraseña:</label>
	                                     		                            <input type="password" id="txtPassword" name="txtPassword"  class="form-control" placeholder="Contraseña" minlength="5" maxlength='100'>
	                                     		                        </div>
	                                     		                      </div>`;


	                                     		    $("#contDatosAdministrador").html(tempAdmin);


                                     				$('#formModificarPersonal').bootstrapValidator('addField','txtCorreo',{
                                     	                        group: '.form-group',
                                     	                        validators: 
                                     	                        {
	                                     	                            notEmpty: {
	                                     	                                message: 'Este campo es requerido'
	                                     	                            },
	                                     			                     regexp: {
	                                     		                            regexp: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,

	                                     		                            message: 'La dirección de correo no es válida',

	                                     		                        },
	     		                                                          callback: {
									                                             message: 'La dirección de correo no esta disponible',
									                                             callback: function(value, validator) {
									                                                 // Get the selected options

									                                                 var valida = true;

									                                                 var datos = {
									                                                                       correo_new:$("#txtCorreo").val().trim(),
									                                                                        correo_origen:$("#txtNombre").prop("correo")
									                                                             }


									                                                             $.ajax(
									                                                             {
									                                                                 type: "POST",
									                                                                 url: base_url+"Personal/checkModificarCorreo",
									                                                                 // url: base_url+"Carreras/checkModificarClaveOficial",
									                                                               dataType:"json",
									                                                                 data: datos,
									                                                                  async: false,
									                                                                 success: function(result)
									                                                                     {
									                                                                          
									                                                                        if( typeof(result.redirect) == 'undefined')
									                                                                        {
									                                                                            if(result.resultado == 'NO_DISPONIBLE')
									                                                                            {
									                                                                               valida = false;
									                                                                            }
									                                                                            else
									                                                                            {
									                                                                              valida = true;
									                                                                            }
									                                                                        }
									                                                                        else
									                                                                        {
									                                                                          location.href = result.url;
									                                                                        }

									                                                                     },
									                                                                error:function(result)
									                                                                   {
									                                                                     alert("Error");
									                                                                    console.log(result.responseText);
									                                                                     
									                                                                   }
									                                                                   
									                                                             });

									                                                             return valida;

									                                             }
									                                         },

                                     	                        }
                         	                    	});

                                 					$('#formModificarPersonal').bootstrapValidator('addField','txtPassword',{
                                 	                        group: '.form-group',
                                 	                        validators: {
                                 	                            notEmpty: {
                                 	                                message: 'Este campo es requerido'
                                 	                            },
                                 			                    stringLength: {
                                 			                        enabled: true,
                                 			                        min: 5,
                                 			                        max: 100,
                                 			                        message: 'La contraseña debe contener como mínimo 5 caracteres'
                                 			                    },

                                 	                        }
                             	                    });
	                                     			
	                                     }

	                                     $("#txtCorreo").val(result.personal[0].correo);
	                                     $("#txtPassword").val(result.personal[0].password);

	                                     $("#modalModificarPersonal").modal("show");
	                                     

	                                     
	                                }
	                                else
	                                {
	                                     $('#modalAlerta .modal-body').text(result.mensaje);
	                                     $('#modalAlerta').modal('show');
	                                }
	                              
	                            }
	                            else
	                            {
	                              location.href = result.url;
	                            }
	                              
	                          

	                         },
	                    error:function(result)
	                     {
	                       console.log(result.responseText);
	                       //$("#error").html(data.responseText); 
	                     }
	                       
	              });



	   });
	


  	$("body").on("change",'#slComite',function() {
  		

  			let comite = $(this).val();

  			let tempAdmin = ``;

  			$("#contDatosAdministrador").html("");

  			if(comite == '2')
  			{

  				tempAdmin = `<div class="col-xs-6">
  		                        <div class="form-group">
  		                            <label for="txtCorreo">Correo:</label>
  		                            <input type="text" id="txtCorreo" name="txtCorreo"  class="form-control" placeholder="Correo" minlength="1" maxlength='100'>
  		                        </div>
  		                     </div>
  		                     <div class="col-xs-6">
  		                        <div class="form-group">
  		                            <label for="txtPassword">Contraseña:</label>
  		                            <input type="password" id="txtPassword" name="txtPassword"  class="form-control" placeholder="Contraseña" minlength="5" maxlength='100'>
  		                        </div>
  		                      </div>`;


  		          $("#contDatosAdministrador").html(tempAdmin);


  				   $('#formModificarPersonal').bootstrapValidator('addField','txtCorreo',{
  	                        group: '.form-group',
  	                        validators: {
  	                            notEmpty: {
  	                                message: 'Este campo es requerido'
  	                            },
  			                     regexp: {
  		                            regexp: /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/,

  		                            message: 'La dirección de correo no es válida',

  		                        },
  		                        callback: {
  	                                 message: 'La dirección de correo no esta disponible',
  	                                 callback: function(value, validator) {
  	                                     // Get the selected options

  	                                     var valida = true;

  	                                     var datos = {
  	                                                           correo_new:$("#txtCorreo").val().trim(),
  	                                                            correo_origen:$("#txtNombre").prop("correo")
  	                                                 }


  	                                                 $.ajax(
  	                                                 {
  	                                                     type: "POST",
  	                                                     url: base_url+"Personal/checkModificarCorreo",
  	                                                   dataType:"json",
  	                                                     data: datos,
  	                                                      async: false,
  	                                                     success: function(result)
  	                                                         {
  	                                                               
  	                                                               
  	                                                            if( typeof(result.redirect) == 'undefined')
  	                                                            {
  	                                                                if(result.resultado == 'NO_DISPONIBLE')
  	                                                                {
  	                                                                   valida = false;
  	                                                                }
  	                                                                else
  	                                                                {
  	                                                                  valida = true;
  	                                                                }
  	                                                            }
  	                                                            else
  	                                                            {
  	                                                              location.href = result.url;
  	                                                            }

  	                                                         },
  	                                                    error:function(result)
  	                                                       {
  	                                                         alert("Error");
  	                                                        console.log(result.responseText);
  	                                                         
  	                                                       }
  	                                                       
  	                                                 });

  	                                                 return valida;

  	                                 }
  	                             },

  	                        }
  	                    });

  					$('#formModificarPersonal').bootstrapValidator('addField','txtPassword',{
  	                        group: '.form-group',
  	                        validators: {
  	                            notEmpty: {
  	                                message: 'Este campo es requerido'
  	                            },
  			                    stringLength: {
  			                        enabled: true,
  			                        min: 5,
  			                        max: 100,
  			                        message: 'La contraseña debe contener como mínimo 5 caracteres'
  			                    },

  	                        }
  	                    });
  			}
  		

  			$("#btnModificarPersonal").attr("disabled",false);


  	});


   
    $("body").on("click",'.btnEliminarPersonal',function() 
    {

         let id_personal = tblPersonal.rows($(this).closest("tr").index()).data().pluck(0)[0];

         let personal = tblPersonal.rows($(this).closest("tr").index()).data().pluck(1)[0];



         $("#modalAlertaEliminarPersonal").prop("id_personal",id_personal);


         $("#modalAlertaEliminarPersonal .modal-body").html("¿Desea dar de baja al personal <strong>"+personal+" </strong> ?");

          $("#modalAlertaEliminarPersonal").modal("show");


    });



     $("body").on("click",'#btnMdlEliminarPersonal',function() 
    {

         let id_personal =  $("#modalAlertaEliminarPersonal").prop("id_personal");

             $.ajax(
             {
                     type: "POST",
                     url: base_url+"Personal/eliminarPersonal",
                     dataType:"json",
                     data: {id_personal:id_personal},
                     async: true,
                     success: function(result)
                     {
                           
                            if( typeof(result.redirect) == 'undefined')
                            {
                                if(result.status == 'OK')
                                {
                                   $("#modalMensajeEliminarPersonal .modal-body").text(result.mensaje);
                                   $("#modalMensajeEliminarPersonal").modal('show');
                                }
                                else
                                {
                                  $("#modalMensajeEliminarPersonal .modal-body").text(result.mensaje);
                                   $("#modalMensajeEliminarPersonal").modal('show');
                                }
                            }
                            else
                            {
                              location.href = result.url;
                            }

                     },
                     error:function(result)
                       {
                         alert("Error");
                        console.log(result.responseText);
                         
                       }
                       
            });


    });




	function cargarSelectCargoPersonal()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectCargoPersonal",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_cargo_personal+'">'+elemento.cargo+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slCargoPersonal").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectCargoPersonal();


	function cargarSelectDepartamentos()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectDepartamentos",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_departamento+'">'+elemento.departamento+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slDepartamento").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	    });


	}
	cargarSelectDepartamentos();



	function cargarSelectComite()
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Personal/cargarSelectComite",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija una opción</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_comite+'">'+elemento.comite+'</option>';
    	                      
    	                  });

    	                   
    	                   		$("#slComite").html(options);

    	                }
    	                else
    	                {
    	                	$("#modalAlerta .modal-body").html("No se encontraron datos disponibles");
    						$("#modalAlerta").modal("show");
    	                }
                    

	                
	              
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	    });


	}
	cargarSelectComite();



	 $('#modalModificarPersonal').on('hide.bs.modal', function (e) 
    	{

    	      $("#formModificarPersonal").bootstrapValidator('resetForm', true);
    	    
    	});


	  $('#modalMensajeModificarPersonal').on('hide.bs.modal', function (e) 
  	{

  		
  		location.reload();
  		
  	    
  	});


    $('#modalMensajeEliminarPersonal').on('hide.bs.modal', function (e) 
    {

        
        location.reload();
        
        
    });






});
$(document).ready(function()
{
	var base_url = $("body").attr("data-base-url");

   $("body").on("submit","#formChangePassword",function(event)
    {

         event.preventDefault();

         $("#formChangePassword").bootstrapValidator();

    });


validaFormChangePassword();


function validaFormChangePassword()
{

	$('#formChangePassword').bootstrapValidator(
          {

                message: 'This value is not valid',
                container: 'tooltip',
                feedbackIcons: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },
                fields: {
                    password1: {
		                group: '.input-group',
		                validators: {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    },
		                    stringLength: {
		                        enabled: true,
		                        min: 5,
		                        max: 100,
		                        message: 'El password debe contener como mínimo 5 caracteres'
		                    },

		                }
		            },
		            password2: {
		                group: '.input-group',
		                validators: {
		                    notEmpty: {
		                        message: 'Este campo es requerido'
		                    },
		                    stringLength: {
		                        enabled: true,
		                        min: 5,
		                        max: 100,
		                        message: 'El password debe contener como mínimo 5 caracteres'
		                    },

		                }
		            },
          
          }
          }).on('success.form.bv', function (e,data) 
          {

            e.preventDefault();

            let $password1 = $("#password1").val().trim();
            let $password2 = $("#password2").val().trim();

            if($password1 == $password2)
            {

            	let datos = { password: $password1,
            				  token: window.location.href.split("=")[1]
            				} 
               
                  $.ajax(
                  {

                      type: "POST",
                      url: base_url+"OlvidarPassword/actualizarPasswordUsuario",
                      dataType:"json",
                      data: datos,
                      async: true,
                      success: function(result)
                          {
                             //debugger;

                           	  //console.log(result);

  	                           if(result.msjConsulta=='OK')
  	                         	{
                                
                                $("#formChangePassword").prop("base_url",result.base_url);

                                  if(result.resultado == 'CADUCADO')
                                  {
                                    $("#modalAlertaCambioPasswordCaducado .modal-body p").text(result.msjUsuario);
                                    $("#modalAlertaCambioPasswordCaducado").modal("show");
                                  }
                                  else
                                  {
                                    $("#modalAlertaCambioPasswordSuccess .modal-body p").text(result.msjUsuario);
                                    $("#modalAlertaCambioPasswordSuccess").modal("show");

                                  }

  	                         	}
                              else
                              {
                                    $("#modalAlertaCambioPasswordSuccess .modal-body p").text(result.msjUsuario);
                                    $("#modalAlertaCambioPasswordSuccess").modal("show");
                              }

                          },
                     error:function(result)
                        {
                          alert("Error");
                         console.log(result.responseText);
                          
                        }
                        
                  });

            }
            else
            {

            	 $("#modalAlertaPasswordNocoinciden .modal-body p").text("Las contraseñas no coinciden");
            	 $("#modalAlertaPasswordNocoinciden").modal("show");
               //$("#formChangePassword").bootstrapValidator('resetForm', true);
            	 //$('#formChangePassword').bootstrapValidator('destroy', true);
                //validaFormChangePassword();
            }

          	


          });


}



 $('#modalAlertaCambioPasswordSuccess').on('hide.bs.modal', function (e) 
 {
     if( $("#formChangePassword").prop("base_url") != undefined)
     {
     	var base_url = $("#formChangePassword").prop("base_url");

     	window.location = base_url;

     }
     
 });

 $('#modalAlertaCambioPasswordCaducado').on('hide.bs.modal', function (e) 
 {
     if( $("#formChangePassword").prop("base_url") != undefined)
     {
       var base_url = $("#formChangePassword").prop("base_url");

       window.location = base_url;

     }
     
 });




});
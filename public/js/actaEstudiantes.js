$(document).on("ready",function()
{

	var base_url = $("body").attr("data-base-url");


	validaGenerarActa();

	$("#formGemerarActa").on("submit",function(event)
	{
		event.preventDefault();

		$("#formGemerarActa").bootstrapValidator();

	});

	function validaGenerarActa()
	{
		

		$('#formGemerarActa').bootstrapValidator(
		{

	        message: 'This value is not valid',
	        container: 'tooltip',
	        feedbackIcons: {
	            valid: 'glyphicon glyphicon-ok',
	            invalid: 'glyphicon glyphicon-remove',
	            validating: 'glyphicon glyphicon-refresh'
	        },
	        fields: {

	            slPeriodoEscolar: {
	                group: '.form-group',
	                validators: 
	                {
	                    notEmpty: {
	                        message: 'Este campo es requerido'
	                    }
	                     

	                }
	            },
	        }
	    }).on('success.form.bv', function (e) 
	    {

	    	e.preventDefault();


			let id_periodo_escolar = $("#slPeriodoEscolar").val();
			let periodo_escolar = $("#slPeriodoEscolar option:selected").text();


			$.ajax(
		    {
		      
			      type: "POST",
			      url: base_url+"ActaEstudiantes/verificarPeriodoEscolarReunion",
			      dataType:"json",
			      data: {id_periodo_escolar:id_periodo_escolar},
			      async: true,
				       success: function(result)
			           {
			           	

				           	if( typeof(result.redirect) == 'undefined')
	                    	{

				            	if(result.status == 'OK')
				            	{

				            			$("body #tableReunionesPeriodoEscolar tbody").html("");

				            			let x=1;
				            			result.solicitudes.forEach(function(solicitud) 
                          				{

                   		                   		tbodyActas = `<tr data-id_reunion='${solicitud.id_reunion}' >
	                   		                   							<td class='text-center no'>${x}</td>
		                												<td class='text-center '>${solicitud.nombre_reunion}</td>
		                												<td class='text-center '>${solicitud.periodo_escolar}</td>
		                												<td class='text-center '>${solicitud.no_libro}</td>
		                                       						    <td class='text-center'>${solicitud.no_acta}</td>
		                												<td>
		                													<p class='demo'>
			                													<a href='${base_url}ActaEstudiantes/generarActaEstudiantes?id_periodo_escolar=${solicitud.id_periodo_escolar}&id_reunion=${solicitud.id_reunion}' target='_blank' class='demo'>
			                														<input type='button' class="btn btn-primary btnVerFormatoActa"  value='Ver acta' />
			                													</a>
		                													</p> 
		                												</td>
						                							</tr>`;

	                      							$("body #tableReunionesPeriodoEscolar tbody").append(tbodyActas);
	                      							$("#lblPeriodoEscolar").text(solicitud.periodo_escolar);
	                      							x++;

						              });

				            			$("#modalTablaReunionesPeriodoEscolar").modal("show");

				            		 	// let url = `${base_url}ActaEstudiantes/generarActaEstudiantes?id_periodo_escolar=${id_periodo_escolar}`;

				            		  //   var a = document.createElement("a");
				            				// a.target = "_blank";
				            				// a.href = url;
				            				// a.click();


				            		$("#btnAceptarActa").prop("disabled","");

				            	}
				            	else if(result.status == 'NO_DATA')
				            	{

				            		$("#modalAlerta .modal-body").html("Se ha detectado que no hay ninguna reunión programada o no hay ninguna solicitud realizada por algún estudiante para el periodo escolar <strong>"+periodo_escolar+"</strong>");
				            		$("#modalAlerta").modal("show");

				            	}
				            	else
				            	{
				            		$("#modalAlerta .modal-body").text(result.mensaje);
				            		$("#modalAlerta").modal("show");
				            	}

				            }
		                    else
		                    {
		                      location.href = result.url;
		                    }
		    	              
			              
			           },
			          error:function(result)
			          {
			            alert("Error");
			           console.log(result.responseText);
			            
			          }
		   });



	    });


	}



	function cargarSelectPeriodosEscolares(solicitud)
	{

		$.ajax(
	    {
	      
	      type: "POST",
	      url: base_url+"Estudiantes/cargarSelectPeriodosEscolares",
	      dataType:"json",
	      data: '',
	      async: true,
	        success: function(result)
	            {

	            	if( typeof(result.redirect) == 'undefined')
	                {
    	                if(result.length > 0)
    	                {
    	                   let options ="<option selected disabled >Elija un periodo escolar</option>";
    	                   result.forEach(function(elemento,index) 
    	                   {
    	  
    	                       options += '<option value="'+elemento.id_periodo_escolar+'">'+elemento.identificacion_larga+'</option>';
    	                      
    	                  });

    	                   if(solicitud=='sn')
    	                   {
    	                   		$("#slPeriodoEscolar").html(options);
    	                   }

    	                }
    	            }
	                else
	                {
	                  location.href = result.url;
	                }
	            },
	       error:function(result)
	          {
	            alert("Error");
	           console.log(result.responseText);
	            
	          }
	   });


	}
	cargarSelectPeriodosEscolares('sn');



	



});
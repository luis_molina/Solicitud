-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-03-2018 a las 17:46:19
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `solicitud_alumno`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acapite`
--

CREATE TABLE IF NOT EXISTS `acapite` (
`id_acapite` int(11) NOT NULL,
  `descripcion` text,
  `id_subcapitulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumnos`
--

CREATE TABLE IF NOT EXISTS `alumnos` (
`id_alumno` bigint(20) unsigned NOT NULL,
  `no_de_control` varchar(100) NOT NULL,
  `id_semestre` int(11) NOT NULL,
  `apellido_paterno` varchar(100) NOT NULL,
  `apellido_materno` varchar(100) NOT NULL,
  `nombre_alumno` varchar(100) NOT NULL,
  `curp_alumno` varchar(50) DEFAULT NULL,
  `creditos_aprobados` int(11) NOT NULL,
  `creditos_cursados` int(11) NOT NULL,
  `clave_oficial` varchar(200) NOT NULL,
  `estado` char(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `alumnos`
--

INSERT INTO `alumnos` (`id_alumno`, `no_de_control`, `id_semestre`, `apellido_paterno`, `apellido_materno`, `nombre_alumno`, `curp_alumno`, `creditos_aprobados`, `creditos_cursados`, `clave_oficial`, `estado`) VALUES
(1, '1020304050', 1, 'Moreno', 'Torres', 'Felipe', 'MTF5325423', 0, 0, 'ITIC-2010-225', '1'),
(2, '6049284672', 1, 'Garcia', 'Huerta', 'Alberto', 'GHA3450962', 0, 0, 'ITIC-2010-225', '1'),
(4, '12345abcde', 1, 'Sanchez', 'García', 'Carlos', 'CSG432443FEIM', 0, 0, 'IGEM-2009-201', '1'),
(5, 'WET123456OUT', 1, 'Perez', 'Mora', 'Alejandro', 'EWHIUREHIFERH', 0, 0, 'IGEM-2009-201', '1'),
(6, 'EYH7472INE64', 1, 'Orozco', 'Rodriguez', 'Jorge', 'USSF2374HDWE', 0, 0, 'IMEC-2010-228', '1'),
(7, '90909', 7, 'Perez', 'Perez', 'Juan', 'asdasd', 0, 0, 'IELC-2010-211', '1'),
(8, 'HEW863YHFE', 1, 'Gutierrez', 'Mora', 'Daniel Alberto', '', 0, 0, 'IGEM-2009-201', '1'),
(9, '5684567245234', 1, 'Morales', 'Rosas', 'Karina', '', 0, 0, 'IQUI-2010-232', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `capitulos`
--

CREATE TABLE IF NOT EXISTS `capitulos` (
`id_capitulo` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE IF NOT EXISTS `carreras` (
  `clave_oficial` varchar(200) NOT NULL,
  `carrera` varchar(200) NOT NULL,
  `nombre_carrera` varchar(200) NOT NULL,
  `nombre_reducido` varchar(100) NOT NULL,
  `carga_maxima` int(11) NOT NULL,
  `carga_minima` int(11) NOT NULL,
  `creditos_totales` int(11) NOT NULL,
  `fecha_ingreso` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`clave_oficial`, `carrera`, `nombre_carrera`, `nombre_reducido`, `carga_maxima`, `carga_minima`, `creditos_totales`, `fecha_ingreso`) VALUES
('IACU-2010-212', 'IAC', 'Ingenieria en Acuicultura', 'Ing. en Acuicultura', 200, 30, 200, '2017-11-16 22:11:39'),
('IELC-2010-211', 'IEL', 'Ingeniería en Electrónica', 'Ing. Electronica', 202, 10, 202, '2017-11-12 20:24:37'),
('IGEM-2009-201', 'IGE', 'Ingenieria en Gestion Empresarial', 'Ing. en Gestion Empresarial', 0, 0, 0, '2017-11-29 00:00:01'),
('IMEC-2010-228', 'IME', 'Ingenieria Mecanica', 'Ing. Mecanica', 210, 44, 210, '2017-11-12 00:00:01'),
('IQUI-2010-232', 'IQU', 'Ingenieria Quimica', 'Ing. Quimica', 0, 0, 0, '2017-11-29 00:00:02'),
('ITIC-2010-225', 'ITI', 'Ingenieria en Tecnologia de la Informacion y Comunicación', 'Ing. en TIC''s', 0, 0, 0, '2017-11-29 00:00:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_cargo_personal`
--

CREATE TABLE IF NOT EXISTS `cat_cargo_personal` (
`id_cargo_personal` int(10) unsigned NOT NULL,
  `cargo` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `cat_cargo_personal`
--

INSERT INTO `cat_cargo_personal` (`id_cargo_personal`, `cargo`) VALUES
(1, 'Jefe'),
(2, 'Subdirector'),
(3, 'Director');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_comite`
--

CREATE TABLE IF NOT EXISTS `cat_comite` (
`id_comite` int(10) unsigned NOT NULL,
  `comite` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cat_comite`
--

INSERT INTO `cat_comite` (`id_comite`, `comite`) VALUES
(1, 'Presidente'),
(2, 'Secretario'),
(3, 'Vocal'),
(4, 'Sin comité');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_departamentos`
--

CREATE TABLE IF NOT EXISTS `cat_departamentos` (
`id_departamento` int(10) unsigned NOT NULL,
  `departamento` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `cat_departamentos`
--

INSERT INTO `cat_departamentos` (`id_departamento`, `departamento`) VALUES
(1, 'Subdirección'),
(2, 'Departamento de estudios'),
(3, 'Departamento de electrónica'),
(4, 'Departamento de mecánica'),
(5, 'Departamento de administración'),
(6, 'Departamento de escolar'),
(7, 'Departamento de ciencias'),
(8, 'Departamento de academicos'),
(9, 'Departamento de básicas'),
(10, 'Departamento de materiales'),
(11, 'Dirección');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_evaluacion_materias`
--

CREATE TABLE IF NOT EXISTS `cat_evaluacion_materias` (
  `aprobada` char(1) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_evaluacion_materias`
--

INSERT INTO `cat_evaluacion_materias` (`aprobada`, `descripcion`) VALUES
('0', 'Reprobada'),
('1', 'Aprobada');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_status_materia`
--

CREATE TABLE IF NOT EXISTS `cat_status_materia` (
  `status` char(1) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_status_materia`
--

INSERT INTO `cat_status_materia` (`status`, `descripcion`) VALUES
('1', 'Aceptado'),
('2', 'Pendiente'),
('3', 'Rechazado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_tipo_materia`
--

CREATE TABLE IF NOT EXISTS `cat_tipo_materia` (
  `tipo_materia` char(1) NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cat_tipo_materia`
--

INSERT INTO `cat_tipo_materia` (`tipo_materia`, `descripcion`) VALUES
('1', 'Ordinaria'),
('2', 'Regularizacion'),
('3', 'Especial');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_tipo_solicitud`
--

CREATE TABLE IF NOT EXISTS `cat_tipo_solicitud` (
`id_tipo_solicitud` int(10) unsigned NOT NULL,
  `tipo_solicitud` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `cat_tipo_solicitud`
--

INSERT INTO `cat_tipo_solicitud` (`id_tipo_solicitud`, `tipo_solicitud`) VALUES
(1, 'Curso de repetición'),
(2, 'Curso especial'),
(3, '2 Cursos especiales con otras asignaturas'),
(4, '2 Cursos especiales en un curso de verano'),
(5, 'Adelantar asignaturas (sobre créditos)'),
(6, 'Residencia Profesional con una asignatura en curso especial'),
(7, 'Baja Definitiva con posibilidad de inscribirse de nuevo a la Institución');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `kardex`
--

CREATE TABLE IF NOT EXISTS `kardex` (
`id_kardex` bigint(20) unsigned NOT NULL,
  `id_alumno` bigint(20) unsigned NOT NULL,
  `no_de_control` varchar(100) NOT NULL,
  `clave_oficial` varchar(200) NOT NULL,
  `id_semestre` int(11) NOT NULL,
  `id_periodo_escolar` int(11) NOT NULL,
  `creditos_aprobados` int(11) DEFAULT NULL,
  `creditos_cursados` int(11) DEFAULT NULL,
  `grupo` varchar(45) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=69 ;

--
-- Volcado de datos para la tabla `kardex`
--

INSERT INTO `kardex` (`id_kardex`, `id_alumno`, `no_de_control`, `clave_oficial`, `id_semestre`, `id_periodo_escolar`, `creditos_aprobados`, `creditos_cursados`, `grupo`, `fecha`) VALUES
(1, 1, '1020304050', 'ITIC-2010-225', 1, 4, NULL, NULL, 'A1', '2017-12-03 00:00:00'),
(2, 2, '6049284672', 'ITIC-2010-225', 1, 4, NULL, NULL, 'A1', '2017-12-04 00:00:00'),
(12, 4, '12345abcde', 'IGEM-2009-201', 1, 4, NULL, NULL, 'A1', '2018-01-07 12:46:03'),
(13, 1, '1020304050', 'ITIC-2010-225', 2, 5, NULL, NULL, 'ax', '2018-01-18 10:23:37'),
(14, 2, '6049284672', 'ITIC-2010-225', 2, 5, NULL, NULL, 'B1', '2018-01-18 10:50:28'),
(15, 4, '12345abcde', 'IGEM-2009-201', 2, 5, NULL, NULL, 'C1', '2018-01-18 10:51:14'),
(19, 1, '1020304050', 'ITIC-2010-225', 3, 6, NULL, NULL, 'C1', '2018-01-18 11:48:03'),
(20, 4, '12345abcde', 'IGEM-2009-201', 3, 6, NULL, NULL, 'ssda', '2018-01-18 16:16:37'),
(21, 2, '6049284672', 'ITIC-2010-225', 3, 6, NULL, NULL, 'CX', '2018-01-18 16:32:15'),
(25, 1, '1020304050', 'ITIC-2010-225', 4, 7, NULL, NULL, 'XA', '2018-01-20 18:27:22'),
(26, 2, '6049284672', 'ITIC-2010-225', 4, 7, NULL, NULL, 'CF', '2018-01-20 18:38:06'),
(27, 4, '12345abcde', 'IGEM-2009-201', 4, 7, NULL, NULL, 'SD', '2018-01-20 18:41:38'),
(28, 1, '1020304050', 'ITIC-2010-225', 5, 8, NULL, NULL, 'ED', '2018-01-20 18:55:55'),
(29, 2, '6049284672', 'ITIC-2010-225', 5, 8, NULL, NULL, 'WT', '2018-01-20 19:07:59'),
(30, 4, '12345abcde', 'IGEM-2009-201', 5, 8, NULL, NULL, 'QS', '2018-01-20 19:13:58'),
(31, 5, 'WET123456OUT', 'IGEM-2009-201', 1, 4, NULL, NULL, 'CE', '2018-01-21 02:24:03'),
(32, 5, 'WET123456OUT', 'IGEM-2009-201', 2, 5, NULL, NULL, 'CY', '2018-01-21 02:25:39'),
(33, 5, 'WET123456OUT', 'IGEM-2009-201', 3, 6, NULL, NULL, 'EN', '2018-01-21 02:26:38'),
(34, 6, 'EYH7472INE64', 'IMEC-2010-228', 1, 4, NULL, NULL, 'Z1', '2018-01-24 12:19:47'),
(35, 7, '90909', 'IELC-2010-211', 1, 4, NULL, NULL, 'F', '2018-01-28 20:27:51'),
(36, 8, 'HEW863YHFE', 'IGEM-2009-201', 1, 4, NULL, NULL, 'X1', '2018-01-29 13:36:31'),
(37, 5, 'WET123456OUT', 'IGEM-2009-201', 4, 7, NULL, NULL, 'CA', '2018-01-30 17:48:14'),
(38, 5, 'WET123456OUT', 'IGEM-2009-201', 5, 8, NULL, NULL, 'AX', '2018-01-30 18:01:14'),
(39, 9, '5684567245234', 'IQUI-2010-232', 1, 4, NULL, NULL, 'CE', '2018-02-01 19:08:07'),
(40, 9, '5684567245234', 'IQUI-2010-232', 2, 5, NULL, NULL, 'CQ', '2018-02-01 23:31:18'),
(41, 9, '5684567245234', 'IQUI-2010-232', 3, 6, NULL, NULL, 'EW', '2018-02-01 23:38:43'),
(42, 9, '5684567245234', 'IQUI-2010-232', 4, 7, NULL, NULL, 'EW', '2018-02-01 23:50:35'),
(43, 7, '90909', 'IELC-2010-211', 2, 5, NULL, NULL, 'XE', '2018-02-02 20:37:10'),
(44, 7, '90909', 'IELC-2010-211', 3, 6, NULL, NULL, 'WD', '2018-02-03 10:36:46'),
(60, 7, '90909', 'IELC-2010-211', 4, 7, NULL, NULL, 'EW', '2018-02-03 13:29:05'),
(61, 7, '90909', 'IELC-2010-211', 5, 8, NULL, NULL, 'CR', '2018-02-03 13:39:16'),
(62, 6, 'EYH7472INE64', 'IMEC-2010-228', 2, 5, NULL, NULL, 'RE', '2018-02-03 13:47:59'),
(63, 6, 'EYH7472INE64', 'IMEC-2010-228', 3, 6, NULL, NULL, 'RC', '2018-02-03 13:50:46'),
(64, 6, 'EYH7472INE64', 'IMEC-2010-228', 4, 7, NULL, NULL, 'D1', '2018-02-03 16:44:48'),
(65, 6, 'EYH7472INE64', 'IMEC-2010-228', 5, 8, NULL, NULL, 'IM1', '2018-02-03 17:15:21'),
(66, 8, 'HEW863YHFE', 'IGEM-2009-201', 2, 5, NULL, NULL, 'CX', '2018-02-13 17:40:43'),
(67, 8, 'HEW863YHFE', 'IGEM-2009-201', 3, 6, NULL, NULL, 'CS', '2018-02-13 17:42:54'),
(68, 7, '90909', 'IELC-2010-211', 6, 9, NULL, NULL, 'Cx', '2018-02-21 11:49:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `materias`
--

CREATE TABLE IF NOT EXISTS `materias` (
  `materia` varchar(100) NOT NULL,
  `nombre_completo_materia` varchar(200) NOT NULL,
  `nombre_abreviado_materia` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `materias`
--

INSERT INTO `materias` (`materia`, `nombre_completo_materia`, `nombre_abreviado_materia`) VALUES
('ACA0907', 'Taller de Etica', 'Ta.Et'),
('ACA0909', 'Taller de Investigacion I', NULL),
('ACA0910', 'Taller de Investigacion II', NULL),
('ACC0906', 'Fundamentos de Investigacion', NULL),
('ACD0908', 'Desarrollo Sustentable', NULL),
('ACF0901', 'Calculo Diferencial ', NULL),
('ACF0902', 'Calculo Integral', NULL),
('ACF0903', 'Algebra Lineal', NULL),
('ACF0904', 'Calculo Vectorial', NULL),
('ACF0905', 'Ecuaciones Diferenciales', NULL),
('AEB1011', 'Desarrollo de Aplicaciones para Dispositivos Moviles', NULL),
('AEB1045', 'Mercadotecnia Electronica', NULL),
('AEB1054', 'Programacion Orientada a Objetos', NULL),
('AEB1055', 'Programacion Web', NULL),
('AEB1082', 'Software de Aplicaci¢n Ejecutivo', NULL),
('AEC1014', 'Dinamica Social', NULL),
('AEC1046', 'Metodos Numericos', NULL),
('AEC1058', 'Quimica', NULL),
('AEC1061', 'Sistema Operativo I', NULL),
('AEC1078', 'Marco Legal de las Organizaciones', NULL),
('AED1015', 'Dise¤o Organizacional', NULL),
('AED1035', 'Gestion Estrategica', NULL),
('AED1043', 'Mecanismos', NULL),
('AED1062', 'Sistema Operativo II', NULL),
('AED1067', 'Vibraciones Mecanicas', NULL),
('AED1069', 'Calidad Aplicada a la Gestion Empresarial', NULL),
('AED1072', 'El Emprendedor y la Innovacion', NULL),
('AEE1051', 'Probabilidad y Estadistica', NULL),
('AEF1003', 'Analisis Instrumental\r', NULL),
('AEF1004', 'Balance de Materia y Energia\r', NULL),
('AEF1010', 'Control II', NULL),
('AEF1020', 'Electromagnetismo', NULL),
('AEF1031', 'Fundamentos de Base de Datos', NULL),
('AEF1032', 'Fundamentos de Programacion', NULL),
('AEF1038', 'Instumentacion', NULL),
('AEF1039', 'Instrumentacion y Control\r', NULL),
('AEF1040', 'Maquinas Electricas', NULL),
('AEF1042', 'Mecanica Clasica', NULL),
('AEF1052', 'Probabilidad y Estadisticas', NULL),
('AEF1060', 'Quimica Inorganica\r', NULL),
('AEF1065', 'Termodinamica\r', NULL),
('AEF1071', 'Economia Empresarial', NULL),
('AEF1073', 'Finanzas en las Organizaciones', NULL),
('AEF1074', 'Fundamentos de Gestion Empresarial', NULL),
('AEF1076', 'Investigacion de Operadores', NULL),
('AEG1059', 'Quimica Analitica\r', NULL),
('AEG1075', 'Gestion del Capital Humano', NULL),
('AEH1063', 'Taller de Base de Datos', NULL),
('AEO1012', 'Dibujo Asistido por Computadora\r', NULL),
('AFC0901', 'Calculo Diferencial', NULL),
('AQA1008', 'Dibujo Asistido por Computadora', NULL),
('AQB1015', 'Formulacion y Evaluacion de Proyectos de Inversion', NULL),
('AQC1002', 'Administracion de Costos', NULL),
('AQC1026', 'Oceanografia', NULL),
('AQC1027', 'Probabilidad y Estadistica', NULL),
('AQC1033', 'Sistema de Informacion Geografica', NULL),
('AQC1034', 'Tecnologia de Pesca Aplicada a la Acuicultura', NULL),
('AQD1004', 'Bioquimica', NULL),
('AQD1005', 'Cultivos de Apoyo', NULL),
('AQD1006', 'Desarrollo de Emprendedores', NULL),
('AQD1009', 'Dise¤o de Sistemas Acuicolas I', NULL),
('AQD1010', 'Dise¤o de Sistemas Acuicolas II', NULL),
('AQD1011', 'Ecologia Acuatica', NULL),
('AQD1013', 'Fisica General', NULL),
('AQD1014', 'Fisiologia de Organismos Acuaticos', NULL),
('AQD1019', 'Limnologia', NULL),
('AQD1020', 'Manejo de Conservacion de Productos Acuicolas', NULL),
('AQD1021', 'Manejo de Conservacion del Agua', NULL),
('AQD1022', 'Mecanica de Fluidos', NULL),
('AQD1023', 'Mecanica de Suelos', NULL),
('AQD10231', 'Quimica Organica', NULL),
('AQD1025', 'Nutricion Acuicola', NULL),
('AQD1028', 'Proceso Administrativo', NULL),
('AQD1029', 'Procesos Litorales', NULL),
('AQD1030', 'Quimica Inorganica', NULL),
('AQD1032', 'Sanidad Acuicola', NULL),
('AQF1003', 'Biologia Acuatica', NULL),
('AQF1012', 'Estadistica Aplicada', NULL),
('AQF1016', 'Genetica Aplicada a la Acuicultura', NULL),
('AQF1017', 'Introduccion a la Acuicultura', NULL),
('AQG1024', 'Microbiologia General', NULL),
('AQH1001', 'Acuariofilia', NULL),
('AQH1035', 'Topografia', NULL),
('AQO1007', 'Desarrollo Humano', NULL),
('AQP1018', 'Legislacion Acuicola', NULL),
('ETD1021', 'Mediciones Electricas', NULL),
('ETD1022', 'Microcontroladores', NULL),
('ETD1024', 'Programacion Estructurada', NULL),
('ETD1025', 'Programacion Visual', NULL),
('ETF1001', 'Administracion Gerencial', NULL),
('ETF1002', 'Amplificadores Operacionales', NULL),
('ETF1003', 'Analisis Numerico', NULL),
('ETF1004', 'Circuitos Electricos I', NULL),
('ETF1005', 'Circuitos Electricos II', NULL),
('ETF1007', 'Control Digital', NULL),
('ETF1008', 'Controladores Logicos Programables', NULL),
('ETF1009', 'Control I', NULL),
('ETF1012', 'Diodos y Transistores', NULL),
('ETF1013', 'Dise¤o de Transistores', NULL),
('ETF1014', 'Dise¤o Digital', NULL),
('ETF1015', 'Dise¤o Digital en VHDL', NULL),
('ETF1016', 'Electronica de Potencia', NULL),
('ETF1017', 'Fisica de Semiconductores', NULL),
('ETF1019', 'Introduccion a las Teleccomunicaciones', NULL),
('ETF1023', 'Optoelectronica', NULL),
('ETF1026', 'Teoria Electromagnetica', NULL),
('ETF1027', 'Topicos Selectos de Fisica', NULL),
('ETO1010', 'Desarrollo Profesional', NULL),
('ETP1018', 'Fundamentos Financieros', NULL),
('ETP1020', 'Marco Legal de la Empresa', NULL),
('ETQ1006', 'Comunicaci¢n Humana', NULL),
('ETQ1009', 'Desarrollo Humano', NULL),
('GEC0905', 'Desarrollo Humano', NULL),
('GEC0909', 'Fundamentos de Fisica', NULL),
('GEC0910', 'Fundamentos de Quimica', NULL),
('GEC0911', 'Gestion de la Produccion I', NULL),
('GEC0912', 'Gestion de la Produccion II', NULL),
('GEC0913', 'Habilidades Directivas I', NULL),
('GEC0914', 'Habilidad Directivas II', NULL),
('GED0903', 'Contabilidad Orientada a los Negocios', NULL),
('GED0904', 'Costos Empresariales', NULL),
('GED0917', 'Instrumentos de Presupuestacion Empresarial', NULL),
('GED0920', 'Plan de Negocios', NULL),
('GED0921', 'Probabilidad y Estadistica Descriptiva', NULL),
('GED0922', 'Sistemas de Informacion de Mercadotecnia', NULL),
('GEE0918', 'Legislacion Laboral', NULL),
('GEF0901', 'Administracion de la Salud y Seguridad Ocupacional', NULL),
('GEF0902', 'Cadena de Suministros', NULL),
('GEF0906', 'Entorno Macroeconomico', NULL),
('GEF0915', 'Ingenieria de Procesos', NULL),
('GEF0916', 'Ingenieria Economica', NULL),
('GEF0919', 'Mercadotecnia', NULL),
('GEG0907', 'Estadistica Inferencial I', NULL),
('GEG0908', 'Estadisticas Inferencial II', NULL),
('IQC1008', 'Ingenieria de Costos\r', NULL),
('IQC1018', 'Programacion\r', NULL),
('IQD1023', 'Simulacion de Procesos\r', NULL),
('IQF1001', 'Analisis de Datos Experimentales\r', NULL),
('IQF1003', '"Electricidad', NULL),
('IQF1004', 'Fisicoquimica I\r', NULL),
('IQF1005', 'Fisicoquimica II\r', NULL),
('IQF1006', 'Gestion de la Calidad\r', NULL),
('IQF1007', 'Ingenieria Ambiental\r', NULL),
('IQF1013', 'Mecanismos de Transferencia\r', NULL),
('IQF1014', '"Balance de Momento', NULL),
('IQF1015', 'Procesos de Separacion I\r', NULL),
('IQF1016', 'Procesos de Separacion II\r', NULL),
('IQF1017', 'Procesos de Separacion III\r', NULL),
('IQF1019', 'Quimica Organica I\r', NULL),
('IQF1020', 'Quimica Organica II\r', NULL),
('IQF1021', 'Reactores Quimicos\r', NULL),
('IQF1022', 'Salud y Seguridad en el Trabajo\r', NULL),
('IQF1024', 'Sintesis y Optimizacion de Procesos\r', NULL),
('IQH1014', 'Metodos Numericos\r', NULL),
('IQM1009', 'Ingenieria de Proyectos\r', NULL),
('IQN1010', 'Laboratorio Integral I\r', NULL),
('IQN1011', 'Laboratorio Integral II\r', NULL),
('IQN1012', 'Laboratorio Integral III\r', NULL),
('IQO1025', 'Taller de Administracion Gerencial\r', NULL),
('MEA1001', 'Algoritmos y Programacion', NULL),
('MEC1011', 'Gestion de Proyectos', NULL),
('MEC1016', 'Mantenimiento', NULL),
('MEC1019', 'Mecanica de Fluidos', NULL),
('MEC1023', 'Probabilidad y Estadistica', NULL),
('MEC1026', 'Quimica', NULL),
('MED1004', 'Circuitos y Maquinas Electricas', NULL),
('MED1007', 'Dinamica', NULL),
('MED1008', 'Dise¤o Mecanico I', NULL),
('MED1009', 'Dise¤o Mecanico II', NULL),
('MED1010', 'Estatica', NULL),
('MED1020', 'Mecanica de Materiales I', NULL),
('MED1021', 'Mecanicas de Materiales II', NULL),
('MED1025', 'Procesos de Manufactura', NULL),
('MED1027', 'Refrigeracion y Aire Acondicionado', NULL),
('MED1029', 'Sistemas e Instalaciones Hidraulicas', NULL),
('MED1050', 'Sistemas Electronicos', NULL),
('MEE1017', 'Maquinas de Fluidos Comprensibles', NULL),
('MEF1002', 'Automatizacion Industrial', NULL),
('MEF1003', 'Calidad', NULL),
('MEF1013', 'Ingenieria de Materiales Metalicos', NULL),
('MEF1014', 'Ingenieria de Materiales No Metalicos', NULL),
('MEF1015', 'Instrumentacion y Control', NULL),
('MEF1018', 'Maquinas de Fluidos Incomprensibles', NULL),
('MEF1031', 'Termodinamica', NULL),
('MEF1032', 'Transferencia de Calor', NULL),
('MEH1022', 'Metrologia y Normalizacion', NULL),
('MEL1028', 'Sistemas de Generacion de Energia', NULL),
('MER1005', 'Contabilidad y Costos', NULL),
('MER1012', 'Higiene y Seguridad Industrial', NULL),
('MER1024', 'Proceso Administrativo', NULL),
('MEV1006', 'Dibujo Mecanico', NULL),
('TIB1024', 'Programacion II', NULL),
('TIC1002', 'Administracion Gerencial', NULL),
('TIC1005', 'Arquitectura de Computadoras', NULL),
('TIC1011', 'Electricidad y Magnetismo', NULL),
('TIC1014', 'Ingenieria de Software', NULL),
('TIC1015', 'Ingenieria del Conocimiento', NULL),
('TIC1022', 'Negocio Electronico II', NULL),
('TIC1023', 'Negocios Electronicos II', NULL),
('TIC1027', 'Taller de Ingenieria de Software', NULL),
('TIC1028', 'Tecnologia Inalambricas', NULL),
('TID1004', 'Analisis de Se¤ales y Sistemas de Comunicaci¢n ', NULL),
('TID1008', 'Circuitos Electricos y Electronicos', NULL),
('TID1010', 'Desarrollo de Empredendores', NULL),
('TID1012', 'Estructura y Organizaci¢n de Datos', NULL),
('TIE1018', 'Matematicas Aplicadas a Comunicaciones', NULL),
('TIF1001', 'Administracion de Proyectos', NULL),
('TIF1003', 'Administracion y Seguridad de Redes', NULL),
('TIF1006', 'Auditoria en Tecnologia de la Informacion', NULL),
('TIF1007', 'Base de Datos Distribuidas', NULL),
('TIF1009', 'Contabilidad y Costos', NULL),
('TIF1013', 'Fundamentos de Redes', NULL),
('TIF1019', 'Matematicas Discretas I', NULL),
('TIF1020', 'Matematicas Discretas II', NULL),
('TIF1021', 'Matematicas para la Tomas de Decisiones', NULL),
('TIF1025', 'Redes de Computadora', NULL),
('TIF1026', 'Redes Emergentes', NULL),
('TIF1029', 'Telecomunicaciones', NULL),
('TIH1016', 'Interaccion Humano Computadora.', NULL),
('TIP1017', 'Introduccion a las TIC''s', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `periodos_escolares`
--

CREATE TABLE IF NOT EXISTS `periodos_escolares` (
`id_periodo_escolar` int(11) NOT NULL,
  `periodo` varchar(100) DEFAULT NULL,
  `identificacion_larga` varchar(200) DEFAULT NULL,
  `identificacion_corta` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Volcado de datos para la tabla `periodos_escolares`
--

INSERT INTO `periodos_escolares` (`id_periodo_escolar`, `periodo`, `identificacion_larga`, `identificacion_corta`) VALUES
(3, '20172', 'Verano/2017', NULL),
(4, '20173', 'Agosto_Diciembre/2017', 'Ago-Dic/2017'),
(5, '20181', 'Enero-Junio/2018', 'Ene-Jun/2018'),
(6, '20182', 'Verano/2018', NULL),
(7, '20183', 'Agosto_Diciembre/2018', 'Ago-Dic/2018'),
(8, '20191', 'Enero-Junio/2019', 'Ene-Jun/2019'),
(9, '20192', 'Verano/2019', NULL),
(10, '20193', 'Agosto_Diciembre/2019', 'Ago-Dic/2019'),
(12, '20171', 'Enero-Junio/2017', 'Ene-Jun/2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE IF NOT EXISTS `personal` (
`id_personal` int(10) unsigned NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `profesion` varchar(100) DEFAULT NULL,
  `id_cargo_personal` int(10) unsigned NOT NULL,
  `id_departamento` int(10) unsigned NOT NULL,
  `id_comite` int(10) unsigned NOT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_fin` date DEFAULT NULL,
  `estado` char(1) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`id_personal`, `nombre`, `apellidos`, `profesion`, `id_cargo_personal`, `id_departamento`, `id_comite`, `fecha_inicio`, `fecha_fin`, `estado`) VALUES
(10, 'Victor Hugo', 'Vargas Contreras', 'Ing', 1, 2, 2, '2018-01-29', '2018-06-30', '1'),
(12, 'Mario', 'Enriquez Nicolás', 'M.T.I', 2, 1, 1, '2018-02-04', '2018-12-17', '1'),
(13, 'Lino Rafael', 'Gil Rodriguez', 'Ing', 1, 2, 3, '2018-02-09', '2018-07-31', '1'),
(14, 'Alejandro', 'Ortíz Villavicencio', 'Ing', 1, 3, 3, '2018-02-09', '2018-10-31', '1'),
(15, 'Francisco', 'Gallegos Jimenez', 'ME', 1, 4, 3, '2018-02-09', '2018-09-30', '1'),
(16, 'Jorge Armando', 'Hernandez Valencia', 'Ma', 1, 5, 3, '2018-02-09', '2018-10-31', '1'),
(17, 'Sofía', 'Maldonado Ventura', 'L.C.P', 1, 6, 3, '2018-02-09', '2019-01-31', '1'),
(18, 'Anita del Carmen', 'Morales Cruz', 'Ing', 1, 7, 3, '2018-02-09', '2018-10-31', '1'),
(19, 'Mario Guillermo', 'Villalobos de la Cruz', 'M en I', 1, 8, 3, '2018-02-09', '2019-01-30', '1'),
(20, 'Macario', 'Quiros Cortes', 'LAEM', 3, 11, 4, '2018-02-12', '2018-12-31', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rel_datos_materia_tipo_solicitud`
--

CREATE TABLE IF NOT EXISTS `rel_datos_materia_tipo_solicitud` (
`id_datos_materia_tipo_solicitud` bigint(11) unsigned NOT NULL,
  `id_kardex` bigint(20) unsigned NOT NULL,
  `num_solicitud` int(10) unsigned NOT NULL,
  `id_alumno` bigint(20) unsigned NOT NULL,
  `clave_oficial` varchar(45) NOT NULL,
  `id_semestre` int(11) NOT NULL,
  `id_periodo_escolar` int(11) NOT NULL,
  `materia` varchar(45) DEFAULT NULL,
  `id_tipo_solicitud` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Volcado de datos para la tabla `rel_datos_materia_tipo_solicitud`
--

INSERT INTO `rel_datos_materia_tipo_solicitud` (`id_datos_materia_tipo_solicitud`, `id_kardex`, `num_solicitud`, `id_alumno`, `clave_oficial`, `id_semestre`, `id_periodo_escolar`, `materia`, `id_tipo_solicitud`) VALUES
(14, 19, 25, 1, 'ITIC-2010-225', 3, 6, 'ACA0907', 1),
(15, 21, 26, 2, 'ITIC-2010-225', 3, 6, 'AEF1032', 1),
(16, 20, 27, 4, 'IGEM-2009-201', 3, 6, 'AEF1074', 1),
(17, 26, 28, 2, 'ITIC-2010-225', 4, 7, 'TIF1009', 1),
(18, 28, 29, 1, 'ITIC-2010-225', 5, 8, 'TIE1018', 1),
(22, 33, 33, 5, 'IGEM-2009-201', 3, 6, 'ACC0906', 1),
(23, 33, 34, 5, 'IGEM-2009-201', 3, 6, 'GEC0905', 1),
(37, 43, 46, 7, 'IELC-2010-211', 2, 5, 'ETF1004', 5),
(38, 34, 47, 6, 'IMEC-2010-228', 1, 4, 'AEF1020', 5),
(39, 65, 48, 6, 'IMEC-2010-228', 5, 8, 'AEF1020', 2),
(45, 67, 54, 8, 'IGEM-2009-201', 3, 6, 'ACA0909', 5),
(46, 67, 55, 8, 'IGEM-2009-201', 3, 6, 'ACC0906', 2),
(49, 68, 58, 7, 'IELC-2010-211', 6, 9, 'ACA0909', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rel_kardex_materias`
--

CREATE TABLE IF NOT EXISTS `rel_kardex_materias` (
  `id_kardex` bigint(19) unsigned NOT NULL,
  `materia` varchar(100) NOT NULL,
  `aprobada` char(1) DEFAULT NULL,
  `status` char(1) NOT NULL,
  `tipo_materia` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rel_kardex_materias`
--

INSERT INTO `rel_kardex_materias` (`id_kardex`, `materia`, `aprobada`, `status`, `tipo_materia`) VALUES
(1, 'ACF0901', '1', '1', '1'),
(1, 'AEF1032', '1', '1', '1'),
(1, 'TIF1019', '1', '1', '1'),
(1, 'TIP1017', '1', '1', '1'),
(1, 'ACA0907', '0', '1', '1'),
(1, 'ACC0906', '1', '1', '1'),
(2, 'ACF0901', '1', '1', '1'),
(2, 'AEF1032', '0', '1', '1'),
(2, 'TIF1019', '1', '1', '1'),
(2, 'TIP1017', '1', '1', '1'),
(2, 'ACA0907', '1', '1', '1'),
(2, 'ACC0906', '1', '1', '1'),
(12, 'ACC0906', '1', '1', '1'),
(12, 'GEC0905', '1', '1', '1'),
(12, 'AEF1074', '0', '1', '1'),
(12, 'GEC0909', '1', '1', '1'),
(12, 'GEC0910', '1', '1', '1'),
(13, 'ACF0902', '1', '1', '1'),
(13, 'AEB1054', '1', '1', '1'),
(13, 'TIF1020', '1', '1', '1'),
(13, 'AEF1052', '1', '1', '1'),
(13, 'TIF1009', '1', '1', '1'),
(14, 'ACF0902', '1', '1', '1'),
(14, 'AEB1054', '1', '1', '1'),
(14, 'TIF1020', '1', '1', '1'),
(14, 'AEF1052', '1', '1', '1'),
(14, 'TIF1009', '0', '1', '1'),
(15, 'AEB1082', '1', '1', '1'),
(15, 'ACF0902', '1', '1', '1'),
(15, 'GED0903', '1', '1', '1'),
(15, 'AEC1014', '1', '1', '1'),
(15, 'ACA0907', '1', '1', '1'),
(15, 'GEE0918', '1', '1', '1'),
(19, 'TIE1018', '0', '1', '1'),
(19, 'TID1012', '1', '1', '1'),
(19, 'TIC1002', '1', '1', '1'),
(19, 'AEF1031', '1', '1', '1'),
(19, 'TIC1011', '1', '1', '1'),
(19, 'ACF0903', '1', '1', '1'),
(20, 'AEC1078', '1', '1', '1'),
(20, 'GED0921', '1', '1', '1'),
(20, 'GED0904', '1', '1', '1'),
(20, 'GEC0913', '1', '1', '1'),
(20, 'AEF1071', '1', '1', '1'),
(20, 'ACF0903', '1', '1', '1'),
(21, 'TIE1018', '1', '1', '1'),
(21, 'TID1012', '1', '1', '1'),
(21, 'TIC1002', '1', '1', '1'),
(21, 'AEF1031', '1', '1', '1'),
(21, 'TIC1011', '1', '1', '1'),
(21, 'ACF0903', '1', '1', '1'),
(19, 'ACA0907', '0', '1', '2'),
(21, 'AEF1032', '1', '1', '2'),
(20, 'AEF1074', '1', '1', '2'),
(25, 'TID1004', '1', '1', '1'),
(25, 'TIB1024', '1', '1', '1'),
(25, 'TIF1021', '1', '1', '1'),
(25, 'AEH1063', '1', '1', '1'),
(25, 'TID1008', '1', '1', '1'),
(25, 'TIC1014', '1', '1', '1'),
(26, 'TID1004', '1', '1', '1'),
(26, 'TIB1024', '1', '1', '1'),
(26, 'TIF1021', '1', '1', '1'),
(26, 'AEH1063', '1', '1', '1'),
(26, 'TID1008', '1', '1', '1'),
(26, 'TIC1014', '1', '1', '1'),
(27, 'GEF0916', '1', '1', '1'),
(27, 'GEG0907', '1', '1', '1'),
(27, 'GED0917', '1', '1', '1'),
(27, 'GEC0914', '1', '1', '1'),
(27, 'GEF0906', '1', '1', '1'),
(27, 'AEF1076', '1', '1', '1'),
(26, 'TIF1009', '0', '1', '2'),
(28, 'TIF1013', '1', '1', '1'),
(28, 'TIF1029', '1', '1', '1'),
(28, 'TIF1001', '1', '1', '1'),
(28, 'TIF1007', '1', '1', '1'),
(28, 'TIC1005', '1', '1', '1'),
(28, 'TIC1027', '1', '1', '1'),
(29, 'TIF1013', NULL, '1', '1'),
(29, 'TIF1029', NULL, '1', '1'),
(29, 'TIF1001', NULL, '1', '1'),
(29, 'TIF1007', NULL, '1', '1'),
(29, 'TIC1005', NULL, '1', '1'),
(29, 'TIC1027', NULL, '1', '1'),
(30, 'AEF1073', NULL, '1', '1'),
(30, 'GEG0908', NULL, '1', '1'),
(30, 'GEF0915', NULL, '1', '1'),
(30, 'AEG1075', NULL, '1', '1'),
(30, 'ACA0909', '0', '1', '1'),
(30, 'GEF0919', NULL, '1', '1'),
(28, 'TIE1018', '0', '1', '2'),
(31, 'ACC0906', '0', '1', '1'),
(31, 'ACF0901', '1', '1', '1'),
(31, 'GEC0905', '0', '1', '1'),
(31, 'AEF1074', '1', '1', '1'),
(31, 'GEC0909', '1', '1', '1'),
(31, 'GEC0910', '1', '1', '1'),
(32, 'AEB1082', '1', '1', '1'),
(32, 'ACF0902', '1', '1', '1'),
(32, 'GED0903', '1', '1', '1'),
(32, 'AEC1014', '1', '1', '1'),
(32, 'ACA0907', '1', '1', '1'),
(32, 'GEE0918', '1', '1', '1'),
(33, 'AEC1078', '1', '1', '1'),
(33, 'GED0921', '1', '1', '1'),
(33, 'GED0904', '1', '1', '1'),
(33, 'GEC0913', '1', '1', '1'),
(33, 'AEF1071', '1', '1', '1'),
(33, 'ACF0903', '1', '1', '1'),
(34, 'MEV1006', '1', '1', '1'),
(34, 'AFC0901', '1', '1', '1'),
(34, 'MEH1022', '1', '1', '1'),
(34, 'MEC1026', '1', '1', '1'),
(34, 'ACA0907', '1', '1', '1'),
(34, 'ACC0906', '1', '1', '1'),
(35, 'AEF1010', '1', '1', '1'),
(35, 'ETF1002', '1', '1', '1'),
(35, 'AEF1038', '1', '1', '1'),
(35, 'ETF1023', '1', '1', '1'),
(35, 'ETF1019', '1', '1', '1'),
(35, 'ACA0910', '1', '1', '1'),
(36, 'ACC0906', '1', '1', '1'),
(36, 'ACF0901', '1', '1', '1'),
(36, 'GEC0905', '1', '1', '1'),
(36, 'AEF1074', '1', '1', '1'),
(36, 'GEC0909', '1', '1', '1'),
(36, 'GEC0910', '1', '1', '1'),
(33, 'ACC0906', '0', '1', '2'),
(33, 'GEC0905', '0', '1', '2'),
(37, 'GEF0916', '1', '1', '1'),
(37, 'GEG0907', '1', '1', '1'),
(37, 'GED0917', '1', '1', '1'),
(37, 'GEC0914', '1', '1', '1'),
(37, 'GEF0906', '1', '1', '1'),
(37, 'AEF1076', '1', '1', '1'),
(38, 'AEF1073', NULL, '1', '1'),
(38, 'GEG0908', NULL, '1', '1'),
(38, 'GEF0915', NULL, '1', '1'),
(38, 'AEG1075', NULL, '1', '1'),
(38, 'ACA0909', NULL, '1', '1'),
(38, 'GEF0919', NULL, '1', '1'),
(39, 'ACA0907', '1', '1', '1'),
(39, 'ACC0906', '1', '1', '1'),
(39, 'ACF0901', '1', '1', '1'),
(39, 'AEF1060', '1', '1', '1'),
(39, 'IQC1018', '0', '1', '1'),
(39, 'AEO1012', '1', '1', '1'),
(40, 'ACF0903', '1', '1', '1'),
(40, 'AEF1042', '1', '1', '1'),
(40, 'ACF0902', '0', '1', '1'),
(40, 'IQF1019', '1', '1', '1'),
(40, 'AEF1065', '0', '1', '1'),
(40, 'AEG1059', '1', '1', '1'),
(41, 'IQF1001', '0', '1', '1'),
(41, 'IQF1003', '0', '1', '1'),
(41, 'ACF0904', '1', '1', '1'),
(41, 'IQF1020', '1', '1', '1'),
(41, 'AEF1004', '1', '1', '1'),
(41, 'IQF1006', '1', '1', '1'),
(42, 'IQH1014', NULL, '1', '1'),
(42, 'ACF0905', NULL, '1', '1'),
(42, 'IQF1013', NULL, '1', '1'),
(42, 'IQF1007', NULL, '1', '1'),
(42, 'IQF1004', NULL, '1', '1'),
(42, 'AEF1003', NULL, '1', '1'),
(43, 'ACF0902', '1', '1', '1'),
(43, 'AEE1051', '1', '1', '1'),
(43, 'ACD0908', '1', '1', '1'),
(43, 'ETD1021', '1', '1', '1'),
(43, 'ETF1027', '1', '1', '1'),
(43, 'ETQ1009', '1', '1', '1'),
(43, 'ETF1004', '1', '1', '1'),
(44, 'ACF0904', '1', '1', '1'),
(44, 'AEF1020', '1', '1', '1'),
(44, 'ACF0903', '1', '1', '1'),
(44, 'ETF1017', '1', '1', '1'),
(44, 'ETD1024', '1', '1', '1'),
(60, 'ACF0905', '1', '1', '1'),
(60, 'ETF1004', '1', '1', '1'),
(60, 'ETP1020', '1', '1', '1'),
(60, 'ETF1003', '1', '1', '1'),
(60, 'ETF1014', '1', '1', '1'),
(60, 'ETD1025', '1', '1', '1'),
(61, 'ETF1005', NULL, '1', '1'),
(61, 'ETF1012', NULL, '1', '1'),
(61, 'ETF1026', NULL, '1', '1'),
(61, 'AEF1040', NULL, '1', '1'),
(61, 'ETF1015', NULL, '1', '1'),
(61, 'ETO1010', NULL, '1', '1'),
(34, 'AEF1020', '0', '1', '1'),
(62, 'MEC1023', '1', '1', '1'),
(62, 'ACF0902', '1', '1', '1'),
(62, 'ACF0903', '1', '1', '1'),
(62, 'MEF1013', '1', '1', '1'),
(62, 'MEA1001', '1', '1', '1'),
(62, 'MER1024', '1', '1', '1'),
(63, 'MED1010', '1', '1', '1'),
(63, 'ACF0904', '1', '1', '1'),
(63, 'MEF1003', '1', '1', '1'),
(63, 'MEF1014', '1', '1', '1'),
(63, 'AEF1020', '0', '1', '2'),
(63, 'MER1005', '1', '1', '1'),
(64, 'MED1020', '1', '1', '1'),
(64, 'ACF0905', '1', '1', '1'),
(64, 'MED1007', '1', '1', '1'),
(64, 'MED1025', '1', '1', '1'),
(64, 'MED1050', '1', '1', '1'),
(64, 'AEC1046', '1', '1', '1'),
(65, 'MED1021', NULL, '1', '1'),
(65, 'AED1043', NULL, '1', '1'),
(65, 'MEF1031', NULL, '1', '1'),
(65, 'MEC1019', NULL, '1', '1'),
(65, 'MED1004', NULL, '1', '1'),
(65, 'ACD0908', NULL, '1', '1'),
(65, 'AEF1020', NULL, '1', '3'),
(66, 'AEB1082', '1', '1', '1'),
(66, 'ACF0902', '1', '1', '1'),
(66, 'GED0903', '1', '1', '1'),
(66, 'AEC1014', '1', '1', '1'),
(66, 'ACA0907', '1', '1', '1'),
(66, 'GEE0918', '1', '1', '1'),
(67, 'AEC1078', NULL, '1', '1'),
(67, 'GED0921', NULL, '1', '1'),
(67, 'GED0904', NULL, '1', '1'),
(67, 'GEC0913', NULL, '1', '1'),
(67, 'AEF1071', NULL, '1', '1'),
(67, 'ACF0903', NULL, '1', '1'),
(67, 'ACA0909', NULL, '2', '1'),
(67, 'ACC0906', NULL, '2', '3'),
(68, 'ETF1009', '1', '1', '1'),
(68, 'ETF1013', '1', '1', '1'),
(68, 'ETP1018', '1', '1', '1'),
(68, 'ETD1022', '1', '1', '1'),
(68, 'ACA0909', '1', '1', '1'),
(68, 'ACA0909', NULL, '2', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rel_materias_carreras`
--

CREATE TABLE IF NOT EXISTS `rel_materias_carreras` (
  `clave_oficial` varchar(100) NOT NULL,
  `materia` varchar(100) NOT NULL,
  `creditos_materia` int(11) NOT NULL,
  `horas_teoricas` float NOT NULL,
  `horas_practicas` float NOT NULL,
  `id_semestre` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rel_materias_carreras`
--

INSERT INTO `rel_materias_carreras` (`clave_oficial`, `materia`, `creditos_materia`, `horas_teoricas`, `horas_practicas`, `id_semestre`) VALUES
('IACU-2010-212', 'ACF0902', 5, 3, 2, 2),
('IACU-2010-212', 'AQD1030', 5, 2, 3, 2),
('IACU-2010-212', 'AQH1001', 4, 1, 3, 2),
('IACU-2010-212', 'AQC1027', 4, 2, 2, 2),
('IACU-2010-212', 'AQD1019', 5, 2, 3, 2),
('IACU-2010-212', 'AQC1026', 4, 2, 2, 2),
('IACU-2010-212', 'ACF0904', 5, 3, 2, 3),
('IACU-2010-212', 'AQD10231', 5, 2, 3, 3),
('IACU-2010-212', 'AQD1028', 5, 2, 3, 3),
('IACU-2010-212', 'AQF1012', 5, 3, 2, 3),
('IACU-2010-212', 'AQA1008', 4, 0, 4, 3),
('IACU-2010-212', 'AQD1011', 5, 2, 3, 3),
('IACU-2010-212', 'AQD1013', 5, 2, 3, 4),
('IACU-2010-212', 'AQD1004', 5, 2, 3, 4),
('IACU-2010-212', 'AQD1029', 5, 2, 3, 4),
('IACU-2010-212', 'AQG1024', 6, 3, 3, 4),
('IACU-2010-212', 'AQH1035', 4, 1, 3, 4),
('IACU-2010-212', 'AQC1002', 4, 2, 2, 4),
('IACU-2010-212', 'AQD1021', 5, 2, 3, 5),
('IACU-2010-212', 'AQD1020', 5, 2, 3, 5),
('IACU-2010-212', 'AQD1022', 5, 2, 3, 5),
('IACU-2010-212', 'AQD1014', 5, 2, 3, 5),
('IACU-2010-212', 'ACD0908', 5, 2, 3, 5),
('IACU-2010-212', 'AQD1023', 5, 2, 3, 5),
('IACU-2010-212', 'AQD1006', 5, 2, 3, 6),
('IACU-2010-212', 'ACA0909', 4, 0, 4, 6),
('IACU-2010-212', 'AQC1034', 4, 2, 2, 6),
('IACU-2010-212', 'AQD1025', 5, 2, 3, 6),
('IACU-2010-212', 'AQC1033', 4, 2, 2, 6),
('IACU-2010-212', 'AQD1005', 5, 2, 3, 6),
('IACU-2010-212', 'AQD1009', 5, 2, 3, 7),
('IACU-2010-212', 'AQF1016', 5, 3, 2, 7),
('IACU-2010-212', 'AQP1018', 3, 3, 0, 7),
('IACU-2010-212', 'AQD1032', 5, 2, 3, 7),
('IACU-2010-212', 'ACA0910', 4, 0, 4, 7),
('IACU-2010-212', 'AQD1010', 5, 2, 3, 8),
('IACU-2010-212', 'AQB1015', 5, 1, 4, 8),
('IMEC-2010-228', 'MEV1006', 5, 0, 5, 1),
('IMEC-2010-228', 'AFC0901', 5, 3, 2, 1),
('IMEC-2010-228', 'MEH1022', 4, 1, 3, 1),
('IMEC-2010-228', 'MEC1026', 4, 2, 2, 1),
('IMEC-2010-228', 'ACA0907', 4, 0, 4, 1),
('IMEC-2010-228', 'ACC0906', 4, 2, 2, 1),
('IMEC-2010-228', 'MEC1023', 4, 2, 2, 2),
('IMEC-2010-228', 'ACF0902', 5, 3, 2, 2),
('IMEC-2010-228', 'ACF0903', 5, 3, 2, 2),
('IMEC-2010-228', 'MEF1013', 5, 3, 2, 2),
('IMEC-2010-228', 'MEA1001', 4, 0, 4, 2),
('IMEC-2010-228', 'MER1024', 3, 2, 1, 2),
('IMEC-2010-228', 'MED1010', 5, 2, 3, 3),
('IMEC-2010-228', 'ACF0904', 5, 3, 2, 3),
('IMEC-2010-228', 'MEF1003', 4, 2, 2, 3),
('IMEC-2010-228', 'MEF1014', 5, 3, 2, 3),
('IMEC-2010-228', 'AEF1020', 5, 3, 2, 3),
('IMEC-2010-228', 'MER1005', 3, 2, 1, 3),
('IMEC-2010-228', 'MED1020', 5, 2, 3, 4),
('IMEC-2010-228', 'ACF0905', 5, 3, 2, 4),
('IMEC-2010-228', 'MED1007', 5, 2, 3, 4),
('IMEC-2010-228', 'MED1025', 5, 2, 3, 4),
('IMEC-2010-228', 'MED1050', 5, 2, 3, 4),
('IMEC-2010-228', 'AEC1046', 4, 2, 2, 4),
('IMEC-2010-228', 'MED1021', 5, 2, 3, 5),
('IMEC-2010-228', 'AED1043', 5, 2, 3, 5),
('IMEC-2010-228', 'MEF1031', 5, 2, 3, 5),
('IMEC-2010-228', 'MEC1019', 4, 2, 2, 5),
('IMEC-2010-228', 'MED1004', 5, 2, 3, 5),
('IMEC-2010-228', 'ACD0908', 5, 2, 3, 5),
('IMEC-2010-228', 'MED1008', 5, 2, 3, 6),
('IMEC-2010-228', 'AED1067', 5, 2, 3, 6),
('IMEC-2010-228', 'MEF1032', 5, 3, 2, 6),
('IMEC-2010-228', 'MED1029', 5, 2, 3, 6),
('IMEC-2010-228', 'MEF1015', 5, 3, 2, 6),
('IMEC-2010-228', 'ACA0909', 4, 0, 4, 6),
('IMEC-2010-228', 'MED1009', 5, 2, 3, 7),
('IMEC-2010-228', 'MER1012', 3, 2, 1, 7),
('IMEC-2010-228', 'MEE1017', 4, 3, 1, 7),
('IMEC-2010-228', 'MEF1018', 5, 3, 2, 7),
('IMEC-2010-228', 'MEF1002', 5, 3, 2, 7),
('IMEC-2010-228', 'ACA0910', 4, 0, 4, 7),
('IMEC-2010-228', 'MEC1016', 4, 2, 2, 8),
('IMEC-2010-228', 'MEL1028', 5, 4, 1, 8),
('IMEC-2010-228', 'MED1027', 5, 2, 3, 8),
('IMEC-2010-228', 'MEC1011', 4, 2, 2, 8),
('IELC-2010-211', 'ACF0901', 5, 3, 2, 1),
('IELC-2010-211', 'AEF1042', 5, 3, 2, 1),
('IELC-2010-211', 'AEC1058', 4, 2, 2, 1),
('IELC-2010-211', 'ACA0907', 4, 0, 4, 1),
('IELC-2010-211', 'ACC0906', 4, 2, 2, 1),
('IELC-2010-211', 'ETQ1006', 3, 1, 2, 1),
('IELC-2010-211', 'ACF0902', 5, 3, 2, 2),
('IELC-2010-211', 'AEE1051', 4, 3, 1, 2),
('IELC-2010-211', 'ACD0908', 5, 2, 3, 2),
('IELC-2010-211', 'ETD1021', 5, 2, 3, 2),
('IELC-2010-211', 'ETF1027', 5, 3, 2, 2),
('IELC-2010-211', 'ETQ1009', 3, 1, 2, 2),
('IELC-2010-211', 'ACF0904', 5, 2, 3, 3),
('IELC-2010-211', 'AEF1020', 5, 2, 3, 3),
('IELC-2010-211', 'ACF0903', 5, 3, 2, 3),
('IELC-2010-211', 'ETF1017', 5, 3, 2, 3),
('IELC-2010-211', 'ETD1024', 5, 2, 3, 3),
('IELC-2010-211', 'ACF0905', 5, 3, 2, 4),
('IELC-2010-211', 'ETF1004', 5, 3, 2, 4),
('IELC-2010-211', 'ETP1020', 3, 3, 0, 4),
('IELC-2010-211', 'ETF1003', 5, 3, 2, 4),
('IELC-2010-211', 'ETF1014', 5, 3, 2, 4),
('IELC-2010-211', 'ETD1025', 5, 2, 3, 4),
('IELC-2010-211', 'ETF1005', 5, 3, 2, 5),
('IELC-2010-211', 'ETF1012', 5, 3, 2, 5),
('IELC-2010-211', 'ETF1026', 5, 3, 2, 5),
('IELC-2010-211', 'AEF1040', 5, 3, 2, 5),
('IELC-2010-211', 'ETF1015', 5, 3, 2, 5),
('IELC-2010-211', 'ETO1010', 3, 0, 3, 5),
('IELC-2010-211', 'ETF1009', 5, 3, 2, 6),
('IELC-2010-211', 'ETF1013', 5, 3, 2, 6),
('IELC-2010-211', 'ETP1018', 3, 3, 0, 6),
('IELC-2010-211', 'ETD1022', 5, 2, 3, 6),
('IELC-2010-211', 'ACA0909', 4, 0, 4, 6),
('IELC-2010-211', 'AEF1010', 5, 3, 2, 7),
('IELC-2010-211', 'ETF1002', 5, 3, 2, 7),
('IELC-2010-211', 'AEF1038', 5, 3, 2, 7),
('IELC-2010-211', 'ETF1023', 5, 3, 2, 7),
('IELC-2010-211', 'ETF1019', 5, 3, 2, 7),
('IELC-2010-211', 'ACA0910', 4, 0, 4, 7),
('IELC-2010-211', 'ETF1007', 5, 3, 2, 8),
('IELC-2010-211', 'ETF1008', 5, 3, 2, 8),
('IELC-2010-211', 'ETF1016', 5, 3, 2, 8),
('IELC-2010-211', 'ETF1001', 3, 2, 1, 8),
('IGEM-2009-201', 'ACC0906', 4, 2, 2, 1),
('IGEM-2009-201', 'ACF0901', 5, 3, 2, 1),
('IGEM-2009-201', 'GEC0905', 4, 2, 2, 1),
('IGEM-2009-201', 'AEF1074', 5, 3, 2, 1),
('IGEM-2009-201', 'GEC0909', 4, 2, 2, 1),
('IGEM-2009-201', 'GEC0910', 5, 3, 2, 1),
('IGEM-2009-201', 'AEB1082', 5, 1, 4, 2),
('IGEM-2009-201', 'ACF0902', 5, 3, 2, 2),
('IGEM-2009-201', 'GED0903', 5, 2, 3, 2),
('IGEM-2009-201', 'AEC1014', 4, 2, 2, 2),
('IGEM-2009-201', 'ACA0907', 4, 0, 4, 2),
('IGEM-2009-201', 'GEE0918', 4, 3, 1, 2),
('IGEM-2009-201', 'AEC1078', 4, 2, 2, 3),
('IGEM-2009-201', 'GED0921', 5, 2, 3, 3),
('IGEM-2009-201', 'GED0904', 5, 2, 3, 3),
('IGEM-2009-201', 'GEC0913', 4, 2, 2, 3),
('IGEM-2009-201', 'AEF1071', 5, 3, 2, 3),
('IGEM-2009-201', 'ACF0903', 5, 3, 2, 3),
('IGEM-2009-201', 'GEF0916', 5, 3, 2, 4),
('IGEM-2009-201', 'GEG0907', 6, 3, 3, 4),
('IGEM-2009-201', 'GED0917', 5, 2, 3, 4),
('IGEM-2009-201', 'GEC0914', 4, 2, 2, 4),
('IGEM-2009-201', 'GEF0906', 5, 3, 2, 4),
('IGEM-2009-201', 'AEF1076', 5, 3, 2, 4),
('IGEM-2009-201', 'AEF1073', 5, 3, 2, 5),
('IGEM-2009-201', 'GEG0908', 6, 3, 3, 5),
('IGEM-2009-201', 'GEF0915', 5, 3, 2, 5),
('IGEM-2009-201', 'AEG1075', 6, 3, 3, 5),
('IGEM-2009-201', 'ACA0909', 4, 0, 4, 5),
('IGEM-2009-201', 'GEF0919', 5, 3, 2, 5),
('IGEM-2009-201', 'GEF0901', 5, 3, 2, 6),
('IGEM-2009-201', 'AED1072', 5, 2, 3, 6),
('IGEM-2009-201', 'GEC0911', 4, 2, 2, 6),
('IGEM-2009-201', 'AED1015', 5, 2, 3, 6),
('IGEM-2009-201', 'ACA0910', 4, 0, 4, 6),
('IGEM-2009-201', 'GED0922', 5, 2, 3, 6),
('IGEM-2009-201', 'AED1069', 5, 2, 3, 7),
('IGEM-2009-201', 'GED0920', 5, 2, 3, 7),
('IGEM-2009-201', 'GEC0912', 4, 2, 2, 7),
('IGEM-2009-201', 'AED1035', 5, 2, 3, 7),
('IGEM-2009-201', 'ACD0908', 5, 2, 3, 7),
('IGEM-2009-201', 'AEB1045', 5, 1, 4, 7),
('IGEM-2009-201', 'GEF0902', 5, 3, 2, 8),
('ITIC-2010-225', 'ACF0901', 5, 3, 2, 1),
('ITIC-2010-225', 'AEF1032', 5, 3, 2, 1),
('ITIC-2010-225', 'TIF1019', 5, 3, 2, 1),
('ITIC-2010-225', 'TIP1017', 3, 3, 0, 1),
('ITIC-2010-225', 'ACA0907', 4, 0, 4, 1),
('ITIC-2010-225', 'ACC0906', 4, 2, 2, 1),
('ITIC-2010-225', 'ACF0902', 5, 3, 2, 2),
('ITIC-2010-225', 'AEB1054', 5, 1, 4, 2),
('ITIC-2010-225', 'TIF1020', 5, 3, 2, 2),
('ITIC-2010-225', 'AEF1052', 5, 3, 2, 2),
('ITIC-2010-225', 'TIF1009', 5, 3, 2, 2),
('ITIC-2010-225', 'TIE1018', 4, 3, 1, 3),
('ITIC-2010-225', 'TID1012', 5, 2, 3, 3),
('ITIC-2010-225', 'TIC1002', 4, 2, 2, 3),
('ITIC-2010-225', 'AEF1031', 5, 3, 2, 3),
('ITIC-2010-225', 'TIC1011', 4, 2, 2, 3),
('ITIC-2010-225', 'ACF0903', 5, 3, 2, 3),
('ITIC-2010-225', 'TID1004', 5, 2, 3, 4),
('ITIC-2010-225', 'TIB1024', 5, 1, 4, 4),
('ITIC-2010-225', 'TIF1021', 5, 3, 2, 4),
('ITIC-2010-225', 'AEH1063', 4, 1, 3, 4),
('ITIC-2010-225', 'TID1008', 5, 2, 3, 4),
('ITIC-2010-225', 'TIC1014', 4, 2, 2, 4),
('ITIC-2010-225', 'TIF1013', 5, 3, 2, 5),
('ITIC-2010-225', 'TIF1029', 5, 3, 2, 5),
('ITIC-2010-225', 'TIF1001', 5, 3, 2, 5),
('ITIC-2010-225', 'TIF1007', 5, 3, 2, 5),
('ITIC-2010-225', 'TIC1005', 4, 2, 2, 5),
('ITIC-2010-225', 'TIC1027', 4, 2, 2, 5),
('ITIC-2010-225', 'TIF1025', 5, 3, 2, 6),
('ITIC-2010-225', 'AEB1055', 5, 1, 4, 6),
('ITIC-2010-225', 'TID1010', 5, 2, 3, 6),
('ITIC-2010-225', 'ACA0909', 4, 0, 4, 6),
('ITIC-2010-225', 'AEC1061', 4, 2, 2, 6),
('ITIC-2010-225', 'TIC1028', 4, 2, 2, 6),
('ITIC-2010-225', 'TIF1026', 5, 3, 2, 7),
('ITIC-2010-225', 'AEB1011', 5, 1, 4, 7),
('ITIC-2010-225', 'ACD0908', 5, 2, 3, 7),
('ITIC-2010-225', 'ACA0910', 4, 0, 4, 7),
('ITIC-2010-225', 'AED1062', 5, 2, 3, 7),
('ITIC-2010-225', 'TIC1022', 4, 2, 2, 7),
('ITIC-2010-225', 'TIF1003', 5, 3, 2, 8),
('ITIC-2010-225', 'TIF1006', 4, 2, 2, 8),
('ITIC-2010-225', 'TIH1016', 4, 1, 3, 8),
('ITIC-2010-225', 'TIC1015', 4, 2, 2, 8),
('ITIC-2010-225', 'TIC1023', 4, 2, 2, 8),
('IQUI-2010-232', 'ACA0907', 4, 0, 4, 1),
('IQUI-2010-232', 'ACC0906', 4, 2, 2, 1),
('IQUI-2010-232', 'ACF0901', 5, 3, 2, 1),
('IQUI-2010-232', 'AEF1060', 5, 3, 2, 1),
('IQUI-2010-232', 'IQC1018', 4, 2, 2, 1),
('IQUI-2010-232', 'AEO1012', 3, 0, 3, 1),
('IQUI-2010-232', 'ACF0903', 5, 2, 3, 2),
('IQUI-2010-232', 'AEF1042', 5, 3, 2, 2),
('IQUI-2010-232', 'ACF0902', 5, 3, 2, 2),
('IQUI-2010-232', 'IQF1019', 5, 3, 2, 2),
('IQUI-2010-232', 'AEF1065', 5, 3, 2, 2),
('IQUI-2010-232', 'AEG1059', 6, 3, 3, 2),
('IQUI-2010-232', 'IQF1001', 5, 3, 2, 3),
('IQUI-2010-232', 'IQF1003', 5, 3, 2, 3),
('IQUI-2010-232', 'ACF0904', 5, 3, 2, 3),
('IQUI-2010-232', 'IQF1020', 5, 3, 2, 3),
('IQUI-2010-232', 'AEF1004', 5, 3, 2, 3),
('IQUI-2010-232', 'IQF1006', 5, 3, 2, 3),
('IQUI-2010-232', 'IQH1014', 4, 1, 3, 4),
('IQUI-2010-232', 'ACF0905', 5, 3, 2, 4),
('IQUI-2010-232', 'IQF1013', 5, 3, 2, 4),
('IQUI-2010-232', 'IQF1007', 5, 3, 2, 4),
('IQUI-2010-232', 'IQF1004', 5, 3, 2, 4),
('IQUI-2010-232', 'AEF1003', 5, 3, 2, 4),
('IQUI-2010-232', 'ACD0908', 5, 2, 3, 5),
('IQUI-2010-232', 'IQC1008', 4, 2, 2, 5),
('IQUI-2010-232', 'IQF1013', 6, 4, 2, 5),
('IQUI-2010-232', 'IQF1015', 5, 3, 2, 5),
('IQUI-2010-232', 'IQF1005', 5, 3, 2, 5),
('IQUI-2010-232', 'ACA0909', 4, 0, 4, 6),
('IQUI-2010-232', 'IQF1016', 5, 3, 2, 6),
('IQUI-2010-232', 'IQN1010', 6, 0, 6, 6),
('IQUI-2010-232', 'IQF1021', 5, 3, 2, 6),
('IQUI-2010-232', 'IQO1025', 3, 0, 3, 7),
('IQUI-2010-232', 'ACA0910', 4, 0, 4, 7),
('IQUI-2010-232', 'IQF1017', 5, 3, 2, 7),
('IQUI-2010-232', 'IQF1024', 5, 3, 2, 7),
('IQUI-2010-232', 'IQF1022', 5, 3, 2, 7),
('IQUI-2010-232', 'IQN1011', 6, 0, 6, 7),
('IQUI-2010-232', 'IQN1012', 6, 0, 6, 8),
('IQUI-2010-232', 'AEF1039', 5, 3, 2, 8),
('IQUI-2010-232', 'IQM1009', 6, 2, 4, 8),
('IQUI-2010-232', 'IQD1023', 5, 2, 3, 8),
('IACU-2010-212', 'ACF0901', 5, 3, 2, 1),
('IACU-2010-212', 'AQF1003', 5, 3, 2, 1),
('IACU-2010-212', 'AQF1017', 5, 3, 2, 1),
('IACU-2010-212', 'ACA0907', 4, 0, 4, 1),
('IACU-2010-212', 'ACC0906', 4, 2, 2, 1),
('IACU-2010-212', 'AQO1007', 3, 0, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rel_solicitud_kardex`
--

CREATE TABLE IF NOT EXISTS `rel_solicitud_kardex` (
`id_solicitud_kardex` bigint(11) unsigned NOT NULL,
  `id_kardex` bigint(20) unsigned NOT NULL,
  `num_solicitud` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reuniones`
--

CREATE TABLE IF NOT EXISTS `reuniones` (
`id_reunion` int(10) unsigned NOT NULL,
  `nombre_reunion` varchar(180) NOT NULL,
  `id_periodo_escolar` int(11) NOT NULL,
  `no_libro` varchar(50) NOT NULL,
  `no_acta` varchar(50) NOT NULL,
  `mensaje_1` text,
  `personal` text,
  `mensaje_2` text,
  `fecha_registro` date DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `reuniones`
--

INSERT INTO `reuniones` (`id_reunion`, `nombre_reunion`, `id_periodo_escolar`, `no_libro`, `no_acta`, `mensaje_1`, `personal`, `mensaje_2`, `fecha_registro`) VALUES
(1, 'PRIMERA REUNIÓN DE COMITÉ ACADÉMICO VERANO/2018', 6, '1', '7', 'EN LA CUIDAD Y PUERTO DE SALINA CRUZ SIENDO LAS TRECE HORAS CON TREINTA MINUTOS DEL DÍA CATORCE DE AGOSTO DEL AÑO DOS MIL DIECISIETE, SE REUNIERON EN LA SALA DE JUNTAS UBICADO DENTRO DE LAS INSTALACIONES QUE OCUPA EL INSTITUTO TECNOLÓGICO DE SALINA CRUZ, CON DIRECCIÓN EN CARRETERA A SAN ANTONIO MONTERREY KM 1,7 EN SALINA CRUZ OAXACA,', 'LOS CC M.T.I MARIO ENRIQUEZ NICOLÁS, ING VICTOR HUGO VARGAS CONTRERAS, ING LINO RAFAEL GIL RODRIGUEZ, ING ALEJANDRO ORTÍZ VILLAVICENCIO, ME FRANCISCO GALLEGOS JIMENEZ, MA JORGE ARMANDO HERNANDEZ VALENCIA, L.C.P SOFÍA MALDONADO VENTURA, ING ANITA DEL CARMEN MORALES CRUZ, M EN I MARIO GUILLERMO VILLALOBOS DE LA CRUZ, PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA :', '1.- PASE DE LA LISTA DE LOS INTEGRANTES DEL COMITÉ ACADÉMICO,\n2.- LECTURA DE LAS SOLICITUDES DE ESTUDIANTES DE LA INSTITUCIÓN ENVIADAS AL COMITÉ ACADÉMICO, PARA SU ANÁLISIS ACADÉMICO Y RECOMENDACIÓN DE LAS MISMAS, PARA EL PERIODO ESCOLAR VERANO DE 2018, APEGÁNDOSE AL MANUAL DE LINEAMIENTOS ACADÉMICO.ADMINISTRATIVO DEL TECNOLÓGICO NACIONAL DE MÉXICO\nCOMO PRIMER PUNTO SE PROCEDIO A DAR PASE A SU LISTA, CONTANDO CON EL CIEN POR CIENTO DE ASISTENCIA, COMO SEGUNDO PUNTO SE DIO LECTURA , PARA ANALIZAR, LAS SOLICITUDES TURNADAS Y RECIBIDAS POR EL COMITÉ ACADÉMICO, PARA SU EFECTO EN EL PERIODO ESCOLAR VERANO DE 2018', '2018-02-11'),
(6, 'SEGUNDA REUNIÓN DE COMITÉ ACADÉMICO VERANO/2018', 6, '10', '10', 'EN LA CUIDAD Y PUERTO DE SALINA CRUZ SIENDO LAS TRECE HORAS CON TREINTA MINUTOS DEL DÍA CATORCE DE AGOSTO DEL AÑO DOS MIL DIECISIETE, SE REUNIERON EN LA SALA DE JUNTAS UBICADO DENTRO DE LAS INSTALACIONES QUE OCUPA EL INSTITUTO TECNOLÓGICO DE SALINA CRUZ, CON DIRECCIÓN EN CARRETERA A SAN ANTONIO MONTERREY KM 1,7 EN SALINA CRUZ OAXACA,', 'LOS CC. LAEM MACARIO QIIROZ CORTES, M.T.I MARIO ENRIQUEZ NICOLÁS, ING VICTOR HUGO VARGAS CONTRERAS, ING ALEJANDRO ORTÍZ VILLAVICENCIO, ME FRANCISCO GALLEGOS JIMENEZ, ING LINO RAFAEL GIL RODRIGUEZ, L.C.P SOFÍA MALDONADO VENTURA, ING ANITA DEL CARMEN MORALES CRUZ, M EN I MARIO GUILLERMO VILLALOBOS DE LA CRUZ, MA JORGE ARMANDO HERNANDEZ VALENCIA,  PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA :', '1.- PASE DE LA LISTA DE LOS INTEGRANTES DEL COMITÉ ACADÉMICO,\n2.- LECTURA DE LAS SOLICITUDES DE ESTUDIANTES DE LA INSTITUCIÓN ENVIADAS AL COMITÉ ACADÉMICO, PARA SU ANÁLISIS ACADÉMICO Y RECOMENDACIÓN DE LAS MISMAS, PARA EL PERIODO ESCOLAR VERANO DE 2018, APEGÁNDOSE AL MANUAL DE LINEAMIENTOS ACADÉMICO.ADMINISTRATIVO DEL TECNOLÓGICO NACIONAL DE MÉXICO\nCOMO PRIMER PUNTO SE PROCEDIO A DAR PASE A SU LISTA, CONTANDO CON EL CIEN POR CIENTO DE ASISTENCIA, COMO SEGUNDO PUNTO SE DIO LECTURA , PARA ANALIZAR, LAS SOLICITUDES TURNADAS Y RECIBIDAS POR EL COMITÉ ACADÉMICO, PARA SU EFECTO EN EL PERIODO ESCOLAR VERANO DE 2018', '2018-02-14'),
(7, 'PRIMERA REUNIÓN DE COMITÉ ACADÉMICO AGOSTO_DICIEMBRE/2018', 7, '1', '1', 'EN LA CUIDAD Y PUERTO DE SALINA CRUZ SIENDO LAS ONCEHORAS CON CUARENTA MINUTOS DEL DÍA QUINCE DE AGOSTO DEL AÑO DOS MIL DIECISIETE, SE REUNIERON EN LA SALA DE JUNTAS UBICADO DENTRO DE LAS INSTALACIONES QUE OCUPA EL INSTITUTO TECNOLÓGICO DE SALINA CRUZ, CON DIRECCIÓN EN CARRETERA A SAN ANTONIO MONTERREY KM 1,7 EN SALINA CRUZ OAXACA,', 'LOS CC. LAEM MACARIO QIIROZ CORTES, M.T.I MARIO ENRIQUEZ NICOLÁS, ING VICTOR HUGO VARGAS CONTRERAS, ING ALEJANDRO ORTÍZ VILLAVICENCIO, ME FRANCISCO GALLEGOS JIMENEZ, ING LINO RAFAEL GIL RODRIGUEZ, L.C.P SOFÍA MALDONADO VENTURA, ING ANITA DEL CARMEN MORALES CRUZ, M EN I MARIO GUILLERMO VILLALOBOS DE LA CRUZ, MA JORGE ARMANDO HERNANDEZ VALENCIA,  PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA :', '1.- PASE DE LA LISTA DE LOS INTEGRANTES DEL COMITÉ ACADÉMICO,\n2.- LECTURA DE LAS SOLICITUDES DE ESTUDIANTES DE LA INSTITUCIÓN ENVIADAS AL COMITÉ ACADÉMICO, PARA SU ANÁLISIS ACADÉMICO Y RECOMENDACIÓN DE LAS MISMAS, PARA EL PERIODO ESCOLAR AGOSTO-DICIEMBRE DE 2018, APEGÁNDOSE AL MANUAL DE LINEAMIENTOS ACADÉMICO.ADMINISTRATIVO DEL TECNOLÓGICO NACIONAL DE MÉXICO\nCOMO PRIMER PUNTO SE PROCEDIO A DAR PASE A SU LISTA, CONTANDO CON EL CIEN POR CIENTO DE ASISTENCIA, COMO SEGUNDO PUNTO SE DIO LECTURA , PARA ANALIZAR, LAS SOLICITUDES TURNADAS Y RECIBIDAS POR EL COMITÉ ACADÉMICO, PARA SU EFECTO EN EL PERIODO ESCOLAR AGOSTO-DICIEMBRE DE 2018', '2018-02-14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semestres`
--

CREATE TABLE IF NOT EXISTS `semestres` (
`id_semestre` int(11) NOT NULL,
  `nombre_semestre` varchar(45) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `semestres`
--

INSERT INTO `semestres` (`id_semestre`, `nombre_semestre`) VALUES
(1, '1° Semestre'),
(2, '2° Semestre'),
(3, '3° Semestre'),
(4, '4° Semestre'),
(5, '5° Semestre'),
(6, '6° Semestre'),
(7, '7° Semestre'),
(8, '8° Semestre'),
(9, '9° Semestre');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `solicitudes`
--

CREATE TABLE IF NOT EXISTS `solicitudes` (
`num_solicitud` int(11) unsigned NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `observacion` varchar(500) NOT NULL,
  `asunto` varchar(350) NOT NULL,
  `lugar` varchar(200) NOT NULL,
  `fecha` datetime NOT NULL,
  `motivos_academicos` varchar(500) DEFAULT NULL,
  `motivos_personales` varchar(500) DEFAULT NULL,
  `otros` varchar(500) DEFAULT NULL,
  `id_semestre` int(11) NOT NULL,
  `id_periodo_escolar` int(11) NOT NULL,
  `id_alumno` bigint(20) unsigned NOT NULL,
  `respuesta_solicitud_acta` text,
  `respuesta_solicitud_dictamen` text,
  `motivos_solicitud_dictamen` text,
  `verificacion_manual_lineamiento` text,
  `no_dictamen` varchar(100) DEFAULT NULL,
  `no_recomendacion_dictamen` varchar(100) DEFAULT NULL,
  `recomienda` char(1) DEFAULT NULL,
  `id_reunion` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Volcado de datos para la tabla `solicitudes`
--

INSERT INTO `solicitudes` (`num_solicitud`, `status`, `observacion`, `asunto`, `lugar`, `fecha`, `motivos_academicos`, `motivos_personales`, `otros`, `id_semestre`, `id_periodo_escolar`, `id_alumno`, `respuesta_solicitud_acta`, `respuesta_solicitud_dictamen`, `motivos_solicitud_dictamen`, `verificacion_manual_lineamiento`, `no_dictamen`, `no_recomendacion_dictamen`, `recomienda`, `id_reunion`) VALUES
(25, 'Aceptado', 'Cursar la materia de Taller de Ética en condición de curso de repetición', 'Solicitud de curso de repetición', 'Veracruz', '2018-01-20 00:00:00', 'Solicitud de curso de repetición', 'adsdacccc', 'asdfccccc', 3, 6, 1, 'DE LO ANTERIOR, ESTE COMITE SI RECOMIENDA QUE CURSE LA ASIGNATURA EN CODICIÓN\nDE "CURSO DE REPETICIÓN" HASTA CON UN TOTAL DE 32 CREDITOS COMO CARGA\nMÁXIMA, PARA PODER REGULARIZAR EL PROCESO EDUCATIVO DEL SOLICITANTE EN TIEMPO\nY FORMA PERMITIDA', 'Cursar la materia de Taller de Ética en condición de curso\nde repetición', 'Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba', 'ACCIÓN NO PERMITIDA DE ACUERDO AL LINEAMIENTO PARA LA EVALUACIÓN Y\nACREDITACIÓN DE ASIGNATURAS VERSIÓN 1.0 PLANES DE ESTUDIO 2009-2010 DEL TECNM\nEN SU APARTADO 4.5.13', 'ITSAL.DIR.2017/0065', 'ITSAL.DIR.SAC.CACAD.2018/004', '1', 1),
(26, 'Aceptado', 'Cursar la materia de Fundamentos de Programación en condición de curso de repetición', 'Solicitud de curso de repecion', 'Veracruz', '2018-01-20 00:00:00', 'Solicitud de curso de repecion', '', '', 3, 6, 2, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, 1),
(27, 'Aceptado', 'Cursar la materia de Fundamentos de Gestión Empresarial', 'Solicitud de curso de repetición', 'Veracruz', '2018-01-20 00:00:00', 'Solicitud de curso de repetición', '', '', 3, 6, 4, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, 1),
(28, 'Aceptado', 'Cursar la materia de Contabilidad y costos en condición de curso de repetición', 'Solicitud de curso de repetición', 'Veracruz', '2018-01-20 00:00:00', 'Solicitud de curso de repetición', '', '', 4, 7, 2, 'DATOS QUE SE DEBEN INGRESAR DE ACUERDO A LA RESPUESTA PARA ESTE CASO DE SOLICITUD', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS QUE SE DEBEN INGRESAR DE ACUERDO AL LINEAMIENTO PARA ESTE CASO DE SOLCITUD', 'ITSAL.DIR.2017/0065', NULL, NULL, 7),
(29, 'Aceptado', 'Cursar la materia de Matemáticas Aplicadas a Comunicaciones en condición de curso de repetición', 'Solicitud de curso de repetición', 'Veracruz', '2018-01-20 00:00:00', 'Solicitud de curso de repetición', '', '', 5, 8, 1, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', 'Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba Motivos de prueba', 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2018/0666', 'ITSAL.DIR.SAC.CACAD.2018/005', '0', NULL),
(33, 'Aceptado', 'Cursar la materia de Fundamentos de Investigacion en condicion de curso de repeticion', 'materia en curso de repeticion', 'México', '2018-01-30 00:00:00', 'materia en curso de repeticion', '', '', 3, 6, 5, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', 'probando', 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, 1),
(34, 'Aceptado', 'Cursar la materia de Desarrollo Humano en condicion de curso de repeticion', 'curso de repeticion', 'México', '2018-01-30 00:00:00', 'curso de repeticion', '', '', 3, 6, 5, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, 1),
(46, 'Aceptado', 'Cursar la materia de circuitos electricos 1 en codicion de adelantar materias', 'Sobre Créditos', 'México', '2018-02-02 00:00:00', 'Sobre Créditos4', '', '', 2, 5, 7, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, NULL),
(47, 'Aceptado', 'Cursar la materia de Electromagnetismo en codicion de Adelantar asignaturas (sobre créditos)', 'Adelantar asignaturas (sobre créditos)', 'México', '2018-02-03 00:00:00', 'Adelantar asignaturas (sobre créditos)', '', '', 1, 4, 6, 'DE LO ANTERIOR, ESTE COMITE SI RECOMIENDA QUE CURSE LA ASIGNATURA EN CODICIÓN\nDE "CURSO DE ADELANTAR MATERIAS SOBRE CREDITOS " HASTA CON UN TOTAL DE 32 CREDITOS COMO CARGA MÁXIMA, PARA PODER REGULARIZAR EL PROCESO EDUCATIVO DEL SOLICITANTE EN TIEMPO Y FORMA PERMITIDA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'ACCIÓN NO PERMITIDA DE ACUERDO AL LINEAMIENTO PARA LA EVALUACIÓN Y ACREDITACIÓN DE ASIGNATURAS VERSIÓN 1.0 PLANES DE ESTUDIO 2009-2010 DEL TECNM EN SU APARTADO 4.5.13', 'ITSAL.DIR.2017/0065', NULL, NULL, NULL),
(48, 'Aceptado', 'Cursar la materia de Electromagnetismo en codicion de Curso especial', 'Curso especial', 'México', '2018-02-03 00:00:00', 'Curso especial', '', '', 5, 8, 6, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, NULL),
(54, 'Pendiente', 'cursar la materia de Taller de investigacion 1 en condicion de solicitud de sobre creditos', 'materia de sobre creditos', 'México', '2018-02-14 00:00:00', 'sobre creditos', '', '', 3, 6, 8, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2017/0065', NULL, NULL, 6),
(55, 'Pendiente', 'Cursar la materia de fundamentos de investigacion en condicion de curso especial', 'Cursar la materia de fundamentos de investigacion en condicion de curso especial', 's', '2018-02-14 00:00:00', 'Cursar la materia de fundamentos de investigacion en condicion de curso especial', 'z', 'z', 3, 6, 8, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'Datos de prueba Datos de prueba  Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba Datos de prueba', NULL, 'DATOS DE PRUEBA DATOS DE PRUEBA  DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA DATOS DE PRUEBA', 'ITSAL.DIR.2018/066', NULL, NULL, 6),
(58, 'Pendiente', 'llevar la materia de Taller de investigacion 1 en codicion de sobre creditos', 'solicitud de sobre creditos', 'Mecixo', '2018-02-21 00:00:00', 'solicitud de sobre creditos', '', '', 6, 9, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcapitulos`
--

CREATE TABLE IF NOT EXISTS `subcapitulos` (
`id_subcapitulo` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `descripcion` text,
  `id_capitulo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id_usuario` int(11) unsigned NOT NULL,
  `id_personal` int(10) unsigned DEFAULT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `correo` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `id_personal`, `nombre`, `apellidos`, `correo`, `password`, `estado`, `token`) VALUES
(1, NULL, 'Luis', 'Moreno', 'luis@hotmail.com', '$2y$10$rhaGkVaAXpxfIaTRbduBUu7DpJ.8U69zIYNGJmcHuy0fMQC24y0C6', 1, NULL),
(10, 10, 'Victor Hugo', 'Vargas Contreras', 'victor@hotmail.com', '$2y$10$nhqPBpB/pCdXexAMYF3.5.BNlxr1CyiTTx9P8bprfwebEHsyyKZCa', 1, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acapite`
--
ALTER TABLE `acapite`
 ADD PRIMARY KEY (`id_acapite`), ADD KEY `fk_acapite_subcapitulos1_idx` (`id_subcapitulo`);

--
-- Indices de la tabla `alumnos`
--
ALTER TABLE `alumnos`
 ADD PRIMARY KEY (`id_alumno`), ADD KEY `fk_alumnos_carreras_idx` (`clave_oficial`), ADD KEY `fk_alumnos_semestres1_idx` (`id_semestre`);

--
-- Indices de la tabla `capitulos`
--
ALTER TABLE `capitulos`
 ADD PRIMARY KEY (`id_capitulo`);

--
-- Indices de la tabla `carreras`
--
ALTER TABLE `carreras`
 ADD PRIMARY KEY (`clave_oficial`);

--
-- Indices de la tabla `cat_cargo_personal`
--
ALTER TABLE `cat_cargo_personal`
 ADD PRIMARY KEY (`id_cargo_personal`);

--
-- Indices de la tabla `cat_comite`
--
ALTER TABLE `cat_comite`
 ADD PRIMARY KEY (`id_comite`);

--
-- Indices de la tabla `cat_departamentos`
--
ALTER TABLE `cat_departamentos`
 ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `cat_evaluacion_materias`
--
ALTER TABLE `cat_evaluacion_materias`
 ADD PRIMARY KEY (`aprobada`);

--
-- Indices de la tabla `cat_status_materia`
--
ALTER TABLE `cat_status_materia`
 ADD PRIMARY KEY (`status`);

--
-- Indices de la tabla `cat_tipo_materia`
--
ALTER TABLE `cat_tipo_materia`
 ADD PRIMARY KEY (`tipo_materia`);

--
-- Indices de la tabla `cat_tipo_solicitud`
--
ALTER TABLE `cat_tipo_solicitud`
 ADD PRIMARY KEY (`id_tipo_solicitud`);

--
-- Indices de la tabla `kardex`
--
ALTER TABLE `kardex`
 ADD PRIMARY KEY (`id_kardex`), ADD KEY `fk_kardex_alumnos1_idx` (`id_alumno`), ADD KEY `fk_kardex_carreras1_idx` (`clave_oficial`), ADD KEY `fk_kardex_semestres1_idx` (`id_semestre`), ADD KEY `fk_kardex_periodos_escolares1_idx` (`id_periodo_escolar`);

--
-- Indices de la tabla `materias`
--
ALTER TABLE `materias`
 ADD PRIMARY KEY (`materia`);

--
-- Indices de la tabla `periodos_escolares`
--
ALTER TABLE `periodos_escolares`
 ADD PRIMARY KEY (`id_periodo_escolar`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
 ADD PRIMARY KEY (`id_personal`), ADD KEY `fk_personal_cat_cargo_personal1_idx` (`id_cargo_personal`), ADD KEY `fk_personal_cat_comite1_idx` (`id_comite`), ADD KEY `fk_personal_cat_departamentos1_idx` (`id_departamento`);

--
-- Indices de la tabla `rel_datos_materia_tipo_solicitud`
--
ALTER TABLE `rel_datos_materia_tipo_solicitud`
 ADD PRIMARY KEY (`id_datos_materia_tipo_solicitud`), ADD KEY `fk_rel_datos_materia_tipo_solicitud_kardex1_idx` (`id_kardex`), ADD KEY `fk_rel_datos_materia_tipo_solicitud_solicitudes1_idx` (`num_solicitud`), ADD KEY `fk_rel_datos_materia_tipo_solicitud_cat_tipo_solicitud1_idx` (`id_tipo_solicitud`), ADD KEY `fk_rel_datos_materia_tipo_solicitud_carreras1_idx` (`clave_oficial`);

--
-- Indices de la tabla `rel_kardex_materias`
--
ALTER TABLE `rel_kardex_materias`
 ADD KEY `fk_rel_kardex_materias_kardex1_idx` (`id_kardex`), ADD KEY `fk_rel_kardex_materias_materias1_idx` (`materia`), ADD KEY `fk_rel_kardex_materias_cat_evaluacion_materias1_idx` (`aprobada`), ADD KEY `fk_rel_kardex_materias_cat_status_materia1_idx` (`status`), ADD KEY `fk_rel_kardex_materias_cat_tipo_materia1_idx` (`tipo_materia`);

--
-- Indices de la tabla `rel_materias_carreras`
--
ALTER TABLE `rel_materias_carreras`
 ADD KEY `FK_rel_materias_carreras_idx` (`clave_oficial`), ADD KEY `fk_rel_materias_carreras_semestres1_idx` (`id_semestre`), ADD KEY `fk_re_materias_carreras2_idx` (`materia`);

--
-- Indices de la tabla `rel_solicitud_kardex`
--
ALTER TABLE `rel_solicitud_kardex`
 ADD PRIMARY KEY (`id_solicitud_kardex`), ADD KEY `fk_rel_solicitud_kardex_kardex1_idx` (`id_kardex`), ADD KEY `fk_rel_solicitud_kardex_solicitudes1_idx` (`num_solicitud`);

--
-- Indices de la tabla `reuniones`
--
ALTER TABLE `reuniones`
 ADD PRIMARY KEY (`id_reunion`), ADD KEY `fk_reuniones_periodos_escolares1_idx` (`id_periodo_escolar`);

--
-- Indices de la tabla `semestres`
--
ALTER TABLE `semestres`
 ADD PRIMARY KEY (`id_semestre`);

--
-- Indices de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
 ADD PRIMARY KEY (`num_solicitud`), ADD KEY `fk_solicitudes_semestres1_idx` (`id_semestre`), ADD KEY `fk_solicitudes_periodos_escolares1_idx` (`id_periodo_escolar`), ADD KEY `fk_solicitudes_alumnos_idx` (`id_alumno`), ADD KEY `fk_solicitudes_reuniones1_idx` (`id_reunion`);

--
-- Indices de la tabla `subcapitulos`
--
ALTER TABLE `subcapitulos`
 ADD PRIMARY KEY (`id_subcapitulo`), ADD KEY `fk_subcapitulos_capitulos1_idx` (`id_capitulo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id_usuario`), ADD KEY `fk_usuarios_personal1_idx` (`id_personal`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acapite`
--
ALTER TABLE `acapite`
MODIFY `id_acapite` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `alumnos`
--
ALTER TABLE `alumnos`
MODIFY `id_alumno` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `capitulos`
--
ALTER TABLE `capitulos`
MODIFY `id_capitulo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `cat_cargo_personal`
--
ALTER TABLE `cat_cargo_personal`
MODIFY `id_cargo_personal` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `cat_comite`
--
ALTER TABLE `cat_comite`
MODIFY `id_comite` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `cat_departamentos`
--
ALTER TABLE `cat_departamentos`
MODIFY `id_departamento` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `cat_tipo_solicitud`
--
ALTER TABLE `cat_tipo_solicitud`
MODIFY `id_tipo_solicitud` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `kardex`
--
ALTER TABLE `kardex`
MODIFY `id_kardex` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT de la tabla `periodos_escolares`
--
ALTER TABLE `periodos_escolares`
MODIFY `id_periodo_escolar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `personal`
--
ALTER TABLE `personal`
MODIFY `id_personal` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `rel_datos_materia_tipo_solicitud`
--
ALTER TABLE `rel_datos_materia_tipo_solicitud`
MODIFY `id_datos_materia_tipo_solicitud` bigint(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `rel_solicitud_kardex`
--
ALTER TABLE `rel_solicitud_kardex`
MODIFY `id_solicitud_kardex` bigint(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `reuniones`
--
ALTER TABLE `reuniones`
MODIFY `id_reunion` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `semestres`
--
ALTER TABLE `semestres`
MODIFY `id_semestre` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
MODIFY `num_solicitud` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT de la tabla `subcapitulos`
--
ALTER TABLE `subcapitulos`
MODIFY `id_subcapitulo` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id_usuario` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acapite`
--
ALTER TABLE `acapite`
ADD CONSTRAINT `fk_acapite_subcapitulos1` FOREIGN KEY (`id_subcapitulo`) REFERENCES `subcapitulos` (`id_subcapitulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `alumnos`
--
ALTER TABLE `alumnos`
ADD CONSTRAINT `fk_alumnos_carreras` FOREIGN KEY (`clave_oficial`) REFERENCES `carreras` (`clave_oficial`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `kardex`
--
ALTER TABLE `kardex`
ADD CONSTRAINT `fk_kardex_alumnos1` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_kardex_carreras1` FOREIGN KEY (`clave_oficial`) REFERENCES `carreras` (`clave_oficial`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_kardex_periodos_escolares1` FOREIGN KEY (`id_periodo_escolar`) REFERENCES `periodos_escolares` (`id_periodo_escolar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_kardex_semestres1` FOREIGN KEY (`id_semestre`) REFERENCES `semestres` (`id_semestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `personal`
--
ALTER TABLE `personal`
ADD CONSTRAINT `fk_personal_cat_cargo_personal1` FOREIGN KEY (`id_cargo_personal`) REFERENCES `cat_cargo_personal` (`id_cargo_personal`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_personal_cat_comite1` FOREIGN KEY (`id_comite`) REFERENCES `cat_comite` (`id_comite`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_personal_cat_departamentos1` FOREIGN KEY (`id_departamento`) REFERENCES `cat_departamentos` (`id_departamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rel_datos_materia_tipo_solicitud`
--
ALTER TABLE `rel_datos_materia_tipo_solicitud`
ADD CONSTRAINT `fk_rel_datos_materia_tipo_solicitud_carreras1` FOREIGN KEY (`clave_oficial`) REFERENCES `carreras` (`clave_oficial`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_rel_datos_materia_tipo_solicitud_cat_tipo_solicitud1` FOREIGN KEY (`id_tipo_solicitud`) REFERENCES `cat_tipo_solicitud` (`id_tipo_solicitud`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_datos_materia_tipo_solicitud_kardex1` FOREIGN KEY (`id_kardex`) REFERENCES `kardex` (`id_kardex`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_datos_materia_tipo_solicitud_solicitudes1` FOREIGN KEY (`num_solicitud`) REFERENCES `solicitudes` (`num_solicitud`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rel_kardex_materias`
--
ALTER TABLE `rel_kardex_materias`
ADD CONSTRAINT `fk_rel_kardex_materias_cat_evaluacion_materias1` FOREIGN KEY (`aprobada`) REFERENCES `cat_evaluacion_materias` (`aprobada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_kardex_materias_cat_status_materia1` FOREIGN KEY (`status`) REFERENCES `cat_status_materia` (`status`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_kardex_materias_cat_tipo_materia1` FOREIGN KEY (`tipo_materia`) REFERENCES `cat_tipo_materia` (`tipo_materia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_kardex_materias_kardex1` FOREIGN KEY (`id_kardex`) REFERENCES `kardex` (`id_kardex`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_kardex_materias_materias1` FOREIGN KEY (`materia`) REFERENCES `materias` (`materia`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Filtros para la tabla `rel_materias_carreras`
--
ALTER TABLE `rel_materias_carreras`
ADD CONSTRAINT `FK_rel_materias_carreras` FOREIGN KEY (`clave_oficial`) REFERENCES `carreras` (`clave_oficial`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_re_materias_carreras2` FOREIGN KEY (`materia`) REFERENCES `materias` (`materia`) ON DELETE NO ACTION ON UPDATE CASCADE,
ADD CONSTRAINT `fk_rel_materias_carreras_semestres1` FOREIGN KEY (`id_semestre`) REFERENCES `semestres` (`id_semestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rel_solicitud_kardex`
--
ALTER TABLE `rel_solicitud_kardex`
ADD CONSTRAINT `fk_rel_solicitud_kardex_kardex1` FOREIGN KEY (`id_kardex`) REFERENCES `kardex` (`id_kardex`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_rel_solicitud_kardex_solicitudes1` FOREIGN KEY (`num_solicitud`) REFERENCES `solicitudes` (`num_solicitud`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `reuniones`
--
ALTER TABLE `reuniones`
ADD CONSTRAINT `fk_reuniones_periodos_escolares1` FOREIGN KEY (`id_periodo_escolar`) REFERENCES `periodos_escolares` (`id_periodo_escolar`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `solicitudes`
--
ALTER TABLE `solicitudes`
ADD CONSTRAINT `fk_solicitudes_alumnos2` FOREIGN KEY (`id_alumno`) REFERENCES `alumnos` (`id_alumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_solicitudes_periodos_escolares1` FOREIGN KEY (`id_periodo_escolar`) REFERENCES `periodos_escolares` (`id_periodo_escolar`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_solicitudes_reuniones1` FOREIGN KEY (`id_reunion`) REFERENCES `reuniones` (`id_reunion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_solicitudes_semestres1` FOREIGN KEY (`id_semestre`) REFERENCES `semestres` (`id_semestre`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `subcapitulos`
--
ALTER TABLE `subcapitulos`
ADD CONSTRAINT `fk_subcapitulos_capitulos1` FOREIGN KEY (`id_capitulo`) REFERENCES `capitulos` (`id_capitulo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
ADD CONSTRAINT `fk_usuarios_personal1` FOREIGN KEY (`id_personal`) REFERENCES `personal` (`id_personal`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

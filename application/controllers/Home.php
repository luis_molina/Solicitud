<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		
	}

	public function index()
	{

		$this->load->view('Solicitudes/home');
		
	}

    public function getComitePersonal()
    {

        $this->session->userdata('id_comite');

        $id_comite = $this->session->userdata('id_comite');

        echo json_encode($id_comite);
        
    }


	public function cargarTablaSolicitudes()
    {
            //echo json_encode($_REQUEST);

             $this->load->model('Solicitudes/Model_Solicitudes');
             $datos = $this->Model_Solicitudes->cargarTablaSolicitudes($_REQUEST);

            echo json_encode($datos);

    }

	public function cerrarSesion()
	{

		   $this->session->sess_destroy();
			
			$datos["sesion"] = false;
			$datos["base_url"] = base_url()."Login";

			echo json_encode($datos);
	}	



	 public function getCountSolicitudesEstudiante()
    {
     
     	$id_alumno = $_REQUEST["id_alumno"];

        $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->getCountSolicitudesEstudiante($id_alumno);

        echo json_encode($datos);

    }


     public function getCountSolicitudesEstudianteDictamen()
    {
     
        $id_alumno = $_REQUEST["id_alumno"];

        $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->getCountSolicitudesEstudianteDictamen($id_alumno);

        echo json_encode($datos);

    }


     public function solicitudPDFEstudiante()
    {
     
     	$num_solicitud = $_REQUEST["num_solicitud"];


     	 $this->load->model('Solicitudes/Model_Solicitudes');

         $datos = $this->Model_Solicitudes->getDatosSolicitudPDF_Estudiante($num_solicitud);

         $nombreCompleto = $datos["solicitudes"][0]->nombreCompleto;

         $arrayDatos["lugarYfecha"] = $datos["solicitudes"][0]->lugarYfecha;
         $arrayDatos["asunto"] = $datos["solicitudes"][0]->asunto;
         $arrayDatos["nombreCompleto"] = $datos["solicitudes"][0]->nombreCompleto;
         $arrayDatos["nombre_semestre"] = $datos["solicitudes"][0]->nombre_semestre;
         $arrayDatos["nombre_carrera"] = $datos["solicitudes"][0]->nombre_carrera;
         $arrayDatos["no_de_control"] = $datos["solicitudes"][0]->no_de_control;
         $arrayDatos["observacion"] = $datos["solicitudes"][0]->observacion;
         $arrayDatos["motivos_academicos"] = $datos["solicitudes"][0]->motivos_academicos;
         $arrayDatos["motivos_personales"] = $datos["solicitudes"][0]->motivos_personales;
         $arrayDatos["otros"] = $datos["solicitudes"][0]->otros;
         $arrayDatos["secretario"] = $datos["solicitudes"][0]->secretario;
         $arrayDatos["departamento"] = $datos["solicitudes"][0]->departamento;

         //$html = $datos;
     	 $html =  $this->load->view('Solicitudes/viewSolicitudPDF_Estudiante',$arrayDatos,true);


		$hoy = date("dmyhis");


        $pdfFilePath = "Solicitud_".$nombreCompleto."_".$hoy.".pdf";
 
        //load mPDF library
        $this->load->library('M_pdf');
        $mpdf = new mPDF('c', 'A4-L'); 
 		//$mpdf->WriteHTML($num_solicitud);
		$mpdf->Output($pdfFilePath, "D");
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 

    }


     public function dictamenPDFEstudiante()
    {
     
        $num_solicitud = $_REQUEST["num_solicitud"];


         $this->load->model('Solicitudes/Model_Solicitudes');

         $datos = $this->Model_Solicitudes->getDatosDictamenPDF_Estudiante($num_solicitud);

         $nombreCompleto = $datos["solicitudes"][0]->nombreCompleto;

         $arrayDatos["fecha"] = $datos["solicitudes"][0]->fecha;
         $arrayDatos["lugar"] = $datos["solicitudes"][0]->lugar;
         $arrayDatos["asunto"] = $datos["solicitudes"][0]->asunto;
         $arrayDatos["nombreCompleto"] = $datos["solicitudes"][0]->nombreCompleto;
         $arrayDatos["no_de_control"] = $datos["solicitudes"][0]->no_de_control;
         $arrayDatos["respuesta_solicitud_dictamen"] = $datos["solicitudes"][0]->respuesta_solicitud_dictamen;
         $arrayDatos["nombre_semestre"] = $datos["solicitudes"][0]->nombre_semestre;
         $arrayDatos["nombre_carrera"] = $datos["solicitudes"][0]->nombre_carrera;
         $arrayDatos["subdirector"] = $datos["solicitudes"][0]->subdirector;
         $arrayDatos["director"] = $datos["solicitudes"][0]->director;
         $arrayDatos["status"] = $datos["solicitudes"][0]->status;
         $arrayDatos["no_dictamen"] = $datos["solicitudes"][0]->no_dictamen;
       
         //$html = $datos;
       $html =  $this->load->view('Solicitudes/viewDictamenPDF_Estudiante',$arrayDatos,true);


        $hoy = date("dmyhis");


        $pdfFilePath = "Dictamen_".$nombreCompleto."_".$hoy.".pdf";

        // $html='<html>
        //         <head>
        //         <style>@page {
        //         margin-top: 0cm;
        //         margin-bottom: 0cm;
        //         margin-left: 0cm;
        //         margin-right: 0cm;
        //         }</style>
        //         </head>
        //         <body>
        //             <div style="border:1px solid red" >hola mundo jeje</div>
        //         </body>
        //         </html>
        //         ';
 
        //load mPDF library
        $this->load->library('M_pdf');
        //
         //$mpdf = new mPDF('c', 'A4-L'); 

          $mpdf = new mPDF('', 'A4'); 

        //$mpdf = new mPDF('', 'A4', 0, '', 0, 0, 0, 0, 0, 0); 

        // $mpdf = new mPDF('utf-8','A4','','','15','15','28','18'); 

        // $this->mpdf->mPDF('utf-8','A4','','','15','15','28','18')

        //$mpdf=new mPDF('utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0);

        // $mpdf = new mPDF('',    // mode - default ''
        //                  'A4',    // format - A4, for example, default ''
        //                  0,     // font size - default 0
        //                  '',    // default font family
        //                  0,    // margin_left
        //                  0,    // margin right
        //                  0,     // margin top
        //                  0,    // margin bottom
        //                  0,     // margin header
        //                  0,     // margin footer
        //                  'L');  // L - landscape, P - portrait

        // $mpdf= new mPDF('utf-8', 'Letter', 0, '', 0, 0, 0, 0, 0, 0);

        //$this->mpdf->mPDF('utf-8','A4','','','15','15','28','18'); 
        


        //$mpdf->WriteHTML($num_solicitud);
        // $mpdf->Output($pdfFilePath, "D");
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 

    }

    public function dictamenDirectorPDFEstudiante()
    {
     
        $num_solicitud = $_REQUEST["num_solicitud"];


         $this->load->model('Solicitudes/Model_Solicitudes');

         $datos = $this->Model_Solicitudes->getDatosDictamenDirectorPDF_Estudiante($num_solicitud);

         $nombreCompleto = $datos["solicitudes"][0]->nombreCompleto;

         $arrayDatos["fecha"] = $datos["solicitudes"][0]->fecha;
         $arrayDatos["lugar"] = $datos["solicitudes"][0]->lugar;
         $arrayDatos["fecha_reunion"] = $datos["solicitudes"][0]->fecha_reunion;
         $arrayDatos["no_acta"] = $datos["solicitudes"][0]->no_acta;
         $arrayDatos["no_libro"] = $datos["solicitudes"][0]->no_libro;
         $arrayDatos["nombreCompleto"] = $datos["solicitudes"][0]->nombreCompleto;
         $arrayDatos["nombre_carrera"] = $datos["solicitudes"][0]->nombre_carrera;
         $arrayDatos["no_de_control"] = $datos["solicitudes"][0]->no_de_control;
         $arrayDatos["peticion_solicitud"] = $datos["solicitudes"][0]->peticion_solicitud;
         $arrayDatos["status"] = $datos["solicitudes"][0]->status;
         $arrayDatos["nombre_semestre"] = $datos["solicitudes"][0]->nombre_semestre;
         $arrayDatos["no_dictamen"] = $datos["solicitudes"][0]->no_dictamen;
        $arrayDatos["presidente"] = $datos["solicitudes"][0]->presidente;
        $arrayDatos["director"] = $datos["solicitudes"][0]->director;

        $arrayDatos["respuesta_solicitud_acta"] = $datos["solicitudes"][0]->respuesta_solicitud_acta;
        $arrayDatos["motivos_solicitud_dictamen"] = $datos["solicitudes"][0]->motivos_solicitud_dictamen;
        $arrayDatos["no_recomendacion_dictamen"] = $datos["solicitudes"][0]->no_recomendacion_dictamen;
        $arrayDatos["recomienda"] = $datos["solicitudes"][0]->recomienda;         
         
       
         //$html = $datos;
         $html =  $this->load->view('Solicitudes/viewDictamenDirectorPDF_Estudiante',$arrayDatos,true);


        $hoy = date("dmyhis");


        $pdfFilePath = "Dictamen_".$nombreCompleto."_".$hoy.".pdf";
 
        //load mPDF library
        $this->load->library('M_pdf');
        $mpdf = new mPDF('', 'A4'); 
        //$mpdf->WriteHTML($num_solicitud);
        $mpdf->Output($pdfFilePath, "D");
       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
        $this->m_pdf->pdf->Output($pdfFilePath, "D"); 

    }


     public function evaluarSolicitudEstudiante()
    {
     
     	$num_solicitud = $_REQUEST["num_solicitud"];
     	$status = $_REQUEST["status"];

     	

        $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->evaluarSolicitudEstudiante($num_solicitud,$status);

        echo json_encode($datos);

    }


    public function getInfoMotivosSolicitud()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->getInfoMotivosSolicitud($num_solicitud);

        echo json_encode($datos);

    }


    public function getDatosActaSolicitud()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->getDatosActaSolicitud($num_solicitud);

        echo json_encode($datos);

    }


    public function guardarDatosRespuestaActaSolicitud()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];
        $verificacion_manual_lineamiento =  $_REQUEST['verificacion_manual_lineamiento'];
        $respuesta_solicitud_acta =  $_REQUEST['respuesta_solicitud_acta'];

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->guardarDatosRespuestaActaSolicitud($num_solicitud,$verificacion_manual_lineamiento,$respuesta_solicitud_acta);

        echo json_encode($datos);

    }




     public function guardarDatosRespuestaDictamenSolicitud()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];
        $respuesta_solicitud_dictamen =  $_REQUEST['respuesta_solicitud_dictamen'];
        $no_dictamen =  $_REQUEST['no_dictamen'];
        

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->guardarDatosRespuestaDictamenSolicitud($num_solicitud,$respuesta_solicitud_dictamen,$no_dictamen);

        echo json_encode($datos);

    }


     public function guardarDatosRespuestaDictamenDirectSolicitud()
    {

        $num_solicitud =  $_REQUEST['num_solicitud'];
        $recomienda =  $_REQUEST['recomienda'];
        $respuesta_solicitud_acta =  $_REQUEST['respuesta_solicitud_acta'];
        $motivos_solicitud_dictamen =  $_REQUEST['motivos_solicitud_dictamen'];
        $no_recomendacion_dictamen =  $_REQUEST['no_recomendacion_dictamen'];


         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->guardarDatosRespuestaDictamenDirectSolicitud($num_solicitud,$recomienda,$respuesta_solicitud_acta,$motivos_solicitud_dictamen,$no_recomendacion_dictamen);

        echo json_encode($datos);

    }
    



     public function modificarSolcitudEstudiante()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];
        $motivos_academicos =  $_REQUEST['motivos_academicos'];
        $motivos_personales =  $_REQUEST['motivos_personales'];
        $otros =  $_REQUEST['otros'];

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->modificarSolcitudEstudiante($num_solicitud,$motivos_academicos,$motivos_personales,$otros);

        echo json_encode($datos);

    }


    public function eliminarSolicitudEstudiante()
    {
        $num_solicitud =  $_REQUEST['num_solicitud'];

         $this->load->model('Solicitudes/Model_Solicitudes');
         $datos = $this->Model_Solicitudes->eliminarSolicitudEstudiante($num_solicitud);

        echo json_encode($datos);

    }

    public function solicitudHistorial()
    {

        $this->load->view('Solicitudes/view_SolicitudHistorial');
        
    }

     public function solicitudDictamen()
    {

        $this->load->view('Solicitudes/view_solicitudDictamen');
        
    }

     public function cargarTablaSolicitudesHistorial()
    {

        $this->load->model('Solicitudes/Model_Solicitudes');
        $datos = $this->Model_Solicitudes->cargarTablaSolicitudesHistorial($_REQUEST);

        echo json_encode($datos);
        
    }







}

?>
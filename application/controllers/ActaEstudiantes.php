<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ActaEstudiantes extends CI_Controller 
{

	 public function __construct()
    {
      parent::__construct();
    
    }

     public function acta()
    {
     
       $this->load->view('ActaEstudiantes/View_ActaEstudiantes.php');

    }


    public function generarActaEstudiantes()
    {

    	     $id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
           $id_reunion = $_REQUEST['id_reunion'];


            $this->load->model('ActaEstudiantes/Model_ActaEstudiantes'); 
            $datos1 = $this->Model_ActaEstudiantes->getInfoReunion($id_periodo_escolar,$id_reunion);


            for($x = 0 ; $x < $datos1["cantidad_reuniones"]; $x++) 
            {
                                   
                    $id_reunion = $datos1["Reunion"][$x]->id_reunion;

                    $this->load->model('ActaEstudiantes/Model_ActaEstudiantes'); 
                    $datos = $this->Model_ActaEstudiantes->generarActaEstudiantes($id_periodo_escolar,$id_reunion);

                    $array["Reunion"] = $datos1["Reunion"][$x];
                    $array["Acta"] = $datos["Acta"];

                    // var_dump($id_reunion);

                     //$html = $datos;
                     $html =  $this->load->view('ActaEstudiantes/viewActaPDF_Estudiante',$array,true);

                    
                    $hoy = date("dmyhis");


                    $pdfFilePath = "Acta_".$datos["Acta"][0]->periodo_escolar."_".$id_reunion."_".$hoy.".pdf";
             
                    //load mPDF library
                    $this->load->library('M_pdf');
                    $mpdf = new mPDF('c', 'A4-L'); 
                    //$mpdf->WriteHTML($num_solicitud);
                      $mpdf->Output($pdfFilePath, "D");
                   //generate the PDF from the given html
                    $this->m_pdf->pdf->WriteHTML($html);
             
                    //download it.
                    $this->m_pdf->pdf->Output($pdfFilePath, "D"); 


            }

     
      	



    }


     public function verificarPeriodoEscolarReunion()
    {

        $id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
     
        $this->load->model('ActaEstudiantes/Model_ActaEstudiantes'); 
        $datos = $this->Model_ActaEstudiantes->verificarPeriodoEscolarReunion($id_periodo_escolar);

        echo json_encode($datos);


    }



}
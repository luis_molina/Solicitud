<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiantes extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		
	}

	public function index()
	{

		$this->load->view('SolicitudEstudiantes/View_SolicitudEstudiantes');
		
	}


	public function verifyNoControlGetInfo()
	{

		$no_de_control = $_REQUEST['no_de_control'];

		$this->load->model('SolicitudEstudiantes/Model_SolicitudEstudiantes');
         $resultado_query = $this->Model_SolicitudEstudiantes->verifyNoControlGetInfo($no_de_control);

        echo json_encode($resultado_query);
		
	}

	public function getDatosSecretario()
	{		

		$this->load->model('SolicitudEstudiantes/Model_SolicitudEstudiantes');
         $resultado_query = $this->Model_SolicitudEstudiantes->getDatosSecretario();

        echo json_encode($resultado_query);
		
	}


	
	public function cargarSelectPeriodosEscolares()
	{


		$this->load->model('SolicitudEstudiantes/Model_SolicitudEstudiantes'); 
		$resultado_query = $this->Model_SolicitudEstudiantes->cargarSelectPeriodosEscolares();

		echo json_encode($resultado_query);

	}


	public function enviarSolicitudEstudiante()
	{
		$no_de_control = $_REQUEST['no_de_control'];
		$datosAlumno = $_REQUEST['datosAlumno'];
		$datosSolicitud = $_REQUEST['datosSolicitud'];
		$datosMateriaCursoEspecial = $_REQUEST['datosMateriaCursoEspecial'];
		$datosMateriaCursoRepeticion = $_REQUEST['datosMateriaCursoRepeticion'];
		$datosMateria2CursosEspecAsig = $_REQUEST['datosMateria2CursosEspecAsig'];
		$datosMateria2CursosEspecVerano = $_REQUEST['datosMateria2CursosEspecVerano'];
		$datosMateriaCursoSobreCreditos = $_REQUEST['datosMateriaCursoSobreCreditos'];
		$datosMateriaCursoEspecialResidencia = $_REQUEST['datosMateriaCursoEspecialResidencia'];
		$datosBajaDefinitiva = $_REQUEST['datosBajaDefinitiva'];

		

		$this->load->model('SolicitudEstudiantes/Model_SolicitudEstudiantes'); 
		$datos = $this->Model_SolicitudEstudiantes->enviarSolicitudEstudiante($no_de_control,$datosAlumno,$datosSolicitud,$datosMateriaCursoEspecial,$datosMateriaCursoRepeticion,$datosMateria2CursosEspecAsig,$datosMateria2CursosEspecVerano,$datosMateriaCursoSobreCreditos,$datosMateriaCursoEspecialResidencia,$datosBajaDefinitiva);

		echo json_encode($datos);

	}


	 public function cargarSelectCarreras()
	{


		$this->load->model('CarrerasMaterias/Model_CarrerasMaterias'); 
		$datosSelect = $this->Model_CarrerasMaterias->cargarSelectCarreras();

		echo json_encode($datosSelect);

	}

	 public function cargarSelectSemestres()
	{
		
		$this->load->model('CarrerasMaterias/Model_CarrerasMaterias'); 
		$datosSelect = $this->Model_CarrerasMaterias->cargarSelectSemestres();



		echo json_encode($datosSelect);

	}


	public function cargarSelectSemestresForSolicitud()
	{
		$no_de_control = $_REQUEST['no_de_control'];
		$tipo_solicitud = $_REQUEST['tipo_solicitud'];

		$this->load->model('CarrerasMaterias/Model_CarrerasMaterias'); 
		$datosSelect = $this->Model_CarrerasMaterias->cargarSelectSemestresForSolicitud($no_de_control,$tipo_solicitud);



		echo json_encode($datosSelect);

	}



	public function cargarSelectMateriaSemestre()
	{
		$no_de_control = $_REQUEST['no_de_control'];
		$id_semestre = $_REQUEST['id_semestre'];
		$clave_oficial = $_REQUEST['clave_oficial'];	
		

		$this->load->model('SolicitudEstudiantes/Model_SolicitudEstudiantes'); 
		$datos = $this->Model_SolicitudEstudiantes->cargarSelectMateriaSemestre($no_de_control,$id_semestre,$clave_oficial);

		echo json_encode($datos);

	}





}

?>
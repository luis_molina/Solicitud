<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Materias extends CI_Controller 
{

    public function __construct()
    {
      parent::__construct();
    
    }

    public function registrar()
    {
     
       $this->load->view('Materias/View_RegistrarMaterias.php');

    }

     public function guardarMaterias()
    {
    	

          $materia = $_REQUEST['materia'];
        	$nombre_completo_materia = $_REQUEST['nombre_completo_materia'];
        	$nombre_abreviado_materia = $_REQUEST['nombre_abreviado_materia'];
        	// $clave_oficial = $_REQUEST['clave_oficial'];
        	// $id_semestre = $_REQUEST['id_semestre'];


            $this->load->model('Materias/Model_Materias'); 
            $resultado_query = $this->Model_Materias->guardarMaterias($materia,$nombre_completo_materia,$nombre_abreviado_materia);

      		echo json_encode($resultado_query);
     

    }

    public function modificar()
    {
     
        $this->load->view('Materias/View_ModificarMaterias.php');

    }

     public function cargarTablaMaterias()
    {
     

            $this->load->model('Materias/Model_Materias');
             $datos = $this->Model_Materias->cargarTablaMaterias($_REQUEST);

            echo json_encode($datos);

    }


     public function getInfoMateria()
    {
     
           $materia = $_REQUEST['claveMateria'];

             $this->load->model('Materias/Model_Materias');
            $datos = $this->Model_Materias->getInfoMateria($materia);

            echo json_encode($datos);

    }


      public function checkModificarClaveMateria()
     {
      
          $claveMateria_new = $_REQUEST['claveMateria_new'];
          $claveMateria_origen = $_REQUEST['claveMateria_origen'];
          

          $this->load->model('Materias/Model_Materias'); 
          $resultado_query = $this->Model_Materias->checkModificarClaveMateria($claveMateria_new,$claveMateria_origen);
        

          echo json_encode($resultado_query);

      }

    public function checkClaveMateria()
    {
      
          $materia = $_REQUEST['materia'];
          

          $this->load->model('Materias/Model_Materias'); 
          $resultado_query = $this->Model_Materias->checkClaveMateria($materia);
        

          echo json_encode($resultado_query);
     

    }


       public function modificarMaterias()
      {
      

	          $claveMateria = $_REQUEST['claveMateria'];
            $claveMateriaOrigen = $_REQUEST['claveMateriaOrigen'];
	          $nombre_completo_materia = $_REQUEST['nombre_completo_materia'];
	          $nombre_abreviado_materia = $_REQUEST['nombre_abreviado_materia'];


	          $this->load->model('Materias/Model_Materias'); 
	          $resultado_query = $this->Model_Materias->modificarMaterias($claveMateria,$claveMateriaOrigen,$nombre_completo_materia,$nombre_abreviado_materia);

      		  echo json_encode($resultado_query);

      }


          public function deleteMaterias()
          {
      
              $claveMateria = $_REQUEST['claveMateria'];
              

              $this->load->model('Materias/Model_Materias'); 
              $resultado_query = $this->Model_Materias->deleteMaterias($claveMateria);
            

              echo json_encode($resultado_query);
     

         }

     




}
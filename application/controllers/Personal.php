<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller 
{

	  public function __construct()
	  {
	    parent::__construct();
	  
	  }

	  public function registrar()
	  {
	    
	     $this->load->view('Personal/view_RegistrarPersonal.php');

	  }

	  public function modificar()
	  {
	    
	     $this->load->view('Personal/view_ModificarPersonal.php');

	  }

	    public function cargarSelectCargoPersonal()
		{


			$this->load->model('Personal/Model_Personal'); 
			$datosSelect = $this->Model_Personal->cargarSelectCargoPersonal();

			echo json_encode($datosSelect);

		}


		public function cargarSelectDepartamentos()
		{


			$this->load->model('Personal/Model_Personal'); 
			$datosSelect = $this->Model_Personal->cargarSelectDepartamentos();

			echo json_encode($datosSelect);

		}

		public function cargarSelectComite()
		{


			$this->load->model('Personal/Model_Personal'); 
			$datosSelect = $this->Model_Personal->cargarSelectComite();

			echo json_encode($datosSelect);

		}


		public function guardarPersonal()
		{

			$nombre = $_REQUEST['nombre'];
			$apellidos = $_REQUEST['apellidos'];
			$profesion = $_REQUEST['profesion'];
			$id_cargo_personal = $_REQUEST['id_cargo_personal'];
			$id_departamento = $_REQUEST['id_departamento'];
			$id_comite = $_REQUEST['id_comite'];
			$fecha_inicio = $_REQUEST['fecha_inicio'];
			$fecha_fin = $_REQUEST['fecha_fin'];
			$correo = $_REQUEST['correo'];
			$password = $_REQUEST['password'];

			$this->load->model('Personal/Model_Personal');
	         $resultado_query = $this->Model_Personal->guardarPersonal($nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,$correo,$password);

	        echo json_encode($resultado_query);
			
		}


		 public function cargarTablaPersonal()
		{
		 

		        $this->load->model('Personal/Model_Personal');
		         $resultado_query = $this->Model_Personal->cargarTablaPersonal($_REQUEST);

		        echo json_encode($resultado_query);

		}


		 public function getInfoPersonal()
		{
		 
		    $id_personal = $_REQUEST["id_personal"];

		    $this->load->model('Personal/Model_Personal');

		    $resultado_query = $this->Model_Personal->getInfoPersonal($id_personal);

		    echo json_encode($resultado_query);
		
		}


		 public function checkCorreoPersonal()
		{
		 
		    $correo = $_REQUEST["correo"];

		    $this->load->model('Personal/Model_Personal');

		    $resultado_query = $this->Model_Personal->checkCorreoPersonal($correo);

		    echo json_encode($resultado_query);
		
		}


		public function checkModificarCorreo()
     	{
      
          $correo_new = $_REQUEST['correo_new'];
          $correo_origen = $_REQUEST['correo_origen'];


          // echo json_encode($correo_origen);
          // exit();
          

          $this->load->model('Personal/Model_Personal'); 
          $resultado_query = $this->Model_Personal->checkModificarCorreo($correo_new,$correo_origen);
        

          echo json_encode($resultado_query);

     	}


 		public function modificarPersonal()
 		{
 			
 			$id_personal = $_REQUEST['id_personal'];
 			$nombre = $_REQUEST['nombre'];
 			$apellidos = $_REQUEST['apellidos'];
 			$profesion = $_REQUEST['profesion'];
 			$id_cargo_personal = $_REQUEST['id_cargo_personal'];
 			$id_departamento = $_REQUEST['id_departamento'];
 			$id_comite = $_REQUEST['id_comite'];
 			$fecha_inicio = $_REQUEST['fecha_inicio'];
 			$fecha_fin = $_REQUEST['fecha_fin'];
 			$correo = $_REQUEST['correo'];
 			$password = $_REQUEST['password'];

 			$this->load->model('Personal/Model_Personal');
 	         $resultado_query = $this->Model_Personal->modificarPersonal($id_personal,$nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,$correo,$password);

 	        echo json_encode($resultado_query);
 			
 		}

 		public function eliminarPersonal()
     	{
      
          $id_personal = $_REQUEST['id_personal'];
          
          $this->load->model('Personal/Model_Personal'); 
          $resultado_query = $this->Model_Personal->eliminarPersonal($id_personal);
        

          echo json_encode($resultado_query);

     	}



  }

?>
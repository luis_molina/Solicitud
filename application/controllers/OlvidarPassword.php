<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OlvidarPassword extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
	
	}

	public function index()
	{
		
		 $this->load->view('OlvidarPassword/view_OlvidarPassword');

	}

	public function verifyEmail()
	{

		$correo = $_REQUEST['email'];

		$this->load->model('OlvidarPassword/Model_OlvidarPassword'); 
		$datosConsultaExisteMail = $this->Model_OlvidarPassword->verifyEmail($correo);
			
		if($datosConsultaExisteMail["msjConsulta"]=='OK')
		{

			$this->load->model('OlvidarPassword/Model_OlvidarPassword');

			$datosProceso = $this->Model_OlvidarPassword->enviarEmailCambioPassword($correo);


			if($datosProceso["msjConsulta"] == "OK")
			{

				$envioEmail = $this->sendMailGmail($correo,$datosProceso["token"]);


				if($envioEmail)
				{
					$datosProceso["msjConsulta"] = "OK";

					//echo json_encode($msjEnvioEmail);
				}
				else
				{
					$datosProceso['msjConsulta'] = "Error";
					//echo json_encode($msjEnvioEmail);
				}

				
			}
			else
			{
				$datosProceso['msjConsulta'] = "Error";
				
			}

			$salida = $datosProceso;
			echo json_encode($salida);


		}
		else
		{
			$salida = $datosConsultaExisteMail;
			echo json_encode($salida);
		}

		
	}


	function sendMailGmail($email,$token)
 	{

 		$this->load->library("email");
 
         //configuracion para gmail
		 $configGmail = array(
		 'protocol' => 'smtp',
		 'smtp_host' => 'ssl://smtp.gmail.com',
		 'smtp_port' => 465,
		 'smtp_user' => 'luis.molina.testing@gmail.com',
		 'smtp_pass' => 'pruebaSendEmail_1',
		 'mailtype' => 'html',
		 'charset' => 'utf-8',
		 'newline' => "\r\n"
		 );    
		 
		 //cargamos la configuración para enviar con gmail
		 $this->email->initialize($configGmail);
		 
		 $this->email->from('luis.molina.testing@gmail.com');
		 $this->email->to($email);
		 //$this->email->to("luisame@outlook.com");
		 $this->email->subject('Solicitud de cambio de contraseña');

		 $datosUsuario["email"]= $email;
		 $datosUsuario["token"]= $token; 

		 $mensaje = $this->load->view('OlvidarPassword/view_MsjEmailOlvidarPassword',$datosUsuario,TRUE);
		 // return $message;

		 $this->email->message($mensaje);

		 $envio = $this->email->send();

		 return $envio;

 	}


 	function cambiarContrasena()
 	{
 		$token = $_REQUEST["token"];

 		$this->load->model('OlvidarPassword/Model_OlvidarPassword');
		$datosTokenUsuarios = $this->Model_OlvidarPassword->verifyTokenChangePassword($token);

		if($datosTokenUsuarios["msjCantidadRegistros"]>0)
		{
			$this->load->view('OlvidarPassword/view_CambiarPassword');
		}
		else
		{
			$this->load->view('OlvidarPassword/view_CambiarPasswordCaducado');
		}

 		
 	}


 	public function actualizarPasswordUsuario()
 	{
 		$password = $_REQUEST["password"];
 		$token = $_REQUEST["token"];

 		$this->load->model('OlvidarPassword/Model_OlvidarPassword');
		$datosUpdatePassword = $this->Model_OlvidarPassword->updatePasswordUsuario($password,$token);

 		echo json_encode($datosUpdatePassword);

		
 	}


 


}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistroEstudiantes extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

	}


	public function index()
	{

		$this->load->view('RegistroEstudiantes/View_RegistroEstudiantes');
		
	}


	public function insertRegistroEstudiantes()
	{

		$nuevoIngreso = $_REQUEST['nuevoIngreso'];
		$no_de_control = $_REQUEST['no_de_control'];
		$id_semestre = $_REQUEST['id_semestre'];
		$apellido_paterno = $_REQUEST['apellido_paterno'];
		$apellido_materno = $_REQUEST['apellido_materno'];
		$nombre_alumno = $_REQUEST['nombre_alumno'];
		$curp_alumno = $_REQUEST['curp_alumno'];
		$creditos_aprobados = $_REQUEST['creditos_aprobados'];
		$creditos_cursados = $_REQUEST['creditos_cursados'];
		$clave_oficial = $_REQUEST['clave_oficial'];
		$id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
		$grupo = $_REQUEST['grupo'];

		$this->load->model('RegistroEstudiantes/Model_RegistroEstudiantes');
        $resultado_query = $this->Model_RegistroEstudiantes->insertRegistroEstudiantes($nuevoIngreso,$no_de_control,$id_semestre,$apellido_paterno,$apellido_materno,$nombre_alumno,$curp_alumno,$creditos_aprobados,$creditos_cursados,$clave_oficial,$id_periodo_escolar,$grupo);                                                                                       

        echo json_encode($resultado_query);
		
	}


	public function validateDatosRegistroEstudiantes()
	{

		$nuevoIngreso = $_REQUEST['nuevoIngreso'];
		$no_de_control = $_REQUEST['no_de_control'];
		$id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
		$clave_oficial = $_REQUEST['clave_oficial'];
		$id_semestre = $_REQUEST['id_semestre'];		

		$this->load->model('RegistroEstudiantes/Model_RegistroEstudiantes');
        $resultado_query = $this->Model_RegistroEstudiantes->validateDatosRegistroEstudiantes($nuevoIngreso,$no_de_control,$id_periodo_escolar,$clave_oficial,$id_semestre);                                                                                       

        echo json_encode($resultado_query);
		
	}


}
 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reunion extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		
	}


	public function registrar()
	{

		$this->load->view('Reunion/View_RegistrarReunionPersonal');
		
	}


	public function modificar()
	{

		$this->load->view('Reunion/View_ModificarReunionPersonal');
		
	}


	public function getInfoPersonalReunion()
	{

		$this->load->model('Reunion/Model_Reunion');
         $resultado_query = $this->Model_Reunion->getInfoPersonalReunion();

        echo json_encode($resultado_query);
		
	}

	public function guardarReunion()
	{

		$nombre_reunion = $_REQUEST['nombre_reunion'];
		$id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
		$no_libro = $_REQUEST['no_libro'];
		$no_acta = $_REQUEST['no_acta'];
		$mensaje_1 = $_REQUEST['mensaje_1'];
		$personal = $_REQUEST['personal'];
		$mensaje_2 = $_REQUEST['mensaje_2'];

		$this->load->model('Reunion/Model_Reunion');
         $resultado_query = $this->Model_Reunion->guardarReunion($nombre_reunion,$id_periodo_escolar,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2);

        echo json_encode($resultado_query);
		
	}

	public function modificarReunion()
	{

		$id_reunion = $_REQUEST['id_reunion'];
		$nombre_reunion = $_REQUEST['nombre_reunion'];
		$id_periodo_escolar = $_REQUEST['id_periodo_escolar'];
		$no_libro = $_REQUEST['no_libro'];
		$no_acta = $_REQUEST['no_acta'];
		$mensaje_1 = $_REQUEST['mensaje_1'];
		$personal = $_REQUEST['personal'];
		$mensaje_2 = $_REQUEST['mensaje_2'];

		$this->load->model('Reunion/Model_Reunion');
         $resultado_query = $this->Model_Reunion->modificarReunion($id_reunion,$nombre_reunion,$id_periodo_escolar,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2);

        echo json_encode($resultado_query);
		
	}


	 public function verifySolicitudesDisponibles()
    {
     
  		$id_periodo_escolar = $_REQUEST['id_periodo_escolar'];

  		
        $this->load->model('Reunion/Model_Reunion');
        $resultado_query = $this->Model_Reunion->verifySolicitudesDisponibles($id_periodo_escolar);

        echo json_encode($resultado_query);

    }


     public function cargarTablaReunion()
    {
     
  
        $this->load->model('Reunion/Model_Reunion');
        $resultado_query = $this->Model_Reunion->cargarTablaReunion($_REQUEST);

        echo json_encode($resultado_query);

    }


      public function getInfoReunion()
    {
     
        $id_reunion = $_REQUEST["id_reunion"];

        $this->load->model('Reunion/Model_Reunion');

        $resultado_query = $this->Model_Reunion->getInfoReunion($id_reunion);

        echo json_encode($resultado_query);
  
    }


     public function eliminarReunion()
    {
     
        $id_reunion = $_REQUEST["id_reunion"];

        $this->load->model('Reunion/Model_Reunion');

        $resultado_query = $this->Model_Reunion->eliminarReunion($id_reunion);

        echo json_encode($resultado_query);
  
    }





}
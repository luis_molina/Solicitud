 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EstudiantesAdmin extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();

		
	}


	public function kardex()
	{

		$this->load->view('EstudiantesAdmin/View_KardexEstudiantes');
		
	}

    public function evaluar()
    {

        $this->load->view('EstudiantesAdmin/View_EvaluarEstudiantes');
        
    }


     public function evaluarExcel()
    {

        $this->load->view('EstudiantesAdmin/View_EvaluarEstudiantesExcel');
        
    }

	 public function cargarTablaEstudiantesKardex()
    {
     
  
        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');
        $resultado_query = $this->Model_EstudiantesAdmin->cargarTablaEstudiantesKardex($_REQUEST);

        echo json_encode($resultado_query);

    }



     public function cargarTablaEstudiantesEvaluar()
    {
     
  
        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');
        $resultado_query = $this->Model_EstudiantesAdmin->cargarTablaEstudiantesEvaluar($_REQUEST);

        echo json_encode($resultado_query);

    }



    public function pdfKardexEstudiante()
    {
     
        $id_alumno = $_REQUEST["id_alumno"];


     	$this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');

        $query = $this->Model_EstudiantesAdmin->getDatoskardexEstudiante($id_alumno);

        $datos['kardex'] = $query['kardex'];

        // exit();

        $nombreCompleto = $datos["kardex"][0]->alumno;

     

     	  $html =  $this->load->view('EstudiantesAdmin/viewkardexEstudiante',$datos,true);



        $data = [];

		    $hoy = date("dmyhis");


        $pdfFilePath = $nombreCompleto."_".$hoy.".pdf";
 
        //load mPDF library
        $this->load->library('M_pdf');
        $mpdf = new mPDF('c', 'A4-L'); 
 		   //$mpdf->WriteHTML($num_solicitud);
		    
            // $mpdf->Output($pdfFilePath, "D");  //--------------


       //generate the PDF from the given html
        $this->m_pdf->pdf->WriteHTML($html);
 
        //download it.
       $this->m_pdf->pdf->Output($pdfFilePath, "D");     // ---------------
  
    }



    public function getInfoEvaluarAlumno()
    {
     
        $id_alumno = $_REQUEST["id_alumno"];

        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');

        $resultado_query = $this->Model_EstudiantesAdmin->getInfoEvaluarAlumno($id_alumno);

        echo json_encode($resultado_query);
  
    }


    public function evaluarAlumnoMateria()
    {
     
        $id_alumno = $_REQUEST["id_alumno"];
        $no_de_control = $_REQUEST["no_de_control"];
        $clave_oficial = $_REQUEST["clave_oficial"];
        $id_periodo_escolar = $_REQUEST["id_periodo_escolar"];
        $id_semestre = $_REQUEST["id_semestre"];

        $clave_materia = $_REQUEST["clave_materia"];

        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');
        
        $resultado_query = $this->Model_EstudiantesAdmin->evaluarAlumnoMateria($id_alumno,$no_de_control,$clave_oficial,$id_periodo_escolar,$id_semestre,$clave_materia);

        echo json_encode($resultado_query);
  
    }

    public function modificar()
    {

        $this->load->view('EstudiantesAdmin/view_ModificarEstudiantes');
        
    }


     public function cargarTablaModificarEstudiantes()
    {
     
  
        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');
        $resultado_query = $this->Model_EstudiantesAdmin->cargarTablaModificarEstudiantes($_REQUEST);

        echo json_encode($resultado_query);

    }


     public function getInfoEstudiante()
    {
     
        $id_alumno = $_REQUEST["id_alumno"];

        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin');

        $resultado_query = $this->Model_EstudiantesAdmin->getInfoEstudiante($id_alumno);

        echo json_encode($resultado_query);
  
    }


      public function checkModificarNumeroControlEstudiante()
     {
      
          $no_de_control_new = $_REQUEST['no_de_control_new'];
          $no_de_control_origen = $_REQUEST['no_de_control_origen'];
          

          $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin'); 
          $resultado_query = $this->Model_EstudiantesAdmin->checkModificarNumeroControlEstudiante($no_de_control_new,$no_de_control_origen);
        

          echo json_encode($resultado_query);

      }


      public function modificarDatosEstudiante()
      {
      

          $id_alumno = $_REQUEST['id_alumno'];
          $no_de_control = $_REQUEST['no_de_control'];
          $nombre_alumno = $_REQUEST['nombre_alumno'];
          $apellido_paterno = $_REQUEST['apellido_paterno'];
          $apellido_materno = $_REQUEST['apellido_materno'];
          $curp_alumno = $_REQUEST['curp_alumno'];


          $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin'); 
          $resultado_query = $this->Model_EstudiantesAdmin->modificarDatosEstudiante($id_alumno,$no_de_control,$nombre_alumno,$apellido_paterno,$apellido_materno,$curp_alumno);

          echo json_encode($resultado_query);

      }


    public function deleteEstudiantes()
    {
  
          $id_alumno = $_REQUEST['id_alumno'];
          

          $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin'); 
          $resultado_query = $this->Model_EstudiantesAdmin->deleteEstudiantes($id_alumno);
        

          echo json_encode($resultado_query);
 

     }



      public function uploadExcelEvaluarEstudiantes()
      {

        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin'); 
        $nombre_archivo= $this->Model_EstudiantesAdmin->uploadExcelEvaluarEstudiantes($_FILES);


        $this->load->library('Excelfile');
        // $file="./uploads/Book1.xlsx";
        $file="./application/uploads/".$nombre_archivo;
        $obj=PHPExcel_IOFactory::load($file);
        $cell=$obj->getActiveSheet()->getCellCollection();
        $x=0;
        foreach($cell as $cl){

          $column=$obj->getActiveSheet()->getCell($cl)->getColumn();
          $row=$obj->getActiveSheet()->getCell($cl)->getRow();
          $data_value=$obj->getActiveSheet()->getCell($cl)->getValue();
          
          if($row==1){
            $header[$row][$column]=$data_value;
          }else{
            if($data_value !== null && $data_value !== '')
            {
              $arr_data[$row][$column]=$data_value;
            }
          }

        }
        //$data['header']=$header;
        //$data['values']=$arr_data;

        $this->load->model('EstudiantesAdmin/Model_EstudiantesAdmin'); 
        $resultado = $this->Model_EstudiantesAdmin->evaluarEstudiantesWithExcel($arr_data);

        $rutaFileEliminar = "./application/uploads/".$nombre_archivo;

        unlink($rutaFileEliminar);


         echo json_encode($resultado);

        // exit();

      

       // $this->load->view('welcome_message',$data);

       // $datosUsuario["aa"] = $datosUsuario;

        
        //var_dump($datosUsuario); 
      }




}
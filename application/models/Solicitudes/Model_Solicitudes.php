<?php 
class Model_Solicitudes extends CI_Model
{
		public function __construct()
		{
			parent::__construct();
		}


		public function cargarTablaSolicitudes($request)
		{


					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];

					
					$sqlSolicitudes =	"select distinct 
										alu.id_alumno,
										ka.no_de_control,
										alu.nombre_alumno,
										alu.apellido_paterno,
										alu.apellido_materno,
										CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
										pe.identificacion_larga as periodo_escolar,
							            car.nombre_reducido,
										se.nombre_semestre,
										'<button  type=''button'' class=''btn btn-primary btn btnVerSolicitudes''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Solicitudes
									  from alumnos alu
							          join kardex ka on (alu.id_alumno=ka.id_alumno)
									  join solicitudes soli on (alu.id_alumno=soli.id_alumno)
									  join carreras car on (ka.clave_oficial=car.clave_oficial)
									  join semestres se on (ka.id_semestre = se.id_semestre)
									  join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar) 
							          where id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno )
							          and alu.estado = '1'
											order by ".$columna." ".$ordenacion." ";


					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sqlSolicitudes);

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sqlSolicitudes = "select
													id_alumno,
													no_de_control,
													nombre_alumno,
													apellido_paterno,
											        apellido_materno,
											        nombreCompleto,
											        periodo_escolar,
											        nombre_reducido,
											        nombre_semestre,
											        Solicitudes
											From(
													select distinct 
													alu.id_alumno,
													ka.no_de_control,
													alu.nombre_alumno,
													alu.apellido_paterno,
													alu.apellido_materno,
													CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
													pe.identificacion_larga as periodo_escolar,
										            car.nombre_reducido,
													se.nombre_semestre,
													'<button  type=''button'' class=''btn btn-primary btn btnVerSolicitudes''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Solicitudes
												  from alumnos alu
										          join kardex ka on (alu.id_alumno=ka.id_alumno)
												  join solicitudes soli on (alu.id_alumno=soli.id_alumno)
												  join carreras car on (ka.clave_oficial=car.clave_oficial)
												  join semestres se on (ka.id_semestre = se.id_semestre) 
												  join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
										          where id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno )
										          and alu.estado = '1'
											) as Mytable 
										where  
											 ( 
												 no_de_control like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
												 nombreCompleto like '%".$this->db->escape_str($requestData['search']['value'])."%' or
												 periodo_escolar like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 nombre_semestre like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 nombre_reducido like '%".$this->db->escape_str($requestData['search']['value'])."%' 
										     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sqlSolicitudes);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sqlSolicitudes .= $limit;
		                
		            $query = $this->db->query($sqlSolicitudes);



		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

								$nestedData[] = $row["id_alumno"];
								$nestedData[] = $row["no_de_control"];
							    $nestedData[] = $row["nombre_alumno"];
							    $nestedData[] = $row["apellido_paterno"];
								$nestedData[] = $row["apellido_materno"];
								$nestedData[] = $row["nombre_reducido"];
								$nestedData[] = $row["periodo_escolar"];
								$nestedData[] = $row["nombre_semestre"];
								$nestedData[] = $row["Solicitudes"];

								$data[] = $nestedData;
							}


					$json_data = array(
						"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
						"recordsTotal"    => intval( $totalData ),  // total number of records
						"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
						"data"            => $data  // total data array
						);


					return $json_data;
			
		}


		public function getCountSolicitudesEstudiante($id_alumno)
		{

			$sql = "select  distinct
							alu.nombre_alumno,
							soli.num_solicitud,
							cts.tipo_solicitud as asunto,
							soli.lugar,
							DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
							se.nombre_semestre,
							peri.identificacion_larga,
							status,
							'<button  type=''button'' class=''btn btn-primary btn btnModificarSolicitud''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
							'<button  type=''button'' class=''btn btn-primary btn btnEliminarSolicitud''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar ,
							'<button  type=''button'' class=''btn btn-primary btn btnActaSolcitud''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Acta
					from solicitudes soli
					join alumnos alu on(soli.id_alumno = alu.id_alumno)
					left join rel_datos_materia_tipo_solicitud rdmt on ( soli.num_solicitud = rdmt.num_solicitud )
                    left join cat_tipo_solicitud cts on (rdmt.id_tipo_solicitud = cts.id_tipo_solicitud)
					join semestres se on(soli.id_semestre = se.id_semestre)
					join periodos_escolares peri on (soli.id_periodo_escolar = peri.id_periodo_escolar)
					where alu.id_alumno = ? and alu.estado = '1'";

			$query = $this->db->query($sql,array($id_alumno));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'solicitudes'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}



		public function getCountSolicitudesEstudianteDictamen($id_alumno)
		{

			$sql = "select  distinct
							alu.nombre_alumno,
							soli.num_solicitud,
							cts.tipo_solicitud as asunto,
							soli.lugar,
							DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
							se.nombre_semestre,
							peri.identificacion_larga,
							'<button  type=''button'' class=''btn btn-primary btn btnDictamenSolicitud''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Respuesta_Dictamen,
							'<button  type=''button'' class=''btn btn-primary btn btnDictamenDirectorSolicitud''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Respuesta_dictamen_director
					from solicitudes soli
					join alumnos alu on(soli.id_alumno = alu.id_alumno)
					left join rel_datos_materia_tipo_solicitud rdmt on ( soli.num_solicitud = rdmt.num_solicitud )
                    left join cat_tipo_solicitud cts on (rdmt.id_tipo_solicitud = cts.id_tipo_solicitud)
					join semestres se on(soli.id_semestre = se.id_semestre)
					join periodos_escolares peri on (soli.id_periodo_escolar = peri.id_periodo_escolar)
					where alu.id_alumno = ? and alu.estado = '1'";

			$query = $this->db->query($sql,array($id_alumno));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'solicitudes'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function getDatosActaSolicitud($num_solicitud)
		{

			$sql = "select 	
							observacion,
							respuesta_solicitud_acta,
							verificacion_manual_lineamiento,
							respuesta_solicitud_dictamen,
							no_dictamen,
							motivos_solicitud_dictamen,
							no_recomendacion_dictamen,
							recomienda
					from solicitudes 
							where num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'datosSolicitud'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['datosSolicitud'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['datosSolicitud'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function guardarDatosRespuestaActaSolicitud($num_solicitud,$verificacion_manual_lineamiento,$respuesta_solicitud_acta)
		{

			$sql = "update solicitudes set 	respuesta_solicitud_acta = ? , 
											verificacion_manual_lineamiento = ?
					 where num_solicitud = ? ";

			$query = $this->db->query($sql,array($respuesta_solicitud_acta,$verificacion_manual_lineamiento,$num_solicitud));		

			if($query)
			{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'Los datos de la solicitud fueron guardados correctamente';


			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}



		public function guardarDatosRespuestaDictamenSolicitud($num_solicitud,$respuesta_solicitud_dictamen,$no_dictamen)
		{

			$sql = "update solicitudes set 
										respuesta_solicitud_dictamen = ?,
										no_dictamen = ?
					 where num_solicitud = ? ";

			$query = $this->db->query($sql,array($respuesta_solicitud_dictamen,$no_dictamen,$num_solicitud));		

			if($query)
			{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'Los datos fueron guardados correctamente';


			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function guardarDatosRespuestaDictamenDirectSolicitud($num_solicitud,$recomienda,$respuesta_solicitud_acta,$motivos_solicitud_dictamen,$no_recomendacion_dictamen)
		{

			$sql = "update solicitudes set 
										respuesta_solicitud_acta = ?,
										motivos_solicitud_dictamen = ?,
										no_recomendacion_dictamen = ?,
										recomienda = ?
					 where num_solicitud = ? ";

			$query = $this->db->query($sql,array($respuesta_solicitud_acta,$motivos_solicitud_dictamen,$no_recomendacion_dictamen,$recomienda,$num_solicitud));		

			if($query)
			{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'Los datos fueron guardados correctamente';


			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}




		public function getDatosSolicitudPDF_Estudiante($num_solicitud)
		{


			$sql = "select * from rel_datos_materia_tipo_solicitud where num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));	

			

			if($query)
			{
				if($query->num_rows()>0)
				{

					

					$sql = "select 	distinct
									CONCAT(soli.lugar,', ', DATE_FORMAT(soli.fecha,'%d/%m/%Y') ) as lugarYfecha,
							        soli.asunto,
							        CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
							       sem.nombre_semestre,
									car.nombre_carrera,
							        alu.id_alumno,
							        soli.observacion,
							        soli.motivos_academicos,
							        soli.motivos_personales,
							        soli.otros,
							        kar.no_de_control as no_de_control,
					       			kar.no_de_control as no_de_control_new,
					       			 (
							        	select 
							        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
							        	from  personal
							        	where id_comite = 2 limit 0,1
							        ) as secretario,
							        (
							        	select 
							        			cd.departamento 
							        	from  personal per
							        	join cat_departamentos cd on (per.id_departamento = cd.id_departamento)
							        	where id_comite = 2 limit 0,1							        	                                    
							        ) as departamento
							from solicitudes soli
									 left join rel_datos_materia_tipo_solicitud rsk on (soli.num_solicitud = rsk.num_solicitud)
				                     join kardex kar on (rsk.id_kardex = kar.id_kardex)
				                     join alumnos alu on (soli.id_alumno = alu.id_alumno)
				                     join semestres sem on (kar.id_semestre = sem.id_semestre)
									join carreras car on (alu.clave_oficial = car.clave_oficial)
		               where soli.num_solicitud  = ? and alu.estado = '1' ";



				}
				// else
				// {


				// 	$sql = "select 	distinct
				// 			CONCAT(soli.lugar,', ', DATE_FORMAT(soli.fecha,'%d/%m/%Y') ) as lugarYfecha,
				// 	        soli.asunto,
				// 	        CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
				// 	        sem.nombre_semestre,
				// 			car.nombre_carrera,
				// 	        alu.id_alumno,
				// 	        soli.observacion,
				// 	        soli.motivos_academicos,
				// 	        soli.motivos_personales,
				// 	        soli.otros,
				// 	        alu.no_de_control
				// 	from solicitudes soli
				// 	join alumnos alu on (soli.id_alumno = alu.id_alumno)
				// 	join semestres sem on (kar.id_semestre = sem.id_semestre)
				// 	join carreras car on (alu.clave_oficial = car.clave_oficial)
				// 	 where soli.num_solicitud  = ? ";

					
				// }
			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 

					return $resultado_query;

				}


			

			$query = $this->db->query($sql,array($num_solicitud));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'solicitudes'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function getDatosDictamenPDF_Estudiante($num_solicitud)
		{


			$sql = "select * from rel_datos_materia_tipo_solicitud where num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));	

			

			if($query)
			{
				if($query->num_rows()>0)
				{

					

					$sql = "select 	distinct
									DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
									soli.lugar,
							        soli.asunto,
							        CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
							        kar.no_de_control as no_de_control,
							        soli.respuesta_solicitud_dictamen,
							       sem.nombre_semestre,
									car.nombre_carrera,
									soli.no_dictamen,
							        UPPER((
							        	select 
							        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
							        	from  personal
							        	where id_cargo_personal = 2 limit 0,1
							        )) as subdirector,
							        UPPER((
							        	select 
							        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
							        	from  personal
							        	where id_cargo_personal = 3 limit 0,1
							        )) as director,
							        soli.status
							from solicitudes soli
									 left join rel_datos_materia_tipo_solicitud rsk on (soli.num_solicitud = rsk.num_solicitud)
				                     join kardex kar on (rsk.id_kardex = kar.id_kardex)
				                     join alumnos alu on (soli.id_alumno = alu.id_alumno)
				                     join semestres sem on (kar.id_semestre = sem.id_semestre)
									join carreras car on (alu.clave_oficial = car.clave_oficial)
		               where soli.num_solicitud  = ? and alu.estado = '1' ";



				}
				
			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 

					return $resultado_query;

				}


			

			$query = $this->db->query($sql,array($num_solicitud));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'solicitudes'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function getDatosDictamenDirectorPDF_Estudiante($num_solicitud)
		{


			$sql = "select * from rel_datos_materia_tipo_solicitud where num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));	

			

			if($query)
			{
				if($query->num_rows()>0)
				{

					

					$sql = "select 	distinct
									DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
									soli.lugar,
                                    DATE_FORMAT(re.fecha_registro,'%d/%m/%Y') as fecha_reunion,
                                    re.no_acta,
                                    re.no_libro,
							        CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
							        car.nombre_carrera,
                                    kar.no_de_control as no_de_control,
							        soli.observacion peticion_solicitud,
                                    soli.status,
							       sem.nombre_semestre,
							       soli.no_dictamen,
							       lower(soli.respuesta_solicitud_acta) as respuesta_solicitud_acta,
							       soli.motivos_solicitud_dictamen,
							       soli.no_recomendacion_dictamen,
							       soli.recomienda,
							        UPPER((
							        	select 
							        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
							        	from  personal
							        	where id_comite = 1 limit 0,1
							        )) as presidente,
							        UPPER((
							        	select 
							        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
							        	from  personal
							        	where id_cargo_personal = 3 limit 0,1
							        )) as director
									from solicitudes soli
									 left join rel_datos_materia_tipo_solicitud rsk on (soli.num_solicitud = rsk.num_solicitud)
				                     join kardex kar on (rsk.id_kardex = kar.id_kardex)
				                     join alumnos alu on (soli.id_alumno = alu.id_alumno)
				                     join semestres sem on (kar.id_semestre = sem.id_semestre)
									join carreras car on (alu.clave_oficial = car.clave_oficial)
                                    left join reuniones re on (soli.id_periodo_escolar = re.id_periodo_escolar)
		               		where soli.num_solicitud  = ? and alu.estado = '1' ";



				}
				
			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 

					return $resultado_query;

				}


			

			$query = $this->db->query($sql,array($num_solicitud));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'solicitudes'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['solicitudes'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}



		public function evaluarSolicitudEstudiante($num_solicitud,$status)
		{


			$this->db->trans_begin();


			$sql =	"select 	rdmts.id_kardex,
								rdmts.materia,
								rdmts.id_tipo_solicitud,
								soli.id_alumno
						from solicitudes soli
							join rel_datos_materia_tipo_solicitud rdmts on(soli.num_solicitud = rdmts.num_solicitud)
						where soli.num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));


			if($query->num_rows() > 0)  
			{


				$id_tipo_solicitud = $query->result()[0]->id_tipo_solicitud;


				if($id_tipo_solicitud  != '7') // si es diferente de solicitud de baja definitiva entra aqui
				{ 

						 $id_kardex = '';
						 $materia = '';

						foreach ($query->result() as $row)
						{
						        

						         $id_kardex = $row->id_kardex;
						         $materia = $row->materia;


						         if($status == 'Aceptado')
						         {


						         		$sql = "update rel_kardex_materias set status = '1' where id_kardex = ? and materia = ? and tipo_materia != '1' ";


						         		$query = $this->db->query($sql,array($id_kardex,$materia));


						         }
						         else
						         {

						         		if($status == 'Pendiente')
						         		{
						         			$sql = "update rel_kardex_materias set status = '2' where id_kardex = ? and materia = ? and tipo_materia != '1' ";


						         			$query = $this->db->query($sql,array($id_kardex,$materia));
						         		}
						         		else
						         		{
						         			if($status == 'Rechazado')
						         			{
						         				$sql = "update rel_kardex_materias set status = '3' where id_kardex = ? and materia = ? and tipo_materia != '1' ";


						         				$query = $this->db->query($sql,array($id_kardex,$materia));
						         			}
						         		}

						         	

						         }

						}

				}
				else  // 
				{

					foreach ($query->result() as $row)
					{
					        
					         $id_alumno = $row->id_alumno;


					         if($status == 'Aceptado')
					         {


					         		$sql = "update alumnos set estado = '0' where id_alumno = ? ";


					         		$query = $this->db->query($sql,array($id_alumno));


					         }
					         else
					         {

					         		if($status == 'Pendiente')
					         		{
					         			$sql = "update alumnos set estado = '1' where id_alumno = ? ";


					         			$query = $this->db->query($sql,array($id_alumno));
					         		}
					         		else
					         		{
					         			if($status == 'Rechazado')
					         			{
					         				$sql = "update alumnos set estado = '1' where id_alumno = ? ";


					         				$query = $this->db->query($sql,array($id_alumno));
					         			}
					         		}

					         	

					         }

					}


				}


			}



			$sql =	"update solicitudes set 
										status = ?
									where 
										num_solicitud = ? ";

			$query = $this->db->query($sql,array($status,$num_solicitud));
			
			$resultado_query = array(
											 'status' => '',
											 'mensaje' => ''
										);


			if ($this->db->trans_status() === FALSE)
			{
					$resultado_query["resultado"] = "ERROR";
					$resultado_query["mensaje"] = "Ocurrio un error a la hora de evaluar la solicitud";

			        $this->db->trans_rollback();
			}
			else
			{
				$resultado_query["resultado"] = "OK";
				$resultado_query["mensaje"] = "El estado de la solcitud ha sido modificado correctamente";

			    $this->db->trans_commit();
			}

			

			return $resultado_query;



		}
	

		public function getInfoMotivosSolicitud($num_solicitud)
		{

			$sql = "select 
							motivos_academicos,
							motivos_personales,
							otros from 
					solicitudes where num_solicitud = ? ";

			$query = $this->db->query($sql,array($num_solicitud));		



			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['motivos'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['motivos'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}


		public function modificarSolcitudEstudiante($num_solicitud,$motivos_academicos,$motivos_personales,$otros)
		{

			$sql = "update solicitudes set 	motivos_academicos = ? , 
											motivos_personales = ? ,
											otros = ?
					 where num_solicitud = ? ";

			$query = $this->db->query($sql,array($motivos_academicos,$motivos_personales,$otros,$num_solicitud));		

			if($query)
			{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'La solicitud fue modificada correctamente';


			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

		}

		public function eliminarSolicitudEstudiante($num_solicitud)
		{

				$this->db->trans_begin();

				$sql = "select materia,
							   id_kardex,
							   id_tipo_solicitud
						from rel_datos_materia_tipo_solicitud 
						where num_solicitud = ?";

				$query = $this->db->query($sql,array($num_solicitud));	

				if($query->num_rows()>0)
				{

					foreach ($query->result() as $row)
					{
						  
					        $materia = $row->materia;
					        $id_kardex = $row->id_kardex;
	 					
	 					// eliminar la materia que afecto el kardex 
					        
				        	$sql = "delete from rel_kardex_materias 
											where id_kardex = ? 
											and  materia = ? 
											and tipo_materia != '1' ";

							$query = $this->db->query($sql,array($id_kardex,$materia));	
					        


					}


				}

				// eliminar la materia que se quedo guardada en la tabla de relacion de la solicitud

				$sql = "delete from rel_datos_materia_tipo_solicitud where num_solicitud = ? ";

				$query = $this->db->query($sql,array($num_solicitud));	

				// eliminar la solicitud del estudiante

				$sql = "delete from solicitudes where num_solicitud = ? ";

				$query = $this->db->query($sql,array($num_solicitud));


				if ($this->db->trans_status() === FALSE)
				{
						$resultado_query["resultado"] = "ERROR";
						$resultado_query["mensaje"] = "Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo";

				        $this->db->trans_rollback();
				}
				else
				{
					$resultado_query["resultado"] = "OK";
					$resultado_query["mensaje"] = "La solicitud fue eliminada correctamente";

				    $this->db->trans_commit();
				}
				
				
				return $resultado_query;

		}



		public function cargarTablaSolicitudesHistorial($request)
		{


					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];

					
					$sqlSolicitudes =	"select 
												CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
												cts.tipo_solicitud as asunto,
										        soli.lugar,
										        DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
										        sem.nombre_semestre as semestre,
										        peri.identificacion_larga as periodo_escolar,
										        car.nombre_reducido as carrera,
										        soli.observacion,
										        soli.motivos_academicos,
										        soli.motivos_personales,
										        soli.otros,
										        soli.respuesta_solicitud_acta,
										        soli.respuesta_solicitud_dictamen,
										        soli.motivos_solicitud_dictamen
										from alumnos alu 
										join solicitudes soli on ( alu.id_alumno = soli.id_alumno )
										left join rel_datos_materia_tipo_solicitud rdmt on ( soli.num_solicitud = rdmt.num_solicitud )
                    					left join cat_tipo_solicitud cts on (rdmt.id_tipo_solicitud = cts.id_tipo_solicitud)".
										// join kardex kar on ( alu.id_alumno = soli.id_alumno )
										"join semestres sem on ( soli.id_semestre = sem.id_semestre)
										join carreras car on ( alu.clave_oficial = car.clave_oficial)
										join periodos_escolares peri on (soli.id_periodo_escolar = peri.id_periodo_escolar)
										where alu.estado = '1'
											order by ".$columna." ".$ordenacion." ";


					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sqlSolicitudes);

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sqlSolicitudes = "select
													nombreCompleto,
													asunto,
													lugar,
													fecha,
											        semestre,
											        carrera,
											        periodo_escolar,
											        observacion,
											        motivos_academicos,
											        motivos_personales,
											        otros,
											        respuesta_solicitud_acta,
											        respuesta_solicitud_dictamen,
											        motivos_solicitud_dictamen
											From(
													select 
														CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as nombreCompleto,
														cts.tipo_solicitud as asunto,
												        soli.lugar,
												        DATE_FORMAT(soli.fecha,'%d/%m/%Y') as fecha,
												        sem.nombre_semestre as semestre,
												        peri.identificacion_larga as periodo_escolar,
												        car.nombre_reducido as carrera,
												        soli.observacion,
												        soli.motivos_academicos,
												        soli.motivos_personales,
												        soli.otros,
												        soli.respuesta_solicitud_acta,
										        		soli.respuesta_solicitud_dictamen,
										        		soli.motivos_solicitud_dictamen
												from alumnos alu 
												join solicitudes soli on ( alu.id_alumno = soli.id_alumno )
												left join rel_datos_materia_tipo_solicitud rdmt on ( soli.num_solicitud = rdmt.num_solicitud )
                    							left join cat_tipo_solicitud cts on (rdmt.id_tipo_solicitud = cts.id_tipo_solicitud)".
												// join kardex kar on ( alu.id_alumno = soli.id_alumno )
												"join semestres sem on ( soli.id_semestre = sem.id_semestre)
												join carreras car on ( alu.clave_oficial = car.clave_oficial)
												join periodos_escolares peri on (soli.id_periodo_escolar = peri.id_periodo_escolar)
												where alu.estado = '1'
											) as Mytable 
										where  
											 ( 
												 nombreCompleto like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
												 asunto like '%".$this->db->escape_str($requestData['search']['value'])."%' or
												 lugar like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 fecha like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 periodo_escolar like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 semestre like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 carrera like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 observacion like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 motivos_academicos like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 motivos_personales like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 otros like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 respuesta_solicitud_acta like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
												 respuesta_solicitud_dictamen like '%".$this->db->escape_str($requestData['search']['value'])."%' or
												 motivos_solicitud_dictamen like '%".$this->db->escape_str($requestData['search']['value'])."%'
										     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sqlSolicitudes);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sqlSolicitudes .= $limit;
		                
		            $query = $this->db->query($sqlSolicitudes);

												

		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

								$nestedData[] = $row["nombreCompleto"];
								$nestedData[] = $row["carrera"];
								$nestedData[] = $row["periodo_escolar"];
								$nestedData[] = $row["semestre"];
								$nestedData[] = $row["asunto"];
							    $nestedData[] = $row["lugar"];
							    $nestedData[] = $row["fecha"];
								$nestedData[] = $row["observacion"];
								$nestedData[] = $row["motivos_academicos"];
								$nestedData[] = $row["motivos_personales"];
								$nestedData[] = $row["otros"];
								$nestedData[] = $row["respuesta_solicitud_acta"];
								$nestedData[] = $row["respuesta_solicitud_dictamen"];
								$nestedData[] = $row["motivos_solicitud_dictamen"];

								$data[] = $nestedData;
							}


					$json_data = array(
						"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
						"recordsTotal"    => intval( $totalData ),  // total number of records
						"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
						"data"            => $data  // total data array
						);


					return $json_data;
			
		}




}

?>





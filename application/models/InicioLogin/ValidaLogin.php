<?php 
	class ValidaLogin extends CI_Model
	{
		public function __construct()
		{
			parent::__construct();
		}

		public function verificarLogin($usuario)
		{
			

			$sql =	"select id_usuario,
							usu.nombre,
							usu.apellidos,
							usu.correo,
                            per.id_comite,
                            usu.password
					from   
				    		usuarios usu
					left join personal per on (usu.id_personal = per.id_personal)
		         	where usu.correo = ? and 
		         		  usu.estado = '1' ";

			$query = $this->db->query($sql,array($usuario["email"]));

			
			$datosLogin = array("msjCantidadRegistros" => 0, "msjNoHayRegistros" => '', "statusLogueo" => false ,"loginUsuario" => array());

			$datosLogin["msjCantidadRegistros"] = $query->num_rows(); 

			if($datosLogin["msjCantidadRegistros"] > 0)
			{
				$password = $query->result()[0]->password;


				 if (password_verify($usuario["password"], $password)) 
				 {
				 	$datosLogin["statusLogueo"] = true; 
				    $datosLogin["loginUsuario"] = $query->result(); 
				    $datosLogin["msjNoHayRegistros"] = "Logueado Correctamente";
				 } 
				else 
				{
					$datosLogin["statusLogueo"] = false; 
				    $datosLogin["msjNoHayRegistros"] = "El nombre de usuario o contraseña son incorrectos";
				}

				

			}
			else{
				$datosLogin["statusLogueo"] = false; 
				$datosLogin["msjNoHayRegistros"] = "El nombre de usuario o contraseña son incorrectos";
			}
			

			return $datosLogin;



			// foreach ($query->result_array() as $row)
			// {
			//         echo $row['title'];
			//         echo $row['name'];
			//         echo $row['body'];
			// }

		}


		public function verificarLogin2($usuario)
		{
			
			$sql =	"select id_usuario,
							usu.nombre,
							usu.apellidos,
							usu.correo,
  		                 per.id_comite
					from   
				    		usuarios usu
					left join personal per on (usu.id_personal = per.id_personal)
		         	where usu.correo = ? and 
		         		  usu.password = ? and 
		         		  usu.estado = '1' ";

			$query = $this->db->query($sql,array($usuario["email"],$usuario["password"]));


			$datosLogin = array("msjCantidadRegistros" => 0, "msjNoHayRegistros" => '',"loginUsuario" => array());

			$datosLogin["msjCantidadRegistros"] = $query->num_rows(); 

			if($datosLogin["msjCantidadRegistros"] > 0)
			{
				$datosLogin["loginUsuario"] = $query->result(); 
			}
			else{
				$datosLogin["msjNoHayRegistros"] = "El nombre de usuario o contraseña son incorrectos";
			}
			

			return $datosLogin;



			// foreach ($query->result_array() as $row)
			// {
			//         echo $row['title'];
			//         echo $row['name'];
			//         echo $row['body'];
			// }

		}


	}
?>
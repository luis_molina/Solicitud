<?php 
class Model_SolicitudEstudiantes extends CI_Model
{


	public function __construct()
	{
		parent::__construct();
	}


	public function verifyNoControlGetInfo($no_de_control)
	{

		$sql =	"select distinct id_alumno from kardex where no_de_control = ? ";

		$query = $this->db->query($sql,array($no_de_control));


		$resultado_query = array(
										'msjCantidadRegistros'=> 0,
										'alumno'=> array(),
										 'status' => '',
										 'mensaje' => ''
									);

		if($query)
		{

			if($query->num_rows()>0)
			{
				$id_alumno = $query->result()[0]->id_alumno;


				$sql =	"select distinct
						alu.id_alumno,
						ka.no_de_control,
						alu.apellido_paterno,
						alu.apellido_materno,
						alu.nombre_alumno,
						alu.curp_alumno,
						ka.clave_oficial,
						ka.id_semestre,
						ka.id_periodo_escolar,
						(
							SELECT count(*) as cantidad_solicitud 
							FROM solicitudes so2
							join alumnos alu2 on (so2.id_alumno = alu2.id_alumno)
							and alu2.id_alumno = ? "
							//and so2.status = 'Aceptado'
						.") as cantidad_solicitud,
						(
				        	select 
				        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
				        	from  personal
				        	where id_comite = 2 limit 0,1
				        ) as secretario,
				        (
				        	select 
				        			cd.departamento 
				        	from  personal per
				        	join cat_departamentos cd on (per.id_departamento = cd.id_departamento)
				        	where id_comite = 2 limit 0,1							        	                                    
				        ) as departamento
					from alumnos alu
					join kardex ka on (alu.id_alumno = ka.id_alumno and id_kardex = (select max(id_kardex) from kardex where id_alumno = ? ) )
					where ka.no_de_control  = ? and alu.estado = '1' ";
			

				$query = $this->db->query($sql,array($id_alumno,$id_alumno,$no_de_control));

				if($query)
				{

					if($query->num_rows()>0)
					{
						$resultado_query['msjCantidadRegistros'] = $query->num_rows();
						$resultado_query['alumno'] = $query->result(); 
						$resultado_query['status'] = 'OK'; 
						$resultado_query['mensaje'] = 'información obtenida';

				
					}
					else
					{
						$resultado_query['msjCantidadRegistros'] = $query->num_rows();
						$resultado_query['alumno'] = $query->result(); 
						$resultado_query['status'] = 'Sin datos';
						$resultado_query['mensaje'] = 'Verifique que su número de control esté escrito correctamente'; 
					}

				}
				else{
						$resultado_query['status'] = 'ERROR'; 
						$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
				}


			}
			else
			{
				$resultado_query['msjCantidadRegistros'] = $query->num_rows();
				$resultado_query['alumno'] = $query->result(); 
				$resultado_query['status'] = 'Sin datos';
				$resultado_query['mensaje'] = 'Verifique que su número de control esté escrito correctamente'; 
			}



		}
		else{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
		}


		

		


		


		
		

		return $resultado_query;



	}


	public function cargarSelectPeriodosEscolares()
	{

		$sql = "select id_periodo_escolar,identificacion_larga,identificacion_corta,periodo from periodos_escolares order by periodo";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}

	public function cargarSelectCarreras()
	{

		$sql = "select clave_oficial,nombre_carrera from carreras order by clave_oficial";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}


	public function cargarSelectSemestres()
	{

		$sql = "select id_semestre,nombre_semestre from semestres order by id_semestre";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}



	public function enviarSolicitudEstudiante($no_de_control,$datosAlumno,$datosSolicitud,$datosMateriaCursoEspecial,$datosMateriaCursoRepeticion,$datosMateria2CursosEspecAsig,$datosMateria2CursosEspecVerano,$datosMateriaCursoSobreCreditos,$datosMateriaCursoEspecialResidencia,$datosBajaDefinitiva)
	{

		//return count($datosMateria2CursosEspecAsig);

		//return count($datosMateria2CursosEspecVerano);

				 $this->db->trans_begin();

				 // obtener el id del alumno 

				 $sql =	"select distinct id_alumno from kardex where no_de_control = ? ";

				 // obtener el id del alumno 

				 // $sql = "select distinct alu.id_alumno 
					// 	from alumnos alu
					// 	join kardex ka on (alu.id_alumno = ka.id_alumno )
					// 	where ka.no_de_control = ? ";
				

					$query = $this->db->query($sql,array($no_de_control));

		
					if($query->num_rows()>0)
					{

						$id_alumno = $query->result()[0]->id_alumno;


						$sql = "select distinct
											    alu.id_alumno,
												ka.no_de_control,
												ka.id_semestre,
												ka.id_periodo_escolar,
												ka.id_kardex,
												ka.clave_oficial
											from alumnos alu
											join kardex ka on (alu.id_alumno = ka.id_alumno and id_kardex = (select max(id_kardex) from kardex where id_alumno = ? ) )
											where ka.no_de_control  = ?  and alu.estado = '1' ";

						$query = $this->db->query($sql,array($id_alumno,$no_de_control));

						$id_semestre = $query->result()[0]->id_semestre;
						$id_periodo_escolar = $query->result()[0]->id_periodo_escolar;
						$id_kardex = $query->result()[0]->id_kardex;
						$clave_oficial = $query->result()[0]->clave_oficial;



						$sql =	"insert into solicitudes 	(
																		num_solicitud,
																		status,
																		observacion,
																		asunto,
																		lugar,
																		fecha,
																		motivos_academicos,
																		motivos_personales,
																		otros,
																		id_semestre,
																		id_periodo_escolar,
																		id_alumno
																		
																	) 
														  values 	(
																		null,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?,
																		?
																	)";
						

						$query = $this->db->query($sql,array($datosSolicitud["status"],$datosSolicitud["observacion"],$datosSolicitud["asunto"],$datosSolicitud["lugar"],$datosSolicitud["fecha"],$datosSolicitud["motivos_academicos"],$datosSolicitud["motivos_personales"],$datosSolicitud["otros"],$id_semestre,$id_periodo_escolar,$id_alumno));


						$sql =	"select max(num_solicitud) as num_solicitud from solicitudes";

						$query = $this->db->query($sql);


						$num_solicitud = $query->result()[0]->num_solicitud;


						if($datosMateriaCursoRepeticion != "_")
						{

								
								$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																							id_datos_materia_tipo_solicitud,
																							id_kardex,
																							num_solicitud,
																							id_alumno,
																							clave_oficial,
																							id_semestre,
																							id_periodo_escolar,
																							materia,
																							id_tipo_solicitud
																						) 
																			  values 	(
																							null,
																							?,
																							?,
																							?,
																							?,
																							?,
																							?,
																							?,
																							?
																						)";

								$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$datosMateriaCursoRepeticion["materia"],1));



								$sql = "insert into rel_kardex_materias 
																 (
																	id_kardex,
																	materia,
																	aprobada,
																	status,
																	tipo_materia
																  )
													             values 
													             (
																	?,
																	?,
																	null,
																	'2',
																	'2'
													             )
													             ";

								$query = $this->db->query($sql,array($id_kardex,$datosMateriaCursoRepeticion["materia"]));



						}
						else if($datosMateriaCursoEspecial != "_")
						{


								$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																						id_datos_materia_tipo_solicitud,
																						id_kardex,
																						num_solicitud,
																						id_alumno,
																						clave_oficial,
																						id_semestre,
																						id_periodo_escolar,
																						materia,
																						id_tipo_solicitud
																					) 
																		  values 	(
																						null,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?
																					)";

								$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$datosMateriaCursoEspecial["materia"],2));


								$sql = "insert into rel_kardex_materias 
																 (
																	id_kardex,
																	materia,
																	aprobada,
																	status,
																	tipo_materia
																  )
													             values 
													             (
																	?,
																	?,
																	null,
																	'2',
																	'3'
													             )
													             ";

								$query = $this->db->query($sql,array($id_kardex,$datosMateriaCursoEspecial["materia"]));



						}
						else if(count($datosMateria2CursosEspecAsig) > 1)
						{
							
								for($x= 0; $x < count($datosMateria2CursosEspecAsig); $x++)
								{

										$materia = $datosMateria2CursosEspecAsig[$x]["materia"];

										$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																								id_datos_materia_tipo_solicitud,
																								id_kardex,
																								num_solicitud,
																								id_alumno,
																								clave_oficial,
																								id_semestre,
																								id_periodo_escolar,
																								materia,
																								id_tipo_solicitud
																							) 
																				  values 	(
																								null,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?
																							)";

										$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$materia,3));

										

										$sql = "insert into rel_kardex_materias 
																		 (
																			id_kardex,
																			materia,
																			aprobada,
																			status,
																			tipo_materia
																		  )
															             values 
															             (
																			?,
																			?,
																			null,
																			'2',
																			'3'
															             )
															             ";

										$query = $this->db->query($sql,array($id_kardex,$materia));

								}


						}
						else if(count($datosMateria2CursosEspecVerano) > 1)
						{
							

								for($x= 0; $x < count($datosMateria2CursosEspecVerano); $x++)
								{
									
										$materia = $datosMateria2CursosEspecVerano[$x]["materia"];



										$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																								id_datos_materia_tipo_solicitud,
																								id_kardex,
																								num_solicitud,
																								id_alumno,
																								clave_oficial,
																								id_semestre,
																								id_periodo_escolar,
																								materia,
																								id_tipo_solicitud
																							) 
																				  values 	(
																								null,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?,
																								?
																							)";

										$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$materia,4));



										$sql = "insert into rel_kardex_materias 
																		 (
																			id_kardex,
																			materia,
																			aprobada,
																			status,
																			tipo_materia
																		  )
															             values 
															             (
																			?,
																			?,
																			null,
																			'2',
																			'3'
															             )
															             ";

										$query = $this->db->query($sql,array($id_kardex,$materia));

								}


						}
						else if($datosMateriaCursoSobreCreditos != "_")
						{


								$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																						id_datos_materia_tipo_solicitud,
																						id_kardex,
																						num_solicitud,
																						id_alumno,
																						clave_oficial,
																						id_semestre,
																						id_periodo_escolar,
																						materia,
																						id_tipo_solicitud
																					) 
																		  values 	(
																						null,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?
																					)";

								$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$datosMateriaCursoSobreCreditos["materia"],5));


								$sql = "insert into rel_kardex_materias 
																 (
																	id_kardex,
																	materia,
																	aprobada,
																	status,
																	tipo_materia
																  )
													             values 
													             (
																	?,
																	?,
																	null,
																	'2',
																	'1'
													             )
													             ";

								$query = $this->db->query($sql,array($id_kardex,$datosMateriaCursoSobreCreditos["materia"]));



						}
						else if($datosMateriaCursoEspecialResidencia != "_")
						{


								$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																						id_datos_materia_tipo_solicitud,
																						id_kardex,
																						num_solicitud,
																						id_alumno,
																						clave_oficial,
																						id_semestre,
																						id_periodo_escolar,
																						materia,
																						id_tipo_solicitud
																					) 
																		  values 	(
																						null,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?
																					)";

								$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$datosMateriaCursoEspecialResidencia["materia"],6));


								$sql = "insert into rel_kardex_materias 
																 (
																	id_kardex,
																	materia,
																	aprobada,
																	status,
																	tipo_materia
																  )
													             values 
													             (
																	?,
																	?,
																	null,
																	'2',
																	'1'
													             )
													             ";

								$query = $this->db->query($sql,array($id_kardex,$datosMateriaCursoEspecialResidencia["materia"]));



						}
						else if($datosBajaDefinitiva != "_")
						{


								$sql =	"insert into rel_datos_materia_tipo_solicitud 	(
																						id_datos_materia_tipo_solicitud,
																						id_kardex,
																						num_solicitud,
																						id_alumno,
																						clave_oficial,
																						id_semestre,
																						id_periodo_escolar,
																						materia,
																						id_tipo_solicitud
																					) 
																		  values 	(
																						null,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?,
																						?
																					)";

								$query = $this->db->query($sql,array($id_kardex,$num_solicitud,$id_alumno,$clave_oficial,$id_semestre,$id_periodo_escolar,$datosBajaDefinitiva["materia"],7));


						}



					    




						// if($datosSolicitudCambioCarrera['no_de_control'] != '')
						// {


						// 	$sql =	"select max(num_solicitud) as num_solicitud from solicitudes";

						// 	$query = $this->db->query($sql);


						// 	$num_solicitud = $query->result()[0]->num_solicitud;



						// 	$sql =	"insert into rel_solicitud_cambio_carrera 	(
						// 															id_alumno,
						// 															no_de_control,
						// 															num_solicitud,
						// 															clave_oficial,
						// 															id_semestre,
						// 															id_periodo_escolar,
						// 															grupo,
						// 															fecha
						// 														) 
						// 											  values 	(
						// 															?,
						// 															?,
						// 															?,
						// 															?,
						// 															?,
						// 															?,
						// 															?,
						// 															now()
						// 														)";

						// 	$query = $this->db->query($sql,array($id_alumno,$datosSolicitudCambioCarrera["no_de_control"],$num_solicitud,$datosSolicitudCambioCarrera["clave_oficial"],$datosSolicitudCambioCarrera["id_semestre"],$datosSolicitudCambioCarrera["id_periodo_escolar"],$datosSolicitudCambioCarrera["grupo"]));




						// }

				
					}
					


					$resultado_query = array(
												 'status' => '',
												 'mensaje' => ''
											);


					if ($this->db->trans_status() === FALSE)
					{
							$resultado_query['status'] = 'ERROR'; 
							$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la pagina e intente de nuevo'; 

					        $this->db->trans_rollback();
					}
					else
					{
						$resultado_query["status"] = "OK";
						$resultado_query["mensaje"] = "La solcitud fue realizada correctamente";

					    $this->db->trans_commit();
					}


		return $resultado_query;

	}


	public function cargarSelectMateriaSemestre($no_de_control,$id_semestre,$clave_oficial)
	{

		$sql = "select 
						ma.materia,
				        ma.nombre_completo_materia
				from materias ma
				join rel_materias_carreras rmc on (ma.materia = rmc.materia)
				where clave_oficial = ? and id_semestre = ?";

		$query = $this->db->query($sql,array($clave_oficial,$id_semestre));		
		
		return $query->result();

	}



	public function getDatosSecretario()
	{

		$sql =	"select (
				        	select 
				        			CONCAT(profesion, ' ', nombre, ' ', apellidos) 
				        	from  personal
				        	where id_comite = 2 limit 0,1
				        ) as secretario,
				        (
				        	select 
				        			cd.departamento 
				        	from  personal per
				        	join cat_departamentos cd on (per.id_departamento = cd.id_departamento)
				        	where id_comite = 2 limit 0,1							        	                                    
				        ) as departamento ";

		$query = $this->db->query($sql);


		$resultado_query = array(
										'msjCantidadRegistros'=> 0,
										'personal'=> array(),
										 'status' => '',
										 'mensaje' => ''
									);

		if($query)
		{

			if($query->num_rows()>0)
			{
				
				if($query)
				{

					if($query->num_rows()>0)
					{
						$resultado_query['msjCantidadRegistros'] = $query->num_rows();
						$resultado_query['personal'] = $query->result(); 
						$resultado_query['status'] = 'OK'; 
						$resultado_query['mensaje'] = 'información obtenida';
					}
					else
					{
						$resultado_query['msjCantidadRegistros'] = $query->num_rows();
						$resultado_query['personal'] = $query->result(); 
						$resultado_query['status'] = 'Sin datos';
						$resultado_query['mensaje'] = 'no hay datos que mostrar'; 
					}

				}
				else{
						$resultado_query['status'] = 'ERROR'; 
						$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
				}

			}

		}
		else{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
		}


		

		


		


		
		

		return $resultado_query;



	}



}
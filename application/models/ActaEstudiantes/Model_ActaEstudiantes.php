<?php 
class Model_ActaEstudiantes extends CI_Model
{
		

	public function __construct()
	{
		parent::__construct();
	}


	public function generarActaEstudiantes($id_periodo_escolar,$id_reunion)
	{

			$sql = "select 	distinct
									soli.num_solicitud,
									rsk.id_tipo_solicitud,
							        UPPER(tp.tipo_solicitud) as tipo_solicitud,
							        UPPER(soli.status) as status,
									UPPER(CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno)) as nombreCompleto,
								   UPPER(car.nombre_carrera) as nombre_carrera,
							       UPPER(kar.no_de_control ) as no_de_control,
							       UPPER(sem.nombre_semestre) as nombre_semestre,
							       UPPER((
							       			select GROUP_CONCAT(TRIM(materia) SEPARATOR ',_') 
							       				from rel_datos_materia_tipo_solicitud rdmt2
							       			  where rdmt2.num_solicitud = rsk.num_solicitud 
							       	     ))
							          as materia,
							       	  UPPER((
								       			select GROUP_CONCAT(TRIM(ma.nombre_completo_materia) SEPARATOR ',_') 
								       				from rel_datos_materia_tipo_solicitud rdmt2
								       				join materias ma on(rdmt2.materia = ma.materia)
								       			where rdmt2.num_solicitud = rsk.num_solicitud 
							       	         )) 
							             as nombre_completo_materia,
	
							       (
											select
											distinct  
										sum(rmc2.creditos_materia) 
									from alumnos alu2
										 join kardex kar2 on (alu2.id_alumno = kar2.id_alumno) 
							             join solicitudes soli2 on (soli2.id_alumno = alu2.id_alumno and soli2.id_semestre = kar2.id_semestre and soli2.id_periodo_escolar = kar2.id_periodo_escolar)
							             join rel_kardex_materias rkm2 on (kar2.id_kardex = rkm2.id_kardex)
							             join rel_datos_materia_tipo_solicitud rmtp2 on (soli2.num_solicitud = rmtp2.num_solicitud and kar2.id_kardex = rmtp2.id_kardex)
							             join rel_materias_carreras rmc2 on ( rkm2.materia = rmc2.materia and kar2.clave_oficial = rmc2.clave_oficial  )
										 join materias ma2 on (rmc2.materia = ma2.materia)
							             where soli2.num_solicitud = soli.num_solicitud and kar2.id_periodo_escolar = kar.id_periodo_escolar
							             group by kar2.id_alumno
							                      ,kar2.id_semestre
							                      ,kar2.id_periodo_escolar
							                      ,rmtp2.id_tipo_solicitud
							                      ,rmtp2.materia
							       ) as total_creditos,
							      UPPER(pe.identificacion_larga) as periodo_escolar,
							      soli.respuesta_solicitud_acta,
							      soli.verificacion_manual_lineamiento
							from alumnos alu
									 join kardex kar on (alu.id_alumno = kar.id_alumno)
									 join solicitudes soli on (soli.id_alumno = alu.id_alumno and soli.id_semestre = kar.id_semestre and soli.id_periodo_escolar = kar.id_periodo_escolar)
							         join rel_datos_materia_tipo_solicitud rsk on (soli.num_solicitud = rsk.num_solicitud)
									 join semestres sem on (kar.id_semestre = sem.id_semestre)
									join carreras car on (alu.clave_oficial = car.clave_oficial)
							        left join materias ma on(rsk.materia = ma.materia)
							        join cat_tipo_solicitud tp on (rsk.id_tipo_solicitud = tp.id_tipo_solicitud)
							        join periodos_escolares pe on (kar.id_periodo_escolar = pe.id_periodo_escolar )
							        join reuniones re on (soli.id_reunion =  re.id_reunion)
							         where kar.id_periodo_escolar = ? and alu.estado = '1' and re.id_reunion = ? ";

			$query = $this->db->query($sql,array($id_periodo_escolar,$id_reunion));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'Acta'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['Acta'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['Acta'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}


	public function getInfoReunion($id_periodo_escolar,$id_reunion)
	{

			$sql = "select 
							id_reunion,
					        nombre_reunion,
					        id_periodo_escolar,
					        no_libro,
					        no_acta,
					        mensaje_1,
					        personal,
					        mensaje_2
					from reuniones
					where id_periodo_escolar = ? and id_reunion = ? ";

			$query = $this->db->query($sql,array($id_periodo_escolar,$id_reunion));		


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['cantidad_reuniones'] = $query->num_rows();
					$resultado_query['Reunion'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['cantidad_reuniones'] = 0;
					$resultado_query['Reunion'] = $query->result(); 
					$resultado_query['status'] = 'NO_DATA';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}



	public function verificarPeriodoEscolarReunion($id_periodo_escolar)
	{

			$sql = "select 
							id_reunion,
					        nombre_reunion,
					        pe.id_periodo_escolar,
					        pe.identificacion_larga as periodo_escolar,
					        no_libro,
					        no_acta,
					        mensaje_1,
					        personal,
					        mensaje_2
					from reuniones re
					join periodos_escolares pe on( re.id_periodo_escolar = pe.id_periodo_escolar)
					where pe.id_periodo_escolar = ?";

			$query = $this->db->query($sql,array($id_periodo_escolar));


			$sql1 = "select 
							num_solicitud
					from solicitudes
					where id_periodo_escolar = ?";

			$query1 = $this->db->query($sql1,array($id_periodo_escolar));			


			if($query)
			{

				if($query->num_rows()>0 && $query1->num_rows()>0)
				{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['solicitudes'] = $query->result();
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{

					$resultado_query['status'] = 'NO_DATA';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}




}

?>


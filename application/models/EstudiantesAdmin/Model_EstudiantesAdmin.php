<?php 
class Model_EstudiantesAdmin extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}



	public function cargarTablaEstudiantesKardex($request)
	{

					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];


					
					$sqlEstudiantesKardex =	"select 
													alu.id_alumno,
													concat(nombre_alumno,' ',apellido_paterno,' ',apellido_materno) as alumno,
													ka.no_de_control,
											        ca.nombre_reducido carrera,
											        se.nombre_semestre semestre,
											        pe.identificacion_larga as periodo_escolar,
											        '<button  type=''button'' class=''btn btn-primary btn btnKardexEstudiante''> <span class=''glyphicon glyphicon-list-alt''></span> </button>' as kardex
													from alumnos alu
											join kardex ka on (alu.id_alumno = ka.id_alumno)
											join carreras ca on (ka.clave_oficial = ca.clave_oficial)
											join semestres se on (ka.id_semestre = se.id_semestre)
											join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
											where ka.id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno) 
											and alu.estado = '1'
											order by ".$columna." ".$ordenacion." ";

					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sqlEstudiantesKardex);


					

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sqlEstudiantesKardex = "select
															id_alumno,
															alumno,
															no_de_control,
															carrera,
															semestre,
															periodo_escolar,
															kardex
												From(
															select 
															alu.id_alumno ,
															concat(nombre_alumno,' ',apellido_paterno,' ',apellido_materno) as alumno,
															ka.no_de_control,
													        ca.nombre_reducido as carrera,
													        se.nombre_semestre as semestre,
													        pe.identificacion_larga as periodo_escolar,
													        '<button  type=''button'' class=''btn btn-primary btn btnKardexEstudiante''> <span class=''glyphicon glyphicon-list-alt''></span> </button>' as kardex
															from alumnos alu
															join kardex ka on (alu.id_alumno = ka.id_alumno)
															join carreras ca on (ka.clave_oficial = ca.clave_oficial)
															join semestres se on (ka.id_semestre = se.id_semestre)
															join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
															where ka.id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno)
															and alu.estado = '1' 
												) as Mytable 
												where  
													 ( 
														 alumno like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
														 no_de_control like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
														 carrera like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
														 semestre like '%".$this->db->escape_str($requestData['search']['value'])."%' or
														 periodo_escolar like '%".$this->db->escape_str($requestData['search']['value'])."%'
												     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sqlEstudiantesKardex);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sqlEstudiantesKardex .= $limit;
		                
		            $query = $this->db->query($sqlEstudiantesKardex);



		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

							    $nestedData[] = $row["id_alumno"];
							    $nestedData[] = $row["alumno"];
								$nestedData[] = $row["no_de_control"];
								$nestedData[] = $row["carrera"];
								$nestedData[] = $row["semestre"];
								$nestedData[] = $row["periodo_escolar"];
								$nestedData[] = $row["kardex"];

								$data[] = $nestedData;
							}


				$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
					"recordsTotal"    => intval( $totalData ),  // total number of records
					"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
					"data"            => $data  // total data array
					);


				return $json_data;


	}


	public function cargarTablaEstudiantesEvaluar($request)
	{

					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];


					
					$sqlEstudiantesKardex =	"select 
													alu.id_alumno,
													concat(nombre_alumno,' ',apellido_paterno,' ',apellido_materno) as alumno,
													ka.no_de_control,
											        ca.nombre_reducido carrera,
											        se.nombre_semestre semestre,
											        pe.identificacion_larga as periodo_escolar,
											        '<button  type=''button'' class=''btn btn-primary btn btnEvaluarEstudiante''> <span class=''glyphicon glyphicon-list-alt''></span> </button>' as Evaluar
													from alumnos alu
											join kardex ka on (alu.id_alumno = ka.id_alumno)
											join carreras ca on (ka.clave_oficial = ca.clave_oficial)
											join semestres se on (ka.id_semestre = se.id_semestre)
											join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
											where ka.id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno)
											and alu.estado = '1' 
											order by ".$columna." ".$ordenacion." ";

					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sqlEstudiantesKardex);


					

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sqlEstudiantesKardex = "select
															id_alumno,
															alumno,
															no_de_control,
															carrera,
															semestre,
															periodo_escolar,
															Evaluar
												From(
															select 
															alu.id_alumno ,
															concat(nombre_alumno,' ',apellido_paterno,' ',apellido_materno) as alumno,
															ka.no_de_control,
													        ca.nombre_reducido as carrera,
													        se.nombre_semestre as semestre,
													        pe.identificacion_larga as periodo_escolar,
													        '<button  type=''button'' class=''btn btn-primary btn btnEvaluarEstudiante''> <span class=''glyphicon glyphicon-list-alt''></span> </button>' as Evaluar
															from alumnos alu
															join kardex ka on (alu.id_alumno = ka.id_alumno)
															join carreras ca on (ka.clave_oficial = ca.clave_oficial)
															join semestres se on (ka.id_semestre = se.id_semestre)
															join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
															where ka.id_kardex = (select max(id_kardex) from kardex where id_alumno = alu.id_alumno)
															and alu.estado = '1' 
												) as Mytable 
												where  
													 ( 
														 alumno like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
														 no_de_control like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
														 carrera like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
														 semestre like '%".$this->db->escape_str($requestData['search']['value'])."%' or
														 periodo_escolar like '%".$this->db->escape_str($requestData['search']['value'])."%'
												     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sqlEstudiantesKardex);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sqlEstudiantesKardex .= $limit;
		                
		            $query = $this->db->query($sqlEstudiantesKardex);



		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

							    $nestedData[] = $row["id_alumno"];
							    $nestedData[] = $row["alumno"];
								$nestedData[] = $row["no_de_control"];
								$nestedData[] = $row["carrera"];
								$nestedData[] = $row["semestre"];
								$nestedData[] = $row["periodo_escolar"];
								$nestedData[] = $row["Evaluar"];

								$data[] = $nestedData;
							}


				$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
					"recordsTotal"    => intval( $totalData ),  // total number of records
					"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
					"data"            => $data  // total data array
					);


				return $json_data;


	}


	public function getDatoskardexEstudiante($id_alumno)
	{

			$sql = "select  
							concat(alu.nombre_alumno,' ',alu.apellido_paterno,' ',alu.apellido_materno) as alumno,
							ka.no_de_control,
							ca.clave_oficial,
					        ca.nombre_carrera,
					        se.nombre_semestre,
					        ma.nombre_completo_materia,
					        rkm.materia,
							rmc.creditos_materia,
							rmc.creditos_materia cursados,
							COALESCE(ka.creditos_aprobados, 0) aprobados,
							rkm.aprobada as materia_aprobada,
							pe.identificacion_larga as periodo_escolar,
							   ( case rkm.tipo_materia
                               when 1 then 'Ev.Ord.1ra'
                               when 2 then 'Ev.Reg.1ra'
                               else 'Ev.Esp.1ra'
                               end ) as tipo_materia
					from alumnos alu
					join kardex ka on (alu.id_alumno = ka.id_alumno)
					join rel_kardex_materias rkm on (ka.id_kardex = rkm.id_kardex)
					join rel_materias_carreras rmc on ( rmc.materia = rkm.materia and rmc.clave_oficial = ka.clave_oficial) 
					join materias ma on (rmc.materia = ma.materia)
					join carreras ca on (rmc.clave_oficial = ca.clave_oficial)
					join semestres se on (ka.id_semestre = se.id_semestre)
					join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
					and alu.estado = '1' and rkm.status = '1'
					where alu.id_alumno = ? ";

			$query = $this->db->query($sql,array($id_alumno));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'kardex'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['kardex'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['kardex'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}


	public function getInfoEvaluarAlumno($id_alumno)
	{

			$sql = "select  
							concat(nombre_alumno,' ',apellido_paterno,' ',apellido_materno) as alumno,
							alu.id_alumno,
							ka.no_de_control,
							ca.clave_oficial,
					        ca.nombre_carrera,
					        se.nombre_semestre,
					        se.id_semestre,
							pe.id_periodo_escolar,
					        ma.nombre_completo_materia,
					        rkm.materia
							, rmc.creditos_materia creditos_cursados
					        ,rkm.aprobada materia_aprobada
					from alumnos alu
					join kardex ka on (alu.id_alumno = ka.id_alumno)
					join rel_kardex_materias rkm on (ka.id_kardex = rkm.id_kardex)
					join rel_materias_carreras rmc on ( rmc.materia = rkm.materia and rmc.clave_oficial = ka.clave_oficial) 
					join materias ma on (rmc.materia = ma.materia)
					join carreras ca on (rmc.clave_oficial = ca.clave_oficial)
					join semestres se on (ka.id_semestre = se.id_semestre)
					join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar)
					where alu.id_alumno = ? and rkm.status = '1' and alu.estado = '1' ";

			$query = $this->db->query($sql,array($id_alumno));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'evaluar'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['evaluar'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['evaluar'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}



	public function evaluarAlumnoMateria($id_alumno,$no_de_control,$clave_oficial,$id_periodo_escolar,$id_semestre,$clave_materia)
	{



			// $sql = "select distinct ka.id_kardex,
			// 						rdmtp.materia,
			// 		                soli.num_solicitud,
			// 		                se.nombre_semestre,
   //              					ctp.tipo_solicitud
			// 								 from kardex ka
			// 							join solicitudes soli on ( ka.id_alumno = soli.id_alumno and status = 'Aceptado')
			// 		                    join rel_datos_materia_tipo_solicitud rdmtp on (soli.num_solicitud = rdmtp.num_solicitud and ka.id_kardex = rdmtp.id_kardex )
			// 		                    join semestres se on ( soli.id_semestre = se.id_semestre )
   //                  					join cat_tipo_solicitud ctp on (ctp.id_tipo_solicitud = rdmtp.id_tipo_solicitud)
			// 							where 		ka.id_alumno = ?
			// 									and ka.no_de_control = ? 
			// 		                            and rdmtp.materia = ?
			// 		                            and ka.id_kardex != (
			// 															select id_kardex 
			// 		                                                    from kardex 
			// 		                                                    where id_alumno = ?
			// 															and no_de_control = ? 
			// 		                                                    and clave_oficial = ?
			// 		                                                    and id_periodo_escolar = ?
			// 		                                                    and id_semestre = ?
																		
			// 														) ";

			// $query = $this->db->query($sql,array($id_alumno,$no_de_control,$clave_materia,$id_alumno,$no_de_control,$clave_oficial,$id_periodo_escolar,$id_semestre));


			// if($query)
			// {

			// 	if($query->num_rows()>0)
			// 	{

			// 		$tipo_solicitud = $query->result()[0]->tipo_solicitud;
			// 		$nombre_semestre = $query->result()[0]->nombre_semestre;

			// 		$resultado_query['status'] = 'Repite'; 
			// 		$resultado_query['mensaje'] = 'No se puede evaluar esta materia debido a que la lleva de '.$tipo_solicitud." en ".$nombre_semestre;
			// 		$resultado_query['creditos_materia'] = '';
			// 	}
			// 	else
			// 	{

					// join rel_materias_carreras rmc on ( rmc.clave_oficial = ka.clave_oficial and rmc.id_semestre = ka.id_semestre)


						$sql = "select distinct ka.id_kardex ,rmc.creditos_materia,horas_teoricas,horas_practicas
									 from kardex ka 
									 join rel_materias_carreras rmc on ( rmc.clave_oficial = ka.clave_oficial)
								where 		ka.id_alumno = ? 
										and ka.no_de_control = ? 
										and ka.clave_oficial = ? 
										and ka.id_periodo_escolar = ? 
										and ka.id_semestre = ? 
										and rmc.materia = ? ";

						$query = $this->db->query($sql,array($id_alumno,$no_de_control,$clave_oficial,$id_periodo_escolar,$id_semestre,$clave_materia));		

						if($query)
						{

							if($query->num_rows()>0)
							{					

								$id_kardex = $query->result()[0]->id_kardex;
								$creditos_materia = $query->result()[0]->creditos_materia;


								$sql = "select aprobada from rel_kardex_materias where id_kardex = ? and materia = ? ";

								$query = $this->db->query($sql,array($id_kardex,$clave_materia));

								$aprobada = $query->result()[0]->aprobada;

								if($aprobada == '0' || $aprobada == "")
								{
									$sql = "update rel_kardex_materias set aprobada = '1' where id_kardex = ? and materia = ? ";

									$query = $this->db->query($sql,array($id_kardex,$clave_materia));

									if($query)
									{
										$resultado_query = array(
															 'status' => '',
															 'creditos_materia' => '',
															 'aprobada' => '1',
															 'mensaje' => ''
														);

										$resultado_query['status'] = 'OK'; 
										$resultado_query['mensaje'] = 'La materia fue aprobada correctamente';
										$resultado_query['creditos_materia'] = $creditos_materia;

									}
									else
									{
											$resultado_query['status'] = 'ERROR'; 
											$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
											$resultado_query['creditos_materia'] = '';
									}

								}
								else
								{
									$sql = "update rel_kardex_materias set aprobada = '0' where id_kardex = ? and materia = ? ";

									$query = $this->db->query($sql,array($id_kardex,$clave_materia));

									if($query)
									{
										$resultado_query = array(
															 'status' => '',
															 'creditos_materia' => '',
															 'aprobada' => '0',
															 'mensaje' => ''
														);

										$resultado_query['status'] = 'OK'; 
										$resultado_query['mensaje'] = 'La materia fue desaprobada correctamente';
										$resultado_query['creditos_materia'] = $creditos_materia;

									}
									else
									{
											$resultado_query['status'] = 'ERROR'; 
											$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
											$resultado_query['creditos_materia'] = '';
									}


								}


						
							}
							

						}
						else{
								$resultado_query['status'] = 'ERROR'; 
								$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
						}



				//}




			// }
			// else{
			// 		$resultado_query['status'] = 'ERROR'; 
			// 		$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			// 	}


			
			
			
			return $resultado_query;

	}


	public function cargarTablaModificarEstudiantes($request)
	{

					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];


					
					$sql =	"select 
													id_alumno,
											        no_de_control,
											        nombre_alumno,
											        apellido_paterno,
											        apellido_materno,
											        curp_alumno,
													CONCAT(nombre_alumno, ' ', apellido_paterno , ' ', apellido_materno) as nombreCompleto,
													'<button  type=''button'' class=''btn btn-primary btn btnModificarEstudiante''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
													'<button  type=''button'' class=''btn btn-primary btn btnEliminarEstudiante''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
											from alumnos where estado = '1'
											order by ".$columna." ".$ordenacion." ";

					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sql);


					

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sql = "select
															id_alumno,
															no_de_control,
															nombre_alumno,
															apellido_paterno,
															apellido_materno,
															curp_alumno,
															nombreCompleto,
															Modificar,
															Eliminar
													From(
															select 
																	id_alumno,
															        no_de_control,
															        nombre_alumno,
															        apellido_paterno,
															        apellido_materno,
															        curp_alumno,
																	CONCAT(nombre_alumno, ' ', apellido_paterno , ' ', apellido_materno) as nombreCompleto,
																	'<button  type=''button'' class=''btn btn-primary btn btnModificarEstudiante''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
													'<button  type=''button'' class=''btn btn-primary btn btnEliminarEstudiante''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
															from alumnos where estado = '1'
												) as Mytable 
												where  
													 ( 
														 no_de_control like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
														 nombreCompleto like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
														 curp_alumno like '%".$this->db->escape_str($requestData['search']['value'])."%' 
												     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sql);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sql .= $limit;
		                
		            $query = $this->db->query($sql);


		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

							    $nestedData[] = $row["id_alumno"];
							    $nestedData[] = $row["no_de_control"];
								$nestedData[] = $row["nombre_alumno"];
								$nestedData[] = $row["apellido_paterno"];
								$nestedData[] = $row["apellido_materno"];
								$nestedData[] = $row["curp_alumno"];
								$nestedData[] = $row["Modificar"];
								$nestedData[] = $row["Eliminar"];
								

								$data[] = $nestedData;
							}


				$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
					"recordsTotal"    => intval( $totalData ),  // total number of records
					"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
					"data"            => $data  // total data array
					);


				return $json_data;


	}


	public function getInfoEstudiante($id_alumno)
	{

			$sql = "select  
							id_alumno,
							no_de_control,
							nombre_alumno,
							apellido_paterno,
							apellido_materno,
							curp_alumno
					from alumnos 
					where id_alumno = ? and estado = '1' ";

			$query = $this->db->query($sql,array($id_alumno));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'alumno'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['alumno'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['alumno'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}


	public function checkModificarNumeroControlEstudiante($no_de_control_new,$no_de_control_origen)
	{

		$sql1 =	"select no_de_control from alumnos where no_de_control = ? and estado = '1' ";
		
		$query1 = $this->db->query($sql1,array($no_de_control_origen));

		if($query1)
		{

			if($query1->num_rows()>0)
			{
				
				$no_de_control_origen = $query1->result()[0]->no_de_control;

				if($no_de_control_origen == $no_de_control_new)
				{
					$resultado_query = array(
										'resultado'=>'OK',
										'mensaje'=>'se puede ocupar el numero de control'
									);
				}
				else
				{
					$sql2 =	"select no_de_control from alumnos where no_de_control = ? and estado = '1' ";
		
					$query2 = $this->db->query($sql2,array($no_de_control_new));

					if($query2)
					{

						if($query2->num_rows()>0)
						{
							$resultado_query = array(
										'resultado'=>'NO_DISPONIBLE',
										'mensaje'=>'El numero de control no esta disponible'
									);
						}
						else
						{
							$resultado_query = array(
										'resultado'=>'OK',
										'mensaje'=>'se puede ocupar'
									);
						}
					}
					else
					{
						$resultado_query = array(
													'resultado'=>'ERROR',
													'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
												);
					}



				}
			}
		}
		else
		{
			$resultado_query = array(
										'resultado'=>'ERROR',
										'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
									);
		}


		

		return $resultado_query;



	}


    public function modificarDatosEstudiante($id_alumno,$no_de_control,$nombre_alumno,$apellido_paterno,$apellido_materno,$curp_alumno)
	{

		$this->db->trans_begin();
		
		$sql =	"update alumnos set 
									no_de_control = ?,
									nombre_alumno = ? ,
									apellido_paterno = ? ,
									apellido_materno = ? ,
									curp_alumno = ?
								where 
									id_alumno = ? and estado = '1' ";

		$query = $this->db->query($sql,array($no_de_control,$nombre_alumno,$apellido_paterno,$apellido_materno,$curp_alumno,$id_alumno));

		$sql =	"update kardex set 
									no_de_control = ?
								where 
									id_alumno = ? ";

		$query = $this->db->query($sql,array($no_de_control,$id_alumno));


		if ($this->db->trans_status() === FALSE)
		{
			$resultado_query['status'] = 'ERROR'; 
			$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la pagina e intente de nuevo'; 

	        $this->db->trans_rollback();
		}
		else
		{
			$resultado_query["status"] = "OK";
			$resultado_query["mensaje"] = "El estudiante ha sido modificado correctamente";

		    $this->db->trans_commit();
		}

		

		return $resultado_query;



	}


	// public function deleteEstudiantes2($id_alumno)
	// {

	// 	$sql =	"select distinct id_alumno 
	// 			from solicitudes 
	// 			where id_alumno = ? ";
		

	// 	$query = $this->db->query($sql,array($id_alumno));


	// 	$resultado_query = array(
	// 									'msjCantidadRegistros'=> 0,
	// 									'materia'=> array(),
	// 									 'status' => '',
	// 									 'mensaje' => ''
	// 								);


	// 	if($query)
	// 	{

	// 		if($query->num_rows()>0)
	// 		{
	// 			$resultado_query['msjCantidadRegistros'] = $query->num_rows();
	// 			$resultado_query['materia'] = $query->result(); 
	// 			$resultado_query['status'] = 'NO_DISPONIBLE'; 
	// 			$resultado_query['mensaje'] = 'No se puede eliminar el estudiante debido a que tiene solicitude(s) realizadas(s)';

		
	// 		}
	// 		else
	// 		{

	// 			$sql2 =	"delete from alumnos where id_alumno = ? and estado = '1' ";
				
	// 			$query2 = $this->db->query($sql2,array($id_alumno));

	// 			if($query2)
	// 			{
	// 				$resultado_query['status'] = 'OK'; 
	// 				$resultado_query['mensaje'] = 'El alumno ha sido eliminado correctamente';
	// 			}
	// 			else{
	// 				$resultado_query['status'] = 'ERROR'; 
	// 				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
	// 			}


	// 		}

	// 	}
	// 	else{
	// 			$resultado_query['status'] = 'ERROR'; 
	// 			$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
	// 	}
		

	// 	return $resultado_query;



	// }

	public function deleteEstudiantes($id_alumno)
	{

		$sql =	"select distinct id_alumno 
				from alumnos 
				where id_alumno = ? ";
		

		$query = $this->db->query($sql,array($id_alumno));


		$resultado_query = array(
										'msjCantidadRegistros'=> 0,
										'materia'=> array(),
										 'status' => '',
										 'mensaje' => ''
									);


		if($query)
		{

			if($query->num_rows()>0)
			{
				$sql2 =	"update alumnos set estado = '0' where id_alumno = ? ";
				
				$query2 = $this->db->query($sql2,array($id_alumno));

				if($query2)
				{
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'El estudiante ha sido dado de baja correctamente';
				}
				else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
				}

		
			}
			

		}
		else{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
		}
		

		return $resultado_query;



	}



	public function uploadExcelEvaluarEstudiantes($files)
	{

			// $this->db->trans_begin();

			// $sql = "select pdf_poliza from rel_polizas_pdf_clientes where id_usuario = ? and id_poliza = ? ";


			// $query = $this->db->query($sql,array($id_usuario,$id_poliza));


					if(!empty($files))
					{
							$nombreAleatorio = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',20)),0,20);

							$ruta = $_SERVER['DOCUMENT_ROOT']."/Solicitud/application/uploads/";

							$nombreOriginal = "";

							$nombreMejorado = "";

							// return $files;
							// exit();

							foreach ( $files as $key) 
							{

								if($key['error'] == UPLOAD_ERR_OK)
								{
									//$nombreOriginal = $key["name"];
									$tipoImage = explode(".", $key["name"]);

									//return $key["name"];

									$nombreMejorado = $nombreAleatorio.".".$tipoImage[1];
									$temporal = $key["tmp_name"];
									//$temporal = $nombreAleatorio;
									$destino = $ruta.$nombreMejorado;

									//return $destino;

									$subir = move_uploaded_file($temporal, $destino);





								}


							}
						

					}

			

			if ($this->db->trans_status() === FALSE)
			{

					$datos["status"] = "ERROR";
					$datos["msjConsulta"] = "Falló al actualizar los datos del usuario intente de nuevo";

			        $this->db->trans_rollback();
			}
			else
			{
				$datos["status"] = "OK";
				$datos["msjConsulta"] = "Archivo subido correctamente";


			    $this->db->trans_commit();
			}

		    return $nombreMejorado;

	}




	public function evaluarEstudiantesWithExcel($arr_data)
	{	

  			$tamano = count($arr_data);

        	$this->db->trans_begin();

        	$datosMaterias = "";

        	$errorEvaluacion = "NO";


			for($x=2; $x <= $tamano+1 ; $x++)
	        {
		            $no_de_control = $arr_data[$x]["A"];
		            $clave_oficial = $arr_data[$x]["B"];
		            $id_semestre = $arr_data[$x]["C"];
		            $materia = $arr_data[$x]["D"];
		            $aprobada = $arr_data[$x]["E"];


		            $sql = "select 	
							      ka.id_kardex,
							      CONCAT(alu.nombre_alumno, ' ', alu.apellido_paterno , ' ', alu.apellido_materno) as estudiante,
							      ma.nombre_completo_materia,
							      se.nombre_semestre
					from rel_kardex_materias rkm
					join kardex ka on (rkm.id_kardex = ka.id_kardex)
					join alumnos alu on (ka.no_de_control = alu.no_de_control)
					join materias ma on (rkm.materia = ma.materia)
					join semestres se on (ka.id_semestre = se.id_semestre)
					where 	ka.no_de_control = ?
							and ka.id_semestre = ?
							and ka.clave_oficial = ?
					        and rkm.materia = ?
					        and rkm.status = '1' ";

		            $query = $this->db->query($sql,array($no_de_control,$id_semestre,$clave_oficial,$materia));

		             
		             if($aprobada == "0" || $aprobada == "1")
		             {
	     		            if($query->num_rows()>0)
	     					{
	     						$id_kardex = $query->result()[0]->id_kardex;
	     						

	     	    	             $sql = "update rel_kardex_materias set aprobada = ? 
	     	    	             				where id_kardex = ? and 
	     	    	             					  materia = ? 
	     	    	             					  and status = '1' ";

	     	    			     $query = $this->db->query($sql,array($aprobada,$id_kardex,$materia));


	     					}
	     					else
	     					{
	     						$errorEvaluacion="SI";

	     						$x = 9999999999;
	     						
	     					}
		             }
		             else{
		             		$errorEvaluacion="SI";

	     						

	     						$x = 9999999999;
		             }
		              

	        }

        	if ($this->db->trans_status() === FALSE)
			{

					$datos["status"] = "ERROR";
					$datos["msjConsulta"] = "Falló al actualizar los datos del usuario intente de nuevo";

			        $this->db->trans_rollback();
			}
			else
			{
				

				if($errorEvaluacion == "SI")
				{
					$this->db->trans_rollback();
				}
				else
				{
					$this->db->trans_commit();
				}

				$datos["status"] = "OK";
				$datos["errorEvaluacion"] = $errorEvaluacion;
				$datos["msjConsulta"] = "bien";

			    
			}
   
		    return $datos;

	}



}
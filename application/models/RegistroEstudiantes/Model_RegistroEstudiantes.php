<?php 
class Model_RegistroEstudiantes extends CI_Model
{


	public function __construct()
	{
		parent::__construct();
	}

	public function insertRegistroEstudiantes($nuevoIngreso,$no_de_control,$id_semestre,$apellido_paterno,$apellido_materno,$nombre_alumno,$curp_alumno,$creditos_aprobados,$creditos_cursados,$clave_oficial,$id_periodo_escolar,$grupo)
	{



		$this->db->trans_begin();


		$sql = "select distinct alu.id_alumno 
					from alumnos alu
					join kardex ka on (alu.id_alumno = ka.id_alumno )
					where ka.no_de_control = ? and alu.estado = '1' ";
			

	   	$query = $this->db->query($sql,array($no_de_control));


	   		// si el alumno es de nuevo ingreso

	   		if($query->num_rows() == 0)
			{

				// insertando datos en la tabla de alumnos

				$sql = "insert into alumnos (
												id_alumno,
												no_de_control,
												id_semestre,
												apellido_paterno,
												apellido_materno,
												nombre_alumno,
												curp_alumno,
												creditos_aprobados,
												creditos_cursados,
												clave_oficial,
												estado
											) 
						values 
											(
												null,
												?,
												?,
												?,
												?,
												?,
												?,
												?,
												?,
												?,
												'1'
											)

											";
			

	   			$query = $this->db->query($sql,array($no_de_control,$id_semestre,$apellido_paterno,$apellido_materno,$nombre_alumno,$curp_alumno,0,0,$clave_oficial));



	   			// obteniendo el ultimo id del alumno acabado de insertar 

	   			$sql = "select max(id_alumno) as id_alumno from alumnos where estado = '1'";
			

	  			$query = $this->db->query($sql);

	  			$id_alumno = $query->result()[0]->id_alumno;


	  			// insertando datos en la tabla de kardex del alumno

	  			$sql = "insert into kardex (
	  											id_kardex,
												id_alumno,
												no_de_control,
												clave_oficial,
												id_semestre,
												id_periodo_escolar,
												creditos_aprobados,
												creditos_cursados,
												grupo,
												fecha
											) 
						values 
											(
												null,
												?,
												?,
												?,
												?,
												?,
												null,
												null,
												?,
												now()
											)

											";
			

	   			$query = $this->db->query($sql,array($id_alumno,$no_de_control,$clave_oficial,$id_semestre,$id_periodo_escolar,$grupo));


	   			$sql = "select max(id_kardex) as id_kardex from kardex ";
			

	  			$query = $this->db->query($sql);

	  			$id_kardex = $query->result()[0]->id_kardex;


	  			$sql = "insert into rel_kardex_materias
									select ?,ma.materia,null,'1','1' from carreras ca
									join rel_materias_carreras rmc on (ca.clave_oficial = rmc.clave_oficial)
									join materias  ma on (rmc.materia = ma.materia)
									where rmc.id_semestre = ? and rmc.clave_oficial = ? ";
									             


				$query = $this->db->query($sql,array($id_kardex,$id_semestre,$clave_oficial));



			}
	   		else  // si el alumno no es de nuevo ingreso 
			{

				if($query->num_rows() > 0)
				{
						$id_alumno = $query->result()[0]->id_alumno;


							 $sql = "select 
												id_kardex
								 from kardex 
								 where  
								 				no_de_control = ? ";


							$query = $this->db->query($sql,array($no_de_control));

							$array_id_kardex = '';


							$x = 0;
							foreach ($query->result() as $row)
							{
								$array_id_kardex[$x] = $query->result()[$x]->id_kardex;

								$x++;
							}




						$sql =	"select no_de_control from kardex where 
													id_kardex = ( select max(id_kardex) from kardex where id_alumno = ? )";
			

						$query = $this->db->query($sql,array($id_alumno));


						$no_de_control = $query->result()[0]->no_de_control;


						$sql = "insert into kardex (
		  											id_kardex,
													id_alumno,
													no_de_control,
													clave_oficial,
													id_semestre,
													id_periodo_escolar,
													creditos_aprobados,
													creditos_cursados,
													grupo,
													fecha
												) 
							values 
												(
													null,
													?,
													?,
													?,
													?,
													?,
													null,
													null,
													?,
													now()
												)

												";
				

		   			$query = $this->db->query($sql,array($id_alumno,$no_de_control,$clave_oficial,$id_semestre,$id_periodo_escolar,$grupo));


		   			$sql = "select max(id_kardex) as id_kardex from kardex ";
			

		  			$query = $this->db->query($sql);

		  			$id_kardex = $query->result()[0]->id_kardex;


		  			$sql = "insert into rel_kardex_materias
										select ?,ma.materia,null,'1','1' from carreras ca
										join rel_materias_carreras rmc on (ca.clave_oficial = rmc.clave_oficial)
										join materias  ma on (rmc.materia = ma.materia)
										where rmc.id_semestre = ? and rmc.clave_oficial = ? ";
										             


					$query = $this->db->query($sql,array($id_kardex,$id_semestre,$clave_oficial));




					/* verificar si alguna materia de las que va a llevar en su actual semestre al cual se esta registrando ya la curso en algun semestre anterior y si es asi si ya la curso en un semestre anterior,  poner esa materia en su actual semestre como aprobada y de forma normal o ponerla como reprobada y de forma de curso de repeticion */



						 $sql = "select 
						 					materia 
								from rel_materias_carreras 
								 where 	id_semestre = ? 
								 	and clave_oficial = ? ";


							$query = $this->db->query($sql,array($id_semestre,$clave_oficial));

							$array_materias = '';


							$x = 0;
							foreach ($query->result() as $row)
							{
								$array_materias[$x] = $query->result()[$x]->materia;

								$x++;
							}



							// $sql = "select 
							// 				id_kardex,
							// 				materia,
							// 		        aprobada,
							// 		        tipo_materia,
							// 		        status
							// 		from rel_kardex_materias
							// 		where id_kardex in (select id_kardex from kardex where no_de_control = ? )
									// and materia in (
									// 					select materia from rel_materias_carreras 
									// 					where id_semestre = ? and clave_oficial = ? ) ";
			             


							// $query = $this->db->query($sql,array($no_de_control,$id_semestre,$clave_oficial));



							// $sql = "select 
							// 				id_kardex,
							// 				materia,
							// 		        aprobada,
							// 		        tipo_materia,
							// 		        status
							// 		from rel_kardex_materias";
			             

				             $this->db->select('id_kardex, materia, aprobada,tipo_materia,status');
							 $this->db->from('rel_kardex_materias');


							$this->db->where_in('id_kardex',$array_id_kardex);
							$this->db->where_in('materia',$array_materias);
							$query = $this->db->get();



							foreach ($query->result() as $row)
							{
								  
							        $materia = $row->materia;
							        $aprobada = $row->aprobada;
							        $tipo_materia = $row->tipo_materia;
							        $status = $row->status;

							        if($aprobada == '1' )
							        {
							        	$sql = "update rel_kardex_materias set aprobada = '1' where  id_kardex = ? and materia = ? ";
							        }
							        else if($aprobada == '0' )
							        {
							        	$sql = "update rel_kardex_materias set aprobada = null, tipo_materia = '2' where  id_kardex = ? and materia = ? ";
							        }


								 $query = $this->db->query($sql,array($id_kardex,$materia));



							}

						



				}

			}
			
	   	



	   			$resultado_query = array(
											 'status' => '',
											 'mensaje' => ''
										);


				if ($this->db->trans_status() === FALSE)
				{
						$resultado_query['status'] = 'ERROR'; 
						$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la pagina e intente de nuevo'; 

				        $this->db->trans_rollback();
				}
				else
				{
					$resultado_query["status"] = "OK";
					$resultado_query["mensaje"] = "El estudiante fue registrado correctamente";

				    $this->db->trans_commit();
				}
	   
				return $resultado_query;

	}


	public function validateDatosRegistroEstudiantes($nuevoIngreso,$no_de_control,$id_periodo_escolar,$clave_oficial,$id_semestre)
	{

		
		$sql =	"select distinct id_alumno from kardex where no_de_control = ? ";

		$query = $this->db->query($sql,array($no_de_control));


		$resultado_query = array(
										 'status' => '',
										 'mensaje' => ''
									);

		$resultado_query['status'] = 'OK'; 
		$resultado_query['mensaje'] = '';

	   	if($query)
		{
			if($query->num_rows()>0)
			{
				if($nuevoIngreso == '1')
				{
					$resultado_query['status'] = 'Error'; 
					$resultado_query['select'] = 'control'; 
					$resultado_query['mensaje'] = 'El número de control <strong> '.$no_de_control. ' </strong> ya esta en uso';
				}
				// else
				// {
				// 	$resultado_query['status'] = 'OK'; 
				// 	$resultado_query['mensaje'] = 'Es Estudiante de nuevo ingreso';
				// }
				else
				{

						$id_alumno = $query->result()[0]->id_alumno;

								// validaciones de periodo escolar , carrera , y semestre 
								

								$sql =	"select distinct
										alu.id_alumno,
										ka.id_periodo_escolar,
										ka.clave_oficial,
										ka.id_semestre,
										pe.periodo
									from alumnos alu
									join kardex ka on (alu.id_alumno = ka.id_alumno and id_kardex = (select max(id_kardex) from kardex where id_alumno = ? ) )
									join periodos_escolares pe on (ka.id_periodo_escolar = pe.id_periodo_escolar) 
									where ka.no_de_control  = ? and alu.estado = '1'";
								

								$query = $this->db->query($sql,array($id_alumno,$no_de_control));

								$ultimo_id_periodo_escolar = (int) $query->result()[0]->id_periodo_escolar;
								$ultimo_clave_oficial = $query->result()[0]->clave_oficial;
								$ultimo_id_semestre = (int) $query->result()[0]->id_semestre;

								$periodoActual = (int) $query->result()[0]->periodo;


								$anioA  = (int) substr($periodoActual,0,4);
								$numA = (int) substr($periodoActual,4,4);


								$sql =	"select periodo from periodos_escolares where id_periodo_escolar = ?";
								
								$query = $this->db->query($sql,array($id_periodo_escolar));

								$periodoFormRegistro = (int) $query->result()[0]->periodo;


								$anioS  = (int) substr($periodoFormRegistro,0,4);
								$numS = (int) substr($periodoFormRegistro,4,4);


								$errorPeriodo = '';

								if($numA < 3)
								  {
								    // 3     =  2    + 1 
								      $num_S = $numA + 1;

								    //     3     ==    3
								      if($num_S  == $numS )
								      {

								      //      2017  ==  2018
								          if($anioA == $anioS)
								          {
								             $errorPeriodo = "OK";
								          }
								           else
								          {
								              $errorPeriodo = "Error";
								          }
								      }
								      else
								      {
								          $errorPeriodo = "Error";
								      }


								  }
								  else
								  {
								     //   2018     2017  + 1
								         $anio_S = $anioA + 1;

								     //      2018      2018
								         if($anio_S == $anioS)
								         {
								            if($numS == 1)
								            {
								                $errorPeriodo = "OK";
								            }
								            else{
								              $errorPeriodo = "Error";
								            }
								         }
								         else{
								            $errorPeriodo = "Error";
								         }

								     
								  }


								$id_semestre = (int) $id_semestre;

								$id_periodo_escolar = (int) $id_periodo_escolar;

						      // if( $id_periodo_escolar > ($ultimo_id_periodo_escolar + 1) || $id_periodo_escolar  < ($ultimo_id_periodo_escolar + 1) )
								 if( $errorPeriodo == "Error")
								//if( $ultimo_id_periodo_escolar >= $id_periodo_escolar)
								{
									$resultado_query['status'] = 'Error'; 
									$resultado_query['select'] = 'periodo'; 
									$resultado_query['mensaje'] = 'No puedes elegir el periodo escolar';
								}
								else
								{
									if( $clave_oficial != $ultimo_clave_oficial )
									{
										$resultado_query['status'] = 'Error'; 
										$resultado_query['select'] = 'carrera'; 
										$resultado_query['mensaje'] = 'No puedes elegir la carrera de ';
									}
									else
									{
											if( $id_semestre  > ($ultimo_id_semestre + 1)  || $id_semestre  < ($ultimo_id_semestre + 1) )
											{


												$resultado_query['status'] = 'Error';
												$resultado_query['select'] = 'semestre'; 
												$resultado_query['mensaje'] = 'No puedes elegir el';
											}
											else
											{

												$resultado_query['status'] = 'OK'; 
												$resultado_query['mensaje'] = 'Es Estudiante que se va a registrar bien';

											}
											// else
											// {

											// 		$sql =	"select rkm.materia,ma.nombre_completo_materia as nombre_materia
											// 								from kardex kar
											// 						        join alumnos alu on (kar.id_alumno = alu.id_alumno)
											// 						        join rel_kardex_materias rkm on (kar.id_kardex = rkm.id_kardex)
											// 						        join materias ma on (rkm.materia = ma.materia)
											// 						where kar.no_de_control = ?
											// 							  and rkm.aprobada is null 
											// 						      and alu.id_alumno = ? and alu.estado = '1' and rkm.status = '1' ";
												

											// 		$query = $this->db->query($sql,array($no_de_control,$id_alumno));


											// 	   	if($query)
											// 		{
											// 			if($query->num_rows()>0)
											// 			{
											// 				$resultado_query['status'] = 'Error'; 
											// 				$resultado_query['select'] = 'materia'; 
											// 				$resultado_query['mensaje'] = 'No tienes todas las materias evaluadas';
											// 				$resultado_query['materias'] = $query->result();
											// 			}
											// 			else
											// 			{

											// 				$resultado_query['status'] = 'OK'; 
											// 				$resultado_query['mensaje'] = 'Es Estudiante que se va a registrar bien';

											// 			}
														
											// 		}

												
											//  }
									}

									
								}

								// Fin de validaciones de periodo escolar , carrera , y semestre 

						//	}
						//}

						
				}



				

			}
			else
			{
				$resultado_query['status'] = 'OK'; 
				$resultado_query['mensaje'] = 'Es Estudiante de nuevo ingreso';
			}
		}

	   		
	   
		return $resultado_query;

	}



}
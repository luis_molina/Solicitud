<?php 
class Model_Materias extends CI_Model
{
		public function __construct()
		{
			parent::__construct();
		}





		public function guardarMaterias($materia,$nombre_completo_materia,$nombre_abreviado_materia)
		{

			
			
			$sql =	"insert into materias (
											materia,
											nombre_completo_materia,
											nombre_abreviado_materia
											) 
											values (
												?,
												?,
												?
											)";

			$query = $this->db->query($sql,array($materia,$nombre_completo_materia,$nombre_abreviado_materia));


			if($query)
			{
				$resultado_query = array(
											'resultado'=>'OK',
											'mensaje'=>'La materia ha sido registrada correctamente'
										);
			}
			else{
				$resultado_query = array(
											'resultado'=>'ERROR',
											'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
										);
			}
			

			return $resultado_query;



		}

		public function checkClaveMateria($materia)
		{

			$sql =	"select materia from materias where materia = ? ";
			

			$query = $this->db->query($sql,array($materia));


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query = array(
											'resultado'=>'NO_DISPONIBLE',
											'mensaje'=>'La clave de la materia no esta disponible'
										);
			
				}
				else
				{
					$resultado_query = array(
											'resultado'=>'OK',
											'mensaje'=>'se puede ocupar la materia'
										);
				}

			}
			else{
				$resultado_query = array(
											'resultado'=>'ERROR',
											'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
										);
			}
			

			return $resultado_query;



		}


		public function cargarTablaMaterias($request)
		{

					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];

					
					$sqlMaterias =	"select 
											materia,
											nombre_completo_materia,
											nombre_abreviado_materia,
										    '<button  type=''button'' class=''btn btn-primary btn btnModificarMateria''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
										    '<button  type=''button'' class=''btn btn-primary btn btnEliminarMateria''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
										  from materias 
											order by ".$columna." ".$ordenacion." ";

					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sqlMaterias);

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sqlMaterias = "select 
											materia,
											nombre_completo_materia,
											nombre_abreviado_materia,
										    '<button  type=''button'' class=''btn btn-primary btn btnModificarMateria''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
										    '<button  type=''button'' class=''btn btn-primary btn btnEliminarMateria''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
										  from materias 
										where  
											 ( 
											 	materia like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
												 nombre_completo_materia like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
												 nombre_abreviado_materia like '%".$this->db->escape_str($requestData['search']['value'])."%'
										     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sqlMaterias);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sqlMaterias .= $limit;
		                
		            $query = $this->db->query($sqlMaterias);



		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

								$nestedData[] = $row["materia"];
							    $nestedData[] = $row["nombre_completo_materia"];
							    $nestedData[] = $row["nombre_abreviado_materia"];
								$nestedData[] = $row["Modificar"];
								$nestedData[] = $row["Eliminar"];

								$data[] = $nestedData;
							}


				$json_data = array(
					"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
					"recordsTotal"    => intval( $totalData ),  // total number of records
					"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
					"data"            => $data  // total data array
					);


				return $json_data;


			
		}


		public function getInfoMateria($materia)
		{

			$sql =	"		select 		materia,
										nombre_completo_materia,
										nombre_abreviado_materia
							 from materias 
							 		where materia = ? ";
			

			$query = $this->db->query($sql,array($materia));


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'materia'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['materia'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['materia'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			

			return $resultado_query;



		}


		public function checkModificarClaveMateria($claveMateria_new,$claveMateria_origen)
		{

			$sql1 =	"select materia from materias where materia = ?";
			
			$query1 = $this->db->query($sql1,array($claveMateria_origen));

			if($query1)
			{

				if($query1->num_rows()>0)
				{
					
					$claveMateria_origen = $query1->result()[0]->materia;

					if($claveMateria_origen == $claveMateria_new)
					{
						$resultado_query = array(
											'resultado'=>'OK',
											'mensaje'=>'se puede ocupar la clave'
										);
					}
					else
					{
						$sql2 =	"select materia from materias where materia = ?";
			
						$query2 = $this->db->query($sql2,array($claveMateria_new));

						if($query2)
						{

							if($query2->num_rows()>0)
							{
								$resultado_query = array(
											'resultado'=>'NO_DISPONIBLE',
											'mensaje'=>'La clave no esta disponible'
										);
							}
							else
							{
								$resultado_query = array(
											'resultado'=>'OK',
											'mensaje'=>'se puede ocupar la clave'
										);
							}
						}
						else
						{
							$resultado_query = array(
														'resultado'=>'ERROR',
														'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
													);
						}



					}
				}
			}
			else
			{
				$resultado_query = array(
											'resultado'=>'ERROR',
											'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
										);
			}


			

			return $resultado_query;



		}


		public function modificarMaterias($claveMateria,$claveMateriaOrigen,$nombre_completo_materia,$nombre_abreviado_materia)
		{

			
			
			$sql =	"update materias set 	materia = ?,
											nombre_completo_materia = ? ,
											nombre_abreviado_materia = ?
										where 
											materia = ? ";

			$query = $this->db->query($sql,array($claveMateria,$nombre_completo_materia,$nombre_abreviado_materia,$claveMateriaOrigen));


			if($query)
			{
				$resultado_query = array(
											'resultado'=>'OK',
											'mensaje'=>'La materia ha sido modificada correctamente'
										);
			}
			else{
				$resultado_query = array(
											'resultado'=>'ERROR',
											'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
										);
			}
			

			return $resultado_query;



		}



		public function deleteMaterias($materia)
		{

			$sql =	"select distinct materia 
					from rel_materias_carreras 
					where materia = ? ";
			

			$query = $this->db->query($sql,array($materia));


			$sql1 =	"select distinct materia 
					from rel_kardex_materias 
					where materia = ? ";
			

			$query1 = $this->db->query($sql1,array($materia));


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'materia'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0 || $query1->num_rows() > 0 )
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['materia'] = $query->result(); 
					$resultado_query['status'] = 'NO_DISPONIBLE'; 
					$resultado_query['mensaje'] = 'No se puede eliminar la materia debido a que tiene carrera(s) asociada(s)';

			
				}
				else
				{

					$sql2 =	"delete from materias where materia = ? ";
					

					$query2 = $this->db->query($sql2,array($materia));

					if($query2)
					{
						$resultado_query['status'] = 'OK'; 
						$resultado_query['mensaje'] = 'La materia ha sido eliminada correctamente';
					}
					else{
						$resultado_query['status'] = 'ERROR'; 
						$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
					}


				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la página e intente de nuevo'; 
			}
			

			return $resultado_query;



		}






}
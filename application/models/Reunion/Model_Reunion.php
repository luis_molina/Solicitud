<?php 
class Model_Reunion extends CI_Model
{
		

	public function __construct()
	{
		parent::__construct();
	}



	public function getInfoPersonalReunion()
	{

		$sql =	"select 
						profesion,
				        nombre,
				        apellidos
				from personal
				where estado = '1' 
				order by id_cargo_personal desc, id_comite asc ";
		

		$query = $this->db->query($sql);


		if($query)
		{

			if($query->num_rows()>0)
			{
				$resultado_query['personal'] = $query->result(); 
				$resultado_query['status'] = 'OK'; 
				$resultado_query['mensaje'] = 'información obtenida';

		
			}
			else
			{
				$resultado_query['personal'] = $query->result(); 
				$resultado_query['status'] = 'NO_DATA';
				$resultado_query['mensaje'] = 'No hay registros'; 
			}

		}
		else{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
		}
		

		return $resultado_query;



	}

	public function verifySolicitudesDisponibles($id_periodo_escolar)
	{

		$sql =	"select 
						num_solicitud
				from solicitudes soli
				join alumnos alu on (soli.id_alumno = alu.id_alumno)
				where alu.estado = '1' and soli.id_periodo_escolar = ? and soli.id_reunion is null ";
		

		$query = $this->db->query($sql,array($id_periodo_escolar));


		if($query)
		{

			if($query->num_rows()>0)
			{
				$resultado_query['rows'] = $query->num_rows();
				$resultado_query['status'] = 'OK'; 
				$resultado_query['mensaje'] = 'información obtenida';

		
			}
			else
			{
				$resultado_query['rows'] = $query->num_rows();
				$resultado_query['status'] = 'NO_DATA';
				$resultado_query['mensaje'] = 'No se encontraron solicitudes disponibles para el periodo escolar'; 
			}

		}
		else{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
		}
		

		return $resultado_query;



	}



	public function guardarReunion($nombre_reunion,$id_periodo_escolar,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2)
	{	

		$this->db->trans_begin();

		$sql =	"insert into reuniones (
											nombre_reunion,
											id_periodo_escolar,
											no_libro,
											no_acta,
											mensaje_1,
											personal,
											mensaje_2,
											fecha_registro
											) 
											values (
												?,
												?,
												?,
												?,
												?,
												?,
												?,
												now()
											)";

			$query = $this->db->query($sql,array($nombre_reunion,$id_periodo_escolar,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2));


			$sql =	"select max(id_reunion) as id_reunion from reuniones";

			$query = $this->db->query($sql);

			$id_reunion = $query->result()[0]->id_reunion;


			$sql =	"update solicitudes set id_reunion = ? 
							where id_periodo_escolar = ? and id_reunion is null;";

			$query = $this->db->query($sql,array($id_reunion,$id_periodo_escolar));


			if ($this->db->trans_status() === FALSE)
			{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la pagina e intente de nuevo'; 

			        $this->db->trans_rollback();
			}
			else
			{
				$resultado_query["status"] = "OK";
				$resultado_query["mensaje"] = "La reunion ha sido registrada correctamente";

			    $this->db->trans_commit();
			}
			

			return $resultado_query;




	}



	public function cargarTablaReunion($request)
	{

					$requestData= $request;

					$columna = $requestData['order'][0]["column"]+1;
					$ordenacion = $requestData['order'][0]["dir"];


					
					$sql =	"select 
									id_reunion,
							        nombre_reunion,
							        pe.identificacion_larga as periodo_escolar,
							        no_libro,
							        no_acta,
									'<button  type=''button'' class=''btn btn-primary btn btnModificarReunion''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
									'<button  type=''button'' class=''btn btn-primary btn btnEliminarReunion''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
							from reuniones re
							join periodos_escolares pe on(re.id_periodo_escolar = pe.id_periodo_escolar)
							order by ".$columna." ".$ordenacion." ";

					

					//$query1 = $this->db->query($sql1,array($id_usuario));
					$query = $this->db->query($sql);


					

					//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

					$totalData = $query->num_rows();
					$totalFiltered = $totalData;

					if( !empty($requestData['search']['value']) ) 
					{   

						

						$sql = "select 
										id_reunion,
								        nombre_reunion,
								        pe.identificacion_larga as periodo_escolar,
								        no_libro,
								        no_acta,
										'<button  type=''button'' class=''btn btn-primary btn btnModificarReunion''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
										'<button  type=''button'' class=''btn btn-primary btn btnEliminarReunion''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar
								from reuniones re
								join periodos_escolares pe on(re.id_periodo_escolar = pe.id_periodo_escolar)
								where  
									 ( 
										 nombre_reunion like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
										 identificacion_larga like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
										 no_libro like '%".$this->db->escape_str($requestData['search']['value'])."%' or
										 no_acta like '%".$this->db->escape_str($requestData['search']['value'])."%' 
								     ) order by ".$columna." ".$ordenacion." ";

					

						$query = $this->db->query($sql);


						$totalFiltered = $query->num_rows(); 
						

					}

					$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
		            $sql .= $limit;
		                
		            $query = $this->db->query($sql);


		            $data = array();

							foreach ($query->result_array() as $row)
							{ 
								$nestedData=array();

							    $nestedData[] = $row["id_reunion"];
							    $nestedData[] = $row["nombre_reunion"];
								$nestedData[] = $row["periodo_escolar"];
								$nestedData[] = $row["no_libro"];
								$nestedData[] = $row["no_acta"];
								$nestedData[] = $row["Modificar"];
								$nestedData[] = $row["Eliminar"];

								$data[] = $nestedData;
							}


					$json_data = array(
						"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
						"recordsTotal"    => intval( $totalData ),  // total number of records
						"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
						"data"            => $data  // total data array
						);


					return $json_data;


	}


	public function getInfoReunion($id_reunion)
	{

			$sql = "select 
							id_reunion,
					        nombre_reunion,
					        id_periodo_escolar,
					        no_libro,
					        no_acta,
					        mensaje_1,
					        personal,
					        mensaje_2
					from reuniones re
					where id_reunion = ?";

			$query = $this->db->query($sql,array($id_reunion));		


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['reunion'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['reunion'] = $query->result(); 
					$resultado_query['status'] = 'NO_DATA';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}



	public function modificarReunion($id_reunion,$nombre_reunion,$id_periodo_escolar,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2)
	{

			$sql = "update reuniones set
									 nombre_reunion = ?,
									 no_libro = ?,
									 no_acta = ?,
									 mensaje_1 = ?,
									 personal = ?,
									 mensaje_2 = ?
					where id_reunion = ? ";

			$query = $this->db->query($sql,array($nombre_reunion,$no_libro,$no_acta,$mensaje_1,$personal,$mensaje_2,$id_reunion));		


			if($query)
			{

					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'La reunión ha sido modificada correctamente';

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}


	public function eliminarReunion($id_reunion)
	{

			$this->db->trans_begin();



			$sql = " update solicitudes set id_reunion = null where id_reunion = ?";

			$query = $this->db->query($sql,array($id_reunion));


			$sql = " delete from reuniones where id_reunion = ? ";

			$query = $this->db->query($sql,array($id_reunion));


			if ($this->db->trans_status() === FALSE)
			{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la pagina e intente de nuevo'; 

			        $this->db->trans_rollback();
			}
			else
			{
				$resultado_query["status"] = "OK";
				$resultado_query["mensaje"] = "La reunion ha sido eliminada correctamente";

			    $this->db->trans_commit();
			}
			
			
			return $resultado_query;

	}





}
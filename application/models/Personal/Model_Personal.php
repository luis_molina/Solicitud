<?php 
class Model_Personal extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}


	public function cargarSelectCargoPersonal()
	{

		$sql = "select id_cargo_personal,cargo from cat_cargo_personal order by id_cargo_personal asc ";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}

	public function cargarSelectDepartamentos()
	{

		$sql = "select id_departamento,departamento from cat_departamentos order by id_departamento asc  ";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}


	public function cargarSelectComite()
	{

		$sql = "select id_comite,comite from cat_comite order by id_comite asc";

		$query = $this->db->query($sql);		
		
		return $query->result();

	}


	public function guardarPersonal($nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,$correo,$password)
	{


		$this->db->trans_begin();
		
		
		$sql =	"insert into personal (
										id_personal,
										nombre,
										apellidos,
										profesion,
										id_cargo_personal,
										id_departamento,
										id_comite,
										fecha_inicio,
										fecha_fin,
										estado
										) 
										values (
											null,
											?,
											?,
											?,
											?,
											?,
											?,
											?,
											?,
											?

										)";

		$query = $this->db->query($sql,array($nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,'1'));

		if($id_comite == '2')
		{
			$sql =	"select max(id_personal) as id_personal from personal ";

			$query = $this->db->query($sql);

			$id_personal = $query->result()[0]->id_personal;


			$sql =	"insert into usuarios (
											id_usuario,
											id_personal,
											nombre,
											apellidos,
											correo,
											password,
											estado
										) 
										values 
										(
											null,
											?,
											?,
											?,
											?,
											?,
											?
										)";

			$query = $this->db->query($sql,array($id_personal,$nombre,$apellidos,$correo,$password,'1'));



		}

		





		if ($this->db->trans_status() === FALSE)
		{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la página e intente de nuevo'; 

		        $this->db->trans_rollback();
		}
		else
		{
			$resultado_query["status"] = "OK";
			$resultado_query["mensaje"] = "El personal ha sido registrado correctamente";

		    $this->db->trans_commit();
		}
		

		return $resultado_query;



	}


	public function cargarTablaPersonal($request)
	{

				$requestData= $request;

				$columna = $requestData['order'][0]["column"]+1;
				$ordenacion = $requestData['order'][0]["dir"];

				
				$sqlPeriodosEscolares =	"select 
												per.id_personal,
										        CONCAT(per.nombre , ' ', per.apellidos) as nombreCompleto,
										        per.profesion,
										        ccp.cargo,
										        cd.departamento,
										        cc.comite,
										       DATE_FORMAT(per.fecha_inicio,'%d/%m/%Y') as fecha_inicio,
										       DATE_FORMAT(per.fecha_fin,'%d/%m/%Y') as fecha_fin,
										       '<button  type=''button'' class=''btn btn-primary btn btnModificarPersonal''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
											   '<button  type=''button'' class=''btn btn-primary btn btnEliminarPersonal''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar                                            
										from personal per
										left join cat_cargo_personal ccp on (per.id_cargo_personal = ccp.id_cargo_personal)
										left join cat_departamentos cd on (per.id_departamento = cd.id_departamento)
										left join cat_comite cc on (per.id_comite = cc.id_comite)
										where per.estado = '1'
										order by ".$columna." ".$ordenacion." ";

				

				//$query1 = $this->db->query($sql1,array($id_usuario));
				$query = $this->db->query($sqlPeriodosEscolares);

				//$this->db->query($sql, array(array(3, 6), 'live', 'Rick') );

				$totalData = $query->num_rows();
				$totalFiltered = $totalData;

				if( !empty($requestData['search']['value']) ) 
				{   

					

					$sqlPeriodosEscolares = "select
													id_personal,
													nombreCompleto,
													profesion,
													cargo,
													departamento,
													comite,
													fecha_inicio,
													fecha_fin,
													Modificar,
													Eliminar
										     From(  select 
														per.id_personal,
												        CONCAT(per.nombre , ' ', per.apellidos) as nombreCompleto,
												        per.profesion,
												        ccp.cargo,
												        cd.departamento,
												        cc.comite,
												       DATE_FORMAT(per.fecha_inicio,'%d/%m/%Y') as fecha_inicio,
												       DATE_FORMAT(per.fecha_fin,'%d/%m/%Y') as fecha_fin,
												       '<button  type=''button'' class=''btn btn-primary btn btnModificarPersonal''> <span class=''glyphicon glyphicon-pencil''></span> </button>' as Modificar,
													   '<button  type=''button'' class=''btn btn-primary btn btnEliminarPersonal''> <span class=''glyphicon glyphicon-trash''></span> </button>' as Eliminar                                            
												from personal per
												left join cat_cargo_personal ccp on (per.id_cargo_personal = ccp.id_cargo_personal)
												left join cat_departamentos cd on (per.id_departamento = cd.id_departamento)
												left join cat_comite cc on (per.id_comite = cc.id_comite)
												where per.estado = '1'
												) as mytable
									where  
										 ( 
											 nombreCompleto like '%".$this->db->escape_str($requestData['search']['value'])."%' or  
											 profesion like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
											 cargo like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
											 departamento like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
											 comite like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
											 fecha_inicio like '%".$this->db->escape_str($requestData['search']['value'])."%' or 
											 fecha_fin like '%".$this->db->escape_str($requestData['search']['value'])."%' 
									     ) order by ".$columna." ".$ordenacion." ";

				

					$query = $this->db->query($sqlPeriodosEscolares);


					$totalFiltered = $query->num_rows(); 
					

				}

				$limit = " LIMIT ".$this->db->escape_str($requestData['start'])." ,".$this->db->escape_str($requestData['length'])." ";
	            $sqlPeriodosEscolares .= $limit;
	                
	            $query = $this->db->query($sqlPeriodosEscolares);



	            $data = array();

						foreach ($query->result_array() as $row)
						{ 
							$nestedData=array();

							$nestedData[] = $row["id_personal"];
						    $nestedData[] = $row["nombreCompleto"];
						    $nestedData[] = $row["profesion"];
						    $nestedData[] = $row["cargo"];
						    $nestedData[] = $row["departamento"];
						    $nestedData[] = $row["comite"];
						    $nestedData[] = $row["fecha_inicio"];
						    $nestedData[] = $row["fecha_fin"];
							$nestedData[] = $row["Modificar"];
							$nestedData[] = $row["Eliminar"];

							$data[] = $nestedData;
						}


			   $json_data = array(
				"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
				"recordsTotal"    => intval( $totalData ),  // total number of records
				"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
				"data"            => $data  // total data array
				);


			  return $json_data;


		
	}


	public function getInfoPersonal($id_personal)
	{

			$sql = "select 
							per.id_personal,
							per.nombre,
							per.apellidos,
							per.profesion,
							per.id_cargo_personal,
							per.id_departamento,
							per.id_comite,
					        usu.correo,
					        usu.password,
							per.fecha_inicio,
							per.fecha_fin
					from personal per
					left join usuarios usu on (per.id_personal = usu.id_personal)
					where per.id_personal = ?";

			$query = $this->db->query($sql,array($id_personal));		


			$resultado_query = array(
											'msjCantidadRegistros'=> 0,
											'personal'=> array(),
											 'status' => '',
											 'mensaje' => ''
										);


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['personal'] = $query->result(); 
					$resultado_query['status'] = 'OK'; 
					$resultado_query['mensaje'] = 'información obtenida';

			
				}
				else
				{
					$resultado_query['msjCantidadRegistros'] = $query->num_rows();
					$resultado_query['personal'] = $query->result(); 
					$resultado_query['status'] = 'Sin datos';
					$resultado_query['mensaje'] = 'No hay registros'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}
			
			
			return $resultado_query;

	}


	public function checkCorreoPersonal($correo)
	{

		$sql = "select correo from usuarios where correo = ? ";

		$query = $this->db->query($sql,array($correo));	


			if($query)
			{

				if($query->num_rows()>0)
				{
					
					$resultado_query['status'] = 'NO_DISPONIBLE'; 
					$resultado_query['mensaje'] = 'Correo no disponible';

			
				}
				else
				{
					
					$resultado_query['status'] = 'OK';
					$resultado_query['mensaje'] = 'Correo correcto'; 
				}

			}
			else{
					$resultado_query['status'] = 'ERROR'; 
					$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos porfavor recargue la pagina e intente de nuevo'; 
			}	
		
		return $resultado_query;

	}


	public function checkModificarCorreo($correo_new,$correo_origen)
	{

			if($correo_origen != "")
			{ 

				$sql1 =	"select correo from usuarios where correo = ?";
				
				$query1 = $this->db->query($sql1,array($correo_origen));

				if($query1)
				{

					if($query1->num_rows()>0)
					{
						
						$correo_origen = $query1->result()[0]->correo;

						if($correo_origen == $correo_new)
						{
							$resultado_query = array(
												'resultado'=>'OK',
												'mensaje'=>'se puede ocupar el correo'
											);
						}
						else
						{
							$sql2 =	"select correo from usuarios where correo = ?";
				
							$query2 = $this->db->query($sql2,array($correo_new));

							if($query2)
							{

								if($query2->num_rows()>0)
								{
									$resultado_query = array(
												'resultado'=>'NO_DISPONIBLE',
												'mensaje'=>'El Correo no esta disponible'
											);
								}
								else
								{
									$resultado_query = array(
												'resultado'=>'OK',
												'mensaje'=>'se puede ocupar el correo'
											);
								}
							}
							else
							{
								$resultado_query = array(
															'resultado'=>'ERROR',
															'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
														);
							}



						}
					}
				}
				else
				{
					$resultado_query = array(
												'resultado'=>'ERROR',
												'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
											);
				}
			}
			else{

					$sql =	"select correo from usuarios where correo = ?";
				
				   $query = $this->db->query($sql,array($correo_new));

				   if($query->num_rows()>0)
					{
						$resultado_query = array(
									'resultado'=>'NO_DISPONIBLE',
									'mensaje'=>'El Correo no esta disponible'
								);
					}
					else
					{
						$resultado_query = array(
									'resultado'=>'OK',
									'mensaje'=>'se puede ocupar el correo'
								);
					}

			}


			


			

			return $resultado_query;



	}



	public function modificarPersonal($id_personal,$nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,$correo,$password)
	{


		$this->db->trans_begin();

		$pass_cifrado = password_hash($password,PASSWORD_DEFAULT);
		
		
		$sql =	"update personal set    
										nombre = ?,
										apellidos = ?,
										profesion = ?,
										id_cargo_personal = ?,
										id_departamento = ?,
										id_comite = ?,
										fecha_inicio = ?,
										fecha_fin = ?
						where id_personal = ? ";

		$query = $this->db->query($sql,array($nombre,$apellidos,$profesion,$id_cargo_personal,$id_departamento,$id_comite,$fecha_inicio,$fecha_fin,$id_personal));

		if($id_comite == '2')
		{

			$sql =	"select * from usuarios where id_personal = ? ";

			$query = $this->db->query($sql,array($id_personal));


			if($query->num_rows() > 0)
			{

				$sql =	"update usuarios set 
										nombre = ?,
										apellidos = ?,
										correo = ?,
										password = ?
						where id_personal = ? ";

				$query = $this->db->query($sql,array($nombre,$apellidos,$correo,$pass_cifrado,$id_personal));


				
			}
			else{

					$sql =	"insert into usuarios (
											id_usuario,
											id_personal,
											nombre,
											apellidos,
											correo,
											password,
											estado
										) 
										values 
										(
											null,
											?,
											?,
											?,
											?,
											?,
											?
										)";

					$query = $this->db->query($sql,array($id_personal,$nombre,$apellidos,$correo,$pass_cifrado,'1'));

			}



			




		}
		else
		{
			$sql =	"select * from usuarios where id_personal = ? ";

			$query = $this->db->query($sql,array($id_personal));


			if($query->num_rows() > 0)
			{
				$sql =	"delete from usuarios where id_personal = ? ";

				$query = $this->db->query($sql,array($id_personal));
			}


		}


		if ($this->db->trans_status() === FALSE)
		{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la página e intente de nuevo'; 

		        $this->db->trans_rollback();
		}
		else
		{
			$resultado_query["status"] = "OK";
			$resultado_query["mensaje"] = "El personal ha sido modificado correctamente";

		    $this->db->trans_commit();
		}
		

		return $resultado_query;



	}



	public function eliminarPersonal($id_personal)
	{

		$sql = "select * from usuarios where id_personal = ? ";

		$query = $this->db->query($sql,array($id_personal));	

		$this->db->trans_begin();

		if($query->num_rows()>0)
		{
			
			// $sql = "delete from usuarios where id_personal = ? ";

			$sql = "update usuarios set estado = '0' where id_personal = ? ";

			$query = $this->db->query($sql,array($id_personal));	


			$sql = "update personal set estado = '0' where id_personal = ? ";

			$query = $this->db->query($sql,array($id_personal));

	
		}
		else
		{

			$sql = "update personal set estado = '0' where id_personal = ? ";

			$query = $this->db->query($sql,array($id_personal));
			
		}


		if ($this->db->trans_status() === FALSE)
		{
				$resultado_query['status'] = 'ERROR'; 
				$resultado_query['mensaje'] = 'Ocurrió un error en la base de datos por favor recargue la página e intente de nuevo'; 

		        $this->db->trans_rollback();
		}
		else
		{
			$resultado_query["status"] = "OK";
			$resultado_query["mensaje"] = "El personal ha sido dado de baja correctamente";

		    $this->db->trans_commit();
		}

			
		
		return $resultado_query;

	}






}
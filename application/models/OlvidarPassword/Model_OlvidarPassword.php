<?php 
class Model_OlvidarPassword extends CI_Model
{
		public function __construct()
		{
			parent::__construct();
		}




		public function verifyEmail($correo)
		{

			$sql =	"select correo from usuarios where correo = ? and estado = '1' ";
			

			$query = $this->db->query($sql,array($correo));


			if($query)
			{

				if($query->num_rows()>0)
				{
					$resultado_query = array(
											'msjConsulta'=>'OK',
											'mensaje'=>'se puede ocupar ese correo'
										);
			
				}
				else
				{
					$resultado_query = array(
											'msjConsulta'=>'NO_DISPONIBLE',
											'mensaje'=>'No existe una cuenta con ese correo electrónico'
											
										);
				}

			}
			else{
				$resultado_query = array(
											'msjConsulta'=>'ERROR',
											'mensaje'=>'Ocurrio un error a la hora de guardar los datos'
										);
			}
			

			return $resultado_query;



		}


		public function enviarEmailCambioPassword($correo)
		{

			$this->db->trans_begin();

			$token = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',80)),0,80);

			$sql =	"update usuarios set token = ? where correo = ? and estado = '1' ";

			$query = $this->db->query($sql,array($token,$correo));

				if ($this->db->trans_status() === FALSE)
				{	
					$datos = array( 
									"msjConsulta" => 'Error',
									"correo" => $correo, 
									"token" => $token);

				        $this->db->trans_rollback();
				}
				else
				{
					
					$datos = array( 
									"msjConsulta" => 'OK',
									"correo" => $correo,  
									"token" => $token);

				    $this->db->trans_commit();
				}

			return $datos;


		}


		

		public function updatePasswordUsuario($password,$token)
		{

			$this->db->trans_begin();

			$sql1 =	"select 
							id_usuario,
							token 
					from usuarios where token = ? and estado = '1' ";

				$query = $this->db->query($sql1,array($token));

				$CantRows = $query->num_rows(); 

				$id_usuario = "";

				if($CantRows > 0)
				{
					$pass_cifrado = password_hash($password,PASSWORD_DEFAULT);
					
					$id_usuario = $query->result()[0]->id_usuario;

					$sql2 =	"update usuarios set password = ? where id_usuario = ? ";

					$query = $this->db->query($sql2,array($pass_cifrado,$id_usuario));

					$sql3 =	"update usuarios set token = null where id_usuario = ?";

					$query = $this->db->query($sql3,array($id_usuario));

					
				}
				


				if ($this->db->trans_status() === FALSE)
				{	
					$datos = array( "msjConsulta" => 'Error',
									"token" => 'Error',
									"msjUsuario" => 'Ocurrio un error al cambiar tu contraseña, por favor intenta de nuevo',
									// "id_usuario" => $id_usuario, 
									// "token" => $token
								);

				        $this->db->trans_rollback();
				}
				else
				{
					
					if($CantRows > 0)
					{
						$datos = array( "msjConsulta" => 'OK',
									"resultado" => 'OK',
									"msjUsuario" => 'Tu contraseña ha sido cambiada correctamente',
									 "cantRows" => $CantRows, 
									"base_url" => base_url()."Login",
									 "token" => $token
								    );
					}
					else
					{
						$datos = array( "msjConsulta" => 'OK',
									"resultado" => 'CADUCADO',
									"msjUsuario" => 'Lo sentimos el formulario ha caducado',
									 "cantRows" => $CantRows, 
									"base_url" => base_url()."OlvidarPassword/cambiarContrasena?token='_'",
									 "token" => $token
								    );
					}
					

				    $this->db->trans_commit();
				}
				
					
				return $datos;


		}



		public function verifyTokenChangePassword($token)
		{


			$sql1 =	"select token from usuarios
					 where 	token = ? and estado = '1' ";

					$query = $this->db->query($sql1,array($token));


			   $datos = array("msjCantidadRegistros" => 0, "msjNoHayRegistros" => '' ,"usuarioTokens" => array());


				$datos["msjCantidadRegistros"] = $query->num_rows(); 

				if($datos["msjCantidadRegistros"] > 0)
				{
					$datos["usuarioTokens"] = $query->result();
					$datos["msjNoHayRegistros"] = "El token existe.";

				}
				else
				{
					$datos["msjNoHayRegistros"] = "No existe un token de ese usuario.";
				}


				return $datos;


		}



}
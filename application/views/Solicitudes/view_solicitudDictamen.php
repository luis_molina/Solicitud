<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dictamen</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/dataTables.bootstrap.min.css"> 

<style>
	
.search-table{table-layout: fixed; margin:40px auto 0px auto; }
.search-table, .search-table td, .search-table th{border-collapse:collapse; border:1px solid #777;}
/*th{padding:20px 7px;}*/
.search-table td{padding:5px 10px; height:35px;text-align: center;}

.search-table-outter { overflow-x: scroll; }
/*.search-table th, .search-table .grand { min-width: 150px; }*/
.search-table th { min-width: 150px; text-align:center;}

</style>
	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >

	

   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
			<div class="row">
				<div class="col-xs-12">
					<h2 style="text-align: center;">Dictamen Solicitudes</h2>
				</div>
			</div>
			<br><br><br>
			<div class="row">
				<div class="col-xs-12">
						
						<div class="table-responsive">
							
								<table class="table table-bordered table-hover" id="tblSolicitudes" cellspacing="0"  width="100%" style="text-align: center;">
										<caption style="text-align: center"><h4><strong>Tabla de estudiantes con solicitudes</strong></h4></caption>
										<thead>
							                    <tr>
							                    	 <th>id_alumno</th>
							                    	 <th>Número de control</th>
								                      <th>Nombre</th>
								                      <th>Apellido paterno</th>
								                      <th>Apellido materno</th>
								                      <th>Carrera</th>
								                      <th>Periodo escolar</th>
								                      <th>Semestre</th>
								                      <th>Solicitud</th>
							                    </tr>
							            </thead>
					                    <tbody>
					                    </tbody>
						        </table>
						</div>
				</div>
			</div>

	</div>

<br>

       <!-- Modal -->

<div id="modalSolicitudesEstudiante" class="modal fade" role="dialog" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width:1080px;margin-left: -133px;'>
	      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Alerta</h4>
	      </div>
		      <div class="modal-body">
			    
				        		<h4 style="text-align: center"><h4>Solicitudes del estudiante <strong id='nameEstudiante'></strong></h4></label>
								
	
					<div class="search-table-outter wrapper">
									<table class="search-table inner" id="tblSolicitudesEstudiante"  style="text-align: center;">
										<thead>
							                    <tr>
							                    	 <th style="min-width: 50px;">No</th>
								                      <th  style="min-width: 300px;">Asunto</th>
								                      <th>Lugar</th>
								                      <th style="min-width:100px;" >Fecha</th>
								                      <th style="min-width: 100px;" >Semestre</th>
								                      <th style="min-width:200px;">Periodo</th>
								                      <th>Solicitud</th>
								                      <th>Dictamen</th>
								                      <th style="min-width:110px;">Respuesta dictamen</th>
								                      <th>Dictamen director</th>
								                      <th style="min-width:110px;">Respuesta dictamen director</th>
							                    </tr>
							            </thead>
					                    <tbody>
					                    </tbody>
						        </table>
								
		      		</div>
		      <div class="modal-footer">
				      <button type="submit" class="btn btn-primary"  id="btnSolicitudesEstudiante" data-dismiss="modal" >Aceptar</button>
				      
		      </div>
	    
    </div>

  </div>
</div>

   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" id='btnModalAlerta'>Aceptar</button>
      </div>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalDatosDictamenSolicitud" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width: 775px;'>
      <div class="modal-header">
       		<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        	<h4 class="modal-title">Alerta</h4>
      </div>
      <form id='formDatosDictamenSolicitud' >
      		<div class="modal-body" style="height: 460px;overflow: auto;">
        	
		        	<h3>Datos Acta Solicitud</h3>

						<br>

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Petición del estudiante:</label> 
						     </div>
						</div>  

						<div style='width: 728px;'>
						      <div class="form-group" >
						          <textarea style='width:728px;height:80px' id="txtObservacionesSolicitud"  class="form-control"  disabled readonly ></textarea>
						     </div>
						</div>  

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Respuesta al dictamen de la solicitud:</label> 
						     </div>
						</div>  

						<div style='width: 728px;'>
						      <div class="form-group" >
						           <textarea style='width:728px;height:120px' placeholder="Respuesta al dictamen de la solicitud" id="txtRespuestaSolicitudDictamen" name="txtRespuestaSolicitudDictamen" class="form-control" minlength="1" ></textarea>
						     </div>
						</div>  

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Número de dictamen:</label> 
						     </div>
						</div>  

						<div style='width: 728px;'>
						      <div class="form-group" >
						           <input type='text' placeholder="ITSAL.DIR.2017/0065" id="txtSolicitudNoDictamen" name="txtSolicitudNoDictamen" class="form-control" minlength="1" maxlength="90" />
						     </div>
						</div>  

		  </div>
	      <div class="modal-footer">
		      <button type="submit" class="btn btn-primary"  id="btnGuardarDatosDictamenSolicitud">Guardar</button>
		      <button type="button" class="btn btn-default"  id="btnCancelarDatosDictamenSolicitud">Cancelar</button>
	      </div>
	  </form>
  </div>

  </div>
</div>


   <!-- Modal -->
<div id="modalDatosDictamenDirectorSolicitud" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width: 775px;'>
      <div class="modal-header">
       		<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        	<h4 class="modal-title">Alerta</h4>
      </div>
      <form id='formDatosDictamenDirectorSolicitud' >

      		<div class="modal-body" style="height: 460px;overflow: auto;">
        	
		        	<h3>Datos Dictamen Director</h3>

						<br>

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Petición del estudiante:</label> 
						     </div>
						</div>  

						<div style='width: 728px;'>
						      <div class="form-group" >
						          <textarea style='width:728px;height:80px' id="txtObservacionesSolicitudDictamen"  class="form-control"  disabled readonly ></textarea>
						     </div>
						</div>  

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Respuesta al dictamen del director:</label> 
						     </div>
						 </div>  
						  <div>
						      <div class="form-group" >
						          <label> (SI) <input type="radio" name="rdSolicitud" id='rdSiRecomienda'> <label style='margin-left:10px'> (NO) <input type="radio" id='rdNoRecomienda' name="rdSolicitud"> </label> </label> <strong style='margin-left:20px'>SE RECOMIENDA:</strong>
						     </div>
						 </div> 

						<div style='width: 728px;'>
						      <div class="form-group" >
						           <textarea style='width:728px;height:120px' placeholder="Respuesta al dictamen del director" id="txtRespuestaSolicitudDictamenDirector" name="txtRespuestaSolicitudDictamenDirector" class="form-control" minlength="1" ></textarea>
						     </div>
						</div>  

						<div>
						      <div class="form-group" >
						          <label>Motivos dictamen director:</label> 
						     </div>
						 </div> 

						<div style='width: 728px;'>
						      <div class="form-group" >
						           <textarea style='width:728px;height:120px' placeholder="Motivos dictamen director" id="txtMotivosDictamenDirector" name="txtMotivosDictamenDirector" class="form-control" minlength="1" ></textarea>
						     </div>
						</div> 

						 <div style='width: 728px;'>
						      <div class="form-group" >
						          <label>Número de recomendación:</label> 
						     </div>
						</div>  

						<div style='width: 728px;'>
						      <div class="form-group" >
						           <input type='text' placeholder="ITSAL.DIR.SAC.CACAD.2016/004" id="txtSolicitudNoRecomendacion" name="txtSolicitudNoRecomendacion" class="form-control" minlength="1" maxlength="90" />
						     </div>
						</div>  

						

		  </div>
	      <div class="modal-footer">
		      <button type="submit" class="btn btn-primary"  id="btnGuardarDatosDictamenDirectorSolicitud">Guardar</button>
		      <button type="button" class="btn btn-default"  id="btnCancelarDatosDictamenDirectorSolicitud">Cancelar</button>
	      </div>
	  </form>
  </div>

  </div>
</div>






	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script>
	
     <script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/solicitudesDictamen.js"></script> 


</body>
</html>
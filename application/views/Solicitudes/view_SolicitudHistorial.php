<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Historial de Solicitudes</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/dataTables.bootstrap.min.css"> 

	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >


   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
			<div class="row">
				<div class="col-xs-12">
					<h2 style="text-align: center;">Historial de Solicitudes</h2>
				</div>
			</div>
			<br><br>
			<div class="row">
				<div class="col-xs-12">
						
						<div class="table-responsive">
							
								<table class="table table-bordered table-hover" id="tblSolicitudesHistorial" cellspacing="0"   style="text-align: center;width:2500px">
										<caption style="text-align: center"><h4><strong>Tabla de historial de solicitudes</strong></h4></caption>
										<thead>
							                    <tr>
							                    	 <th style='width:200px'>Alumno</th>
								                     <th style='width:200px'>Carrera</th>
							                    	 <th style='width:60px'>Periodo escolar</th>
							                    	 <th style='width:60px'>Semestre</th>
							                    	 <th style='width:300px'>Asunto</th>
							                    	 <th style='width:150px'>Lugar</th>
							                    	 <th style='width:50px'>Fecha</th>
								                      <th style='width:350px'>Observaciones</th>
								                      <th style='width:350px'>Motivos academicos</th>
								                      <th style='width:350px'>Motivos personales</th>
								                      <th style='width:350px'>Otros</th>
								                      <th style='width:500px'>Respuesta acta</th>
								                      <th style='width:500px'>Respuesta dictamen</th>
								                      <th style='width:500px'>Respuesta motivos dictamen</th>
							                    </tr>
							            </thead>
					                    <tbody>
					                    </tbody>
						        </table>
						</div>
				</div>
			</div>

	</div>




   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>
      </div>
    </div>

  </div>
</div>


 



	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<!-- <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script> -->
	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script>
	
     <script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/solicitudesHistorial.js"></script> 


</body>
</html>
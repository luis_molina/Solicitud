<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Solicitudes</title>
</head>

<body >
	

    <div style='font-family:Arial;font-size: 12px'> 

        <h3 style='text-align: center;font-size: 16px'>SOLICTUD DEL ESTUDIANTE PARA EL ANÁLISIS DEL COMITÉ ACADÉMICO</h3>
        <br> 
        <h3 style='text-align: center;font-size: 14px'>Instituto tecnológico de Salina Cruz </h3>
		<br> <br>
		<div style='text-align: right'>Lugar y Fecha: <strong><ins> <?php echo $lugarYfecha; ?> </ins></strong> </div> 
		<div style='text-align: right'>Asunto: <strong><ins> <?php echo $asunto; ?> </ins> </strong></div> 
		<br><br>
		<strong>
			C. <?php echo $secretario; ?><br>
	    	<!-- Jefe de la Division de Estudios Profesionales<br> -->
            <?php echo $departamento; ?> <br>
	         PRESENTE
         </strong>
        <br><br><br>


       

          <div>
        	
        	<div style="text-align: justify">
        		El (la) que suscribe C. 
        		<strong><ins><?php echo $nombreCompleto; ?></ins> </strong>
        		estudiante del 
        		<strong><ins><?php echo $nombre_semestre; ?></ins></strong>
        		de la carrera de <strong><ins><?php echo $nombre_carrera; ?></ins></strong>
        	    con número de control <strong><ins><?php echo $no_de_control; ?></ins></strong>.
        	    <br><br><br>
        	    Solicito de la manera mas atenta:
        	   <strong> <ins><?php echo $observacion; ?></ins></strong>
        	    <br><br><br>
				Por los siguientes motivos: <br><br><br>

				Motivos académicos: <br><br>
				<strong><ins><?php echo $motivos_academicos; ?></ins></strong>

				<br><br><br><br>

				Motivos personales: <br><br>
				<strong><ins><?php echo $motivos_personales; ?></ins></strong>

				<br><br><br><br>

				Otros: <br><br>
				<strong><ins><?php echo $otros; ?></ins></strong>
				
				<br><br><br><br>
				<div style='width:300px;border:0px solid red;margin:0px auto;text-align: center'>
					<strong>ATENTAMENTE </strong>
				</div>
				<br><br><br><br>
                    
                    

					<div style="border-bottom: 1px solid black;width:300px;margin:0px auto;text-align: center">
                        <strong><?php echo $nombreCompleto; ?> </strong>
                    </div>
				<br><br>
				<div>
					c.c.p Interesado.
				</div>
        	</div>
        	
        	
        	
        </div>



    </div>

</body>
</html>
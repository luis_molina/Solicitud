<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Dictamen</title>
    <style>@page {
                margin-top: 4.5cm;
                margin-bottom: 3cm;
                margin-left: 2.8cm;
                margin-right: 2.2cm;
                }
    </style>
</head>

<body >
	   
       <?php 

           $arrayFecha = explode("/",$fecha);

           $diaNum = $arrayFecha[0];
           $mesNum = $arrayFecha[1];
           $anioNum = $arrayFecha[2];

           $mes = '';

           switch ($mesNum) {
               case '01':
                   $mes = 'Enero';
                   break;

               case '02':
                   $mes = 'Febrero';
                   break;

               case '03':
                   $mes = 'Marzo';
                   break;

               case '04':
                   $mes = 'Abril';
                   break;

               case '05':
                   $mes = 'Mayo';
                   break;

               case '06':
                   $mes = 'Junio';
                   break;

               case '07':
                   $mes = 'Julio';
                   break;

               case '08':
                   $mes = 'Agosto';
                   break;

               case '09':
                   $mes = 'Septiembre';
                   break;

               case '10':
                   $mes = 'Octubre';
                   break;

               case '11':
                   $mes = 'Noviembre';
                   break;

               case '12':
                   $mes = 'Diciembre';
                   break;
       
           }


       ?>

       <?php 

            $diaNumR = "S/N";
           $mesNumR = "S/N";
           $anioNumR = "S/N";
           $mesR = "S/N";




            if(isset($fecha_reunion))
            {
                   $arrayFechaR = explode("/",$fecha_reunion);

                   $diaNumR = $arrayFechaR[0];
                   $mesNumR = $arrayFechaR[1];
                   $anioNumR = $arrayFechaR[2];

                   $mesR = '';

                   switch ($mesNumR) {
                       case '01':
                           $mesR = 'Enero';
                           break;

                       case '02':
                           $mesR = 'Febrero';
                           break;

                       case '03':
                           $mesR = 'Marzo';
                           break;

                       case '04':
                           $mesR = 'Abril';
                           break;

                       case '05':
                           $mesR = 'Mayo';
                           break;

                       case '06':
                           $mesR = 'Junio';
                           break;

                       case '07':
                           $mesR = 'Julio';
                           break;

                       case '08':
                           $mesR = 'Agosto';
                           break;

                       case '09':
                           $mesR = 'Septiembre';
                           break;

                       case '10':
                           $mesR = 'Octubre';
                           break;

                       case '11':
                           $mesR = 'Noviembre';
                           break;

                       case '12':
                           $mesR = 'Diciembre';
                           break;
               
                   }
            }

           


       ?>


    <div style="font-family:'frutiger';font-weight:normal;font-size:9px;border:0px solid red"> 

       <!--  <h3 style='width:410px;margin:0px 332px;' >TECNOLÓGICO NACIONAL DE MÉXICO</h3>
        <div style='width:250px;margin:0px 432px;'>Instituto Tecnológico de Salina Cruz </div>
        <br> <br>
        <div style='width:700px;margin: 0px auto;'>Año del Centenario de la Promulgación de la Constitución Política de los Estados Unidos Mexicanos</div>
		<br> -->
		<div style='text-align: right'><?php echo $lugar; ?> , <?php echo $diaNum."/".$mes."/".$anioNum; ?> </div> 
		<div style='text-align: right'>No. de recomendación: <?php echo $no_recomendacion_dictamen; ?></div> 
		<br>
		<strong style="font-size:10px">
			<?php echo $director; ?> <br>
            <!-- LAEM. MACARIO QUIZO CORTEZ -->
	    	DITECTOR DEL INSTITUTO TECNOLÓGICO DE SALINA CRUZ<br>
	         PRESENTE
         </strong>
        <br><br><br>
       

          <div>
        	
        	<div style="text-align: justify" >
        		Por este conducto, le informo que en reunión del Comité Académico, celebrada el <ins><?php echo $diaNumR; ?> </ins>  de <ins> <?php echo $mesR; ?> </ins>   
                del año en curso, asentada en el Libro de Actas No. <ins><?php echo ($no_libro == "") ? "S/N" : $no_libro ?></ins> en la hoja con folio 
                <ins><?php echo ($no_acta == "") ? "S/N" : $no_acta ; ?></ins> y en virtud de haber sido analizada la situación del estudiante 
                <ins><?php echo $nombreCompleto; ?></ins> de la carrera <ins><?php echo $nombre_carrera; ?></ins>
                con número de control <ins><?php echo $no_de_control; ?></ins> y quien solicita <ins><?php echo $peticion_solicitud; ?></ins> 
                <?php if($recomienda == '1'){
                      ?>
                        (<ins>SI</ins>) (NO) 
                      <?php
                      }
                      else if($recomienda == '0') 
                      {
                          ?> 
                            (SI) (<ins>NO</ins>)
                        <?php
                      }
                      else{
                        ?> 
                            (SI) (NO)
                        <?php
                      }
                      ?> 
                                                
                SE RECOMIENDA: 
                <br>
                <ins><?php echo $respuesta_solicitud_acta; ?></ins>
        		<br>
                Por los siguientes motivos:
        		<ins><?php echo $motivos_solicitud_dictamen; ?></ins>
				
				<br><br><br><br><br><br><br>
                <div style='width:120px;border:0px solid red;margin:0px;font-size:10px'>
                    <strong>ATENTAMENTE </strong>
                </div>
                <br><br><br><br><br>

                <div style='width:400px;border:0px solid red;margin:0px;font-style: italic;'>
                    "La misión va más allá de nuestras costas"
                </div>
                <br><br><br><br><br><br><br>


                <div style='width:400px;border:0px solid red;margin:0px;font-size:10px'>
                    <strong><?php echo $director; ?></strong><br>
                    <strong>DIRECTOR</strong>
                </div>
                <br><br><br>
                    
                    
                <br><br><br><br><br><br>
                <div style='font-size:8px'>
                    C.p Interesado(a).<br>
                    C.p Archivo.
                </div>

        	</div>
        	
        	
        	
        </div>



    </div>

</body>
</html>
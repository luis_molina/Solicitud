<html>
<head>
	<meta charset="UTF-8">
	<title>Dictamen</title>
    <style>@page {
                margin-top: 4.5cm;
                margin-bottom: 3cm;
                margin-left: 2.8cm;
                margin-right: 2.2cm;
                }
    </style>
</head>

<body>

    <?php 

        $arrayFecha = explode("/",$fecha);

        $diaNum = $arrayFecha[0];
        $mesNum = $arrayFecha[1];
        $anioNum = $arrayFecha[2];

        $mes = '';

        switch ($mesNum) {
            case '01':
                $mes = 'Enero';
                break;

            case '02':
                $mes = 'Febrero';
                break;

            case '03':
                $mes = 'Marzo';
                break;

            case '04':
                $mes = 'Abril';
                break;

            case '05':
                $mes = 'Mayo';
                break;

            case '06':
                $mes = 'Junio';
                break;

            case '07':
                $mes = 'Julio';
                break;

            case '08':
                $mes = 'Agosto';
                break;

            case '09':
                $mes = 'Septiembre';
                break;

            case '10':
                $mes = 'Octubre';
                break;

            case '11':
                $mes = 'Noviembre';
                break;

            case '12':
                $mes = 'Diciembre';
                break;
   
        }


    ?>


	

    <div style="font-family:'frutiger';font-weight:normal;font-size:9px;border:0px solid red"> 

        <div style='text-align: right;border:0px solid red;font-size:10px;font-weight: bold;font-size:10px'>TECNOLÓGICO NACIONAL DE MÉXICO</div>
        <div style='text-align: right;border:0px solid red;'>Instituto Tecnológico de Salina Cruz </div>
        <br> <br> <br>
        <div style='text-align:center;border:0px solid red;'>Año del Centenario de la Promulgación de la Constitución Política de los Estados Unidos Mexicanos</div>
		<br><br>
		<div style='text-align: right;border:0px solid red;'><?php echo $lugar; ?>, <?php echo $diaNum."/".$mes."/".$anioNum; ?> </div> 
		<div style='text-align: right;border:0px solid red;'>Dictamen No.: <?php echo $no_dictamen; ?></div> 
		<br><br><br>
		<strong style='font-size:10px'>
			<?php echo $subdirector; ?> <br>
	    	SUBDIRECTOR ACADÉMICO DEL I.T DE SALINA CRUZ<br>
	        PRESENTE
         </strong>
        <br><br><br>
       

          <div>
        	
        	<div style="text-align: justify" >
        		Por este conducto, y atendiendo la recomendación del Comité Académico, comunico a usted que
                <?php echo ($status == 'Aceptado') ? 'SE AUTORIZA': 'NO SE AUTORIZA'; ?> la solicitud de la interesada
        		<ins>C. <?php echo $nombreCompleto; ?> (Núm de Control <?php echo $no_de_control; ?> ) </ins>
        		con referencia a 
        		<ins><?php echo $respuesta_solicitud_dictamen; ?>.</ins>
        		<br><br>
                <div>Sin otro particular por el momento, quedo de Usted.</div>
				
				<br><br><br><br><br><br><br>
				<div style='width:120px;border:0px solid red;margin:0px;font-size:10px'>
					<strong>ATENTAMENTE </strong>
				</div>
				<br><br><br><br>

                <div style='width:400px;border:0px solid red;margin:0px;font-style: italic;'>
                    "La misión va más allá de nuestras costas"
                </div>
                <br><br><br><br><br><br>


                <div style='width:400px;border:0px solid red;margin:0px;font-size:10px'>
                    <strong><?php echo $director; ?></strong><br>
                    <strong>DIRECTOR</strong>
                </div>
                <br><br><br><br>
                    
                    
				<br><br><br><br><br><br><br>
				<div style='font-size:8px'>
					C.p Interesado(a).<br>
                    C.p Archivo.
				</div>
        	</div>
        	
        	
        	
        </div>



    </div>

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modificar Reunión</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/dataTables.bootstrap.min.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/select.dataTables.min.css">

	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation" >

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >

   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
								<div class="row">
									<div class="col-xs-12 text-center">
										
										<h3>Modificar reunión</h3>	
									
									</div>
								</div>
								<br>
								
								
								
									
										<div class="row">
											<div class="col-xs-12 ">
												<div class="table-responsive">
													<table class="table table-bordered table-hover" id="tblReunion" cellspacing="0"  width="100%" style="text-align: center;">
															<caption style="text-align: center"><h4><strong>Personal</strong></h4></caption>
															<thead>
												                    <tr>
												                    	 <th>id reunion</th>
													                      <th>Nombre reunión</th>
													                      <th>Periodo escolar</th>
													                      <th>Número de libro</th>
													                      <th>Número de acta</th>
													                      <th>Modificar</th>
													                      <th>Eliminar</th>
												                    </tr>
											                </thead>
											        </table>
												</div>
											</div>
										</div>
									
									<br><br>
										


	</div>




   <!-- Modal -->
<div id="modalModificarReunion" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width:850px;margin-left:-40px;'>
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modificar reunión</h4>
      </div>
       <form id="formModificarReunion">
		      <div class="modal-body" style="height:455px;overflow: auto;">
	                    <div class="row">
                            
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label for="txtNombreReunion">Nombre de la reunión:</label>
                                    <input type="text" id="txtNombreReunion" name="txtNombreReunion"  class="form-control" placeholder="Nombre" minlength="1" maxlength='180'>
                                </div>
                            </div>

                              <div class="col-xs-6">
                                <div class="form-group"> 
                                      <label for="slPeriodoEscolar"  >Periodo escolar:</label>
                                      <select id="slPeriodoEscolar" disabled class="form-control" name="slPeriodoEscolar">
                                        <option value="" selected disabled>Elija el periodo escolar</option>
                                      </select> 
                                </div>
                              </div>

                            <div class="col-xs-6">
                              <div class="form-group">
                                  <label for="txtNoLibro">Número de libro:</label>
                                  <input type="text" id="txtNoLibro" name="txtNoLibro"  class="form-control" placeholder="Número" minlength="1" maxlength='20'>
                              </div>
                            </div>
                            <div class="col-xs-6">
                              <div class="form-group">
                                  <label for="txtNoActa">Número de acta:</label>
                                  <input type="text" id="txtNoActa" name="txtNoActa"  class="form-control" placeholder="Número" minlength="1" maxlength='20'>
                              </div>
                            </div>

                              <div class="col-xs-12">
                                  <div class="form-group">
                                    <label for="txtPrimeraParteReunion">1er Mensaje:</label>
                                    <textarea style='height:140px;width: 100%' class="form-control" id='txtPrimeraParteReunion' name='txtPrimeraParteReunion' 
                                    placeholder="EN LA CUIDAD Y PUERTO DE SALINA CRUZ SIENDO LAS TRECE HORAS CON TREINTA MINUTOS DEL DIA CATORCE DE AGOSTRO DEL AÑO DOS MIL DIECISIETE, SE REUNIERON EN LA SALA DE JUNTAS UBICADO DENTRO DE LAS INSTALACIONES QUE OCUPA EL INSTITUTO TECNOLOGICO DE SALINA CRUZ" ></textarea>
                                  </div>
                              </div>

                              
                              <div class="col-xs-12">
                                <div class="form-group">
                                  <label for="txtPersonal">Personal:</label>
                                  <textarea style='height:140px;width: 100%' class="form-control" id='txtPersonal' name='txtPersonal' ></textarea>
                                    </select>
                                </div>
                              </div>

                              <div class="col-xs-12">
                                  <div class="form-group">
                                    <label for="txtSegundaParteReunion">2do Mensaje:</label>
                                    <textarea style='height:200px;width: 100%' class="form-control" id='txtSegundaParteReunion' name='txtSegundaParteReunion' 
                                    placeholder="PRESIDENTE, SECRETARIO Y VOCALES RESPECTIVAMENTE DEL COMITÉ ACADÉMICO DEL ITSAL, BAJO EL SIGUIENTE ORDEN DEL DÍA : 
1.- PASE DE LA LISTA DE LOS INTEGRANTES DEL COMITÉ ACADÉMICO,
2.- LECTURA DE LAS SOLICITUDES DE ESTUDIANTES DE LA INSTITUCIÓN ENVIADAS AL COMITÉ ACADÉMICO, PARA SU ANÁLISIS ACADÉMICO Y RECOMENDACIÓN DE LAS MISMAS"></textarea>
                                  </div>
                              </div>

                       
                      </div>
		      </div>
		      <div class="modal-footer">
		      		<button type="submit" class="btn btn-primary"  id="btnModificarReunion">Modificar</button>
		      		 <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
		      </div>
       </form>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalMensajeModificarReunion" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>





   <!-- Modal -->
<div id="modalAlertaEliminarReunion" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" id='btnMdlEliminarReunion' data-dismiss="modal" >Aceptar</button>
      <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
      </div>
    </div>

  </div>
</div>


   <!-- Modal -->
<div id="modalMensajeEliminarReunion" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" style='margin-left: 165px;'>
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal"  >Aceptar</button>
      </div>
    </div>

  </div>
</div>

	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
 	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/modificarReunion.js"></script>



</body>
</html>|
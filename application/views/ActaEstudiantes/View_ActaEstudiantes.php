<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Acta Estudiantes</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">


	<style>
		
	.search-table{table-layout: fixed; margin:40px auto 0px auto; }
	.search-table, .search-table td, .search-table th{border-collapse:collapse; border:1px solid #777;}
	/*th{padding:20px 7px;}*/
	.search-table td{padding:5px 10px; height:35px;text-align: center;}

	.search-table-outter { overflow-x: scroll; }
	/*.search-table th, .search-table .grand { min-width: 150px; }*/
	.search-table th { min-width: 150px; text-align:center;}

	</style>

	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >


   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
								<div class="row">
									<div class="col-xs-12 text-center">
										
										<h3>Generar acta</h3>	
									
									</div>
								</div>
								<br>
								
								<form action="" id="formGemerarActa" style="width:385px;margin-left: 32%;margin-top:6%;">
								
									
										<div class="row" >
											<div class="col-xs-12">
												<div class="form-group">
													  <label for="slPeriodoEscolar" >Periodo escolar:</label>
													  <select id="slPeriodoEscolar" class="form-control" name="slPeriodoEscolar" style='width:385px;'>
													    <option value="" selected disabled>Elija el periodo escolar</option>
													</select> 
												</div>
											</div>
										</div>	

										<div class="row" style="margin-top:9%;margin-left:34%">
											<div class="col-xs-12">
												<div class="form-group">
													  <input type="submit" id='btnAceptarActa' value='Aceptar' class='btn btn-primary'>
												</div>
											</div>
										</div>	
										
										

								</form>

	  </div>




   <!-- Modal -->
<div id="modalAlertaModificarMateria" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width: 800px'>
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modificar Carrera</h4>
      </div>
       <form id="formDatosMaterias">
		      <div class="modal-body">
					
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="txtClaveMateria">Clave materia:</label>
								<input type="text" id="txtClaveMateria" name="txtClaveMateria"  class="form-control" placeholder="Clave carrera" minlength="1"  maxlength="70" >
							</div>
						</div>
					</div>	

					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="txtNombreMateria">Nombre materia:</label>
								<input type="text" id="txtNombreMateria" name="txtNombreMateria"  class="form-control" placeholder="Nombre carrera" minlength="1"  maxlength="100" >
							</div>
						</div>
					</div>	
					
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
								<label for="txtNombreMateriaAbreviado">Nombre materia abreviado:</label>
								<input type="text" id="txtNombreMateriaAbreviado" name="txtNombreMateriaAbreviado"  class="form-control" placeholder="Nombre carrera" minlength="1"  maxlength="60" >
							</div>
						</div>
					</div>	



		      </div>
		      <div class="modal-footer">
		      		<button type="submit" class="btn btn-primary"  id="btnMdlAlertModificarMaterias">Modificar</button>
		      		 <button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
		      </div>
       </form>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalAlertaEliminarMaterias" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnMdlAlertEliminarMaterias">Aceptar</button>
		<button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
      </div>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalAlertaMsjEliminarCarreras" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnMdlAlertSaveMaterias">Aceptar</button>

      </div>
    </div>

  </div>
</div>


   <!-- Modal -->
<div id="modalTablaReunionesPeriodoEscolar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style="width:940px;margin-left:-60px;">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
      	<h4>Reuniones del periodo escolar <strong id='lblPeriodoEscolar'></strong></h4>
      	<br>
	      	<div class="search-table-outter wrapper">
			        <table class="search-table inner" id='tableReunionesPeriodoEscolar' style='width:880px'>
			        	<thead>
			        		<tr>
			        			<th style="width: 50px;">No.</th>
			        			<th style="width:350px;">Reunión</th>
			        			<th style="width:150px;">Periodo escolar</th>
			        			<th style="width:100px;">Número de libro</th>
			        			<th style="width:100px;">Numero de acta</th>
			        			<th style="width:130px;">Generar acta</th>
			        		</tr>
			        	</thead>
			        	<tbody>
			        	</tbody>
			        </table>
	    	</div>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnMdlReunionesPeriodoEscolar">Aceptar</button>

      </div>
    </div>

  </div>
</div>

	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>

	<!-- <script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script> -->
	<!-- <script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script> -->
	
	<script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
 	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/actaEstudiantes.js"></script>



</body>
</html>|
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Actas</title>
	
</head>

<body>

    <h3 style='border:0px solid red;text-align: center;font-family:Arial;font-size:14px' ><?php echo $Reunion->nombre_reunion;?></h3> 
    <br>
    <h3 style='border:0px solid red;text-align: center;font-family:Arial;font-size:14px'>LIBRO NO. <?php echo $Reunion->no_libro;?> 
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ACTA NO. <?php echo $Reunion->no_acta;?></h3> 

       <br><br> 




    <div style="font-family:Arial;font-size:12px"> 

        	<div style="text-align: justify">
            
             <?php echo $Reunion->mensaje_1; ?>
             <?php echo $Reunion->personal; ?>
             <?php echo $Reunion->mensaje_2; ?>
              <br> 
              ....................................................................................................................................................................................................
                <?php 
                           
                   $longitud = count($Acta);
 
                    
                      for($i=0; $i<$longitud; $i++)
                      {


                            $materias = explode(",_",$Acta[$i]->materia);

                            $nombre_completo_materias = explode(",_",$Acta[$i]->nombre_completo_materia);

                            if( $Acta[$i]->id_tipo_solicitud != '7')
                            {

                                    if(count($materias) > 1)
                                    {
                                        ?>
                                    
                                                  El <strong>C. <?php echo $Acta[$i]->nombreCompleto; ?></strong>, ESTUDIANTE DE LA CARRERA DE <strong><?php echo $Acta[$i]->nombre_carrera; ?></strong>,
                                                  EN <strong><?php echo $Acta[$i]->nombre_semestre; ?></strong>
                                                  CON NUMERO DE CONTROL  <strong> <?php echo $Acta[$i]->no_de_control; ?></strong>, SOLICITA A ESTE COMITÉ ACADÉMICO LA AUTORIZACIÓN DE TENER UNA CARGA
                                                  ACADÉMICA DE <strong> <?php echo $Acta[$i]->total_creditos; ?> CREDITOS</strong>  CON LAS ASIGNATURAS <strong> <?php echo $nombre_completo_materias[0]; ?></strong>
                                                  <strong>(<?php echo $materias[0] ?>)</strong> Y <strong> <?php echo $nombre_completo_materias[1]; ?></strong> <strong>(<?php echo $materias[1] ?>)</strong>
                                                  EN CONDICIÓN DE <strong> "<?php echo $Acta[$i]->tipo_solicitud; ?>"</strong> <br>
                                                  <?php echo $Acta[$i]->verificacion_manual_lineamiento; ?>
                                                  <!-- ACCIÓN NO PERMITIDA DE ACUERDO AL LINEAMIENTO PARA LA EVALUACIÓN Y ACREDITACIÓN DE ASIGNATURAS VERSIÓN 1.0 PLANES DE ESTUDIO 2009-2010 DEL TECNM EN SU APARTADO 4.5.13 -->

                                                  ......................................................................................................................................................................................................
                                                  <br>
                                                  <?php echo $Acta[$i]->respuesta_solicitud_acta; ?>
                                                  <!-- DE LO ANTERIOR, ESTE COMITE <strong><?php echo ($Acta[$i]->status == 'ACEPTADO') ? 'SI': 'NO'; ?> RECOMIENDA</strong> QUE CURSE LAS ASIGNATURAS EN CODICIÓN DE 
                                                  <strong> "<?php echo $Acta[$i]->tipo_solicitud; ?>"</strong> HASTA CON UN TOTAL DE <strong> <?php echo $Acta[$i]->total_creditos; ?> CREDITOS</strong> COMO CARGA
                                                  MÁXIMA, PARA PODER REGULARIZAR EL PROCESO EDUCATIVO DEL SOLICITANTE EN TIEMPO Y FORMA PERMITIDA -->
                                                  ......................................................................................................................................................................................................
                                                 <br>
                                             <?php 
                                    }
                                    else
                                    {
                                        ?>

                                                  El <strong>C. <?php echo $Acta[$i]->nombreCompleto; ?></strong>, ESTUDIANTE DE LA CARRERA DE <strong><?php echo $Acta[$i]->nombre_carrera; ?></strong>,
                                                  EN <strong><?php echo $Acta[$i]->nombre_semestre; ?></strong>
                                                  CON NUMERO DE CONTROL  <strong> <?php echo $Acta[$i]->no_de_control; ?></strong>, SOLICITA A ESTE COMITÉ ACADÉMICO LA AUTORIZACIÓN DE TENER UNA CARGA
                                                  ACADÉMICA DE <strong> <?php echo $Acta[$i]->total_creditos; ?> CREDITOS</strong>  CON LA ASIGNATURA <strong> <?php echo $Acta[$i]->nombre_completo_materia; ?></strong>
                                                  <strong>(<?php echo $Acta[$i]->materia; ?>)</strong> EN CONDICIÓN DE <strong> "<?php echo $Acta[$i]->tipo_solicitud; ?>"</strong> <br>
                                                  <?php echo $Acta[$i]->verificacion_manual_lineamiento; ?>
                                                  <!-- ACCIÓN NO PERMITIDA DE ACUERDO AL LINEAMIENTO PARA LA EVALUACIÓN Y ACREDITACIÓN DE ASIGNATURAS VERSIÓN 1.0 PLANES DE ESTUDIO 2009-2010 DEL TECNM EN SU APARTADO 4.5.13 -->
                                                  ......................................................................................................................................................................................................
                                                  <br>
                                                  <?php echo $Acta[$i]->respuesta_solicitud_acta; ?>
                                                  <!-- DE LO ANTERIOR, ESTE COMITE <strong><?php echo ($Acta[$i]->status == 'ACEPTADO') ? 'SI': 'NO'; ?> RECOMIENDA</strong> QUE CURSE LA ASIGNATURA EN CODICIÓN DE 
                                                  <strong> "<?php echo $Acta[$i]->tipo_solicitud; ?>"</strong> HASTA CON UN TOTAL DE <strong> <?php echo $Acta[$i]->total_creditos; ?> CREDITOS</strong> COMO CARGA
                                                  MÁXIMA, PARA PODER REGULARIZAR EL PROCESO EDUCATIVO DEL SOLICITANTE EN TIEMPO Y FORMA PERMITIDA -->
                                                  ......................................................................................................................................................................................................
                                                 <br>
                                             <?php 

                                    }

                            }
                            else
                            {
                                // FALTA hacer lo que va a llevar cuando sea la solicitud de baja definitiva  


                                ?>

                                                  El <strong>C. <?php echo $Acta[$i]->nombreCompleto; ?></strong>, ESTUDIANTE DE LA CARRERA DE <strong><?php echo $Acta[$i]->nombre_carrera; ?></strong>,
                                                  EN <strong><?php echo $Acta[$i]->nombre_semestre; ?></strong>
                                                  CON NUMERO DE CONTROL  <strong> <?php echo $Acta[$i]->no_de_control; ?></strong>, SOLICITA A ESTE COMITÉ ACADÉMICO LA AUTORIZACIÓN DE DARSE DE <strong> "<?php echo $Acta[$i]->tipo_solicitud; ?>"</strong> 
                                                  <br>
                                                  <?php echo $Acta[$i]->verificacion_manual_lineamiento; ?>
                                                  <!-- ACCIÓN NO PERMITIDA DE ACUERDO AL LINEAMIENTO PARA LA EVALUACIÓN Y ACREDITACIÓN DE ASIGNATURAS VERSIÓN 1.0 PLANES DE ESTUDIO 2009-2010 DEL TECNM EN SU APARTADO 4.5.13 -->
                                                  ......................................................................................................................................................................................................
                                                  <br>
                                                  <?php echo $Acta[$i]->respuesta_solicitud_acta; ?>
                                                  <!-- DE LO ANTERIOR, ESTE COMITE <strong><?php echo ($Acta[$i]->status == 'ACEPTADO') ? 'SI': 'NO'; ?> RECOMIENDA</strong>  DARSE DE <?php echo $Acta[$i]->tipo_solicitud; ?> -->
                                                  ......................................................................................................................................................................................................
                                                 <br>
                                             <?php 


                            }

                           




                     
                      
                      }
                            

                ?>

                
               
    

        		
        	</div>
        	
        	
        	
   



    </div>

</body>
</html>
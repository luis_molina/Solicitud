<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Evaluar Estudiantes Excel</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/fileInput/fileinput.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation" >

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >

   </div>
	
	<div class="container" style="margin-left:18%;width:78%;" >
						
								<div class="row">
									<div class="col-xs-12 text-center">
										
										<h3>Evaluar Estudiantes Excel</h3>	
									
									</div>
								</div>
								<br>
									<br>
							   <form id='formValidaUploadExcel' style="width:50%;margin:0px auto">			
									<div class="row">
										<div class="col-xs-12 ">
								        	 <div class="form-group">
											 	<label for="elegir" class="center-block text-center" >Subir Excel:</label> 
												<input id="fileExcel" name="fileExcel" type="file" class="file file-loading" data-show-upload="false" data-show-caption="true">
									          </div>

								         </div>

								    </div>
							   <br><br><br>
							   <div style='width:200px;margin:0px auto;border: 0px solid red'>
							   		<button type="submit" class="btn btn-primary" id="btnEvaluarEstudiantesExcel" >Subir y Evaluar Estudiantes</button>
							   </div>
							   	

							  </form>
					<br><br>
	</div>





   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnMdlAlertSaveCarreras">Aceptar</button>

      </div>
    </div>

  </div>
</div>




	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/fileInput/fileinput.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/fileInput/es.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>
	

	<script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/evaluarEstudiantesExcel.js"></script>



<script>




	$("#fileExcel").fileinput({
         overwriteInitial: false,
        maxFileSize: 5000,
        showClose: false,
        showCaption: false,
        maxFileCount:1,
        showBrowse: true,
        // browseOnZoneClick: true,
        language: "es",
         preferIconicPreview: true, 
          previewFileIconSettings: { // configure your icon file extensions
	        'xls': '<i class="fa fa-file-excel-o text-success"></i>',
	        'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
	        'csv': '<i class="fa fa-file-excel-o text-success"></i>',    
	    },
	     previewFileExtSettings: { // configure the logic for determining icon file extensions
	        'xls': function(ext) {
	            return ext.match(/(xls|xlsx)$/i);
	        }
    	},
    	allowedFileExtensions: ["xlsx","xls","csv"],
        
    });

    
    


	
</script>

</body>
</html>|
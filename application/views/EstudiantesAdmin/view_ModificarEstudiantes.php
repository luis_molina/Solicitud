<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modificar Estudiantes</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/dataTables.bootstrap.min.css"> 
	

	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >

   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
								<div class="row">
									<div class="col-xs-12 text-center">
										
										<h3>Modificar Estudiantes</h3>	
									
									</div>
								</div>
								<br>
								
								<form action="" id="formModificarCarreras">
								
									<fieldset>
										<legend>Seleccionar Estudiantes:</legend>
										<div class="row">
											<div class="col-xs-12 ">
												<div class="table-responsive">
													<table class="table table-bordered table-hover" id="tblModificarEstudiantes" cellspacing="0"  width="100%" style="text-align: center;">
															<caption style="text-align: center"><h4><strong>Estudiantes</strong></h4></caption>
															<thead>
												                    <tr>
												                    	  <th>id alumno</th>
																		  <th>Número de control</th>
													                      <th>Nombre</th>
													                      <th>Apellido paterno</th>
													                      <th>Apellido materno</th>
													                      <th>Curp</th>
													                      <th>Modificar</th>
													                      <th>Eliminar</th>
												                    </tr>
											                </thead>
											        </table>
												</div>
											</div>
										</div>
									</fieldset>
									<br><br>
										

								</form>

	</div>




   <!-- Modal -->
<div id="modalAlertaModificarEstudiante" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width: 800px'>
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modificar Estudiante</h4>
      </div>
       <form id="formDatosModificarEstudiantes">
		    <div class="modal-body">
					
					<div class="row">
						  <div class="col-xs-12">
			                <div class="form-group">
			                  <label for="txtNumControl">Número de control:</label> 
			                  <input type="text" id="txtNumControl" name="txtNumControl"  class="form-control" placeholder="Número de control"  minlength="1" maxlength="30" >  
			                </div>
			             </div>
		             </div>

					<div class="row">
			              <div class="col-xs-12">
			                  <div class="form-group">
			                    <label for="txtNombreEstudiante">Nombre:</label>
			                    <input type="text" id="txtNombreEstudiante" name="txtNombreEstudiante"  class="form-control" placeholder="Nombre"  minlength="1" maxlength="50">
			                  </div>
			              </div>
		             </div>
	
					<div class="row">
			              <div class="col-xs-12">
			                <div class="form-group">
			                  <label for="txtApellidoPaterno">Apellido Paterno:</label>
			                  <input type="text" id="txtApellidoPaterno" name="txtApellidoPaterno"  class="form-control" placeholder="Apellido Paterno"  minlength="1" maxlength="50"> 
			                </div>
			              </div>
		           </div>
					
					<div class="row">
			              <div class="col-xs-12">
			                <div class="form-group">
			                  <label for="txtApellidoMaterno">Apellido Materno:</label>
			                  <input type="text" id="txtApellidoMaterno" name="txtApellidoMaterno"  class="form-control" placeholder="Apellido Materno"  minlength="1" maxlength="50"> 
			                </div>
			              </div>
		           </div>
					
					<div class="row">
						 <div class="col-xs-12">
			                <div class="form-group">
			                  <label for="txtCurp">Curp:</label>
			                  <input type="text" id="txtCurp" name="txtCurp"  class="form-control" placeholder="Curp"  minlength="1" maxlength="50"> 
		                    </div>
		              	 </div>
	              	</div>


		  </div>
		      <div class="modal-footer">
		      		<button type="submit" class="btn btn-primary"  id="btnMdlAlertModificarEstudiante">Modificar</button>
		      		 <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
		      </div>
       </form>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalAlertaEliminarEstudiante" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnMdlAlertEliminarEstudiante">Aceptar</button>
		<button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
      </div>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalAlertaMsjEliminarEstudiante" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalMsjModificarEstudiante" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>

	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
 	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/modificarEstudiantes.js"></script>



</body>
</html>|
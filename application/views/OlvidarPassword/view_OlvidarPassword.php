<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Olvidaste tu contraseña</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloLogin.css">
  </head>
  <body data-base-url="<?php echo base_url();?>">
    
    <div class="container">
      <br>
      <br><br><br>
      
      <div class="card card-container" style='max-width:400px !important'>

      <h3 class="text-center" style='color:black'>¿Olvidaste tu contraseña?</h3>
      <br><br>
        <div style="text-align: justify;">
          Por favor ingresa tu correo electrónico y te enviaremos las instrucciones para restablecer tu contraseña.
        </div>
        <br><br>
        <form name="form" id="FormOlvidarPassword" class="form-horizontal" >
          <div class="input-group">
          <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>
          <input id="email" name="email" placeholder="Escribe tu correo" class="form-control"  type="text" maxlength="70" >
          </div>
          <br>
          <br>
          <br>
          <div class="form-group">
            <div class="col-sm-12 controls">
              <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"  >Enviar</button>
            </div>
            

          </div>
          <br><br><br>
            <div>
                    <a href="<?php echo base_url();?>Login">Iniciar sesión</a>
            </div>
        </form>
        
      </div>
    </div>

    <!-- Modal -->
    <div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
              </div>
        </div>
    </div>


    <script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>
    <script src="<?php echo base_url(); ?>public/js/olvidarPassword.js"></script>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Cambiar contraseña</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloLogin.css">
  </head>
  <body data-base-url="<?php echo base_url();?>">

   <div class="container">
      <br>
      <br><br><br>
      
      <div class="card card-container" style='max-width:400px !important'>

            <div class="text-center">
                <h3><i class="fa fa-lock fa-4x"></i></h3>
                <h3 class="text-center">Elige una contraseña nueva</h3>
                <br><br>
                <p>Por favor ingresa una nueva contraseña de por lo menos 5 caracteres.</p>
                <br><br>
                
                
                <form id="formChangePassword">
                  
                    <div class="form-group">
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                          <input type="password"  id="password1" name="password1" class="form-control" placeholder="Contraseña nueva" minlength="5" maxlength="100" >
                        </div>
                        <br>
                        <div class="input-group">
                          <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                          <input type="password"  id="password2" name="password2" class="form-control" placeholder="Verificar contraseña" minlength="5" maxlength="100" >
                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                      <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit"  >Cambiar Contraseña</button>
                    </div>
                 </form>

            </div>
            <br><br><br>
            <div>
              <a href="<?php echo base_url();?>Login" class="forgot-password" >
               Iniciar sesión
              </a>
            </div>
        
      </div>
    </div>

       <!-- Modal -->
    <div id="modalAlertaPasswordNocoinciden" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static" >
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alerta</h4>
          </div>
          <div class="modal-body">
            <p></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal -->
    <div id="modalAlertaCambioPasswordSuccess" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static" >
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alerta</h4>
          </div>
          <div class="modal-body">
            <p></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" >Iniciar sesión</button>
          </div>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div id="modalAlertaCambioPasswordCaducado" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static" >
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alerta</h4>
          </div>
          <div class="modal-body">
            <p></p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>
          </div>
        </div>
      </div>
    </div>

    <script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>
    <script src="<?php echo base_url(); ?>public/js/cambiarPassword.js"></script>
  </body>
</html>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Solicitud de Estudiantes</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloSolicitudEstudiantes.css">
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap-multiselect.css" /> -->

  </head>



  <body data-base-url="<?php echo base_url();?>">
    
    <div class="container">

      
                <br><br>

              <div class="card card-container">

                    <form id="formSolicitudEstudiante" class="form-inline">

                               <div style="width:75%" class="container-fluid" >
                                  
                                                      <div class="row">

                                                        <div class="col-xs-12 text-center">
                                                          
                                                         <h3 class="titleLogin text-center" >Solicitud del estudiante para el análisis del comité académico</h3>
                                                         <br>
                                                         <h3 class="titleLogin text-center" >Instituto Tecnológico de Salina Cruz</h3>            
                                                        </div>
                                                      </div>


                                                      <br>

                                                      <div style='border:0px solid red;width: 80%;margin-left: 530px;' >
                                                              <label for="txtLugar" >Cantidad de solicitudes realizadas: <strong id='strCantSolid' style='font-size:15px'></strong></label>
                                                            </div>
                                                      <br>
                                                       
                                                       <div style='border:0px solid red;width: 80%;margin-left: 220px;'>
                                                            
                                                            
                                                            <br>
                                                            <div class="form-group form-right">
                                                                 <label for="txtLugar" >Lugar:</label>
                                                                 <input type="text" id="txtLugar" name="txtLugar"  class="form-control" placeholder="Lugar" style='width:180px;' minlength="1" maxlength="200">
                                                            </div>

                                                            <div class="form-group form-right">
                                                                 <label for="txtFecha" >Fecha:</label>
                                                                 <input type="date" id="txtFecha" name="txtFecha"  class="form-control" style='width:180px;'>
                                                            </div>
                                                       
                                                       </div>
                                                       <br>
                                                       <label style='color:red; display:block;margin-left:394px;'>Último periodo escolar cursado</label>
                      
                                                        <div style='border:0px solid red;width: 80%;margin-left: 220px;'>
                                                            <div class="form-group form-right">
                                                                  
                                                                  <label for="slPeriodoEscolar" >Periodo escolar:</label>
                                                                  <select id="slPeriodoEscolar" class="form-control" name="slPeriodoEscolar" style='width:385px;margin-left:10px;'>
                                                                    <option value="" selected disabled>Elija el periodo escolar</option>
                                                                </select> 
                                                            </div>
                                                       </div> 
                                                       <br>
                                                       <div style='border:0px solid red;width: 80%;margin-left: 220px;'>
                                                             <div class="form-group form-right">
                                                                 <label for="txtAsunto">Asunto:</label>
                                                                 <textarea style='width:448px;height: 58px;' id="txtAsunto" name="txtAsunto" class="form-control" placeholder="Asunto" minlength="1" maxlength="350" ></textarea>
                                                            </div>
                                                       </div>

                                                       <br><br>

                                                       <div class="cont-jefe">
                                                         <strong>C.</strong> <label id="jefeEstudiosProfesionales"></label><br>
                                                         <label id='departamentoSecretario' ></label><br>
                                                         <label>PRESENTE</label>
                                                       </div>


                                                       <br><br><br>
                                                       
                                                       <div style='width: 780px;'>
                                                         
                                                       
                                                           <div class="form-group">
                                                                <label>El (la) que suscribe C. </label>
                                                                <input type="text" id="txtNombreEstudiante" name="txtNombreEstudiante" readonly="readonly" class="form-control" placeholder="Nombre" style='width:230px;' minlength="1" maxlength="100">

                                                           </div>

                                                           <div class="form-group">
                                                                <input type="text" id="txtApellidoPaterno" name="txtApellidoPaterno" readonly="readonly" class="form-control" placeholder="Apellido Paterno" style='width:191px;' minlength="1" maxlength="100"> 
                                                           </div>
                                                          
                                                          <div class="form-group">
                                                                <input type="text" id="txtApellidoMaterno" name="txtApellidoMaterno" readonly="readonly" class="form-control" placeholder="Apellido Materno" style='width:191px' minlength="1" maxlength="100"> 
                                                           </div>

                                                       </div>
                                                       <br>
                                                       <div style='width: 780px;'>
                                                            <label style='color:red; display:block;margin-left: 150px;'>Último semestre cursado</label>
                                                            <div class="form-group">
                                                                 <label>Estudiante del </label> 
                                                            </div>
                                                            <div class="form-group">
                                                                
                                                                  <select id="slSemestres" class="form-control" name="slSemestres" style='width:185px;margin-left: 50px;'>
                                                                </select> 
                                                            </div>
                                                            <div class="form-group" style='margin-left: 42px;'>
                                                                  <label> de la carrera de </label> 
                                                            </div>

                                                            <div class="form-group" style='margin-left: 50px;'>
                                                                   <select id="slCarreras" class="form-control" name="slCarreras" style='width:230px;'>
                                                                 </select> 
                                                            </div>

                                                       </div>
                                                       
                                                       <br>
                                                       <div style='width: 780px;'>

                                                            <div class="form-group" >
                                                                  <label> con número de control  </label> 
                                                            </div>
                                                            <div class="form-group" style='margin-left: 45px;'>
                                                                  <input type="text" id="txtNumControl" name="txtNumControl"  class="form-control" placeholder="Número de control" style='width:280px;' minlength="1" maxlength="30" readonly="readonly" >  
                                                            </div>
                                                            <div class="form-group" style='margin-left: 40px;'>
                                                                  <label> solicito de la manera más atenta: </label> 
                                                            </div>
                                                       </div>
                                                       
                                                       <br>
                                                       <div style='width: 780px;'>
                                                            <div class="form-group" >
                                                                 <textarea style='width:776px;height:70px' id="txtObservacion" name="txtObservacion" placeholder="Observación" class="form-control" minlength="1" maxlength="500"></textarea>
                                                            </div>
                                                       </div>
                                                       <br><br>

                                                       <div style='width: 780px;display:block;' id='containerTipoSolicitud'>
                                                            <div class="form-group" >
                                                                    <label for='slTipoSolicitud'>Solicitud a realizar: </label>
                                                            </div>
                                                            <div class="form-group" >
                                                                  <button type="button" class="btn btn-primary" id='btnOpenMdlTipoSolitud'>Seleccionar Solicitud</button>
                                                            </div>
                                                       </div>




                                                      <!--  <div style='width: 780px;'>

                                                          <div class="form-group" >
                                                                    <label for='slTipoSolicitud'>¿Qué tipo de solicitud va a realizar?: </label>
                                                            </div>


                                                            <div class="form-group" >
                                                              
                                                                 <select id="slTipoSolicitud" class="form-control"  name="slTipoSolicitud" style="width:530px" >
                                                                  <option value='' selected disabled >Elija una opción</option>
                                                                  <option value='1'>Curso de repetición</option>
                                                                  <option value='2'>Curso especial</option>
                                                                  <option value='3'>Otro 1</option>
                                                                  <option value='4'>Otro 2</option>
                                                                  <option value='5'>Otro 3</option>
                                                                  <option value='6'>Otro 4</option>
                                                                  <option value='7'>Otro 5</option>
                                                                 </select> 
                                                            </div>
                                                       </div>
                                                       <br>

                                                       <div style='width: 780px;display:block;' id='divContainerTipoSolicitud'>
                                                            
                                                       </div> -->




                                                        <!-- <div style='width: 780px;'>
                                                            <div class="form-group">
                                                                <label for='txtCurpEstudiante'>Curp: </label>
                                                                <input type="text" id="txtCurpEstudiante" name="txtCurpEstudiante" readonly="readonly" class="form-control" placeholder="Curp del estudiante" style='width:735px' minlength="1" maxlength="30">

                                                            </div>     
                                                         </div>  -->

                                                         <br>


                                                       <br><br>

                                                       <div style='width: 780px;'>
                                                            <div class="form-group" >
                                                                 <label>Por los siguientes motivos:</label> 
                                                            </div>                                                            
                                                       </div>  
                                                       <br><br>
               
                                                        <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                 <label>Motivos Académicos:</label> 
                                                            </div>
                                                       </div>  

                                                       <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                 <textarea style='width:776px;height:70px' id="txtMotivosAcademicos" name="txtMotivosAcademicos" placeholder="Motivos académicos" class="form-control" minlength="1" maxlength="500"></textarea>
                                                            </div>
                                                       </div>  

                                                       <br>
                                                       
                                                        <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                 <label>Motivos Personales:</label> 
                                                            </div>
                                                       </div>  

                                                       <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                  <textarea style='width:776px;height:70px' id="txtMotivosPersonales" name="txtMotivosPersonales" placeholder="Motivos personales" class="form-control" minlength="1" maxlength="500"></textarea>
                                                            </div>
                                                       </div>  

                                                       <br>
                                                       
                                                        <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                 <label>Otros:</label> 
                                                            </div>
                                                       </div>  

                                                       <div style='width: 780px;'>
                                                             <div class="form-group" >
                                                                  <textarea style='width:776px;height:70px' placeholder="Otros" id="txtOtros" name="txtOtros" class="form-control" minlength="1" maxlength="500"></textarea>
                                                            </div>
                                                       </div>  

                                     
                              </div>

                                        <div class="row">
                                              <div class="col-xs-12 ">
                                                  <br><br>
                                                  <button type="submit" class="btn btn-primary center-block text-center" id="btnAceptarSolicitudEstudiante" >Aceptar</button>
                                              </div>
                                        </div>
                              
                    </form>
                          

               </div>
        
    </div>



    <!-- Modal -->
    <div id="modalAlerta" class="modal fade" role="dialog">
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">
                        <p></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
              </div>
        </div>
    </div>


       <!-- Modal -->
    <div id="modalAlertaMsJSolicitudEstudiante" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false"  >
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Hacer otra solicitud</button>
                    </div>
              </div>
        </div>
    </div>



        <!-- Modal -->
    <div id="modalAlertaIngresaNocontrol" class="modal fade" role="dialog"  data-backdrop="static" data-keyboard="false"  >
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <h4 class="modal-title">Alerta</h4>
                    </div>
               <form id="formNumeroControlAlumno">
                    <div class="modal-body">

                         <div class="form-group">
                              <label for="mdlTxtNumControl">Ingrese su número de control:</label>
                              <input type="text" id="mdlTxtNumControl" name="mdlTxtNumControl" style="width: 100%;" class="form-control" placeholder="Número de control" minlength="1"  maxlength="100">  
                         </div>    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id='btnNumControlGetDatosEstudiante'>Aceptar</button>
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button> -->
                    </div>
               </form>
              </div>
         
        </div>
    </div>



            <!-- Modal -->
    <div id="modalConfirDatosCorrectosSolicitud" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body">
                    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id='btnEnviarSolicitudEstudiante'>
                         <span class="glyphicon glyphicon-arrow-right"></span> Enviar Solicitud
                         </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Regresar</button>
                    </div>
              
              </div>
         
        </div>
    </div>



                <!-- Modal -->
    <div id="modalImageDiagrama" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content" style='margin-left: -224px;width: 1040px;'>
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body" style='width:930px;margin: 0px auto'>
                       <div>
                          <img style='width:900px;height:425px' src="<?php echo base_url(); ?>public/images/diagrama.jpg">
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>
                    </div>
              
              </div>
         
        </div>
    </div>
  
                <!-- Modal -->
    <div id="modalSeleccionarTipoSolicitud" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content" style='margin-left:-18%;width:130%;'>

                <form id='formTipoSolicitudEstudiante'>

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body">
                          
                          <div style='width:100%;'>

                                <div class="form-group" >
                                          <label for='slTipoSolicitud'>¿Qué tipo de solicitud va a realizar?: </label>
                                  </div>


                                  <div class="form-group" >
                                    
                                       <select id="slTipoSolicitud" class="form-control"  name="slTipoSolicitud" style="width:100%" >
                                        <option value='' selected disabled >Elija una opción</option>
                                        <option value='1'>Curso de repetición</option>
                                        <option value='2'>Curso especial</option>
                                        <option value='3'>2 Cursos especiales con otras asignaturas</option>
                                        <option value='4'>2 Cursos especiales en un curso de verano</option>
                                        <option value='5'>Sobre créditos</option>
                                        <option value='6'>Residencia Profesional con una asignatura en curso especial</option>
                                        <option value='7'>Baja Definitiva con posibilidad de inscribirse de nuevo a la Institución</option>
                                       </select> 
                                  </div>
                             </div>

                             <div style='width:100%;display:block;' id='divContainerTipoSolicitud'>
                                  
                             </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id='btnValidaTipoSolicitud' >Aceptar</button>
                        <button type="button" class="btn btn-default"  data-dismiss="modal" >Cancelar</button>
                    </div>
                  
                  </form>

              </div>
         
        </div>
    </div>



                 <!-- Modal -->
    <div id="modalMensjValidaDatosTipoSolicitud" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>
                    </div>
              
              </div>
         
        </div>
    </div>


    <script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>

    <!-- <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap-multiselect.js"></script> -->

    <script src="<?php echo base_url(); ?>public/js/solicitudEstudiante.js"></script>

  </body>
</html>
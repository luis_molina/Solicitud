<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modificar Personal</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloHomeMenu.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloBarraSuperior.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/dataTables.bootstrap.min.css"> 
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/select.dataTables.min.css">

	
</head>

<body data-base-url="<?php echo base_url();?>">
	
	<div class="navbar navbar-default navbar-fixed-top" role="navigation" >

	    <div class="container-fluid"> 

	        <div class="navbar-header">
	        
	        </div>

	        <div class="collapse navbar-collapse">
	            
	            <ul class="nav navbar-nav navbar-right">
	                <li class="dropdown">
	                	
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style='display: inline-block;'>
	                        <span class="glyphicon glyphicon-user"></span> 
	                        <strong><?php echo $this->session->userdata('nombre'); ?></strong>
	                        <span class="glyphicon glyphicon-chevron-down"></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li>
	                            <div class="navbar-login">
	                                <div class="row">
	                                    <div class="col-lg-4">
	                                        <p class="text-center">
	                                            <img src="<?php echo base_url();?>public/uploads/default.png" alt="" width="100px" height="100px">
	                                        </p>
	                                    </div>
	                                    <div class="col-lg-8">
	                                        <p class="text-left"><strong><?php echo $this->session->userdata('nombre'); ?></strong></p>
	                                        <p class="text-left small"><?php echo $this->session->userdata('correo')?></p>
	                                       <!--  <p class="text-left">
	                                            <a href="#" class="btn btn-primary btn-block btn-sm" id="btnUpdateMyData">Actualizar Datos</a>
	                                        </p> -->
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                        <li class="divider"></li>
	                        <li>
	                            <div class="navbar-login navbar-login-session">
	                                <div class="row">
	                                    <div class="col-lg-12">
	                                        <p>
	                                            <a href="#" class="btn btn-danger btn-block" id="btnCerrarSesion">Cerrar Sesion</a>
	                                        </p>
	                                    </div>
	                                </div>
	                            </div>
	                        </li>
	                    </ul>
	                </li>
	            </ul>
	        </div>

	    </div>

	</div>


	<div class="sidebar left" >

   </div>
	

	<div class="container" style="margin-left:18%;width:78%;" >
						
								<div class="row">
									<div class="col-xs-12 text-center">
										
										<h3>Modificar personal</h3>	
									
									</div>
								</div>
								<br>

										<div class="row">
											<div class="col-xs-12 ">
												<div class="table-responsive">
													<table class="table table-bordered table-hover" id="tblPersonal" cellspacing="0"  width="100%" style="text-align: center;">
															<caption style="text-align: center"><h4><strong>Personal</strong></h4></caption>
															<thead>
												                    <tr>
												                    	 <th>id personal</th>
													                      <th>Nombre</th>
													                      <th>Profesión</th>
													                      <th>Cargo personal</th>
													                      <th>Departamento</th>
													                      <th>Comite</th>
													                      <th>Fecha inicio</th>
													                      <th>Fecha fin</th>
													                      <th>Modificar</th>
													                      <th>Eliminar</th>
												                    </tr>
											                </thead>
											        </table>
												</div>
											</div>
										</div>
									
									<br><br>

	</div>




   <!-- Modal -->
<div id="modalModificarPersonal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" style='width: 800px'>
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modificar Personal</h4>
      </div>
       <form id="formModificarPersonal">
		      <div class="modal-body">
	                    <div class="row">
                                              
                                              <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="txtNombre">Nombre:</label>
                                                    <input type="text" id="txtNombre" name="txtNombre"  class="form-control" placeholder="Nombre" minlength="1" maxlength='100'>
                                                </div>
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="txtApellidos">Apellidos:</label>
                                                    <input type="text" id="txtApellidos" name="txtApellidos"  class="form-control" placeholder="Apellidos" minlength="1" maxlength='100'>
                                                </div>
                                              </div>
                                              <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="txtProfesion">Profesión:</label>
                                                    <input type="text" id="txtProfesion" name="txtProfesion"  class="form-control" placeholder="Nombre" minlength="1" maxlength='100'>
                                                </div>
                                              </div>

                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                      <label for="slCargoPersonal">Cargo personal:</label>
                                                      <select id="slCargoPersonal" class="form-control" name="slCargoPersonal">
                                                      </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                  <div class="form-group">
                                                    <label for="slDepartamento">Departamento:</label>
                                                    <select id="slDepartamento" class="form-control" name="slDepartamento">
                                                      </select>
                                                  </div>
                                                </div>

                                                <div class="col-xs-6">
                                                  <div class="form-group">
                                                    <label for="slComite">Comité:</label> 
                                                    <select id="slComite" class="form-control" name="slComite">
                                                    </select> 
                                                  </div>
                                                </div>

                                                <div id='contDatosAdministrador'>
                                                  
                                                </div>

              
                                                <div class="col-xs-6">
                                                  <div class="form-group">
                                                    <label for="txtFechaInicio">Fecha de inicio:</label>
                                                    <input type="date" id="txtFechaInicio" name="txtFechaInicio"  class="form-control" placeholder="Fecha de inicio">
                                                  </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="form-group">
                                                      <label for="txtFechaFin">Fecha de fin:</label> 
                                                      <input type="date" id="txtFechaFin" name="txtFechaFin"  class="form-control" placeholder="Fecha de fin">            
                                                      </select> 
                                                    </div>
                                                </div>
                                               
                                                
                                         
                                        </div>
		      </div>
		      <div class="modal-footer">
		      		<button type="submit" class="btn btn-primary" >Modificar</button>
		      		 <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
		      </div>
       </form>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>



   <!-- Modal -->
<div id="modalMensajeModificarPersonal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>

      </div>
    </div>

  </div>
</div>




   <!-- Modal -->
<div id="modalAlertaEliminarPersonal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" id='btnMdlEliminarPersonal' data-dismiss="modal" >Aceptar</button>
      <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
      </div>
    </div>

  </div>
</div>


   <!-- Modal -->
<div id="modalMensajeEliminarPersonal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Alerta</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-primary" data-dismiss="modal"  >Aceptar</button>
      </div>
    </div>

  </div>
</div>

	<br><br><br><br><br><br>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>

	<script src="<?php echo base_url(); ?>public/libreriasJS/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url(); ?>public/libreriasJS/dataTables.bootstrap.min.js"></script>
	
	<script src="<?php echo base_url(); ?>public/js/cargarMenu.js"></script> 
 	<script src="<?php echo base_url(); ?>public/js/selectElementMenu.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/cerrarSesion.js"></script> 
	<script src="<?php echo base_url(); ?>public/js/modificarPersonal.js"></script>



</body>
</html>|
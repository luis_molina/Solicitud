<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Registro de estudiantes</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/css/estiloFormRegistroPersonal.css">
  </head>
  
  <body data-base-url="<?php echo base_url();?>">
    
    <div class="container">

      <!-- <div class="container-logo">
        
      </div> -->
      
<br><br>
      <div class="card card-container">

       <div style="width:75%" class="container-fluid" >
          
          <div class="row">

            <div class="col-xs-12 text-center">
              
             <h3 class="titleLogin text-center" >Registro de estudiantes</h3>
            
            </div>
          </div>
          <br><br>
          
          <form action="" id="formRegistrarEstudiantes">

            <div class="row">
            
            <h4 style='width: 250px;margin:0px auto;'>¿Eres de nuevo ingreso?</h4>
                                  
              <div style='width: 120px;height: 60px;margin: 0px auto;'>
                <div class="form-group">
                       <label style="display: inline;margin-left:-20px;">SI</label>
                       <input type="radio" id='rdSINuevoIngreso' class="form-control"  name="rdRegistro" value="SI" style="width:20px;display:inline;position:relative;top:11px;margin-left:2px;"/>
                       <label style="display: inline;margin-left: 20px;">NO</label> 
                      <input type="radio" id='rdNONuevoIngreso' class="form-control"  name="rdRegistro" value="NO" style="width:20px;display: inline;position:relative;top:11px;margin-left:2px;"/>
                  </div>
              </div>

              <br><br>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="txtNumControl">Número de control:</label> 
                  <input type="text" id="txtNumControl" name="txtNumControl"  class="form-control" placeholder="Número de control"  minlength="1" maxlength="30" >  
                </div>
              </div>

              <div class="col-xs-6">
                  <div class="form-group">
                    <label for="txtNombreEstudiante">Nombre:</label>
                    <input type="text" id="txtNombreEstudiante" name="txtNombreEstudiante"  class="form-control" placeholder="Nombre"  minlength="1" maxlength="100">
                  </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="txtApellidoPaterno">Apellido Paterno:</label>
                  <input type="text" id="txtApellidoPaterno" name="txtApellidoPaterno"  class="form-control" placeholder="Apellido Paterno"  minlength="1" maxlength="100"> 
                </div>
              </div>

              <div class="col-xs-6">
                <div class="form-group">
                  <label for="txtApellidoMaterno">Apellido Materno:</label>
                  <input type="text" id="txtApellidoMaterno" name="txtApellidoMaterno"  class="form-control" placeholder="Apellido Materno"  minlength="1" maxlength="100"> 
                </div>
              </div>

              <div class="col-xs-6">
                  <div class="form-group">
                    <label for="slPeriodoEscolar">Periodo escolar:</label> 
                    <select id="slPeriodoEscolar" class="form-control" name="slPeriodoEscolar" >
                    </select> 
                  </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                    <label for="slCarreras">Carrera:</label>
                    <select id="slCarreras" class="form-control" name="slCarreras" >
                    </select> 
                </div>
              </div>
              <div class="col-xs-6">
                <div class="form-group">
                    <label for="slSemestres">Semestre:</label>
                    <select id="slSemestres" class="form-control" name="slSemestres" >
                    </select> 
                </div>
              </div>

              <!-- <div class="col-xs-6">
                <div class="form-group">
                  <label for="txtCurp">Curp:</label>
                  <input type="text" id="txtCurp" name="txtCurp"  class="form-control" placeholder="Curp"  minlength="1" maxlength="100"> 
                </div>
              </div> -->
              
              <div class="col-xs-6">
                <div class="form-group">
                  <label for="txtGrupo">Grupo:</label>
                  <input type="text" id="txtGrupo" name="txtGrupo"  class="form-control" placeholder="Grupo"  minlength="1" maxlength="100"> 
                </div>
              </div>
             
            </div>
            <div class="row">
              <div class="col-xs-12 ">
                  <br><br>
                  <button type="submit" class="btn btn-primary center-block text-center"  id='btnRegistroEstudiante' >Aceptar</button>
              </div>

              
            </div>
              

          </form>

      </div>
        
      </div>
    </div>



     <!-- Modal -->
    <div id="modalMsjInicialEstudiante" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
              <div class="modal-content">

                   <form action="" id="formNumeroControlEstudiantes">
                          <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Alerta</h4>
                          </div>
                          <div class="modal-body">
                    
                                        <div class="container">
                                            

                                                <div class="row">
                                                    <div class="col-xs-6">
                                                      <div class="form-group">
                                                        <label for="mdlTxtNumControl">Número de control:</label> 
                                                        <input type="text" id="mdlTxtNumControl" name="mdlTxtNumControl"  class="form-control" placeholder="Número de control"  minlength="1" maxlength="30"  >  
                                                      </div>
                                                    </div>
                                                </div>
                                            

                                      </div>

                          </div>
                          <div class="modal-footer">
                              <button type="submit" class="btn btn-primary" id='btnAceptarNoControl'>Aceptar</button>
                              <button type="button" class="btn btn-default" data-dismiss="modal" >Cancelar</button>
                          </div>

                    </form>
             </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="modalAlerta" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <!-- Modal content-->
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
              </div>
        </div>
    </div>


<!-- Modal -->
    <div id="modalAlertaValidateDatosRegistro" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
            <!-- Modal content-->
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
                    <div class="modal-body">
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
              </div>
        </div>
    </div>

            <!-- Modal -->
    <div id="modalConfirDatosCorrectosRegistroEstudiantes" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body">
                    
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id='btnEnviarRegistroEstudiante'>
                         <span class="glyphicon glyphicon-arrow-right"></span> Realizar registro 
                         </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal" >Regresar</button>
                    </div>
              
              </div>
         
        </div>
    </div>


                  <!-- Modal -->
    <div id="modalImageDiagrama" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false" >
        <div class="modal-dialog">
           
              <div class="modal-content" style='margin-left: -224px;width: 1040px;'>
                    <div class="modal-header">
                        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                        <h4 class="modal-title">Alerta</h4>
                    </div>
              
                    <div class="modal-body" style='width:930px;margin: 0px auto'>
                       <div>
                          <img style='width:900px;height:425px' src="<?php echo base_url(); ?>public/images/diagrama.jpg">
                       </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal" >Aceptar</button>
                    </div>
              
              </div>
         
        </div>
    </div>


    <script src="<?php echo base_url(); ?>public/libreriasJS/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>public/libreriasJS/bootstrapValidator.js"></script>
    <script src="<?php echo base_url(); ?>public/js/registroEstudiantes.js"></script>
  </body>
</html>